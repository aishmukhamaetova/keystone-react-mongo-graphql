"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _Block = _interopRequireDefault(require("../../../../../evoke-me/layout/web/Block"));

var _BlockNotify = _interopRequireDefault(require("../block/BlockNotify"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

var Section = _styledComponents.default.div.withConfig({
  displayName: "SectionNotify__Section",
  componentId: "sc-8drdnj-0"
})(["position:fixed;top:0;width:100vw;height:100vh;pointer-events:none;text-align:right;z-index:10000;"]);

var SectionNotify = function SectionNotify(_ref) {
  var theme = _ref.theme;
  return _react.default.createElement(Section, null, _react.default.createElement(_Block.default, {
    right: true
  }, _react.default.createElement(_BlockNotify.default, null)));
};

var _default = SectionNotify;
exports.default = _default;
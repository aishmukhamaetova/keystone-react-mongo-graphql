"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _reactPose = require("react-pose");

var _compose = _interopRequireDefault(require("recompose/compose"));

var _lifecycle = _interopRequireDefault(require("recompose/lifecycle"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _withSpace = _interopRequireDefault(require("../../../../../evoke-me/space/all/withSpace"));

var _spaceNotify = _interopRequireDefault(require("../../space/spaceNotify"));

var _NotifyList = _interopRequireDefault(require("../NotifyList"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

var _default = (0, _compose.default)((0, _withSpace.default)(_spaceNotify.default))(function (_ref) {
  var fromNotify = _ref.fromNotify;
  var data = fromNotify.data,
      orderMap = fromNotify.orderMap;
  return _react.default.createElement(_NotifyList.default, {
    data: data,
    order: orderMap.show,
    onClose: function onClose(_ref2) {
      var id = _ref2.id;
      return fromNotify.orderMove(id, "show", "history");
    }
  });
});

exports.default = _default;
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _react = _interopRequireWildcard(require("react"));

var _reactPose = _interopRequireDefault(require("react-pose"));

var _compose = _interopRequireDefault(require("recompose/compose"));

var _defaultProps = _interopRequireDefault(require("recompose/defaultProps"));

var _lifecycle = _interopRequireDefault(require("recompose/lifecycle"));

var _withHandlers = _interopRequireDefault(require("recompose/withHandlers"));

var _withStateHandlers = _interopRequireDefault(require("recompose/withStateHandlers"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _Block = _interopRequireDefault(require("../../../../evoke-me/layout/web/Block"));

var _posed$div;

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var delay = function delay(time) {
  return new Promise(function (resolve) {
    return setTimeout(resolve, time);
  });
};

var poseMap = {
  BEGIN: "BEGIN",
  OPEN: "OPEN",
  CLOSE: "CLOSE"
};
var enhance = (0, _compose.default)((0, _defaultProps.default)({
  message: "Message"
}), (0, _withStateHandlers.default)(function () {
  return {
    pose: poseMap.BEGIN
  };
}, {
  poseSet: function poseSet() {
    return function (pose) {
      return {
        pose: pose
      };
    };
  }
}), (0, _withHandlers.default)(function () {
  return {
    mount: function mount(_ref) {
      var poseSet = _ref.poseSet,
          onClose = _ref.onClose;
      return (
        /*#__PURE__*/
        _asyncToGenerator(
        /*#__PURE__*/
        _regenerator.default.mark(function _callee() {
          return _regenerator.default.wrap(function _callee$(_context) {
            while (1) {
              switch (_context.prev = _context.next) {
                case 0:
                  poseSet(poseMap.OPEN);
                  _context.next = 3;
                  return delay(1000);

                case 3:
                  poseSet(poseMap.CLOSE);
                  _context.next = 6;
                  return delay(300);

                case 6:
                  onClose && onClose();

                case 7:
                case "end":
                  return _context.stop();
              }
            }
          }, _callee, this);
        }))
      );
    }
  };
}), (0, _lifecycle.default)({
  componentDidMount: function componentDidMount() {
    this.props.mount();
  }
}));

var background = function background(_ref3) {
  var error = _ref3.error;
  var color = error ? "rgba(200, 56, 56, 0.8)" : "rgba(56, 56, 200, 0.8)";
  return "background: ".concat(color, ";");
};

var Item = (0, _styledComponents.default)(_reactPose.default.div((_posed$div = {}, _defineProperty(_posed$div, poseMap.BEGIN, {
  y: -50,
  opacity: 0
}), _defineProperty(_posed$div, poseMap.OPEN, {
  y: 0,
  opacity: 1
}), _defineProperty(_posed$div, poseMap.CLOSE, {
  y: -50,
  opacity: 0
}), _posed$div))).withConfig({
  displayName: "Notify__Item",
  componentId: "h47qov-0"
})(["", ";color:white;position:absolute;width:300px;height:50px;border-radius:10px;margin-top:5px;"], background);
var Notify = enhance(function (_ref4) {
  var message = _ref4.message,
      error = _ref4.error,
      pose = _ref4.pose;
  return _react.default.createElement(Item, {
    error: error,
    pose: pose
  }, _react.default.createElement(_Block.default, {
    center: true,
    middle: true
  }, message));
});
var _default = Notify;
exports.default = _default;
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _nodemailer = _interopRequireDefault(require("nodemailer"));

var _authErrorMap = _interopRequireDefault(require("../../../../../../auth/manager/api/authErrorMap"));

var _config = _interopRequireDefault(require("../../../../../../auth/manager/api/config"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var smtpSettings = {
  host: 'smtp.gmail.com',
  port: 465,
  secure: true,
  // use SSL
  auth: {
    user: 'system@all-itera.ru',
    pass: 'H<j6V6=A'
  }
};

var transporter = _nodemailer.default.createTransport(smtpSettings);

var send = function send(email) {
  return new Promise(function (resolve, reject) {
    var mailOptions = {
      from: '"latelierdumiel.com" <system@all-itera.ru>',
      to: email,
      subject: 'latelierdumiel.com - Email confirmed',
      text: "You email confirmed",
      html: "You email confirmed"
    };
    transporter.sendMail(mailOptions, function (error, info) {
      if (error) {
        return reject(error);
      }

      return resolve(info.response);
    });
  });
};

var who = [_config.default.namespace, "classic", "api", "keystone", "graphql", "server", "authClassicConfirm"];

var keystoneLogin = function keystoneLogin(keystone, req, res, userId) {
  return new Promise(function (resolve, reject) {
    keystone.session.signin(userId, req, res, function (user) {
      return resolve(user);
    }, function (err) {
      return reject(err);
    });
  });
};

var boxAuthClassicConfirm =
/*#__PURE__*/
function () {
  var _ref2 = _asyncToGenerator(
  /*#__PURE__*/
  _regenerator.default.mark(function _callee(obj, args, _ref, info) {
    var keystone, req, res, _args$payload, payload, emailCode, User, userCurrent, userAuthenticated, message;

    return _regenerator.default.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            keystone = _ref.keystone, req = _ref.req, res = _ref.res;
            _context.prev = 1;
            _args$payload = args.payload, payload = _args$payload === void 0 ? {} : _args$payload;
            emailCode = payload.emailCode;
            User = keystone.list('User');
            _context.next = 7;
            return User.model.findOne({
              emailCode: {
                $eq: emailCode
              }
            });

          case 7:
            userCurrent = _context.sent;

            if (!userCurrent) {
              _context.next = 26;
              break;
            }

            if (!userCurrent.isConfirmed) {
              _context.next = 13;
              break;
            }

            return _context.abrupt("return", {
              result: null,
              errors: [{
                type: _config.default.namespace,
                code: _authErrorMap.default.CONFIRM_CODE_ALREADY_USED,
                message: "Email code already used",
                props: [{
                  name: "emailCode",
                  value: emailCode
                }]
              }]
            });

          case 13:
            _context.next = 15;
            return send(userCurrent.email);

          case 15:
            userCurrent.isConfirmed = true;
            _context.next = 18;
            return userCurrent.save();

          case 18:
            _context.next = 20;
            return keystoneLogin(keystone, req, res, String(userCurrent._id));

          case 20:
            userAuthenticated = _context.sent;

            if (!userAuthenticated) {
              _context.next = 25;
              break;
            }

            return _context.abrupt("return", {
              result: {
                userId: String(userCurrent._id)
              },
              errors: []
            });

          case 25:
            return _context.abrupt("return", {
              result: null,
              errors: [{
                type: _config.default.namespace,
                code: _authErrorMap.default.CONFIRM_LOGIN_FAILED,
                message: "Email confirmation login failed",
                props: [{
                  name: "emailCode",
                  value: emailCode
                }]
              }]
            });

          case 26:
            return _context.abrupt("return", {
              result: null,
              errors: [{
                type: _config.default.namespace,
                code: _authErrorMap.default.CONFIRM_CODE_NOT_FOUND,
                message: "Email code not found",
                props: [{
                  name: "emailCode",
                  value: emailCode
                }]
              }]
            });

          case 29:
            _context.prev = 29;
            _context.t0 = _context["catch"](1);
            console.log("ERROR", who.join(':'), _context.t0);
            message = process.env.NODE_ENV === "development" ? _context.t0 : "Signup unknown error";
            return _context.abrupt("return", {
              result: null,
              errors: [{
                type: _config.default.namespace,
                code: _authErrorMap.default.UNKNOWN,
                message: message
              }]
            });

          case 34:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, this, [[1, 29]]);
  }));

  return function boxAuthClassicConfirm(_x, _x2, _x3, _x4) {
    return _ref2.apply(this, arguments);
  };
}();

var _default = boxAuthClassicConfirm;
exports.default = _default;
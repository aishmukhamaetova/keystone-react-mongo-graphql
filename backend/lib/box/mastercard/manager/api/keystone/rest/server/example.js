"use strict";

var headers = {
  pragma: 'no-cache',
  'cache-control': 'no-cache',
  'user-agent': 'NING/1.0',
  accept: '*/*',
  'proxy-authorization': 'Basic Og==',
  'proxy-connection': 'keep-alive',
  'x-notification-secret': '81B052A22AA67AB40836EBFB7F6CA71A',
  'content-type': 'application/json;charset=utf-8',
  'x-notification-id': 'b6555e269907b8cb6173d03ef9507144777f4864e6d93e593a3175bd1ed69272',
  'x-notification-attempt': '1',
  'accept-encoding': 'gzip',
  'content-length': '1842',
  connection: 'upgrade',
  'x-forwarded-proto': 'https',
  'x-forwarded-for': '12.22.155.240',
  'x-real-ip': '12.22.155.240',
  host: 'localhost:3010'
};
var response = {
  authorizationResponse: {
    avsCode: 'Y',
    cardSecurityCodeError: 'M',
    commercialCard: '!01',
    commercialCardIndicator: '0',
    date: '1201',
    processingCode: '000000',
    responseCode: '00',
    returnAci: 'Y',
    stan: '327771',
    time: '221509'
  },
  billing: {
    address: {
      street: 'asd, asd'
    }
  },
  customer: {
    firstName: 'PAVEL'
  },
  device: {
    browser: 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36',
    ipAddress: '90.155.222.248'
  },
  gatewayEntryPoint: 'CHECKOUT',
  merchant: 'ADM',
  order: {
    amount: 336,
    chargeback: {
      amount: 0,
      currency: 'USD'
    },
    creationTime: '2018-12-01T22:15:09.333Z',
    currency: 'USD',
    description: 'Ordered goods',
    fundingStatus: 'NOT_SUPPORTED',
    id: '5c030784641c786d026a845b',
    merchantCategoryCode: '5499',
    status: 'CAPTURED',
    totalAuthorizedAmount: 336,
    totalCapturedAmount: 336,
    totalRefundedAmount: 0
  },
  response: {
    acquirerCode: '00',
    acquirerMessage: 'Approved',
    cardSecurityCode: {
      acquirerCode: 'M',
      gatewayCode: 'MATCH'
    },
    cardholderVerification: {
      avs: [Object]
    },
    gatewayCode: 'APPROVED'
  },
  result: 'SUCCESS',
  sourceOfFunds: {
    provided: {
      card: [Object]
    },
    type: 'CARD'
  },
  timeOfRecord: '2018-12-01T22:15:09.333Z',
  transaction: {
    acquirer: {
      batch: 20181202,
      date: '1202',
      id: 'BOBEIRUT_S2I',
      merchantId: '231000000243',
      settlementDate: '2018-12-02',
      timeZone: '+0200'
    },
    amount: 336,
    authorizationCode: '024061',
    currency: 'USD',
    frequency: 'SINGLE',
    funding: {
      status: 'NOT_SUPPORTED'
    },
    id: '1',
    receipt: '833522327771',
    source: 'INTERNET',
    terminal: 'BOBSAL02',
    type: 'PAYMENT'
  },
  version: '49'
};
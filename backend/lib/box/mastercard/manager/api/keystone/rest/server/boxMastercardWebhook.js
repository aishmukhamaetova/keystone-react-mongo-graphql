"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _typeMap = _interopRequireDefault(require("../../../../../../eshop/order/api/typeMap"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var keystone = require('keystone');

var BoxEshopOrder = keystone.list('BoxEshopOrder');
var HEADER_SECRET = "x-notification-secret";
var statusMap = {
  CAPTURED: "CAPTURED"
};

var _default =
/*#__PURE__*/
function () {
  var _ref = _asyncToGenerator(
  /*#__PURE__*/
  _regenerator.default.mark(function _callee(req, res, next) {
    var notificationSecret, body, _body$order, order, id, amount, status, boxEshopOrder;

    return _regenerator.default.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            notificationSecret = req.headers[HEADER_SECRET];
            body = req.body;
            console.log("HEADERS", req.headers);
            console.log("RECEIVED BODY", body);
            _body$order = body.order, order = _body$order === void 0 ? {} : _body$order;
            id = order.id, amount = order.amount, status = order.status;
            console.log("ID AMOUNT STATUS", id, amount, status);

            if (!(notificationSecret !== process.env.MASTERCARD_WEBHOOK_SECRET)) {
              _context.next = 10;
              break;
            }

            console.log("Wron process.env.MASTERCARD_WEBHOOK_SECRET");
            return _context.abrupt("return", res.status(400).send('Something wrong'));

          case 10:
            if (!(amount && id && status === "CAPTURED")) {
              _context.next = 20;
              break;
            }

            _context.next = 13;
            return BoxEshopOrder.model.findById(id);

          case 13:
            boxEshopOrder = _context.sent;
            console.log("FOUND BOX_ESHOP_ORDER", boxEshopOrder);

            if (!boxEshopOrder) {
              _context.next = 20;
              break;
            }

            boxEshopOrder.type = _typeMap.default.PAID;
            _context.next = 19;
            return boxEshopOrder.save();

          case 19:
            return _context.abrupt("return", res.send('processed-webhook'));

          case 20:
            return _context.abrupt("return", res.status(400).send('Something wrong'));

          case 21:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, this);
  }));

  return function (_x, _x2, _x3) {
    return _ref.apply(this, arguments);
  };
}();

exports.default = _default;
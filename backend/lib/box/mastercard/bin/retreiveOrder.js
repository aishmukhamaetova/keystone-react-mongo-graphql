"use strict";

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _dotenv = _interopRequireDefault(require("dotenv"));

var _retreiveOrder = _interopRequireDefault(require("../gateway/api/v50/retreiveOrder"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

_dotenv.default.config({
  path: "../.env"
});

var main =
/*#__PURE__*/
function () {
  var _ref = _asyncToGenerator(
  /*#__PURE__*/
  _regenerator.default.mark(function _callee() {
    var orderId,
        result,
        _args = arguments;
    return _regenerator.default.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            orderId = _args.length > 0 && _args[0] !== undefined ? _args[0] : "393cf2ba-60a0-4fbd-8e39-57972804206b";
            _context.next = 3;
            return (0, _retreiveOrder.default)({
              orderId: orderId
            }, {
              baseURL: process.env.MASTERCARD_BASE_URL,
              merchantId: process.env.MASTERCARD_MERCHANT_ID,
              merchantPassword: process.env.MASTERCARD_MERCHANT_PASSWORD
            });

          case 3:
            result = _context.sent;

          case 4:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, this);
  }));

  return function main() {
    return _ref.apply(this, arguments);
  };
}();

if (process.argv.length >= 2) {
  main(process.argv[2]);
}
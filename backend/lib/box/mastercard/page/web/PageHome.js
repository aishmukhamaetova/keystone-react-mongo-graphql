"use strict";

var _react = _interopRequireDefault(require("react"));

var _Page = _interopRequireDefault(require("../../../../evoke-me/page/web/Page"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var PageHome = function PageHome() {
  return _react.default.createElement(_Page.default, null);
};
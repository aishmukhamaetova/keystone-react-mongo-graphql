"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _config = _interopRequireDefault(require("../../../manager/api/config"));

var _retreiveOrder = _interopRequireDefault(require("../../api/retreiveOrder"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _default = [_config.default.namespace, "gateway", [], function () {
  return {
    baseURL: "https://test-bobsal.gateway.mastercard.com",
    merchant: {
      id: ""
    },
    operator: {
      id: "",
      password: ""
    }
  };
}, {
  retrieveOrder: function retrieveOrder(getState, setState) {
    return (
      /*#__PURE__*/
      _regenerator.default.mark(function _callee(_ref) {
        var orderId, _ref2, linkGate, pathVersion, merchant, URI;

        return _regenerator.default.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                orderId = _ref.orderId;
                _context.next = 3;
                return getState();

              case 3:
                _ref2 = _context.sent;
                linkGate = _ref2.linkGate;
                pathVersion = _ref2.pathVersion;
                merchant = _ref2.merchant;
                URI = "".concat(linkGate, "/").concat(pathVersion, "/merchant/").concat(merchant.id, "/order/").concat(orderId);

              case 8:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      })
    );
  }
}];
exports.default = _default;
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _axios = _interopRequireDefault(require("axios"));

var _config = _interopRequireDefault(require("./config"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var who = ["box", "mastercard", "api", "v50", "retreiveOrder"];

var _default =
/*#__PURE__*/
function () {
  var _ref3 = _asyncToGenerator(
  /*#__PURE__*/
  _regenerator.default.mark(function _callee(_ref, _ref2) {
    var orderId, baseURL, merchantId, merchantPassword, url, response, data;
    return _regenerator.default.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            orderId = _ref.orderId;
            baseURL = _ref2.baseURL, merchantId = _ref2.merchantId, merchantPassword = _ref2.merchantPassword;
            _context.prev = 2;
            url = "/".concat(_config.default.path, "/merchant/").concat(merchantId, "/order/").concat(orderId);
            _context.next = 6;
            return _axios.default.request({
              method: "get",
              baseURL: baseURL,
              url: url,
              auth: {
                username: "merchant.".concat(merchantId),
                password: merchantPassword
              }
            });

          case 6:
            response = _context.sent;

            if (!(response.status === 401)) {
              _context.next = 9;
              break;
            }

            return _context.abrupt("return", {
              errors: [{
                type: "mastercard",
                message: "Unauthorized"
              }]
            });

          case 9:
            if (!(response.status === 200)) {
              _context.next = 13;
              break;
            }

            data = response.data;
            console.log("RESPONSE", data);
            return _context.abrupt("return", data);

          case 13:
            throw new Error(who.concat(["error"]).join(':'));

          case 16:
            _context.prev = 16;
            _context.t0 = _context["catch"](2);
            console.warn(_context.t0);

          case 19:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, this, [[2, 16]]);
  }));

  return function (_x, _x2) {
    return _ref3.apply(this, arguments);
  };
}();

exports.default = _default;
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _BlockAuthClassicConfirmAuto = _interopRequireDefault(require("./block/BlockAuthClassicConfirmAuto"));

var _BlockAuthClassicFormLogin = _interopRequireDefault(require("./block/BlockAuthClassicFormLogin"));

var _BlockAuthClassicFormLogout = _interopRequireDefault(require("./block/BlockAuthClassicFormLogout"));

var _BlockAuthClassicFormSignup = _interopRequireDefault(require("./block/BlockAuthClassicFormSignup"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _default = {
  BlockAuthClassicConfirmAuto: _BlockAuthClassicConfirmAuto.default,
  BlockAuthClassicFormLogin: _BlockAuthClassicFormLogin.default,
  BlockAuthClassicFormLogout: _BlockAuthClassicFormLogout.default,
  BlockAuthClassicFormSignup: _BlockAuthClassicFormSignup.default
};
exports.default = _default;
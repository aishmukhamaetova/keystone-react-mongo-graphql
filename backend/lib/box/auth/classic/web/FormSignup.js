"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _compose = _interopRequireDefault(require("recompose/compose"));

var _defaultProps = _interopRequireDefault(require("recompose/defaultProps"));

var _withPropsOnChange = _interopRequireDefault(require("recompose/withPropsOnChange"));

var _semanticUiReact = require("semantic-ui-react");

var _runOnceInTime = _interopRequireDefault(require("../../../../evoke-me/space/api/runOnceInTime"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

var enhance = (0, _compose.default)((0, _defaultProps.default)({
  username: "",
  password: "",
  validateErrors: [],
  isLoading: false
}), (0, _withPropsOnChange.default)(['onSubmit'], function (_ref) {
  var onSubmit = _ref.onSubmit;
  return {
    onSubmit: onSubmit ? (0, _runOnceInTime.default)(onSubmit, 1000) : null
  };
}));
var FormSignup = enhance(function (props) {
  var children = props.children,
      nameFirst = props.nameFirst,
      nameFirstSet = props.nameFirstSet,
      nameLast = props.nameLast,
      nameLastSet = props.nameLastSet,
      username = props.username,
      usernameSet = props.usernameSet,
      password = props.password,
      passwordSet = props.passwordSet,
      validateErrors = props.validateErrors,
      isLoading = props.isLoading,
      onSubmit = props.onSubmit;
  var errorFields = validateErrors.map(function (_ref2) {
    var path = _ref2.props.path;
    return path[0];
  });
  return _react.default.createElement(_semanticUiReact.Form, {
    loading: isLoading,
    error: validateErrors.length > 0
  }, validateErrors.length ? _react.default.createElement(_semanticUiReact.Message, {
    error: true,
    content: validateErrors[0].message
  }) : _react.default.createElement(_semanticUiReact.Header, null, "Sign up"), _react.default.createElement(_semanticUiReact.Form.Field, {
    error: errorFields.includes("nameFirst")
  }, _react.default.createElement("label", null, "First name"), _react.default.createElement("input", {
    placeholder: "First name",
    value: nameFirst,
    onChange: function onChange(_ref3) {
      var value = _ref3.target.value;
      return nameFirstSet && nameFirstSet(value);
    }
  })), _react.default.createElement(_semanticUiReact.Form.Field, {
    error: errorFields.includes("nameLast")
  }, _react.default.createElement("label", null, "Second name"), _react.default.createElement("input", {
    placeholder: "Last name",
    value: nameLast,
    onChange: function onChange(_ref4) {
      var value = _ref4.target.value;
      return nameLastSet && nameLastSet(value);
    }
  })), _react.default.createElement(_semanticUiReact.Form.Field, {
    error: errorFields.includes("username")
  }, _react.default.createElement("label", null, "Email"), _react.default.createElement("input", {
    placeholder: "Email",
    value: username,
    onChange: function onChange(_ref5) {
      var value = _ref5.target.value;
      return usernameSet && usernameSet(value);
    }
  })), _react.default.createElement(_semanticUiReact.Form.Field, {
    error: errorFields.includes("password")
  }, _react.default.createElement("label", null, "Password"), _react.default.createElement("input", {
    placeholder: "Password",
    type: "password",
    value: password,
    onChange: function onChange(_ref6) {
      var value = _ref6.target.value;
      return passwordSet && passwordSet(value);
    }
  })), _react.default.createElement(_semanticUiReact.Button, {
    type: "submit",
    fluid: true,
    positive: true,
    onClick: function onClick(e) {
      e.preventDefault();
      onSubmit && onSubmit();
    }
  }, "Register Now"), children);
});
var _default = FormSignup;
exports.default = _default;
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _Grid = _interopRequireDefault(require("@material-ui/core/Grid"));

var _react = _interopRequireWildcard(require("react"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _BlockAuthClassicConfirmAuto = _interopRequireDefault(require("../block/BlockAuthClassicConfirmAuto"));

var _theme = _interopRequireDefault(require("../theme"));

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Section = _styledComponents.default.div.withConfig({
  displayName: "SectionAuthClassicConfirmAuto__Section",
  componentId: "sc-110u2z5-0"
})([""]);

var Item = (0, _styledComponents.default)(_Grid.default).withConfig({
  displayName: "SectionAuthClassicConfirmAuto__Item",
  componentId: "sc-110u2z5-1"
})(["max-width:320px !important;"]);

var SectionAuthClassicConfirmAuto = function SectionAuthClassicConfirmAuto() {
  return _react.default.createElement(Section, null, _react.default.createElement(_Grid.default, {
    container: true,
    justify: "center",
    spacing: 8
  }, _react.default.createElement(Item, {
    item: true,
    xs: 8
  }, _react.default.createElement(_BlockAuthClassicConfirmAuto.default, {
    theme: _theme.default
  }))));
};

var _default = SectionAuthClassicConfirmAuto;
exports.default = _default;
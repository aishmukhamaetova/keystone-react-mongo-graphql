"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _reactApollo = require("react-apollo");

var _compose = _interopRequireDefault(require("recompose/compose"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _withSpace = _interopRequireDefault(require("../../../../../evoke-me/space/all/withSpace"));

var _spaceAuthClassic = _interopRequireDefault(require("../../space/spaceAuthClassic"));

var _FormLogin = _interopRequireDefault(require("../FormLogin"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

var enhance = (0, _compose.default)(_reactApollo.withApollo, (0, _withSpace.default)(_spaceAuthClassic.default));
var BlockAuthClassicFormLogin = enhance(function (_ref) {
  var children = _ref.children,
      client = _ref.client,
      fromAuthClassic = _ref.fromAuthClassic;
  return _react.default.createElement(_FormLogin.default, _extends({}, fromAuthClassic, {
    onSubmit: function onSubmit() {
      return fromAuthClassic.login({}, {
        client: client
      });
    }
  }), children);
});
var _default = BlockAuthClassicFormLogin;
exports.default = _default;
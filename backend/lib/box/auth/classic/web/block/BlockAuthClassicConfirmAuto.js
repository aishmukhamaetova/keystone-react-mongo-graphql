"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _router = require("next/router");

var _react = _interopRequireWildcard(require("react"));

var _reactApollo = require("react-apollo");

var _compose = _interopRequireDefault(require("recompose/compose"));

var _withHandlers = _interopRequireDefault(require("recompose/withHandlers"));

var _lifecycle = _interopRequireDefault(require("recompose/lifecycle"));

var _spaceAuthClassic = _interopRequireDefault(require("../../space/spaceAuthClassic"));

var _withSpace = _interopRequireDefault(require("../../../../../evoke-me/space/all/withSpace"));

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var enhance = (0, _compose.default)(_router.withRouter, _reactApollo.withApollo, (0, _withSpace.default)(_spaceAuthClassic.default), (0, _withHandlers.default)({
  mount: function mount(_ref) {
    var client = _ref.client,
        fromAuthClassic = _ref.fromAuthClassic,
        router = _ref.router;
    return (
      /*#__PURE__*/
      _asyncToGenerator(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee() {
        var emailCode;
        return _regenerator.default.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                emailCode = router.query.emailCode;
                _context.next = 3;
                return fromAuthClassic.confirm({
                  emailCode: emailCode
                }, {
                  client: client
                });

              case 3:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      }))
    );
  }
}), (0, _lifecycle.default)({
  componentDidMount: function () {
    var _componentDidMount = _asyncToGenerator(
    /*#__PURE__*/
    _regenerator.default.mark(function _callee2() {
      return _regenerator.default.wrap(function _callee2$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
              this.props.mount();

            case 1:
            case "end":
              return _context2.stop();
          }
        }
      }, _callee2, this);
    }));

    function componentDidMount() {
      return _componentDidMount.apply(this, arguments);
    }

    return componentDidMount;
  }(),
  componentDidUpdate: function componentDidUpdate() {
    var _this$props = this.props,
        fromAuthClassic = _this$props.fromAuthClassic,
        router = _this$props.router;
    var isConfirmed = fromAuthClassic.isConfirmed,
        redirectAfterConfirm = fromAuthClassic.redirectAfterConfirm;

    if (isConfirmed) {
      router.push(redirectAfterConfirm);
    }
  }
}));
var BlockAuthClassicConfirmAuto = enhance(function (_ref3) {
  var fromAuthClassic = _ref3.fromAuthClassic;
  var errors = fromAuthClassic.errors;
  return errors.map(function (_ref4, index) {
    var message = _ref4.message;
    return _react.default.createElement("div", {
      key: index
    }, message);
  });
});
var _default = BlockAuthClassicConfirmAuto;
exports.default = _default;
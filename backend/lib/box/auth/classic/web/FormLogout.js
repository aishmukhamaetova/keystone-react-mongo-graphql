"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _compose = _interopRequireDefault(require("recompose/compose"));

var _defaultProps = _interopRequireDefault(require("recompose/defaultProps"));

var _withPropsOnChange = _interopRequireDefault(require("recompose/withPropsOnChange"));

var _semanticUiReact = require("semantic-ui-react");

var _runOnceInTime = _interopRequireDefault(require("../../../../evoke-me/space/api/runOnceInTime"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

var enhance = (0, _compose.default)((0, _defaultProps.default)({
  errors: [],
  isLoading: false
}), (0, _withPropsOnChange.default)(['onSubmit'], function (_ref) {
  var onSubmit = _ref.onSubmit;
  return {
    onSubmit: onSubmit ? (0, _runOnceInTime.default)(onSubmit, 1000) : null
  };
}));
var FormLogout = enhance(function (_ref2) {
  var errors = _ref2.errors,
      isLoading = _ref2.isLoading,
      onSubmit = _ref2.onSubmit;
  return _react.default.createElement(_semanticUiReact.Form, {
    loading: isLoading,
    error: errors.length > 0
  }, errors.length ? _react.default.createElement(_semanticUiReact.Message, {
    error: true,
    content: errors[0].message
  }) : _react.default.createElement(_semanticUiReact.Message, {
    info: true,
    content: "Welcome %username%"
  }), _react.default.createElement(_semanticUiReact.Button, {
    type: "submit",
    positive: true,
    fluid: true,
    onClick: function onClick(e) {
      e.preventDefault();
      onSubmit && onSubmit();
    }
  }, "Logout"));
});
var _default = FormLogout;
exports.default = _default;
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _compose = _interopRequireDefault(require("recompose/compose"));

var _defaultProps = _interopRequireDefault(require("recompose/defaultProps"));

var _withPropsOnChange = _interopRequireDefault(require("recompose/withPropsOnChange"));

var _semanticUiReact = require("semantic-ui-react");

var _runOnceInTime = _interopRequireDefault(require("../../../../evoke-me/space/api/runOnceInTime"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

var enhance = (0, _compose.default)((0, _defaultProps.default)({
  username: "",
  password: "",
  errors: [],
  isLoading: false
}), (0, _withPropsOnChange.default)(['onSubmit'], function (_ref) {
  var onSubmit = _ref.onSubmit;
  return {
    onSubmit: onSubmit ? (0, _runOnceInTime.default)(onSubmit, 1000) : null
  };
}));
var FormLogin = enhance(function (props) {
  var children = props.children,
      username = props.username,
      usernameSet = props.usernameSet,
      password = props.password,
      passwordSet = props.passwordSet,
      errors = props.errors,
      isLoading = props.isLoading,
      onSubmit = props.onSubmit;

  var count = _react.default.Children.count(children);

  return _react.default.createElement(_semanticUiReact.Form, {
    loading: isLoading,
    error: errors.length > 0
  }, errors.length ? _react.default.createElement(_semanticUiReact.Message, {
    error: true,
    content: errors[0].message
  }) : _react.default.createElement(_semanticUiReact.Header, null, "Log in"), _react.default.createElement(_semanticUiReact.Form.Field, null, _react.default.createElement("label", null, "Email"), _react.default.createElement("input", {
    placeholder: "Email",
    value: username,
    onChange: function onChange(_ref2) {
      var value = _ref2.target.value;
      return usernameSet && usernameSet(value);
    }
  })), _react.default.createElement(_semanticUiReact.Form.Field, null, _react.default.createElement("label", null, "Password"), _react.default.createElement("input", {
    placeholder: "Password",
    type: "password",
    value: password,
    onChange: function onChange(_ref3) {
      var value = _ref3.target.value;
      return passwordSet && passwordSet(value);
    }
  })), _react.default.createElement(_semanticUiReact.Button, {
    type: "submit",
    positive: true,
    fluid: true,
    onClick: function onClick(e) {
      e.preventDefault();
      onSubmit && onSubmit();
    }
  }, "Login"), count ? _react.default.createElement(_react.default.Fragment, null, _react.default.createElement(_semanticUiReact.Divider, {
    horizontal: true
  }, "Or"), children) : null);
});
var _default = FormLogin;
exports.default = _default;
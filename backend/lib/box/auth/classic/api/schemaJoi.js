"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _joi = _interopRequireDefault(require("joi"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _default = _joi.default.object().keys({
  nameFirst: _joi.default.string().min(2).label("First name"),
  nameLast: _joi.default.string().min(4).label("Last name"),
  username: _joi.default.string().email({
    minDomainAtoms: 2,
    errorLevel: 10
  }).label("Email"),
  password: _joi.default.string().regex(/^[a-zA-Z0-9]{3,30}$/).label("Password")
}).with("nameFirst", ["nameLast", "username", "password"]);

exports.default = _default;
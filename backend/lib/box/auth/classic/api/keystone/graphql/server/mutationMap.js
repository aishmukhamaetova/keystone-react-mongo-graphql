"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _boxAuthClassicConfirm = _interopRequireDefault(require("./boxAuthClassicConfirm"));

var _boxAuthClassicLogin = _interopRequireDefault(require("./boxAuthClassicLogin"));

var _boxAuthClassicLogout = _interopRequireDefault(require("./boxAuthClassicLogout"));

var _boxAuthClassicSignup = _interopRequireDefault(require("./boxAuthClassicSignup"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _default = {
  boxAuthClassicConfirm: _boxAuthClassicConfirm.default,
  boxAuthClassicLogin: _boxAuthClassicLogin.default,
  boxAuthClassicLogout: _boxAuthClassicLogout.default,
  boxAuthClassicSignup: _boxAuthClassicSignup.default
};
exports.default = _default;
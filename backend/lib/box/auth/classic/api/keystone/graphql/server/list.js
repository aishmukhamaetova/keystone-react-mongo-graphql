"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _boxAuthClassicCheckQuery = _interopRequireDefault(require("./boxAuthClassicCheckQuery"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _default = [_boxAuthClassicCheckQuery.default];
exports.default = _default;
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var mutationConfirm = "\n  boxAuthClassicConfirm(\n    payload: BoxAuthClassicConfirmPayload\n  ): BoxAuthClassicConfirm\n";
var mutationLogin = "\n  boxAuthClassicLogin(\n    payload: BoxAuthClassicLoginPayload\n  ): BoxAuthClassicLogin    \n";
var mutationLogout = "\n  boxAuthClassicLogout: BoxAuthClassicLogout\n";
var mutationSignup = "\n  boxAuthClassicSignup(\n    payload: BoxAuthClassicSignupPayload\n  ): BoxAuthClassicSignup\n";

var _default = [mutationConfirm, mutationLogin, mutationLogout, mutationSignup].join("\n");

exports.default = _default;
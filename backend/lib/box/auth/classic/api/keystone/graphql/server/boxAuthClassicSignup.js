"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _v = _interopRequireDefault(require("uuid/v4"));

var _nodemailer = _interopRequireDefault(require("nodemailer"));

var _authErrorMap = _interopRequireDefault(require("../../../../../manager/api/authErrorMap"));

var _config = _interopRequireDefault(require("../../../../../manager/api/config"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var smtpSettings = {
  host: 'smtp.gmail.com',
  port: 465,
  secure: true,
  // use SSL
  auth: {
    user: 'system@all-itera.ru',
    pass: 'H<j6V6=A'
  }
};

var transporter = _nodemailer.default.createTransport(smtpSettings);

var link = "".concat(process.env.CLIENT_HOST, "/box.auth/classic/confirm");

var send = function send(email, emailCode) {
  return new Promise(function (resolve, reject) {
    var mailOptions = {
      from: '"latelierdumiel.com" <system@all-itera.ru>',
      to: email,
      subject: 'latelierdumiel.com - Email confirmation',
      text: "Open next link to confirm your email ".concat(link, "/").concat(emailCode),
      html: "<a href='".concat(link, "/").concat(emailCode, "'>Click to confirm your email</a>")
    };
    transporter.sendMail(mailOptions, function (error, info) {
      if (error) {
        return reject(error);
      }

      return resolve(info.response);
    });
  });
};

var who = [_config.default.namespace, "classic", "api", "keystone", "graphql", "server", "authClassicSignup"];

var boxAuthClassicSignup =
/*#__PURE__*/
function () {
  var _ref2 = _asyncToGenerator(
  /*#__PURE__*/
  _regenerator.default.mark(function _callee(obj, args, _ref, info) {
    var keystone, req, res, _args$payload, payload, username, password, first, last, User, userCurrent, emailCode, user, mail, message;

    return _regenerator.default.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            keystone = _ref.keystone, req = _ref.req, res = _ref.res;
            _context.prev = 1;
            _args$payload = args.payload, payload = _args$payload === void 0 ? {} : _args$payload;
            username = payload.username, password = payload.password, first = payload.nameFirst, last = payload.nameLast;
            User = keystone.list('User');
            _context.next = 7;
            return User.model.findOne({
              email: {
                $eq: username
              }
            });

          case 7:
            userCurrent = _context.sent;

            if (!userCurrent) {
              _context.next = 12;
              break;
            }

            return _context.abrupt("return", {
              result: null,
              errors: [{
                type: _config.default.namespace,
                code: _authErrorMap.default.SIGNUP_USER_ALREADY_EXISTS,
                message: "User already exists",
                props: [{
                  name: "username",
                  value: username
                }]
              }]
            });

          case 12:
            emailCode = (0, _v.default)();
            _context.next = 15;
            return new User.model({
              email: username,
              emailCode: emailCode,
              password: password,
              name: {
                first: first,
                last: last
              }
            }).save();

          case 15:
            user = _context.sent;
            _context.next = 18;
            return send(username, emailCode);

          case 18:
            mail = _context.sent;
            console.log("Mail sent", mail);
            return _context.abrupt("return", {
              result: {
                status: true
              },
              errors: []
            });

          case 21:
            _context.next = 28;
            break;

          case 23:
            _context.prev = 23;
            _context.t0 = _context["catch"](1);
            console.log("ERROR", who.join(':'), _context.t0);
            message = process.env.NODE_ENV === "development" ? _context.t0 : "Signup catch error";
            return _context.abrupt("return", {
              result: null,
              errors: [{
                type: _config.default.namespace,
                code: _authErrorMap.default.UNKNOWN,
                message: message
              }]
            });

          case 28:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, this, [[1, 23]]);
  }));

  return function boxAuthClassicSignup(_x, _x2, _x3, _x4) {
    return _ref2.apply(this, arguments);
  };
}();

var _default = boxAuthClassicSignup;
exports.default = _default;
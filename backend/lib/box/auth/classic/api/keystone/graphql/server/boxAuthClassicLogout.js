"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _config = _interopRequireDefault(require("../../../../../manager/api/config"));

var _authErrorMap = _interopRequireDefault(require("../../../../../manager/api/authErrorMap"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var who = [_config.default.namespace, "classic", "api", "keystone", "graphql", "server", "authClassicLogout"];

var keystoneLogout = function keystoneLogout(keystone, req, res) {
  return new Promise(function (resolve, reject) {
    keystone.session.signout(req, res, function () {
      return resolve();
    });
  });
};

var boxAuthClassicLogout =
/*#__PURE__*/
function () {
  var _ref2 = _asyncToGenerator(
  /*#__PURE__*/
  _regenerator.default.mark(function _callee(obj, args, _ref, info) {
    var keystone, req, res, message;
    return _regenerator.default.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            keystone = _ref.keystone, req = _ref.req, res = _ref.res;
            _context.prev = 1;
            _context.next = 4;
            return keystoneLogout(keystone, req, res);

          case 4:
            return _context.abrupt("return", {
              result: {
                status: true
              },
              errors: []
            });

          case 7:
            _context.prev = 7;
            _context.t0 = _context["catch"](1);
            console.log("ERROR", who.join(':'), _context.t0);
            message = process.env.NODE_ENV === "development" ? _context.t0 : "Logout unknown error";
            return _context.abrupt("return", {
              result: null,
              errors: [{
                type: _config.default.namespace,
                code: _authErrorMap.default.UNKNOWN,
                message: message
              }]
            });

          case 12:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, this, [[1, 7]]);
  }));

  return function boxAuthClassicLogout(_x, _x2, _x3, _x4) {
    return _ref2.apply(this, arguments);
  };
}();

var _default = boxAuthClassicLogout;
exports.default = _default;
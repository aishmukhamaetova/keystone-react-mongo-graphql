"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _authErrorMap = _interopRequireDefault(require("../../../../../manager/api/authErrorMap"));

var _config = _interopRequireDefault(require("../../../../../manager/api/config"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var who = [_config.default.namespace, "classic", "api", "keystone", "graphql", "server", "authClassicLogin"];

var keystoneLogin = function keystoneLogin(keystone, req, res, userId) {
  return new Promise(function (resolve, reject) {
    keystone.session.signin(userId, req, res, function (user) {
      return resolve(user);
    }, function (err) {
      return reject(err);
    });
  });
};

var passwordCompare = function passwordCompare(user, password) {
  return new Promise(function (resolve, reject) {
    user._.password.compare(password, function (err, isMatch) {
      if (err) return reject(err);
      return resolve(isMatch);
    });
  });
};

var boxAuthClassicLogin =
/*#__PURE__*/
function () {
  var _ref2 = _asyncToGenerator(
  /*#__PURE__*/
  _regenerator.default.mark(function _callee(obj, args, _ref, info) {
    var keystone, req, res, _args$payload, payload, username, password, User, userCurrent, errorWrongUsernameOrPassword, isMatch, userAuthenticated, message;

    return _regenerator.default.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            keystone = _ref.keystone, req = _ref.req, res = _ref.res;
            _context.prev = 1;
            _args$payload = args.payload, payload = _args$payload === void 0 ? {} : _args$payload;
            username = payload.username, password = payload.password;
            User = keystone.list('User');
            _context.next = 7;
            return User.model.findOne({
              email: {
                $eq: username
              }
            });

          case 7:
            userCurrent = _context.sent;
            errorWrongUsernameOrPassword = {
              type: _config.default.namespace,
              code: _authErrorMap.default.LOGIN_WRONG_USERNAME_OR_PASSWORD,
              message: "Wrong username or password",
              props: [{
                name: "username",
                value: username
              }]
            };

            if (!userCurrent) {
              _context.next = 26;
              break;
            }

            if (!userCurrent.isConfirmed) {
              _context.next = 23;
              break;
            }

            _context.next = 13;
            return passwordCompare(userCurrent, password);

          case 13:
            isMatch = _context.sent;

            if (!isMatch) {
              _context.next = 20;
              break;
            }

            _context.next = 17;
            return keystoneLogin(keystone, req, res, String(userCurrent._id));

          case 17:
            userAuthenticated = _context.sent;

            if (!userAuthenticated) {
              _context.next = 20;
              break;
            }

            return _context.abrupt("return", {
              result: {
                userId: String(userAuthenticated._id)
              },
              errors: []
            });

          case 20:
            return _context.abrupt("return", {
              result: null,
              errors: [errorWrongUsernameOrPassword]
            });

          case 23:
            return _context.abrupt("return", {
              result: null,
              errors: [{
                type: _config.default.namespace,
                code: _authErrorMap.default.LOGIN_NOT_CONFIRMED,
                message: "Your email not confirmed",
                props: [{
                  name: "username",
                  value: username
                }]
              }]
            });

          case 24:
            _context.next = 27;
            break;

          case 26:
            return _context.abrupt("return", {
              result: null,
              errors: [errorWrongUsernameOrPassword]
            });

          case 27:
            _context.next = 34;
            break;

          case 29:
            _context.prev = 29;
            _context.t0 = _context["catch"](1);
            console.log("ERROR", who.join(':'), _context.t0);
            message = process.env.NODE_ENV === "development" ? _context.t0 : "Login catch error";
            return _context.abrupt("return", {
              result: null,
              errors: [{
                type: _config.default.namespace,
                code: _authErrorMap.default.UNKNOWN,
                message: message
              }]
            });

          case 34:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, this, [[1, 29]]);
  }));

  return function boxAuthClassicLogin(_x, _x2, _x3, _x4) {
    return _ref2.apply(this, arguments);
  };
}();

var _default = boxAuthClassicLogin;
exports.default = _default;
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var typeConfirm = "\n  type BoxAuthClassicConfirm {\n    result: BoxAuthClassicConfirmResult\n    errors: [Error]\n  }\n  \n  input BoxAuthClassicConfirmPayload {\n    emailCode: String\n  }\n\n  type BoxAuthClassicConfirmResult {\n    userId: ID\n  }\n";
var typeLogin = "\n  type BoxAuthClassicLogin {\n    result: BoxAuthClassicLoginResult\n    errors: [Error]\n  }\n  \n  input BoxAuthClassicLoginPayload {\n    username: String\n    password: String\n  }\n  \n  type BoxAuthClassicLoginResult {\n    userId: ID\n  }\n";
var typeLogout = "\n  type BoxAuthClassicLogout {\n    result: BoxAuthClassicLogoutResult\n    errors: [Error]\n  }\n  \n  type BoxAuthClassicLogoutResult {\n    status: Boolean\n  }\n";
var typeSignup = "\n  type BoxAuthClassicSignup {\n    result: BoxAuthClassicSignupResult\n    errors: [Error]    \n  }\n  \n  input BoxAuthClassicSignupPayload {\n    nameFirst: String\n    nameLast: String\n    username: String\n    password: String  \n  }\n  \n  type BoxAuthClassicSignupResult {\n    status: Boolean\n  }\n";

var _default = [typeConfirm, typeLogin, typeLogout, typeSignup].join("\n");

exports.default = _default;
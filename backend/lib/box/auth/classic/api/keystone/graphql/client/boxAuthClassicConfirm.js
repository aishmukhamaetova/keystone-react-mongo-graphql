"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _graphqlTag = _interopRequireDefault(require("graphql-tag"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _templateObject() {
  var data = _taggedTemplateLiteral(["\nmutation boxAuthClassicConfirm(\n  $emailCode: String\n) {\n  boxAuthClassicConfirm(\n    payload: {\n      emailCode: $emailCode\n    }\n  ) {\n    result {\n      userId\n    }\n    \n    errors {\n      type\n      code\n      message\n      props {\n        name\n        value\n      }\n    }\n  }\n}\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var _default = (0, _graphqlTag.default)(_templateObject());

exports.default = _default;
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _joi = _interopRequireDefault(require("joi"));

var _config = _interopRequireDefault(require("../../../error/manager/api/config"));

var _errorMap = _interopRequireDefault(require("../../../error/manager/api/errorMap"));

var _spacePersist = _interopRequireDefault(require("../../../persist/manager/space/spacePersist"));

var _authScreenMap = _interopRequireDefault(require("../../manager/api/authScreenMap"));

var _config2 = _interopRequireDefault(require("../../manager/api/config"));

var _spaceAuth = _interopRequireDefault(require("../../manager/space/spaceAuth"));

var _boxAuthClassicCheckQuery = _interopRequireDefault(require("../api/keystone/graphql/client/boxAuthClassicCheckQuery"));

var _boxAuthClassicConfirm = _interopRequireDefault(require("../api/keystone/graphql/client/boxAuthClassicConfirm"));

var _boxAuthClassicLogin = _interopRequireDefault(require("../api/keystone/graphql/client/boxAuthClassicLogin"));

var _boxAuthClassicLogout = _interopRequireDefault(require("../api/keystone/graphql/client/boxAuthClassicLogout"));

var _boxAuthClassicSignup = _interopRequireDefault(require("../api/keystone/graphql/client/boxAuthClassicSignup"));

var _schemaJoi = _interopRequireDefault(require("../api/schemaJoi"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ENTITY = "authClassic";
var _default = [_config2.default.namespace, ENTITY, [_spaceAuth.default, _spacePersist.default], function () {
  return {
    nameFirst: "",
    nameLast: "",
    username: "",
    password: "",
    errors: [],
    validateErrors: [],
    isConfirmed: false,
    isLoading: false,
    redirectAfterConfirm: "/",
    persist: true,
    persistName: "".concat(_config2.default.namespace, ".").concat(ENTITY)
  };
}, {
  restore: function restore(getState, setState, methods, _ref) {
    var fromPersist = _ref.fromPersist;
    return (
      /*#__PURE__*/
      _regenerator.default.mark(function _callee() {
        var _ref2, persist, persistName, statePersist, username;

        return _regenerator.default.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.next = 2;
                return getState();

              case 2:
                _ref2 = _context.sent;
                persist = _ref2.persist;
                persistName = _ref2.persistName;

                if (!persist) {
                  _context.next = 13;
                  break;
                }

                _context.next = 8;
                return fromPersist.get(persistName);

              case 8:
                statePersist = _context.sent;

                if (!statePersist) {
                  _context.next = 13;
                  break;
                }

                username = statePersist.username;
                _context.next = 13;
                return setState({
                  username: username
                });

              case 13:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      })
    );
  },
  validate: function validate(getState, setState) {
    return (
      /*#__PURE__*/
      _regenerator.default.mark(function _callee2(payload) {
        var _ref3, nameFirst, nameLast, username, password, _Joi$validate, error;

        return _regenerator.default.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _context2.next = 2;
                return getState();

              case 2:
                _ref3 = _context2.sent;
                nameFirst = _ref3.nameFirst;
                nameLast = _ref3.nameLast;
                username = _ref3.username;
                password = _ref3.password;
                _Joi$validate = _joi.default.validate({
                  nameFirst: nameFirst,
                  nameLast: nameLast,
                  username: username,
                  password: password
                }, _schemaJoi.default), error = _Joi$validate.error;

                if (!(error && error.details)) {
                  _context2.next = 13;
                  break;
                }

                _context2.next = 11;
                return setState({
                  validateErrors: error.details.map(function (current) {
                    var path = current.path,
                        message = current.message;
                    return {
                      type: _config.default.namespace,
                      code: _errorMap.default.VALIDATE,
                      message: message,
                      props: {
                        path: path
                      }
                    };
                  })
                });

              case 11:
                _context2.next = 15;
                break;

              case 13:
                _context2.next = 15;
                return setState({
                  validateErrors: []
                });

              case 15:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, this);
      })
    );
  },
  nameFirstSet: function nameFirstSet(getState, setState, _ref4) {
    var validate = _ref4.validate;
    return (
      /*#__PURE__*/
      _regenerator.default.mark(function _callee3(nameFirst) {
        return _regenerator.default.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                _context3.next = 2;
                return setState({
                  nameFirst: nameFirst
                });

              case 2:
                _context3.next = 4;
                return validate({
                  nameFirst: nameFirst
                });

              case 4:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3, this);
      })
    );
  },
  nameLastSet: function nameLastSet(getState, setState, _ref5) {
    var validate = _ref5.validate;
    return (
      /*#__PURE__*/
      _regenerator.default.mark(function _callee4(nameLast) {
        return _regenerator.default.wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                _context4.next = 2;
                return setState({
                  nameLast: nameLast
                });

              case 2:
                _context4.next = 4;
                return validate({
                  nameLast: nameLast
                });

              case 4:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4, this);
      })
    );
  },
  usernameSet: function usernameSet(getState, setState, _ref6, _ref7) {
    var validate = _ref6.validate;
    var fromPersist = _ref7.fromPersist;
    return (
      /*#__PURE__*/
      _regenerator.default.mark(function _callee5(username) {
        var _ref8, persist, persistName;

        return _regenerator.default.wrap(function _callee5$(_context5) {
          while (1) {
            switch (_context5.prev = _context5.next) {
              case 0:
                _context5.next = 2;
                return getState();

              case 2:
                _ref8 = _context5.sent;
                persist = _ref8.persist;
                persistName = _ref8.persistName;
                _context5.next = 7;
                return setState({
                  username: username
                });

              case 7:
                _context5.next = 9;
                return validate({
                  username: username
                });

              case 9:
                if (!persist) {
                  _context5.next = 12;
                  break;
                }

                _context5.next = 12;
                return fromPersist.set(persistName, {
                  username: username
                });

              case 12:
              case "end":
                return _context5.stop();
            }
          }
        }, _callee5, this);
      })
    );
  },
  passwordSet: function passwordSet(getState, setState, _ref9) {
    var validate = _ref9.validate;
    return (
      /*#__PURE__*/
      _regenerator.default.mark(function _callee6(password) {
        return _regenerator.default.wrap(function _callee6$(_context6) {
          while (1) {
            switch (_context6.prev = _context6.next) {
              case 0:
                _context6.next = 2;
                return setState({
                  password: password
                });

              case 2:
                _context6.next = 4;
                return validate({
                  password: password
                });

              case 4:
              case "end":
                return _context6.stop();
            }
          }
        }, _callee6, this);
      })
    );
  },
  check: function check(getState, setState, methods, _ref10) {
    var fromAuth = _ref10.fromAuth,
        fromNotify = _ref10.fromNotify;
    return (
      /*#__PURE__*/
      _regenerator.default.mark(function _callee7(_ref11, _ref12) {
        var emailCode, client, _ref13, data, boxAuthClassicCheck;

        return _regenerator.default.wrap(function _callee7$(_context7) {
          while (1) {
            switch (_context7.prev = _context7.next) {
              case 0:
                emailCode = _ref11.emailCode;
                client = _ref12.client;
                _context7.prev = 2;

                if (!client) {
                  _context7.next = 16;
                  break;
                }

                _context7.next = 6;
                return client.query({
                  query: _boxAuthClassicCheckQuery.default
                });

              case 6:
                _ref13 = _context7.sent;
                data = _ref13.data;
                boxAuthClassicCheck = data.boxAuthClassicCheck;

                if (!(boxAuthClassicCheck && boxAuthClassicCheck.result && boxAuthClassicCheck.result.status)) {
                  _context7.next = 14;
                  break;
                }

                _context7.next = 12;
                return fromAuth.isAuthSet(true);

              case 12:
                _context7.next = 16;
                break;

              case 14:
                _context7.next = 16;
                return fromAuth.isAuthSet(false);

              case 16:
                _context7.next = 21;
                break;

              case 18:
                _context7.prev = 18;
                _context7.t0 = _context7["catch"](2);
                console.log(_context7.t0);

              case 21:
              case "end":
                return _context7.stop();
            }
          }
        }, _callee7, this, [[2, 18]]);
      })
    );
  },
  confirm: function confirm(getState, setState, methods, _ref14) {
    var fromAuth = _ref14.fromAuth,
        fromNotify = _ref14.fromNotify;
    return (
      /*#__PURE__*/
      _regenerator.default.mark(function _callee8(_ref15, _ref16) {
        var emailCode, client, _ref17, data, boxAuthClassicConfirm, result, errors;

        return _regenerator.default.wrap(function _callee8$(_context8) {
          while (1) {
            switch (_context8.prev = _context8.next) {
              case 0:
                emailCode = _ref15.emailCode;
                client = _ref16.client;
                _context8.prev = 2;

                if (!client) {
                  _context8.next = 25;
                  break;
                }

                _context8.next = 6;
                return setState({
                  isLoading: true
                });

              case 6:
                _context8.next = 8;
                return client.mutate({
                  mutation: _boxAuthClassicConfirm.default,
                  variables: {
                    emailCode: emailCode
                  }
                });

              case 8:
                _ref17 = _context8.sent;
                data = _ref17.data;
                boxAuthClassicConfirm = data.boxAuthClassicConfirm;

                if (!boxAuthClassicConfirm) {
                  _context8.next = 23;
                  break;
                }

                result = boxAuthClassicConfirm.result;
                errors = boxAuthClassicConfirm.errors.map(function (_ref18) {
                  var type = _ref18.type,
                      code = _ref18.code,
                      message = _ref18.message,
                      props = _ref18.props;
                  return {
                    type: type,
                    code: code,
                    message: message,
                    props: props.reduce(function (acc, _ref19) {
                      var name = _ref19.name,
                          value = _ref19.value;
                      return acc[name] = value;
                    }, {})
                  };
                });

                if (!errors.length) {
                  _context8.next = 19;
                  break;
                }

                _context8.next = 17;
                return setState({
                  isLoading: false,
                  errors: errors
                });

              case 17:
                _context8.next = 23;
                break;

              case 19:
                _context8.next = 21;
                return fromAuth.isAuthSet(true);

              case 21:
                _context8.next = 23;
                return setState({
                  password: "",
                  isLoading: false,
                  isConfirmed: true,
                  errors: []
                });

              case 23:
                _context8.next = 26;
                break;

              case 25:
                console.log("apolloClient miss");

              case 26:
                _context8.next = 33;
                break;

              case 28:
                _context8.prev = 28;
                _context8.t0 = _context8["catch"](2);
                console.log(_context8.t0);
                _context8.next = 33;
                return setState({
                  isLoading: false,
                  errors: []
                });

              case 33:
              case "end":
                return _context8.stop();
            }
          }
        }, _callee8, this, [[2, 28]]);
      })
    );
  },
  login: function login(getState, setState, methods, _ref20) {
    var fromAuth = _ref20.fromAuth,
        fromNotify = _ref20.fromNotify;
    return (
      /*#__PURE__*/
      _regenerator.default.mark(function _callee9(payload, _ref21) {
        var client, _ref22, username, password, _ref23, data, boxAuthClassicLogin, result, errors;

        return _regenerator.default.wrap(function _callee9$(_context9) {
          while (1) {
            switch (_context9.prev = _context9.next) {
              case 0:
                client = _ref21.client;
                _context9.prev = 1;

                if (!client) {
                  _context9.next = 29;
                  break;
                }

                _context9.next = 5;
                return setState({
                  isLoading: true
                });

              case 5:
                _context9.next = 7;
                return getState();

              case 7:
                _ref22 = _context9.sent;
                username = _ref22.username;
                password = _ref22.password;
                _context9.next = 12;
                return client.mutate({
                  mutation: _boxAuthClassicLogin.default,
                  variables: {
                    username: username,
                    password: password
                  }
                });

              case 12:
                _ref23 = _context9.sent;
                data = _ref23.data;
                boxAuthClassicLogin = data.boxAuthClassicLogin;

                if (!boxAuthClassicLogin) {
                  _context9.next = 27;
                  break;
                }

                result = boxAuthClassicLogin.result;
                errors = boxAuthClassicLogin.errors.map(function (_ref24) {
                  var type = _ref24.type,
                      code = _ref24.code,
                      message = _ref24.message,
                      props = _ref24.props;
                  return {
                    type: type,
                    code: code,
                    message: message,
                    props: props.reduce(function (acc, _ref25) {
                      var name = _ref25.name,
                          value = _ref25.value;
                      return acc[name] = value;
                    }, {})
                  };
                });

                if (!errors.length) {
                  _context9.next = 23;
                  break;
                }

                _context9.next = 21;
                return setState({
                  isLoading: false,
                  errors: errors
                });

              case 21:
                _context9.next = 27;
                break;

              case 23:
                _context9.next = 25;
                return fromAuth.isAuthSet(true);

              case 25:
                _context9.next = 27;
                return setState({
                  password: "",
                  isLoading: false,
                  errors: []
                });

              case 27:
                _context9.next = 30;
                break;

              case 29:
                console.log("apolloClient miss");

              case 30:
                _context9.next = 37;
                break;

              case 32:
                _context9.prev = 32;
                _context9.t0 = _context9["catch"](1);
                console.log(_context9.t0);
                _context9.next = 37;
                return setState({
                  isLoading: false,
                  errors: []
                });

              case 37:
              case "end":
                return _context9.stop();
            }
          }
        }, _callee9, this, [[1, 32]]);
      })
    );
  },
  logout: function logout(getState, setState, methods, _ref26) {
    var fromAuth = _ref26.fromAuth,
        fromNotify = _ref26.fromNotify;
    return (
      /*#__PURE__*/
      _regenerator.default.mark(function _callee10(payload, _ref27) {
        var client, _ref28, data, boxAuthClassicLogout, result, errors;

        return _regenerator.default.wrap(function _callee10$(_context10) {
          while (1) {
            switch (_context10.prev = _context10.next) {
              case 0:
                client = _ref27.client;
                _context10.prev = 1;

                if (!client) {
                  _context10.next = 24;
                  break;
                }

                _context10.next = 5;
                return setState({
                  isLoading: true
                });

              case 5:
                _context10.next = 7;
                return client.mutate({
                  mutation: _boxAuthClassicLogout.default
                });

              case 7:
                _ref28 = _context10.sent;
                data = _ref28.data;
                boxAuthClassicLogout = data.boxAuthClassicLogout;

                if (!boxAuthClassicLogout) {
                  _context10.next = 22;
                  break;
                }

                result = boxAuthClassicLogout.result;
                errors = boxAuthClassicLogout.errors.map(function (_ref29) {
                  var type = _ref29.type,
                      code = _ref29.code,
                      message = _ref29.message,
                      props = _ref29.props;
                  return {
                    type: type,
                    code: code,
                    message: message,
                    props: props.reduce(function (acc, _ref30) {
                      var name = _ref30.name,
                          value = _ref30.value;
                      return acc[name] = value;
                    }, {})
                  };
                });

                if (!errors.length) {
                  _context10.next = 18;
                  break;
                }

                _context10.next = 16;
                return setState({
                  isLoading: false,
                  errors: errors
                });

              case 16:
                _context10.next = 22;
                break;

              case 18:
                _context10.next = 20;
                return fromAuth.isAuthSet(false);

              case 20:
                _context10.next = 22;
                return setState({
                  isLoading: false,
                  errors: []
                });

              case 22:
                _context10.next = 25;
                break;

              case 24:
                console.log("apolloClient miss");

              case 25:
                _context10.next = 32;
                break;

              case 27:
                _context10.prev = 27;
                _context10.t0 = _context10["catch"](1);
                console.log(_context10.t0);
                _context10.next = 32;
                return setState({
                  isLoading: false,
                  errors: []
                });

              case 32:
              case "end":
                return _context10.stop();
            }
          }
        }, _callee10, this, [[1, 27]]);
      })
    );
  },
  signup: function signup(getState, setState, methods, _ref31) {
    var fromAuth = _ref31.fromAuth,
        fromNotify = _ref31.fromNotify;
    return (
      /*#__PURE__*/
      _regenerator.default.mark(function _callee11(payload, _ref32) {
        var client, _ref33, username, password, nameFirst, nameLast, _ref34, data, boxAuthClassicSignup, result, errors;

        return _regenerator.default.wrap(function _callee11$(_context11) {
          while (1) {
            switch (_context11.prev = _context11.next) {
              case 0:
                client = _ref32.client;
                _context11.prev = 1;

                if (!client) {
                  _context11.next = 31;
                  break;
                }

                _context11.next = 5;
                return setState({
                  isLoading: true
                });

              case 5:
                _context11.next = 7;
                return getState();

              case 7:
                _ref33 = _context11.sent;
                username = _ref33.username;
                password = _ref33.password;
                nameFirst = _ref33.nameFirst;
                nameLast = _ref33.nameLast;
                _context11.next = 14;
                return client.mutate({
                  mutation: _boxAuthClassicSignup.default,
                  variables: {
                    username: username,
                    password: password,
                    nameFirst: nameFirst,
                    nameLast: nameLast
                  }
                });

              case 14:
                _ref34 = _context11.sent;
                data = _ref34.data;
                boxAuthClassicSignup = data.boxAuthClassicSignup;

                if (!boxAuthClassicSignup) {
                  _context11.next = 29;
                  break;
                }

                result = boxAuthClassicSignup.result;
                errors = boxAuthClassicSignup.errors.map(function (_ref35) {
                  var type = _ref35.type,
                      code = _ref35.code,
                      message = _ref35.message,
                      props = _ref35.props;
                  return {
                    type: type,
                    code: code,
                    message: message,
                    props: props.reduce(function (acc, _ref36) {
                      var name = _ref36.name,
                          value = _ref36.value;
                      return acc[name] = value;
                    }, {})
                  };
                });

                if (!errors.length) {
                  _context11.next = 25;
                  break;
                }

                _context11.next = 23;
                return setState({
                  isLoading: false,
                  errors: errors
                });

              case 23:
                _context11.next = 29;
                break;

              case 25:
                _context11.next = 27;
                return fromAuth.screenSet(_authScreenMap.default.LOGIN);

              case 27:
                _context11.next = 29;
                return setState({
                  password: "",
                  isLoading: false,
                  errors: []
                });

              case 29:
                _context11.next = 32;
                break;

              case 31:
                console.log("apolloClient miss");

              case 32:
                _context11.next = 39;
                break;

              case 34:
                _context11.prev = 34;
                _context11.t0 = _context11["catch"](1);
                console.log(_context11.t0);
                _context11.next = 39;
                return setState({
                  isLoading: false,
                  errors: []
                });

              case 39:
              case "end":
                return _context11.stop();
            }
          }
        }, _callee11, this, [[1, 34]]);
      })
    );
  }
}];
exports.default = _default;
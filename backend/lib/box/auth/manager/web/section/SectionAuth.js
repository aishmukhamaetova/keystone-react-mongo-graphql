"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _Grid = _interopRequireDefault(require("@material-ui/core/Grid"));

var _react = _interopRequireWildcard(require("react"));

var _compose = _interopRequireDefault(require("recompose/compose"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _withSpace = _interopRequireDefault(require("../../../../../evoke-me/space/all/withSpace"));

var _spaceAuth = _interopRequireDefault(require("../../space/spaceAuth"));

var _BlockAuth = _interopRequireDefault(require("../block/BlockAuth"));

var _BlockAuthSwitch = _interopRequireDefault(require("../block/BlockAuthSwitch"));

var _theme = _interopRequireDefault(require("../theme"));

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Section = _styledComponents.default.div.withConfig({
  displayName: "SectionAuth__Section",
  componentId: "q9mbst-0"
})([""]);

var Item = (0, _styledComponents.default)(_Grid.default).withConfig({
  displayName: "SectionAuth__Item",
  componentId: "q9mbst-1"
})(["max-width:320px !important;"]);
var enhance = (0, _compose.default)((0, _withSpace.default)(_spaceAuth.default));
var SectionAuth = enhance(function (_ref) {
  var fromAuth = _ref.fromAuth;
  var isAuth = fromAuth.isAuth;
  return _react.default.createElement(Section, null, _react.default.createElement(_Grid.default, {
    container: true,
    justify: "center",
    spacing: 8
  }, _react.default.createElement(Item, {
    item: true,
    xs: 8
  }, _react.default.createElement(_BlockAuth.default, {
    theme: _theme.default
  }))), !isAuth ? _react.default.createElement(_Grid.default, {
    container: true,
    justify: "center",
    spacing: 8
  }, _react.default.createElement(Item, {
    item: true,
    xs: 8
  }, _react.default.createElement(_BlockAuthSwitch.default, {
    theme: _theme.default
  }))) : null);
});
var _default = SectionAuth;
exports.default = _default;
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _compose = _interopRequireDefault(require("recompose/compose"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _withSpace = _interopRequireDefault(require("../../../../../evoke-me/space/all/withSpace"));

var _authScreenMap = _interopRequireDefault(require("../../api/authScreenMap"));

var _authTypeMap = _interopRequireDefault(require("../../api/authTypeMap"));

var _spaceAuth = _interopRequireDefault(require("../../space/spaceAuth"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

var enhance = (0, _compose.default)((0, _withSpace.default)(_spaceAuth.default));
var BlockAuth = enhance(function (_ref) {
  var fromAuth = _ref.fromAuth,
      theme = _ref.theme;
  var BlockAuthClassicFormLogin = theme.BlockAuthClassicFormLogin;
  var BlockAuthClassicFormLogout = theme.BlockAuthClassicFormLogout;
  var BlockAuthClassicFormSignup = theme.BlockAuthClassicFormSignup;
  var BlockAuthFacebookButtonLogin = theme.BlockAuthFacebookButtonLogin;

  if (fromAuth.isAuth) {
    return _react.default.createElement(BlockAuthClassicFormLogout, null);
  }

  return fromAuth.screen === _authScreenMap.default.LOGIN ? _react.default.createElement(BlockAuthClassicFormLogin, null, fromAuth.supportList.includes(_authTypeMap.default.FACEBOOK) ? _react.default.createElement(BlockAuthFacebookButtonLogin, null) : null) : _react.default.createElement(BlockAuthClassicFormSignup, null);
});
var _default = BlockAuth;
exports.default = _default;
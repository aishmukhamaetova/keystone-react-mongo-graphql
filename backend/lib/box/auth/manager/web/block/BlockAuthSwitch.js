"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _compose = _interopRequireDefault(require("recompose/compose"));

var _semanticUiReact = require("semantic-ui-react");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _withSpace = _interopRequireDefault(require("../../../../../evoke-me/space/all/withSpace"));

var _authScreenMap = _interopRequireDefault(require("../../api/authScreenMap"));

var _spaceAuth = _interopRequireDefault(require("../../space/spaceAuth"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

var enhance = (0, _compose.default)((0, _withSpace.default)(_spaceAuth.default));
var BlockAuthSwitch = enhance(function (_ref) {
  var fromAuth = _ref.fromAuth;
  return fromAuth.screen === _authScreenMap.default.LOGIN ? _react.default.createElement(_semanticUiReact.Message, {
    warning: true
  }, _react.default.createElement(_semanticUiReact.Icon, {
    name: "help"
  }), "Don't have an account?\xA0", _react.default.createElement("a", {
    href: "#",
    onClick: function onClick(e) {
      e.preventDefault();
      fromAuth.screenSet(_authScreenMap.default.SIGNUP);
    }
  }, "Sign up")) : _react.default.createElement(_semanticUiReact.Message, {
    warning: true
  }, _react.default.createElement(_semanticUiReact.Icon, {
    name: "help"
  }), "Already signed up?\xA0", _react.default.createElement("a", {
    href: "#",
    onClick: function onClick(e) {
      e.preventDefault();
      fromAuth.screenSet(_authScreenMap.default.LOGIN);
    }
  }, "Login\xA0here"));
});
var _default = BlockAuthSwitch;
exports.default = _default;
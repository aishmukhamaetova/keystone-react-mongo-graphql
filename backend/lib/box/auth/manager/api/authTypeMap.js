"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _default = {
  AZURE: "AZURE",
  CLASSIC: "CLASSIC",
  FACEBOOK: "FACEBOOK",
  GITHUB: "GITHUB",
  GOOGLE: "GOOGLE",
  LOCAL: "LOCAL",
  TWITTER: "TWITTER",
  STEAM: "STEAM",
  VKONTAKTE: "VKONTAKTE"
};
exports.default = _default;
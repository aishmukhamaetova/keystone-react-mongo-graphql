"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _spaceNotify = _interopRequireDefault(require("../../../notify/manager/space/spaceNotify"));

var _spacePersist = _interopRequireDefault(require("../../../persist/manager/space/spacePersist"));

var _config = _interopRequireDefault(require("../api/config"));

var _authTypeMap = _interopRequireDefault(require("../api/authTypeMap"));

var _authScreenMap = _interopRequireDefault(require("../api/authScreenMap"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

var ENTITY = "auth";
var _default = [_config.default.namespace, ENTITY, [_spaceNotify.default, _spacePersist.default], function () {
  return {
    isAuth: false,
    screen: _authScreenMap.default.LOGIN,
    supportList: [_authTypeMap.default.CLASSIC, _authTypeMap.default.FACEBOOK],
    persist: true,
    persistName: "".concat(_config.default.namespace, ".").concat(ENTITY)
  };
}, {
  restore: function restore(getState, setState, methods, _ref) {
    var fromPersist = _ref.fromPersist;
    return (
      /*#__PURE__*/
      _regenerator.default.mark(function _callee() {
        var _ref2, persist, persistName, statePersist, isAuth;

        return _regenerator.default.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.next = 2;
                return getState();

              case 2:
                _ref2 = _context.sent;
                persist = _ref2.persist;
                persistName = _ref2.persistName;

                if (!persist) {
                  _context.next = 13;
                  break;
                }

                _context.next = 8;
                return fromPersist.get(persistName);

              case 8:
                statePersist = _context.sent;

                if (!statePersist) {
                  _context.next = 13;
                  break;
                }

                isAuth = statePersist.isAuth;
                _context.next = 13;
                return setState({
                  isAuth: isAuth
                });

              case 13:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      })
    );
  },
  isAuthSet: function isAuthSet(getState, setState, methods, _ref3) {
    var fromPersist = _ref3.fromPersist;
    return (
      /*#__PURE__*/
      _regenerator.default.mark(function _callee2(isAuth) {
        var _ref4, persist, persistName, _ref5, data, orderMap;

        return _regenerator.default.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _context2.next = 2;
                return getState();

              case 2:
                _ref4 = _context2.sent;
                persist = _ref4.persist;
                persistName = _ref4.persistName;
                _context2.next = 7;
                return setState({
                  isAuth: isAuth
                });

              case 7:
                if (!persist) {
                  _context2.next = 15;
                  break;
                }

                _context2.next = 10;
                return getState();

              case 10:
                _ref5 = _context2.sent;
                data = _ref5.data;
                orderMap = _ref5.orderMap;
                _context2.next = 15;
                return fromPersist.set(persistName, {
                  isAuth: isAuth
                });

              case 15:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, this);
      })
    );
  },
  screenSet: function screenSet(getState, setState) {
    return (
      /*#__PURE__*/
      _regenerator.default.mark(function _callee3(screen) {
        return _regenerator.default.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                _context3.next = 2;
                return setState({
                  screen: screen
                });

              case 2:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3, this);
      })
    );
  },
  supportListAdd: function supportListAdd(getState, setState) {
    return (
      /*#__PURE__*/
      _regenerator.default.mark(function _callee4(authType) {
        var _ref6, supportList, index;

        return _regenerator.default.wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                _context4.next = 2;
                return getState();

              case 2:
                _ref6 = _context4.sent;
                supportList = _ref6.supportList;
                index = supportList.indexOf(authType);

                if (!(index === -1)) {
                  _context4.next = 8;
                  break;
                }

                _context4.next = 8;
                return setState({
                  supportList: _toConsumableArray(supportList).concat([authType])
                });

              case 8:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4, this);
      })
    );
  },
  supportListRemove: function supportListRemove(getState, setState) {
    return (
      /*#__PURE__*/
      _regenerator.default.mark(function _callee5(authType) {
        var _ref7, supportList, index;

        return _regenerator.default.wrap(function _callee5$(_context5) {
          while (1) {
            switch (_context5.prev = _context5.next) {
              case 0:
                _context5.next = 2;
                return getState();

              case 2:
                _ref7 = _context5.sent;
                supportList = _ref7.supportList;
                index = supportList.indexOf(authType);

                if (!(index !== -1)) {
                  _context5.next = 8;
                  break;
                }

                _context5.next = 8;
                return setState({
                  supportList: _toConsumableArray(supportList.slice(0, index)).concat(_toConsumableArray(supportList.slice(index + 1)))
                });

              case 8:
              case "end":
                return _context5.stop();
            }
          }
        }, _callee5, this);
      })
    );
  }
}];
exports.default = _default;
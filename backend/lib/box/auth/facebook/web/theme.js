"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _BlockAuthFacebookButtonLogin = _interopRequireDefault(require("./block/BlockAuthFacebookButtonLogin"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _default = {
  BlockAuthFacebookButtonLogin: _BlockAuthFacebookButtonLogin.default
};
exports.default = _default;
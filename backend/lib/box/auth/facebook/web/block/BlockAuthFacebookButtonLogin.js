"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _router = require("next/router");

var _react = _interopRequireWildcard(require("react"));

var _compose = _interopRequireDefault(require("recompose/compose"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _withSpace = _interopRequireDefault(require("../../../../../evoke-me/space/all/withSpace"));

var _spaceAuthFacebook = _interopRequireDefault(require("../../space/spaceAuthFacebook"));

var _ButtonFacebookLogin = _interopRequireDefault(require("../ButtonFacebookLogin"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

var enhance = (0, _compose.default)(_router.withRouter, (0, _withSpace.default)(_spaceAuthFacebook.default));
var BlockAuthFacebookButtonLogin = enhance(function (_ref) {
  var fromAuthFacebook = _ref.fromAuthFacebook,
      router = _ref.router;
  return _react.default.createElement(_ButtonFacebookLogin.default, {
    onClick: function onClick() {
      return router.push('/api/box.auth/facebook/login');
    }
  });
});
var _default = BlockAuthFacebookButtonLogin;
exports.default = _default;
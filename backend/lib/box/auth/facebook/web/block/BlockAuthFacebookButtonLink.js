"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _reactApollo = require("react-apollo");

var _react = _interopRequireWildcard(require("react"));

var _compose = _interopRequireDefault(require("recompose/compose"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _withSpace = _interopRequireDefault(require("../../../../../evoke-me/space/all/withSpace"));

var _spaceAuthFacebook = _interopRequireDefault(require("../../space/spaceAuthFacebook"));

var _ButtonFacebookLogin = _interopRequireDefault(require("../ButtonFacebookLogin"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

var enhance = (0, _compose.default)(_reactApollo.withApollo, (0, _withSpace.default)(_spaceAuthFacebook.default));
var BlockAuthFacebookButtonLink = enhance(function (_ref) {
  var fromAuthClassic = _ref.fromAuthClassic;
  return _react.default.createElement(_ButtonFacebookLogin.default, _extends({}, fromAuthClassic, {
    onClick: function onClick() {
      return console.log("BlockAuthFacebookButtonLogin", "ButtonLogin", "onClick");
    }
  }));
});
var _default = BlockAuthFacebookButtonLink;
exports.default = _default;
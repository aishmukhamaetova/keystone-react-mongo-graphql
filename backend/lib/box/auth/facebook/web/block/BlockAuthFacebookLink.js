"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _router = require("next/router");

var _reactApollo = require("react-apollo");

var _react = _interopRequireWildcard(require("react"));

var _compose = _interopRequireDefault(require("recompose/compose"));

var _lifecycle = _interopRequireDefault(require("recompose/lifecycle"));

var _withHandlers = _interopRequireDefault(require("recompose/withHandlers"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _withSpace = _interopRequireDefault(require("../../../../../evoke-me/space/all/withSpace"));

var _spaceAuthFacebook = _interopRequireDefault(require("../../space/spaceAuthFacebook"));

var _ButtonFacebookLink = _interopRequireDefault(require("../ButtonFacebookLink"));

var _FacebookAvatar = _interopRequireDefault(require("../FacebookAvatar"));

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var AvatarStyled = (0, _styledComponents.default)(_FacebookAvatar.default).withConfig({
  displayName: "BlockAuthFacebookLink__AvatarStyled",
  componentId: "qoak3r-0"
})(["margin:5px;"]);
var enhance = (0, _compose.default)(_reactApollo.withApollo, _router.withRouter, (0, _withSpace.default)(_spaceAuthFacebook.default), (0, _withHandlers.default)({
  mount: function mount(_ref) {
    var client = _ref.client,
        fromAuthFacebook = _ref.fromAuthFacebook;
    return (
      /*#__PURE__*/
      _asyncToGenerator(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee() {
        return _regenerator.default.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                fromAuthFacebook.me({}, {
                  client: client
                });

              case 1:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      }))
    );
  }
}), (0, _lifecycle.default)({
  componentDidMount: function componentDidMount() {
    this.props.mount();
  }
}));
var BlockAuthFacebookLink = enhance(function (_ref3) {
  var fromAuthFacebook = _ref3.fromAuthFacebook,
      router = _ref3.router;
  var avatar = fromAuthFacebook.avatar,
      isLoading = fromAuthFacebook.isLoading;
  return _react.default.createElement(_react.default.Fragment, null, avatar ? _react.default.createElement(AvatarStyled, {
    imageLink: avatar
  }) : null, _react.default.createElement(_ButtonFacebookLink.default, {
    onClick: function onClick() {
      return router.push('/api/box.auth/facebook/link');
    }
  }));
});
var _default = BlockAuthFacebookLink;
exports.default = _default;
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

var background = function background(_ref) {
  var imageLink = _ref.imageLink;
  return "background-image: url(".concat(imageLink, ");");
};

var Image = _styledComponents.default.div.withConfig({
  displayName: "FacebookAvatar__Image",
  componentId: "sc-10jwecc-0"
})(["", " width:200px;height:200px;background-size:cover;background-position:center;background-repeat:no-repeat;"], background);

var FacebookAvatar = function FacebookAvatar(props) {
  return _react.default.createElement(Image, props);
};

var _default = FacebookAvatar;
exports.default = _default;
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _compose = _interopRequireDefault(require("recompose/compose"));

var _withPropsOnChange = _interopRequireDefault(require("recompose/withPropsOnChange"));

var _semanticUiReact = require("semantic-ui-react");

var _runOnceInTime = _interopRequireDefault(require("../../../../evoke-me/space/api/runOnceInTime"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

var enhance = (0, _compose.default)((0, _withPropsOnChange.default)(['onClick'], function (_ref) {
  var onClick = _ref.onClick;
  return {
    onClick: (0, _runOnceInTime.default)(onClick, 1000)
  };
}));
var ButtonFacebookLogin = enhance(function (props) {
  var _onClick = props.onClick;
  return _react.default.createElement(_semanticUiReact.Button, {
    type: "submit",
    icon: true,
    fluid: true,
    secondary: true,
    onClick: function onClick(e) {
      e.preventDefault();

      _onClick();
    }
  }, "Link profile with Facebook\xA0", _react.default.createElement(_semanticUiReact.Icon, {
    name: "facebook"
  }));
});
var _default = ButtonFacebookLogin;
exports.default = _default;
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _default = function _default(keystone) {
  var Types = keystone.Field.Types;
  var UserFacebook = new keystone.List('UserFacebook');
  UserFacebook.add({
    facebookId: {
      type: String
    },
    avatar: {
      type: String
    },
    user: {
      type: Types.Relationship,
      ref: 'User'
    }
  });
  UserFacebook.defaultColumns = 'facebookId, avatar';
  UserFacebook.register();
};

exports.default = _default;
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.query = exports.type = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _config = _interopRequireDefault(require("../../../config"));

var _authErrorMap = _interopRequireDefault(require("../../../../../manager/api/authErrorMap"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var passport = require('passport');

var passportFacebook = require('passport-facebook');

var credentials = {
  clientID: process.env.FACEBOOK_CLIENT_ID,
  clientSecret: process.env.FACEBOOK_CLIENT_SECRET,
  callbackURL: process.env.FACEBOOK_CALLBACK_URL
};
var NAME = "Link";
var name = _config.default.name;
var nameType = _config.default.nameType;
var queryName = "".concat(name).concat(NAME);
var typeQuery = "".concat(nameType).concat(NAME);
var typePayload = "".concat(typeQuery, "Payload");
var typeResult = "".concat(typeQuery, "Result");
var type = "\n  type ".concat(typeQuery, " {\n    result: ").concat(typeResult, "\n    errors: [Error]\n  }\n  \n  type ").concat(typeResult, " {\n    status: Boolean\n  }\n  \n  input ").concat(typePayload, " {\n    empty: Boolean\n  }\n");
exports.type = type;
var query = "";
exports.query = query;
var mutation = "\n  ".concat(queryName, "(\n    payload: ").concat(typePayload, "\n  ): ").concat(typeQuery, "\n");

var error = function error(code, message) {
  var props = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : [];
  return {
    result: null,
    errors: [{
      type: _config.default.namespace,
      code: code,
      message: message,
      props: props
    }]
  };
};

var mutationMap = _defineProperty({}, queryName, function () {
  var _ref2 = _asyncToGenerator(
  /*#__PURE__*/
  _regenerator.default.mark(function _callee(obj, args, _ref, info) {
    var keystone, req, res, user, message;
    return _regenerator.default.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            keystone = _ref.keystone, req = _ref.req, res = _ref.res;
            _context.prev = 1;
            user = res.locals.user;

            if (!user) {
              _context.next = 6;
              break;
            }

            console.log("START LINK");
            return _context.abrupt("return", {
              result: {
                status: true,
                errors: []
              }
            });

          case 6:
            return _context.abrupt("return", error(_authErrorMap.default.FACEBOOK_NOT_AUTHENTICATED, "You are not authenticated"));

          case 9:
            _context.prev = 9;
            _context.t0 = _context["catch"](1);
            console.log("ERROR", queryName, _context.t0);
            message = process.env.NODE_ENV === "development" ? _context.t0 : "".concat(queryName, " unknown error");
            return _context.abrupt("return", error(_authErrorMap.default.UNKNOWN, message));

          case 14:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, this, [[1, 9]]);
  }));

  return function (_x, _x2, _x3, _x4) {
    return _ref2.apply(this, arguments);
  };
}());

var queryMap = {};
var _default = {
  type: type,
  mutation: mutation,
  mutationMap: mutationMap,
  query: query,
  queryMap: queryMap
};
exports.default = _default;
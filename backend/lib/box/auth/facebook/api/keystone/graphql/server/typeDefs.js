"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var typeMe = "\n  type BoxAuthFacebookMe {\n    result: BoxAuthClassicConfirmResult\n    errors: [Error]\n  }\n  \n  type BoxAuthClassicConfirmResult {\n    avatar: String\n  }\n";

var _default = [typeMe].join("\n");

exports.default = _default;
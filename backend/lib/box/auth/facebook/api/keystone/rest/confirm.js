"use strict";

var keystone = require('keystone');

var User = keystone.list('User');

var processKeystoneLogin = function processKeystoneLogin(req, res, next) {
  console.log("OUR USER IS", req.session.user);

  var onSuccess = function onSuccess(user) {
    console.log('[auth.confirm] - Successfully signed in.');
    console.log('------------------------------------------------------------');
    return res.redirect("/enter");
  };

  var onFail = function onFail(err) {
    console.log('[auth.confirm] - Failed signing in.', err);
    console.log('------------------------------------------------------------');
    return res.redirect("/enter");
  };

  keystone.session.signin(String(req.session.user._id), req, res, onSuccess, onFail);
};

var processExisting = function processExisting(req, res, next) {
  var _req$session$authFb = req.session.authFb,
      avatar = _req$session$authFb.avatar,
      _req$session$authFb$n = _req$session$authFb.name,
      first = _req$session$authFb$n.first,
      last = _req$session$authFb$n.last,
      profileId = _req$session$authFb.profileId;
  User.model.findOne({
    facebookId: profileId
  }).exec(function (err, userResult) {
    if (userResult) {
      console.log("USER EXIST", userResult);
      req.session.user = userResult;
      return processKeystoneLogin(req, res, next);
    } else {
      var doc = {
        name: {
          first: first,
          last: last
        },
        facebookId: profileId,
        password: Math.random().toString(36).slice(-8)
      };
      var user = new User.model(doc);
      user.save(function (err) {
        console.log("CREATE USER", userResult);
        req.session.user = user;
        return processKeystoneLogin(req, res, next);
      });
    }
  });
};

exports = module.exports = function (req, res, next) {
  if (req.session.authFb) {
    return processExisting(req, res, next);
  }
};
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var keystone = require('keystone');

var passport = require('passport');

var passportFacebook = require('passport-facebook');

var User = keystone.list('User');
var UserFacebook = keystone.list('UserFacebook');
var credentials = {
  clientID: process.env.FACEBOOK_CLIENT_ID,
  clientSecret: process.env.FACEBOOK_CLIENT_SECRET,
  callbackURL: process.env.FACEBOOK_LOGIN_URL
};

var keystoneLogin = function keystoneLogin(keystone, req, res, userId) {
  return new Promise(function (resolve, reject) {
    keystone.session.signin(userId, req, res, function (user) {
      return resolve(user);
    }, function (err) {
      return reject(err);
    });
  });
};

var _default =
/*#__PURE__*/
function () {
  var _ref = _asyncToGenerator(
  /*#__PURE__*/
  _regenerator.default.mark(function _callee2(req, res, next) {
    var facebookStrategy;
    return _regenerator.default.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            facebookStrategy = new passportFacebook.Strategy(credentials, function (accessToken, refreshToken, profile, done) {
              done(null, {
                accessToken: accessToken,
                refreshToken: refreshToken,
                profile: profile,
                profileFields: ['id', 'displayName', 'photos', 'email']
              });
            });
            passport.use(facebookStrategy);
            passport.authenticate('facebook', {
              session: false
            },
            /*#__PURE__*/
            function () {
              var _ref2 = _asyncToGenerator(
              /*#__PURE__*/
              _regenerator.default.mark(function _callee(err, data, info) {
                var avatar, facebookId, userFacebook, userCurrent;
                return _regenerator.default.wrap(function _callee$(_context) {
                  while (1) {
                    switch (_context.prev = _context.next) {
                      case 0:
                        if (!(err || !data)) {
                          _context.next = 4;
                          break;
                        }

                        console.log('[services.facebook] - Error retrieving Facebook account data - ' + JSON.stringify(err));
                        console.log('DATA - ' + JSON.stringify(data));
                        return _context.abrupt("return", res.redirect('/enter'));

                      case 4:
                        avatar = 'https://graph.facebook.com/' + data.profile.id + '/picture?width=600&height=600';
                        facebookId = data.profile.id;
                        _context.next = 8;
                        return UserFacebook.model.findOne({
                          facebookId: facebookId
                        }).populate("user");

                      case 8:
                        userFacebook = _context.sent;
                        userCurrent = userFacebook.user;

                        if (!(userFacebook && userCurrent)) {
                          _context.next = 13;
                          break;
                        }

                        _context.next = 13;
                        return keystoneLogin(keystone, req, res, String(userCurrent._id));

                      case 13:
                        return _context.abrupt("return", res.redirect("".concat(process.env.FACEBOOK_LOGIN_REDIRECT)));

                      case 14:
                      case "end":
                        return _context.stop();
                    }
                  }
                }, _callee, this);
              }));

              return function (_x4, _x5, _x6) {
                return _ref2.apply(this, arguments);
              };
            }())(req, res, next);

          case 3:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2, this);
  }));

  return function (_x, _x2, _x3) {
    return _ref.apply(this, arguments);
  };
}();

exports.default = _default;
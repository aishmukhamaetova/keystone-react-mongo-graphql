"use strict";

var keystone = require('keystone');

exports = module.exports = function (req, res, next) {
  keystone.session.signout(req, res, function () {
    return res.redirect("/enter");
  });
};
"use strict";

var keystone = require('keystone');

var passport = require('passport');

var passportFacebook = require('passport-facebook');

var User = keystone.list('User');
var credentials = {
  clientID: process.env.FACEBOOK_CLIENT_ID,
  clientSecret: process.env.FACEBOOK_CLIENT_SECRET,
  callbackURL: process.env.FACEBOOK_CALLBACK_URL
};

exports = module.exports = function (req, res, next) {
  var facebookStrategy = new passportFacebook.Strategy(credentials, function (accessToken, refreshToken, profile, done) {
    done(null, {
      accessToken: accessToken,
      refreshToken: refreshToken,
      profile: profile,
      profileFields: ['id', 'displayName', 'photos', 'email']
    });
  });
  passport.use(facebookStrategy);
  passport.authenticate('facebook', {
    scope: ['email']
  })(req, res, next);
};
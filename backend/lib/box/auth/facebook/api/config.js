"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _lodash = _interopRequireDefault(require("lodash.camelcase"));

var _lodash2 = _interopRequireDefault(require("lodash.upperfirst"));

var _config = _interopRequireDefault(require("../../manager/api/config"));

var _typeMap = _interopRequireDefault(require("../../../../evoke-me/type/api/typeMap"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var ENTITY = "facebook";
var name = "".concat((0, _lodash.default)(_config.default.namespace)).concat((0, _lodash2.default)(ENTITY));
var nameType = (0, _lodash2.default)(name);

var _default = _objectSpread({}, _config.default, {
  entity: ENTITY,
  name: name,
  nameType: nameType,
  fields: {
    avatar: {
      type: _typeMap.default.STRING
    },
    facebookId: {
      type: _typeMap.default.STRING
    },
    user: {
      type: _typeMap.default.RELATION,
      to: "user"
    }
  }
});

exports.default = _default;
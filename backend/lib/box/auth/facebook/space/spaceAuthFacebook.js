"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _config = _interopRequireDefault(require("../api/config"));

var _boxAuthFacebookMeQuery = _interopRequireDefault(require("../api/keystone/graphql/client/boxAuthFacebookMeQuery"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _default = [_config.default.namespace, "authFacebook", [], function () {
  return {
    avatar: null,
    isLoading: false
  };
}, {
  avatarSet: function avatarSet(getState, setState, _ref) {
    var validate = _ref.validate;
    return (
      /*#__PURE__*/
      _regenerator.default.mark(function _callee(avatar) {
        return _regenerator.default.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.next = 2;
                return setState({
                  avatar: avatar
                });

              case 2:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      })
    );
  },
  me: function me(getState, setState, _ref2, _ref3) {
    var avatarSet = _ref2.avatarSet;
    var fromAuth = _ref3.fromAuth,
        fromNotify = _ref3.fromNotify;
    return (
      /*#__PURE__*/
      _regenerator.default.mark(function _callee2(payload, _ref4) {
        var client, _ref5, data, boxAuthFacebookMe, result, errors;

        return _regenerator.default.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                client = _ref4.client;
                _context2.prev = 1;

                if (!client) {
                  _context2.next = 19;
                  break;
                }

                _context2.next = 5;
                return setState({
                  isLoading: true
                });

              case 5:
                _context2.next = 7;
                return client.query({
                  query: _boxAuthFacebookMeQuery.default
                });

              case 7:
                _ref5 = _context2.sent;
                data = _ref5.data;
                boxAuthFacebookMe = data.boxAuthFacebookMe;

                if (!boxAuthFacebookMe) {
                  _context2.next = 17;
                  break;
                }

                result = boxAuthFacebookMe.result, errors = boxAuthFacebookMe.errors;

                if (!(result && result.avatar)) {
                  _context2.next = 17;
                  break;
                }

                _context2.next = 15;
                return avatarSet(result.avatar);

              case 15:
                _context2.next = 17;
                return setState({
                  isLoading: false
                });

              case 17:
                _context2.next = 19;
                return setState({
                  isLoading: false
                });

              case 19:
                _context2.next = 26;
                break;

              case 21:
                _context2.prev = 21;
                _context2.t0 = _context2["catch"](1);
                console.log(_context2.t0);
                _context2.next = 26;
                return setState({
                  isLoading: false
                });

              case 26:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, this, [[1, 21]]);
      })
    );
  }
}];
exports.default = _default;
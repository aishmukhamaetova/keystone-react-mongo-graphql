"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _default = function _default(keystone) {
  var Types = keystone.Field.Types;
  var BoxEshopOrderItem = new keystone.List('BoxEshopOrderItem', {
    label: "Order Items",
    singular: "OrderItem",
    plural: "Order Items"
  });
  BoxEshopOrderItem.add({
    price: {
      type: Types.Money
    },
    quantity: {
      type: Types.Number
    },
    product: {
      type: Types.Relationship,
      ref: "Product"
    },
    order: {
      type: Types.Relationship,
      ref: "BoxEshopOrder"
    }
  });
  BoxEshopOrderItem.defaultColumns = 'price, quantity';
  BoxEshopOrderItem.register();
};

exports.default = _default;
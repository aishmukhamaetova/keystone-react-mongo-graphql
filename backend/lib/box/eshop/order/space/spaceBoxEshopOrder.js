"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _spaceBoxEshopCart = _interopRequireDefault(require("../../cart/space/spaceBoxEshopCart"));

var _createErrorResult = _interopRequireDefault(require("../../../error/manager/api/createErrorResult"));

var _errorMap = _interopRequireDefault(require("../../../error/manager/api/errorMap"));

var _spaceDefaults = _interopRequireDefault(require("../../../../evoke-me/space/api/spaceDefaults"));

var _config = _interopRequireDefault(require("../api/config"));

var _boxEshopOrderMyQuery = _interopRequireDefault(require("../api/keystone/graphql/client/boxEshopOrderMyQuery"));

var _boxEshopOrderCreate = _interopRequireDefault(require("../api/keystone/graphql/client/boxEshopOrderCreate"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var _default = [_config.default.namespace, _config.default.name, [_spaceBoxEshopCart.default], function () {
  return _objectSpread({}, _spaceDefaults.default.state, {
    currentOrderId: null,
    isLoading: false
  });
}, _objectSpread({}, _spaceDefaults.default.updaters, {
  currentOrderIdSet: function currentOrderIdSet(getState, setState) {
    return (
      /*#__PURE__*/
      _regenerator.default.mark(function _callee(currentOrderId) {
        return _regenerator.default.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.next = 2;
                return setState({
                  currentOrderId: currentOrderId
                });

              case 2:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      })
    );
  },
  queryMy: function queryMy(getState, setState, _ref) {
    var create = _ref.create,
        orderClear = _ref.orderClear;
    return (
      /*#__PURE__*/
      _regenerator.default.mark(function _callee2(payload, _ref2) {
        var client, _ref3, data, json, _iteratorNormalCompletion, _didIteratorError, _iteratorError, _iterator, _step, doc;

        return _regenerator.default.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                client = _ref2.client;
                _context2.prev = 1;

                if (!client) {
                  _context2.next = 41;
                  break;
                }

                _context2.next = 5;
                return setState({
                  isLoading: true
                });

              case 5:
                _context2.next = 7;
                return client.query({
                  query: _boxEshopOrderMyQuery.default
                });

              case 7:
                _ref3 = _context2.sent;
                data = _ref3.data;
                json = data.boxEshopOrderMy;

                if (!(json && json.result)) {
                  _context2.next = 39;
                  break;
                }

                _context2.next = 13;
                return orderClear('my');

              case 13:
                _iteratorNormalCompletion = true;
                _didIteratorError = false;
                _iteratorError = undefined;
                _context2.prev = 16;
                _iterator = json.result[Symbol.iterator]();

              case 18:
                if (_iteratorNormalCompletion = (_step = _iterator.next()).done) {
                  _context2.next = 25;
                  break;
                }

                doc = _step.value;
                _context2.next = 22;
                return create(doc, {
                  orderName: "my"
                });

              case 22:
                _iteratorNormalCompletion = true;
                _context2.next = 18;
                break;

              case 25:
                _context2.next = 31;
                break;

              case 27:
                _context2.prev = 27;
                _context2.t0 = _context2["catch"](16);
                _didIteratorError = true;
                _iteratorError = _context2.t0;

              case 31:
                _context2.prev = 31;
                _context2.prev = 32;

                if (!_iteratorNormalCompletion && _iterator.return != null) {
                  _iterator.return();
                }

              case 34:
                _context2.prev = 34;

                if (!_didIteratorError) {
                  _context2.next = 37;
                  break;
                }

                throw _iteratorError;

              case 37:
                return _context2.finish(34);

              case 38:
                return _context2.finish(31);

              case 39:
                _context2.next = 41;
                return setState({
                  isLoading: false
                });

              case 41:
                _context2.next = 48;
                break;

              case 43:
                _context2.prev = 43;
                _context2.t1 = _context2["catch"](1);
                _context2.next = 47;
                return setState({
                  isLoading: false
                });

              case 47:
                console.log("ERROR", _config.default.name, _context2.t1);

              case 48:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, this, [[1, 43], [16, 27, 31, 39], [32,, 34, 38]]);
      })
    );
  },
  checkout: function checkout(getState, setState, _ref4, _ref5) {
    var create = _ref4.create,
        currentOrderIdSet = _ref4.currentOrderIdSet;
    var fromBoxEshopCart = _ref5.fromBoxEshopCart;
    return (
      /*#__PURE__*/
      _regenerator.default.mark(function _callee3(payload, _ref6) {
        var client, itemList, amount, _iteratorNormalCompletion2, _didIteratorError2, _iteratorError2, _iterator2, _step2, _ref12, price, quantity, _ref8, data, json, errors, result;

        return _regenerator.default.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                client = _ref6.client;
                _context3.prev = 1;

                if (!client) {
                  _context3.next = 48;
                  break;
                }

                _context3.next = 5;
                return setState({
                  isLoading: true
                });

              case 5:
                _context3.next = 7;
                return fromBoxEshopCart.getItemList();

              case 7:
                itemList = _context3.sent;
                amount = 0;
                _iteratorNormalCompletion2 = true;
                _didIteratorError2 = false;
                _iteratorError2 = undefined;
                _context3.prev = 12;

                for (_iterator2 = itemList[Symbol.iterator](); !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
                  _ref12 = _step2.value;
                  price = _ref12.price, quantity = _ref12.quantity;
                  amount += quantity * price;
                }

                _context3.next = 20;
                break;

              case 16:
                _context3.prev = 16;
                _context3.t0 = _context3["catch"](12);
                _didIteratorError2 = true;
                _iteratorError2 = _context3.t0;

              case 20:
                _context3.prev = 20;
                _context3.prev = 21;

                if (!_iteratorNormalCompletion2 && _iterator2.return != null) {
                  _iterator2.return();
                }

              case 23:
                _context3.prev = 23;

                if (!_didIteratorError2) {
                  _context3.next = 26;
                  break;
                }

                throw _iteratorError2;

              case 26:
                return _context3.finish(23);

              case 27:
                return _context3.finish(20);

              case 28:
                _context3.next = 30;
                return client.mutate({
                  mutation: _boxEshopOrderCreate.default,
                  variables: {
                    amount: amount,
                    items: itemList.map(function (_ref9) {
                      var productId = _ref9.id,
                          price = _ref9.price,
                          quantity = _ref9.quantity;
                      return {
                        productId: productId,
                        price: price,
                        quantity: quantity
                      };
                    })
                  }
                });

              case 30:
                _ref8 = _context3.sent;
                data = _ref8.data;
                json = data.boxEshopOrderCreate;

                if (!json) {
                  _context3.next = 47;
                  break;
                }

                errors = json.errors, result = json.result;

                if (!(errors && errors.length)) {
                  _context3.next = 39;
                  break;
                }

                _context3.next = 38;
                return setState({
                  isLoading: false,
                  errors: errors.map(function (_ref10) {
                    var type = _ref10.type,
                        code = _ref10.code,
                        message = _ref10.message,
                        props = _ref10.props;
                    return {
                      type: type,
                      code: code,
                      message: message,
                      props: props.reduce(function (acc, _ref11) {
                        var name = _ref11.name,
                            value = _ref11.value;
                        return acc[name] = value;
                      }, {})
                    };
                  })
                });

              case 38:
                return _context3.abrupt("return", _context3.sent);

              case 39:
                if (!result) {
                  _context3.next = 47;
                  break;
                }

                _context3.next = 42;
                return create(result);

              case 42:
                _context3.next = 44;
                return currentOrderIdSet(result.id);

              case 44:
                _context3.next = 46;
                return setState({
                  isLoading: false
                });

              case 46:
                return _context3.abrupt("return", result.id);

              case 47:
                throw new Error("checkout json is wrong", json);

              case 48:
                throw new Error("checkout error");

              case 51:
                _context3.prev = 51;
                _context3.t1 = _context3["catch"](1);
                _context3.next = 55;
                return setState({
                  isLoading: false
                });

              case 55:
                console.log("ERROR", _config.default.name, _context3.t1);

              case 56:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3, this, [[1, 51], [12, 16, 20, 28], [21,, 23, 27]]);
      })
    );
  }
})];
exports.default = _default;
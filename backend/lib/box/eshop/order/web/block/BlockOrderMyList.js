"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _reactApollo = require("react-apollo");

var _compose = _interopRequireDefault(require("recompose/compose"));

var _lifecycle = _interopRequireDefault(require("recompose/lifecycle"));

var _withHandlers = _interopRequireDefault(require("recompose/withHandlers"));

var _semanticUiReact = require("semantic-ui-react");

var _spaceBoxEshopOrder = _interopRequireDefault(require("../../space/spaceBoxEshopOrder"));

var _withSpace = _interopRequireDefault(require("../../../../../evoke-me/space/all/withSpace"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

var enhance = (0, _compose.default)(_reactApollo.withApollo, (0, _withSpace.default)(_spaceBoxEshopOrder.default), (0, _withHandlers.default)({
  mount: function mount(_ref) {
    var client = _ref.client,
        fromBoxEshopOrder = _ref.fromBoxEshopOrder;
    return function () {
      fromBoxEshopOrder.queryMy({}, {
        client: client
      });
    };
  }
}), (0, _lifecycle.default)({
  componentDidMount: function componentDidMount() {
    this.props.mount();
  }
}));
var BlockOrderMyList = enhance(function (_ref2) {
  var fromBoxEshopOrder = _ref2.fromBoxEshopOrder;
  var data = fromBoxEshopOrder.data,
      orderMap = fromBoxEshopOrder.orderMap,
      isLoading = fromBoxEshopOrder.isLoading;

  if (!orderMap.my || !orderMap.my.length) {
    return _react.default.createElement(_semanticUiReact.Segment, {
      placeholder: true,
      loading: isLoading
    }, _react.default.createElement(_semanticUiReact.Header, {
      icon: true
    }, _react.default.createElement(_semanticUiReact.Icon, {
      name: "hand peace"
    }), "No orders"));
  }

  return orderMap.my.map(function (id) {
    var type = data[id].type;
    return _react.default.createElement("div", {
      key: id
    }, "ORDER ", id, " - ", type);
  });
});
var _default = BlockOrderMyList;
exports.default = _default;
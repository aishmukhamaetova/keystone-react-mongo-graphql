"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _default = {
  DRAFT: "Draft",
  PENDING_FOR_PAY: "Pending-for-Pay",
  PAID: "Paid",
  NOT_SHIPPED: "Not-Shipped",
  SHIPPED: "Shipped"
};
exports.default = _default;
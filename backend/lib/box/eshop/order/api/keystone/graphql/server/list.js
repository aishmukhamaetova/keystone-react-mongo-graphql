"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _boxEshopOrderCreate = _interopRequireDefault(require("./boxEshopOrderCreate"));

var _boxEshopOrderMyQuery = _interopRequireDefault(require("./boxEshopOrderMyQuery"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _default = [_boxEshopOrderCreate.default, _boxEshopOrderMyQuery.default];
exports.default = _default;
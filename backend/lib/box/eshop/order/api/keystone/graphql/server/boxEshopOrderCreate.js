"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.query = exports.type = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _errorMap = _interopRequireDefault(require("../../../../../../error/manager/api/errorMap"));

var _config = _interopRequireDefault(require("../../../config"));

var _typeMap = _interopRequireDefault(require("../../../typeMap"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var NAME = "Create";
var name = _config.default.name;
var nameType = _config.default.nameType;
var queryName = "".concat(name).concat(NAME);
var typeQuery = "".concat(nameType).concat(NAME);
var typePayload = "".concat(typeQuery, "Payload");
var typeResult = "".concat(typeQuery, "Result");
var type = "\n  type ".concat(typeQuery, " {\n    result: ").concat(typeResult, "\n    errors: [Error]\n  }\n  \n  type ").concat(typeResult, " {\n    id: ID\n    \n    amount: Float\n    description: String\n    email: String    \n  }\n\n  input ").concat(typeResult, "Item {\n    productId: ID\n    quantity: Int\n    price: Float\n  }\n  \n  input ").concat(typePayload, " {\n    amount: Float\n    description: String\n    email: String\n    \n    items: [").concat(typeResult, "Item]\n  }\n");
exports.type = type;
var query = "";
exports.query = query;
var mutation = "\n  ".concat(queryName, "(\n    payload: ").concat(typePayload, "\n  ): ").concat(typeQuery, "\n");

var error = function error(type, code, message) {
  var props = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : [];
  return {
    result: null,
    errors: [{
      type: type,
      code: code,
      message: message,
      props: props
    }]
  };
};

var mutationMap = _defineProperty({}, queryName, function () {
  var _ref2 = _asyncToGenerator(
  /*#__PURE__*/
  _regenerator.default.mark(function _callee(obj, args, _ref, info) {
    var keystone, req, res, _args$payload, payload, amount, description, items, email, user, BoxEshopOrder, BoxEshopOrderItem, boxEshopOrder, _iteratorNormalCompletion, _didIteratorError, _iteratorError, _iterator, _step, _ref4, productId, quantity, price, boxEshopOrderItem, message;

    return _regenerator.default.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            keystone = _ref.keystone, req = _ref.req, res = _ref.res;
            _context.prev = 1;
            _args$payload = args.payload, payload = _args$payload === void 0 ? {} : _args$payload;
            amount = payload.amount, description = payload.description, items = payload.items, email = payload.email;
            user = res.locals.user;
            BoxEshopOrder = keystone.list('BoxEshopOrder');
            BoxEshopOrderItem = keystone.list('BoxEshopOrderItem');
            _context.next = 9;
            return new BoxEshopOrder.model({
              author: user,
              type: _typeMap.default.DRAFT,
              amount: amount,
              description: description,
              email: email
            }).save();

          case 9:
            boxEshopOrder = _context.sent;
            _iteratorNormalCompletion = true;
            _didIteratorError = false;
            _iteratorError = undefined;
            _context.prev = 13;
            _iterator = items[Symbol.iterator]();

          case 15:
            if (_iteratorNormalCompletion = (_step = _iterator.next()).done) {
              _context.next = 24;
              break;
            }

            _ref4 = _step.value;
            productId = _ref4.productId, quantity = _ref4.quantity, price = _ref4.price;
            _context.next = 20;
            return new BoxEshopOrderItem.model({
              price: price,
              quantity: quantity,
              product: productId,
              order: boxEshopOrder
            }).save();

          case 20:
            boxEshopOrderItem = _context.sent;

          case 21:
            _iteratorNormalCompletion = true;
            _context.next = 15;
            break;

          case 24:
            _context.next = 30;
            break;

          case 26:
            _context.prev = 26;
            _context.t0 = _context["catch"](13);
            _didIteratorError = true;
            _iteratorError = _context.t0;

          case 30:
            _context.prev = 30;
            _context.prev = 31;

            if (!_iteratorNormalCompletion && _iterator.return != null) {
              _iterator.return();
            }

          case 33:
            _context.prev = 33;

            if (!_didIteratorError) {
              _context.next = 36;
              break;
            }

            throw _iteratorError;

          case 36:
            return _context.finish(33);

          case 37:
            return _context.finish(30);

          case 38:
            if (!boxEshopOrder) {
              _context.next = 40;
              break;
            }

            return _context.abrupt("return", {
              result: boxEshopOrder,
              errors: []
            });

          case 40:
            throw new Error("Cannot create boxEshopOrder");

          case 43:
            _context.prev = 43;
            _context.t1 = _context["catch"](1);
            console.log("ERROR", queryName, _context.t1);
            message = process.env.NODE_ENV === "development" ? _context.t1 : "".concat(queryName, " unknown error");
            return _context.abrupt("return", error("box.error", _errorMap.default.UNKNOWN, message));

          case 48:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, this, [[1, 43], [13, 26, 30, 38], [31,, 33, 37]]);
  }));

  return function (_x, _x2, _x3, _x4) {
    return _ref2.apply(this, arguments);
  };
}());

var queryMap = {};
var _default = {
  type: type,
  mutation: mutation,
  mutationMap: mutationMap,
  query: query,
  queryMap: queryMap
};
exports.default = _default;
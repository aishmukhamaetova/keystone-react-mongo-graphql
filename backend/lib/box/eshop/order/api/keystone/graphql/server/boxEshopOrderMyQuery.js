"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.query = exports.type = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _errorMap = _interopRequireDefault(require("../../../../../../error/manager/api/errorMap"));

var _config = _interopRequireDefault(require("../../../config"));

var _typeMap = _interopRequireDefault(require("../../../typeMap"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var NAME = "My";
var name = _config.default.name;
var nameType = _config.default.nameType;
var queryName = "".concat(name).concat(NAME);
var typeQuery = "".concat(nameType).concat(NAME);
var typePayload = "".concat(typeQuery, "Payload");
var typeResult = "".concat(typeQuery, "Result");
var type = "\n  type ".concat(typeQuery, " {\n    result: [").concat(typeResult, "]\n    errors: [Error]\n  }\n  \n  type ").concat(typeResult, " {\n    id: ID\n    \n    amount: Float\n    description: String\n    type: String\n    email: String    \n  }\n\n  input ").concat(typeResult, "Item {\n    productId: ID\n    quantity: Int\n    price: Float\n  }\n  \n  input ").concat(typePayload, " {\n    amount: Float\n    description: String\n    email: String\n    \n    items: [").concat(typeResult, "Item]\n  }\n");
exports.type = type;
var query = "\n  ".concat(queryName, "(\n    payload: ").concat(typePayload, "\n  ): ").concat(typeQuery, "\n");
exports.query = query;
var mutation = "";

var error = function error(type, code, message) {
  var props = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : [];
  return {
    result: null,
    errors: [{
      type: type,
      code: code,
      message: message,
      props: props
    }]
  };
};

var mutationMap = {};

var queryMap = _defineProperty({}, queryName, function () {
  var _ref2 = _asyncToGenerator(
  /*#__PURE__*/
  _regenerator.default.mark(function _callee(obj, args, _ref, info) {
    var keystone, req, res, _args$payload, payload, user, BoxEshopOrder, boxEshopOrderList, message;

    return _regenerator.default.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            keystone = _ref.keystone, req = _ref.req, res = _ref.res;
            _context.prev = 1;
            _args$payload = args.payload, payload = _args$payload === void 0 ? {} : _args$payload;
            user = res.locals.user;

            if (user) {
              _context.next = 6;
              break;
            }

            throw new Error("Need authorization");

          case 6:
            BoxEshopOrder = keystone.list('BoxEshopOrder');
            _context.next = 9;
            return BoxEshopOrder.model.find({
              author: user
            });

          case 9:
            boxEshopOrderList = _context.sent;

            if (!boxEshopOrderList) {
              _context.next = 12;
              break;
            }

            return _context.abrupt("return", {
              result: boxEshopOrderList,
              errors: []
            });

          case 12:
            throw new Error("Cannot create boxEshopOrder");

          case 15:
            _context.prev = 15;
            _context.t0 = _context["catch"](1);
            console.log("ERROR", queryName, _context.t0);
            message = process.env.NODE_ENV === "development" ? _context.t0 : "".concat(queryName, " unknown error");
            return _context.abrupt("return", error("box.error", _errorMap.default.UNKNOWN, message));

          case 20:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, this, [[1, 15]]);
  }));

  return function (_x, _x2, _x3, _x4) {
    return _ref2.apply(this, arguments);
  };
}());

var _default = {
  type: type,
  mutation: mutation,
  mutationMap: mutationMap,
  query: query,
  queryMap: queryMap
};
exports.default = _default;
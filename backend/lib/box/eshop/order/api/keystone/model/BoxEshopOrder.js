"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _typeMap = _interopRequireDefault(require("../../typeMap"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var options = Object.values(_typeMap.default).join(', ');

var _default = function _default(keystone) {
  var Types = keystone.Field.Types;
  var BoxEshopOrder = new keystone.List('BoxEshopOrder', {
    label: "Orders",
    singular: "Order",
    plural: "Orders"
  });
  BoxEshopOrder.add({
    amount: {
      type: Types.Money
    },
    description: {
      type: String
    },
    email: {
      type: String
    },
    type: {
      type: Types.Select,
      options: options
    },
    author: {
      type: Types.Relationship,
      ref: 'User'
    }
  });
  BoxEshopOrder.relationship({
    ref: 'BoxEshopOrderItem',
    path: 'items',
    refPath: 'order'
  });
  BoxEshopOrder.defaultColumns = 'author';
  BoxEshopOrder.register();
};

exports.default = _default;
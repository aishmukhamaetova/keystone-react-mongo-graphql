"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _spacePersist = _interopRequireDefault(require("../../../persist/manager/space/spacePersist"));

var _spaceDefaults = _interopRequireDefault(require("../../../../evoke-me/space/api/spaceDefaults"));

var _config = _interopRequireDefault(require("../api/config"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var _default = [_config.default.namespace, _config.default.name, [_spacePersist.default], function () {
  return _objectSpread({}, _spaceDefaults.default.state, {
    persist: true,
    persistName: _config.default.name
  });
}, _objectSpread({}, _spaceDefaults.default.updaters, {
  getItemList: function getItemList(getState) {
    return (
      /*#__PURE__*/
      _regenerator.default.mark(function _callee() {
        var _ref, orderMap, data;

        return _regenerator.default.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.next = 2;
                return getState();

              case 2:
                _ref = _context.sent;
                orderMap = _ref.orderMap;
                data = _ref.data;
                return _context.abrupt("return", orderMap.default.map(function (id) {
                  return data[id];
                }));

              case 6:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      })
    );
  }
})];
exports.default = _default;
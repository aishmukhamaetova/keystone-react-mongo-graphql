"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _compose = _interopRequireDefault(require("recompose/compose"));

var _semanticUiReact = require("semantic-ui-react");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

var enhance = (0, _compose.default)();
var CartProductQuantity = enhance(function (_ref) {
  var quantity = _ref.quantity,
      onMinus = _ref.onMinus,
      onPlus = _ref.onPlus;
  return _react.default.createElement(_semanticUiReact.Button.Group, null, _react.default.createElement(_semanticUiReact.Button, {
    icon: "minus",
    onClick: onMinus
  }), _react.default.createElement(_semanticUiReact.Button.Or, {
    text: quantity
  }), _react.default.createElement(_semanticUiReact.Button, {
    icon: "plus",
    positive: true,
    onClick: onPlus
  }));
});
var _default = CartProductQuantity;
exports.default = _default;
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _reactGridSystem = require("react-grid-system");

var _Block = _interopRequireDefault(require("../../../../../evoke-me/layout/web/Block"));

var _BlockCart = _interopRequireDefault(require("../block/BlockCart"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

var SectionCart = function SectionCart() {
  return _react.default.createElement(_reactGridSystem.Container, {
    fluid: true,
    style: {
      padding: 10
    }
  }, _react.default.createElement(_reactGridSystem.Row, {
    justify: "center"
  }, _react.default.createElement(_reactGridSystem.Col, {
    sm: 10,
    lg: 6
  }, _react.default.createElement(_Block.default, {
    center: true,
    middle: true,
    style: {
      padding: 5
    }
  }, _react.default.createElement(_BlockCart.default, null)))));
};

var _default = SectionCart;
exports.default = _default;
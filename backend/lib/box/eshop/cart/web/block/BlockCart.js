"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _react = _interopRequireWildcard(require("react"));

var _reactApollo = require("react-apollo");

var _compose = _interopRequireDefault(require("recompose/compose"));

var _lifecycle = _interopRequireDefault(require("recompose/lifecycle"));

var _withHandlers = _interopRequireDefault(require("recompose/withHandlers"));

var _withPropsOnChange = _interopRequireDefault(require("recompose/withPropsOnChange"));

var _semanticUiReact = require("semantic-ui-react");

var _withSpace = _interopRequireDefault(require("../../../../../evoke-me/space/all/withSpace"));

var _spaceBoxEshopCart = _interopRequireDefault(require("../../space/spaceBoxEshopCart"));

var _spaceBoxEshopOrder = _interopRequireDefault(require("../../../order/space/spaceBoxEshopOrder"));

var _CartProduct = _interopRequireDefault(require("../CartProduct"));

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var enhance = (0, _compose.default)(_reactApollo.withApollo, (0, _withSpace.default)(_spaceBoxEshopCart.default), (0, _withSpace.default)(_spaceBoxEshopOrder.default), (0, _withPropsOnChange.default)(function (_ref, nextProps) {
  var fromBoxEshopCart = _ref.fromBoxEshopCart;
  return fromBoxEshopCart.data !== nextProps.fromBoxEshopCart.data;
}, function (_ref2) {
  var _ref2$fromBoxEshopCar = _ref2.fromBoxEshopCart,
      data = _ref2$fromBoxEshopCar.data,
      orderMap = _ref2$fromBoxEshopCar.orderMap;
  return {
    amount: orderMap.default.reduce(function (result, id) {
      return result + data[id].quantity * data[id].price;
    }, 0)
  };
}), (0, _withHandlers.default)({
  onCheckout: function onCheckout(_ref3) {
    var amount = _ref3.amount,
        client = _ref3.client,
        fromBoxEshopCart = _ref3.fromBoxEshopCart,
        fromBoxEshopOrder = _ref3.fromBoxEshopOrder;
    return (
      /*#__PURE__*/
      _asyncToGenerator(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee() {
        var data, orderMap;
        return _regenerator.default.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                data = fromBoxEshopCart.data, orderMap = fromBoxEshopCart.orderMap;
                fromBoxEshopOrder.checkout({}, {
                  client: client
                });

              case 2:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      }))
    );
  }
}), (0, _lifecycle.default)({
  componentDidMount: function componentDidMount() {},
  componentDidUpdate: function componentDidUpdate() {
    var _this$props = this.props,
        _amount = _this$props.amount,
        fromBoxEshopOrder = _this$props.fromBoxEshopOrder,
        fromBoxEshopCart = _this$props.fromBoxEshopCart;

    if (fromBoxEshopOrder.currentOrderId && !this.checkout) {
      this.checkout = true;
      Checkout.configure({
        merchant: 'ADM',
        order: {
          amount: function amount() {
            return _amount;
          },
          currency: 'USD',
          description: "Ordered goods",
          id: fromBoxEshopOrder.currentOrderId
        },
        interaction: {
          merchant: {
            name: 'AMD',
            address: {
              line1: '200 Sample St',
              line2: '1234 Example Town'
            }
          }
        }
      });
      Checkout.showPaymentPage();
      fromBoxEshopOrder.currentOrderIdSet(null);
      fromBoxEshopCart.removeAll();
    }
  }
}));
var BlockCart = enhance(function (_ref5) {
  var amount = _ref5.amount,
      fromBoxEshopCart = _ref5.fromBoxEshopCart,
      fromBoxEshopOrder = _ref5.fromBoxEshopOrder,
      onCheckout = _ref5.onCheckout;
  var data = fromBoxEshopCart.data,
      orderMap = fromBoxEshopCart.orderMap;
  return _react.default.createElement(_semanticUiReact.Segment, null, "CURRENT ORDER ID: ", fromBoxEshopOrder.currentOrderId, _react.default.createElement(_semanticUiReact.Item.Group, null, orderMap.default.map(function (id) {
    var quantity = data[id].quantity;
    return _react.default.createElement(_CartProduct.default, _extends({
      key: id
    }, data[id], {
      onMinus: function onMinus() {
        if (quantity === 1) {
          fromBoxEshopCart.remove({
            id: id
          });
        } else {
          fromBoxEshopCart.update({
            id: id,
            quantity: quantity > 0 ? quantity - 1 : quantity
          });
        }
      },
      onPlus: function onPlus() {
        return fromBoxEshopCart.update({
          id: id,
          quantity: quantity + 1
        });
      }
    }));
  })), _react.default.createElement(_semanticUiReact.Input, {
    action: {
      color: 'teal',
      labelPosition: 'left',
      icon: 'cart',
      content: 'Checkout',
      onClick: onCheckout
    },
    placeholder: "TOTAL",
    value: "$".concat(amount)
  }));
});
var _default = BlockCart;
exports.default = _default;
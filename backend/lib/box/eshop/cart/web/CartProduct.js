"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _compose = _interopRequireDefault(require("recompose/compose"));

var _semanticUiReact = require("semantic-ui-react");

var _CartProductQuantity = _interopRequireDefault(require("./CartProductQuantity"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

var enhance = (0, _compose.default)();
var CartProduct = enhance(function (_ref) {
  var name = _ref.name,
      price = _ref.price,
      sku = _ref.sku,
      quantity = _ref.quantity,
      onMinus = _ref.onMinus,
      onPlus = _ref.onPlus;
  return _react.default.createElement(_semanticUiReact.Item, null, _react.default.createElement(_semanticUiReact.Item.Image, {
    size: "tiny",
    src: "https://react.semantic-ui.com/images/wireframe/image.png"
  }), _react.default.createElement(_semanticUiReact.Item.Content, null, _react.default.createElement(_semanticUiReact.Item.Header, null, name, " - ", sku), _react.default.createElement(_semanticUiReact.Item.Meta, null, _react.default.createElement("span", null, "$", price)), _react.default.createElement(_semanticUiReact.Item.Description, null, _react.default.createElement(_CartProductQuantity.default, {
    quantity: quantity,
    onMinus: onMinus,
    onPlus: onPlus
  }))));
});
var _default = CartProduct;
exports.default = _default;
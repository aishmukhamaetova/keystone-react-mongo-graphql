"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _store = _interopRequireDefault(require("store"));

var _config = _interopRequireDefault(require("../../config/api/config"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _default = [_config.default.namespace, 'persist', [], function () {
  return {};
}, {
  get: function get() {
    return (
      /*#__PURE__*/
      _regenerator.default.mark(function _callee(persistName) {
        return _regenerator.default.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.prev = 0;
                return _context.abrupt("return", JSON.parse(_store.default.get(persistName)));

              case 4:
                _context.prev = 4;
                _context.t0 = _context["catch"](0);
                return _context.abrupt("return", null);

              case 7:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this, [[0, 4]]);
      })
    );
  },
  remove: function remove() {
    return (
      /*#__PURE__*/
      _regenerator.default.mark(function _callee2(name) {
        return _regenerator.default.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _store.default.remove(name);

              case 1:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, this);
      })
    );
  },
  set: function set() {
    return (
      /*#__PURE__*/
      _regenerator.default.mark(function _callee3(persistName, state) {
        return _regenerator.default.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                _store.default.set(persistName, JSON.stringify(state));

              case 1:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3, this);
      })
    );
  }
}];
exports.default = _default;
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _config = _interopRequireDefault(require("../api/config"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ENTITY = "profile";
var _default = [_config.default.namespace, ENTITY, [], function () {
  return {};
}, {}];
exports.default = _default;
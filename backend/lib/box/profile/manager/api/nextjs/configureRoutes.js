"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _default = function _default(routes) {
  return routes.add({
    name: "boxProfile",
    pattern: "/profile",
    page: "boxAuthClassicConfirmAuto"
  });
};

exports.default = _default;
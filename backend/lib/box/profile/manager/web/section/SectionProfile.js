"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _Grid = _interopRequireDefault(require("@material-ui/core/Grid"));

var _react = _interopRequireWildcard(require("react"));

var _compose = _interopRequireDefault(require("recompose/compose"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _BlockAuthFacebookLink = _interopRequireDefault(require("../../../../auth/facebook/web/block/BlockAuthFacebookLink"));

var _BlockOrderMyList = _interopRequireDefault(require("../../../../eshop/order/web/block/BlockOrderMyList"));

var _withSpace = _interopRequireDefault(require("../../../../../evoke-me/space/all/withSpace"));

var _spaceProfile = _interopRequireDefault(require("../../space/spaceProfile"));

var _theme = _interopRequireDefault(require("../theme"));

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Section = _styledComponents.default.div.withConfig({
  displayName: "SectionProfile__Section",
  componentId: "sc-1vnb2xx-0"
})([""]);

var enhance = (0, _compose.default)((0, _withSpace.default)(_spaceProfile.default));
var SectionProfile = enhance(function (_ref) {
  var fromProfile = _ref.fromProfile;
  return _react.default.createElement(Section, null, _react.default.createElement(_Grid.default, {
    container: true,
    justify: "center",
    spacing: 8
  }, _react.default.createElement(_Grid.default, {
    item: true,
    xs: 4
  }, _react.default.createElement(_BlockOrderMyList.default, null)), _react.default.createElement(_Grid.default, {
    item: true,
    xs: 4
  }, _react.default.createElement(_BlockAuthFacebookLink.default, null))));
});
var _default = SectionProfile;
exports.default = _default;
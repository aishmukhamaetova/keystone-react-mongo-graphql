"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _default = function _default() {
  var type = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 'box.error';
  var code = arguments.length > 1 ? arguments[1] : undefined;
  var message = arguments.length > 2 ? arguments[2] : undefined;
  var props = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : [];
  return {
    result: null,
    errors: [{
      type: type,
      code: code,
      message: message,
      props: props
    }]
  };
};

exports.default = _default;
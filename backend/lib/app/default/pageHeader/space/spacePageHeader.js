"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _config = _interopRequireDefault(require("../../config/api/config"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _default = [_config.default.namespace, "pageHeader", [], function () {
  return {
    logo: {
      imageLink: "https://i.imgur.com/fGM1JLc.jpg"
    },
    height: 64
  };
}, {}];
exports.default = _default;
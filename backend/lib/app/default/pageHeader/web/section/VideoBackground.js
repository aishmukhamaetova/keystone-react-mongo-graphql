"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _reactContainerDimensions = _interopRequireDefault(require("react-container-dimensions"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

var Root = _styledComponents.default.div.withConfig({
  displayName: "VideoBackground__Root",
  componentId: "sc-11d4l85-0"
})(["width:100vw;height:630px;overflow:hidden;position:absolute;z-index:-99;"]);

var Video = _styledComponents.default.video.withConfig({
  displayName: "VideoBackground__Video",
  componentId: "sc-11d4l85-1"
})(["video{@media (min-aspect-ratio:16/9){width:100%;height:auto;}@media (max-aspect-ratio:16/9){width:auto;height:100%;left:50%;transform:translateX(-50%);}}"]);

var VideoBackground = function VideoBackground() {
  return _react.default.createElement(Root, null, _react.default.createElement(_reactContainerDimensions.default, null, function (_ref) {
    var widthSource = _ref.width;
    var width = widthSource < 1480 ? 1480 : widthSource;
    var height = 630 * (width / 1480);
    var marginLeft = widthSource < 740 ? -1480 / 2 + widthSource / 2 : 0;
    return _react.default.createElement(Video, {
      autoPlay: true,
      controls: false,
      muted: true,
      loop: true,
      width: width,
      height: height,
      style: {
        marginLeft: marginLeft
      }
    }, _react.default.createElement("source", {
      src: "https://cdn.evokeme.io/static/latelierdumiel/launch.mp4",
      type: "video/mp4"
    }), _react.default.createElement("source", {
      src: "https://cdn.evokeme.io/static/latelierdumiel/launch.ogv",
      type: "video/ogg"
    })) // <iframe
    //   allowFullScreen
    //   frameBorder={0}
    //   width={width}
    //   height={height}
    //   src='https://www.youtube.com/embed/2fkCsrI-Rz4?autoplay=1&showinfo=0&controls=0&&mute=1&loop=1&playlist=2fkCsrI-Rz4'
    //   style={{ marginLeft }}
    // />
    ;
  }));
};

var _default = VideoBackground; // <source src='https://my.mixtape.moe/wzpbpv.mp4' type='video/mp4' />
// https://streamable.com/s/vte31/hhrvdz

exports.default = _default;
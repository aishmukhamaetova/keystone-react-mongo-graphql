"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _semanticUiReact = require("semantic-ui-react");

var _VideoBackground = _interopRequireDefault(require("./VideoBackground"));

var _core = require("@material-ui/core");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _polished = require("polished");

var _compose = _interopRequireDefault(require("recompose/compose"));

var _defaultProps = _interopRequireDefault(require("recompose/defaultProps"));

var _withStateHandlers = _interopRequireDefault(require("recompose/withStateHandlers"));

var _routes = require("../../../../honey/page/api/routes");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

var map = {
  sm: 600,
  md: 960,
  lg: 1280,
  xl: 1920
};

var mediaGrid = function mediaGrid(type) {
  return function () {
    return "@media (max-width: ".concat(map[type] - 1, "px)");
  };
};

var TagLine = (0, _styledComponents.default)(function (_ref) {
  var color = _ref.color,
      transform = _ref.transform,
      border = _ref.border,
      margin = _ref.margin,
      fweight = _ref.fweight,
      other = _objectWithoutProperties(_ref, ["color", "transform", "border", "margin", "fweight"]);

  return _react.default.createElement(_core.Typography, other);
}).withConfig({
  displayName: "SectionPageHeader__TagLine",
  componentId: "es76-0"
})(["&&{font-size:", ";font-weight:", ";color:", ";margin:", ";text-align:center;font-style:normal;line-height:normal;}"], function (props) {
  return props.size || '1rem';
}, function (props) {
  return props.fweight || 300;
}, function (props) {
  return props.color;
}, function (props) {
  return props.margin;
});
var ButtonHeader = (0, _styledComponents.default)(_core.Button).withConfig({
  displayName: "SectionPageHeader__ButtonHeader",
  componentId: "es76-1"
})(["&&{color:#fff;background-color:rgba(255,255,255,0.05);box-shadow:1px 1px 0px  white inset,-1px -1px 0px  white inset;font-weight:600;font-size:1.25em;text-transform:uppercase;min-width:10.5rem;margin:1.312rem;height:", ";border-radius:0;a{color:#fff;&:hover{color:#fff;}}&:hover{color:#fff;}}"], (0, _polished.rem)(50));
var Block = (0, _styledComponents.default)(function (_ref2) {
  var height = _ref2.height,
      other = _objectWithoutProperties(_ref2, ["height"]);

  return _react.default.createElement(_core.Grid, other);
}).withConfig({
  displayName: "SectionPageHeader__Block",
  componentId: "es76-2"
})(["&&{height:", ";position:relative;width:100%;padding:", " ", ";background-color:rgba(0,0,0,0.1);}"], function (props) {
  return props.height || '630px';
}, (0, _polished.rem)(20), (0, _polished.rem)(40));

var IconLogoFirst = function IconLogoFirst(props) {
  return _react.default.createElement(_core.SvgIcon, _extends({
    width: "221px",
    height: "36px",
    viewBox: "0 0 221 36",
    version: "1.1",
    xmlns: "http://www.w3.org/2000/svg"
  }, props), _react.default.createElement("title", null, "symbol"), _react.default.createElement("desc", null, "Created with Sketch."), _react.default.createElement("defs", null, _react.default.createElement("polygon", {
    id: "path-1",
    points: "0 36 220.958884 36 220.958884 0.249636364 0 0.249636364"
  })), _react.default.createElement("g", {
    id: "Page-1",
    stroke: "none",
    strokeWidth: "1",
    fill: "none",
    fillRule: "evenodd"
  }, _react.default.createElement("g", {
    id: "Desktop-HD",
    transform: "translate(-343.000000, -108.000000)"
  }, _react.default.createElement("g", {
    id: "symbol",
    transform: "translate(343.000000, 108.000000)"
  }, _react.default.createElement("g", {
    id: "Group-24"
  }, _react.default.createElement("polygon", {
    id: "Fill-1",
    fill: "#FEFEFE",
    points: "24 25 24 11 25.4052916 11 25.4052916 23.8594569 33 23.8594569 33 25"
  }), _react.default.createElement("polygon", {
    id: "Fill-2",
    fill: "#FEFEFE",
    points: "33.1703638 11 34.7657498 11 35 13.3697449 33.5323869 16 33 15.4610133 33.9370009 13.283507 33.1703638 13.283507"
  }), _react.default.createElement("path", {
    d: "M46.0925676,21.3599333 L38.9074324,21.3599333 L37.4756757,25 L36,25 L41.7744595,11 L43.2272973,11 L49,25 L47.5243243,25 L46.0925676,21.3599333 Z M39.3501351,20.2393997 L45.6498649,20.2393997 L42.4894595,12.2606003 L39.3501351,20.2393997 Z",
    id: "Fill-3",
    fill: "#FEFEFE"
  }), _react.default.createElement("polygon", {
    id: "Fill-4",
    fill: "#FEFEFE",
    points: "54.2056772 12.1405431 54.2056772 25 52.8139497 25 52.8139497 12.1405431 48 12.1405431 48 11 59 11 59 12.1405431"
  }), _react.default.createElement("polygon", {
    id: "Fill-5",
    fill: "#FEFEFE",
    points: "62 25 62 11 70.8752166 11 70.8752166 12.1405431 63.3500867 12.1405431 63.3500867 17.4397332 69.5857886 17.4397332 69.5857886 18.5602668 63.3500867 18.5602668 63.3500867 23.8594569 71 23.8594569 71 25"
  }), _react.default.createElement("polygon", {
    id: "Fill-6",
    fill: "#FEFEFE",
    points: "74 25 74 11 75.4052916 11 75.4052916 23.8594569 83 23.8594569 83 25"
  }), _react.default.createElement("polygon", {
    id: "Fill-7",
    fill: "#FEFEFE",
    points: "85.221 25 86.779 25 86.779 11 85.221 11"
  }), _react.default.createElement("polygon", {
    id: "Fill-8",
    fill: "#FEFEFE",
    points: "90 25 90 11 98.8752166 11 98.8752166 12.1405431 91.3500867 12.1405431 91.3500867 17.4397332 97.5857886 17.4397332 97.5857886 18.5602668 91.3500867 18.5602668 91.3500867 23.8594569 99 23.8594569 99 25"
  }), _react.default.createElement("g", {
    id: "Group-23"
  }, _react.default.createElement("path", {
    d: "M108.0027,19.1149495 L104.118925,19.1149495 L104.118925,25.2485657 L102.784359,25.2485657 L102.784359,11.172101 L108.126049,11.172101 C111.043591,11.172101 113.364948,12.1580404 113.364948,15.1326263 C113.364948,17.7081414 111.619219,18.8131313 109.337266,19.0545859 L113.385506,25.2485657 L111.864204,25.2485657 L108.0027,19.1149495 Z M108.208281,12.2988889 L104.118925,12.2988889 L104.118925,17.9898384 L108.105491,17.9898384 C110.365173,17.9898384 111.989266,17.2051111 111.989266,15.1326263 C111.989266,13.0618182 110.365173,12.2988889 108.208281,12.2988889 Z",
    id: "Fill-9",
    fill: "#FEFEFE"
  }), _react.default.createElement("path", {
    d: "M126.609358,25.248901 L122.398366,25.248901 L122.398366,11.1707596 L126.609358,11.1707596 C130.678157,11.1707596 133.246211,13.1828808 133.246211,18.2098303 C133.246211,23.2384566 130.678157,25.248901 126.609358,25.248901 M126.568242,12.3176687 L123.731219,12.3176687 L123.731219,24.1019919 L126.568242,24.1019919 C130.224164,24.1019919 131.827699,22.232396 131.827699,18.2098303 C131.827699,14.1889414 130.224164,12.3176687 126.568242,12.3176687",
    id: "Fill-11",
    fill: "#FEFEFE"
  }), _react.default.createElement("path", {
    d: "M146.958319,20.2816444 C146.958319,23.1975434 145.704272,25.4695636 141.781094,25.4695636 C137.815086,25.4695636 136.56104,23.1975434 136.56104,20.2816444 L136.56104,11.1717657 L137.895605,11.1717657 L137.895605,20.2816444 C137.895605,22.515099 138.596295,24.3042101 141.781094,24.3042101 C144.924776,24.3042101 145.62204,22.515099 145.62204,20.2816444 L145.62204,11.1717657 L146.958319,11.1717657 L146.958319,20.2816444 Z",
    id: "Fill-13",
    fill: "#FEFEFE"
  }), _react.default.createElement("polygon", {
    id: "Fill-15",
    fill: "#FEFEFE",
    points: "168.378016 13.3837576 163.673628 22.8960606 162.727953 22.8960606 158.021853 13.4038788 158.021853 25.2485657 156.689 25.2485657 156.689 11.172101 158.105798 11.172101 163.202504 21.4272121 168.275225 11.172101 169.712581 11.172101 169.712581 25.2485657 168.378016 25.2485657"
  }), _react.default.createElement("mask", {
    id: "mask-2",
    fill: "white"
  }, _react.default.createElement("use", {
    xlinkHref: "#path-1"
  })), _react.default.createElement("g", {
    id: "Clip-18"
  }), _react.default.createElement("polygon", {
    id: "Fill-17",
    fill: "#FEFEFE",
    mask: "url(#mask-2)",
    points: "173.873891 25.2485657 175.206744 25.2485657 175.206744 11.1704242 173.873891 11.1704242"
  }), _react.default.createElement("polygon", {
    id: "Fill-19",
    fill: "#FEFEFE",
    mask: "url(#mask-2)",
    points: "179.383302 25.248901 179.383302 11.1707596 188.156488 11.1707596 188.156488 12.3176687 180.717867 12.3176687 180.717867 17.6464364 186.883596 17.6464364 186.883596 18.7732242 180.717867 18.7732242 180.717867 24.1019919 188.279836 24.1019919 188.279836 25.248901"
  }), _react.default.createElement("polygon", {
    id: "Fill-20",
    fill: "#FEFEFE",
    mask: "url(#mask-2)",
    points: "191.489305 25.248901 191.489305 11.1707596 192.827297 11.1707596 192.827297 24.1019919 200.038064 24.1019919 200.038064 25.248901"
  }), _react.default.createElement("polygon", {
    id: "Stroke-21",
    fill: "#FEFEFE",
    fillRule: "nonzero",
    mask: "url(#mask-2)",
    points: "-0.000342635659 35.8304727 -0.000342635659 35.3304727 220.891727 35.3304727 220.891727 35.8304727"
  }), _react.default.createElement("polygon", {
    id: "Stroke-22",
    fill: "#FEFEFE",
    fillRule: "nonzero",
    mask: "url(#mask-2)",
    points: "0.0616744186 0.918828283 0.0616744186 0.418828283 220.958884 0.418828283 220.958884 0.918828283"
  })))))));
};

var SectionVideo = _styledComponents.default.div.withConfig({
  displayName: "SectionPageHeader__SectionVideo",
  componentId: "es76-3"
})(["&&{width:100vw;height:630px;position:relative;.TagLine{", "{font-size:2.1rem;}", "{font-size:2.1rem;}", "{font-size:1.5rem;}", "{font-size:1.3rem;}}}"], mediaGrid("xl"), mediaGrid("lg"), mediaGrid("md"), mediaGrid("sm"));

var enhance = (0, _compose.default)((0, _defaultProps.default)({
  A: {}
}), (0, _withStateHandlers.default)(function () {
  return {
    menu: [{
      title: 'subscriptions',
      name: 'subscriptions'
    }, {
      title: 'shop',
      name: 'products'
    }, {
      title: 'our story',
      name: 'about'
    }]
  };
}, {}));
var SectionPageHeader = enhance(function (_ref3) {
  var _React$createElement;

  var _ref3$theme = _ref3.theme,
      theme = _ref3$theme === void 0 ? {} : _ref3$theme,
      menu = _ref3.menu;
  var BlockPageHeaderLogo = theme.BlockPageHeaderLogo,
      BlockPageHeaderMenu = theme.BlockPageHeaderMenu;

  if (!BlockPageHeaderLogo || !BlockPageHeaderMenu) {
    return _react.default.createElement(_semanticUiReact.Segment, {
      placeholder: true
    }, _react.default.createElement(_semanticUiReact.Header, {
      icon: true
    }, _react.default.createElement(_semanticUiReact.Icon, {
      name: "pdf file outline"
    }), "SectionPageHeader cannot find BlockPageHeaderLogo or BlockPageHeaderMenu"));
  }

  return _react.default.createElement(_react.default.Fragment, null, _react.default.createElement(SectionVideo, null, _react.default.createElement(_VideoBackground.default, null), _react.default.createElement(Block, {
    container: true,
    height: '630px',
    direction: "row",
    justify: "space-between",
    alignItems: "center",
    spacing: 0
  }, _react.default.createElement(_core.Grid, {
    item: true,
    xs: 12
  }, _react.default.createElement(_core.Grid, {
    container: true,
    alignItems: "center",
    direction: "column",
    justify: "center",
    space: 0
  }, _react.default.createElement(_core.Grid, {
    item: true
  }, _react.default.createElement("a", {
    href: "/"
  }, _react.default.createElement(IconLogoFirst, {
    style: {
      fontSize: 221
    }
  }))))), _react.default.createElement(_core.Grid, {
    item: true,
    xs: 12
  }, _react.default.createElement(_core.Grid, {
    container: true,
    alignItems: "center",
    direction: "column",
    justify: "space-between",
    space: 0
  }, _react.default.createElement(_core.Grid, {
    item: true,
    xs: 10,
    sm: 6
  }, _react.default.createElement(TagLine, {
    color: "#fff",
    size: "2.1em",
    fweight: "300",
    className: 'TagLine'
  }, "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas fringilla massa vitae nisl scelerisque lobortis.")))), _react.default.createElement(_core.Grid, {
    item: true,
    xs: 12
  }, _react.default.createElement(_core.Grid, {
    container: true,
    alignItems: "center",
    direction: "column",
    justify: "space-between",
    space: 0
  }, _react.default.createElement(_core.Grid, {
    item: true,
    xs: 10,
    sm: 8
  }, _react.default.createElement(_core.Grid, (_React$createElement = {
    container: true,
    justify: "center"
  }, _defineProperty(_React$createElement, "justify", "space-between"), _defineProperty(_React$createElement, "spacing", 0), _React$createElement), menu.map(function (value, index) {
    return _react.default.createElement(_core.Grid, {
      key: index,
      item: true
    }, _react.default.createElement(ButtonHeader, {
      variant: 'contained'
    }, _react.default.createElement(_routes.Link, {
      route: value.name
    }, _react.default.createElement("a", null, value.title))));
  }))))))));
});
var _default = SectionPageHeader;
exports.default = _default;
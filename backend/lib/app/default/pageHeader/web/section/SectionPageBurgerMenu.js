"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _core = require("@material-ui/core");

var _icons = require("@material-ui/icons");

var _compose = _interopRequireDefault(require("recompose/compose"));

var _recompose = require("recompose");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _defaultProps = _interopRequireDefault(require("recompose/defaultProps"));

var _reactPose = _interopRequireDefault(require("react-pose"));

var _polished = require("polished");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

var SubMenu = (0, _styledComponents.default)(_reactPose.default.div({
  show: {
    opacity: 1,
    height: 'auto'
  },
  hide: {
    opacity: 0,
    height: 0
  }
})).withConfig({
  displayName: "SectionPageBurgerMenu__SubMenu",
  componentId: "sc-1fitoz7-0"
})([""]);
var ContainerIcon = (0, _styledComponents.default)(_reactPose.default.div({
  show: {
    opacity: 1,
    delayChildren: 100,
    staggerChildren: 50
  },
  hide: {
    opacity: 0,
    delayChildren: 100,
    staggerChildren: 50
  }
})).withConfig({
  displayName: "SectionPageBurgerMenu__ContainerIcon",
  componentId: "sc-1fitoz7-1"
})(["&&{position:absolute;right:", ";top:auto;display:", ";}"], (0, _polished.rem)(20), function (props) {
  return props.display || 'flex';
});
var Root = (0, _styledComponents.default)(_reactPose.default.div({
  show: {
    x: 0
  },
  hide: {
    x: '+100%'
  }
})).withConfig({
  displayName: "SectionPageBurgerMenu__Root",
  componentId: "sc-1fitoz7-2"
})(["position:relative;z-index:10000;top:0;left:0;height:100%;width:100%;background:#ffffff;z-index:1000;box-shadow:0px 2px 4px -1px rgba(0,0,0,0.2),0px 4px 5px 0px rgba(0,0,0,0.14),0px 1px 10px 0px rgba(0,0,0,0.12);"]);
var TitleButton = (0, _styledComponents.default)(_core.Button).withConfig({
  displayName: "SectionPageBurgerMenu__TitleButton",
  componentId: "sc-1fitoz7-3"
})(["&&{font-size:", ";position:relative;}"], (0, _polished.rem)(18));

var Container = _styledComponents.default.div.withConfig({
  displayName: "SectionPageBurgerMenu__Container",
  componentId: "sc-1fitoz7-4"
})(["&&{position:relative;}"]);

var enhance = (0, _compose.default)((0, _defaultProps.default)({}), (0, _recompose.withStateHandlers)(function () {
  return {
    showSubMenu: {},
    menuIdx: 0
  };
}, {
  toggleSubMenu: function toggleSubMenu(_ref) {
    var showSubMenu = _ref.showSubMenu;
    return function (menuIdx) {
      showSubMenu[menuIdx] = !showSubMenu[menuIdx];
      return {
        showSubMenu: showSubMenu,
        menuIdx: menuIdx
      };
    };
  }
}));

var MenuHeader = function MenuHeader(_ref2) {
  var title = _ref2.title,
      showSubMenu = _ref2.showSubMenu,
      toggleSubMenu = _ref2.toggleSubMenu,
      idx = _ref2.idx,
      menuIdx = _ref2.menuIdx;
  return _react.default.createElement(Container, null, _react.default.createElement(_core.Grid, {
    container: true,
    direction: 'column',
    justify: "center",
    alignItems: "stretch",
    spacing: 0
  }, _react.default.createElement(_core.Grid, {
    item: true
  }, _react.default.createElement(TitleButton, {
    onClick: function onClick() {
      return toggleSubMenu(idx);
    },
    fullWidth: true,
    disableripple: "true",
    disabletouchripple: "true"
  }, title, _react.default.createElement(ContainerIcon, {
    pose: !showSubMenu[idx] ? 'show' : 'hide',
    display: !showSubMenu[idx] ? 'flex' : 'none'
  }, _react.default.createElement(_icons.ExpandMore, null)), _react.default.createElement(ContainerIcon, {
    pose: showSubMenu[idx] ? 'show' : 'hide',
    display: showSubMenu[idx] ? 'flex' : 'none'
  }, _react.default.createElement(_icons.ExpandLess, null))))));
};

var SectionPageBurgerMenu = enhance(function (_ref3) {
  var showMenu = _ref3.showMenu,
      toggleSubMenu = _ref3.toggleSubMenu,
      showSubMenu = _ref3.showSubMenu,
      menuIdx = _ref3.menuIdx;
  return _react.default.createElement(Root, {
    pose: showMenu ? 'show' : 'hide',
    style: {
      display: showMenu ? 'flex' : 'none',
      height: showMenu ? 'auto' : '0'
    }
  }, _react.default.createElement(_core.Grid, {
    container: true,
    direction: 'column',
    justify: "center",
    alignItems: "stretch",
    spacing: 8
  }, ['title1', 'title2', 'title3'].map(function (arg, idx) {
    return _react.default.createElement(_core.Grid, {
      item: true,
      key: idx
    }, _react.default.createElement(MenuHeader, {
      menuIdx: menuIdx,
      idx: idx,
      title: arg,
      toggleSubMenu: toggleSubMenu,
      showSubMenu: showSubMenu
    }), _react.default.createElement(SubMenu, {
      pose: showSubMenu[idx] ? 'show' : 'hide',
      style: {
        display: showSubMenu[idx] ? 'flex' : 'none',
        height: showSubMenu[idx] ? 'auto' : '0'
      }
    }, _react.default.createElement(_core.Grid, {
      container: true,
      direction: 'column',
      justify: "center",
      alignItems: "stretch",
      spacing: 0
    }, ['sub1', 'sub2', 'sub3'].map(function (sub, index) {
      return _react.default.createElement(_core.Grid, {
        item: true,
        key: index
      }, _react.default.createElement(TitleButton, {
        fullWidth: true,
        disableripple: "true",
        disabletouchripple: "true"
      }, sub));
    }))));
  })));
});
var _default = SectionPageBurgerMenu;
exports.default = _default;
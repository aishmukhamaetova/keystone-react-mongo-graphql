"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _compose = _interopRequireDefault(require("recompose/compose"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _polished = require("polished");

var _stripe = _interopRequireDefault(require("../../../../honey/media/stripe1.svg"));

var _stripe2 = _interopRequireDefault(require("../../../../honey/media/stripe2.svg"));

var _stripe3 = _interopRequireDefault(require("../../../../honey/media/stripe3.svg"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

var StripeBlock = _styledComponents.default.div.withConfig({
  displayName: "SectionStripe__StripeBlock",
  componentId: "sc-1xmh214-0"
})(["background-position:center left;background-repeat:repeat-x;background-image:url(", ");height:", ";margin:", " 0;"], function (props) {
  return props.type === 1 ? _stripe.default : props.type === 2 ? _stripe2.default : props.type === 3 ? _stripe3.default : _stripe.default;
}, (0, _polished.rem)(40), (0, _polished.rem)(70));

var StripeEmpty = _styledComponents.default.div.withConfig({
  displayName: "SectionStripe__StripeEmpty",
  componentId: "sc-1xmh214-1"
})(["margin:0 0 ", " 0;"], (0, _polished.rem)(70));

var enhance = (0, _compose.default)();
var Stripe = enhance(function (_ref) {
  var _ref$type = _ref.type,
      type = _ref$type === void 0 ? 1 : _ref$type,
      _ref$empty = _ref.empty,
      empty = _ref$empty === void 0 ? false : _ref$empty;
  return empty ? _react.default.createElement(StripeEmpty, null) : _react.default.createElement(StripeBlock, {
    type: type
  });
});
var _default = Stripe;
exports.default = _default;
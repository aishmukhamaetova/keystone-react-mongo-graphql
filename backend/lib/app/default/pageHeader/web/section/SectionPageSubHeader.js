"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _core = require("@material-ui/core");

var _compose = _interopRequireDefault(require("recompose/compose"));

var _recompose = require("recompose");

var _defaultProps = _interopRequireDefault(require("recompose/defaultProps"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _polished = require("polished");

var _icons = require("../../../../honey/icons");

var _icons2 = require("@material-ui/icons");

var _routes = require("../../../../honey/page/api/routes");

var _SectionPageBurgerMenu = _interopRequireDefault(require("./SectionPageBurgerMenu"));

var _reactPose = _interopRequireDefault(require("react-pose"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

var map = {
  sm: 600,
  md: 960,
  lg: 1280,
  xl: 1920
};

var mediaGrid = function mediaGrid(type) {
  return function () {
    return "@media (max-width: ".concat(map[type] - 1, "px)");
  };
};

var enhance = (0, _compose.default)((0, _defaultProps.default)({
  A: {
    IconCart: (0, _styledComponents.default)(_icons.IconCart).withConfig({
      displayName: "SectionPageSubHeader__IconCart",
      componentId: "sc-1v3ldp0-0"
    })(["&&{}"]),
    ButtonMenu: _styledComponents.default.div.withConfig({
      displayName: "SectionPageSubHeader__ButtonMenu",
      componentId: "sc-1v3ldp0-1"
    })(["&&{display:block;font-size:", ";padding-top:", ";font-weight:700;text-transform:uppercase;color:#000000;cursor:pointer;a{text-transform:uppercase;color:#000000;&:hover{color:rgba(0,0,0,0.7);text-decoration:none;}}&:hover{color:rgba(0,0,0,0.7);text-decoration:none;}}"], (0, _polished.rem)(14), (0, _polished.rem)(4)),
    IconLogoWrap: _styledComponents.default.div.withConfig({
      displayName: "SectionPageSubHeader__IconLogoWrap",
      componentId: "sc-1v3ldp0-2"
    })(["&&{margin-top:-98px;text-align:left;", "{text-align:center;}}"], mediaGrid("sm")),
    Root: _styledComponents.default.div.withConfig({
      displayName: "SectionPageSubHeader__Root",
      componentId: "sc-1v3ldp0-3"
    })(["&&{position:relative;overflow:hidden;}"]),
    GridCustom: (0, _styledComponents.default)(function (_ref) {
      var background = _ref.background,
          margin = _ref.margin,
          other = _objectWithoutProperties(_ref, ["background", "margin"]);

      return _react.default.createElement(_core.Grid, other);
    }).withConfig({
      displayName: "SectionPageSubHeader__GridCustom",
      componentId: "sc-1v3ldp0-4"
    })(["&&{position:relative;height:", ";background:#ffffff;z-index:1000;box-shadow:0px 2px 4px -1px rgba(0,0,0,0.2),0px 4px 5px 0px rgba(0,0,0,0.14),0px 1px 10px 0px rgba(0,0,0,0.12);}"], (0, _polished.rem)(72)),
    ContainerIcon: (0, _styledComponents.default)(_reactPose.default.div({
      show: {
        opacity: 1
      },
      hide: {
        opacity: 0
      }
    })).withConfig({
      displayName: "SectionPageSubHeader__ContainerIcon",
      componentId: "sc-1v3ldp0-5"
    })(["&&{display:", ";}"], function (props) {
      return props.display || 'flex';
    })
  }
}), (0, _recompose.withStateHandlers)(function () {
  return {
    showMenu: false,
    menu: [{
      title: 'subscriptions',
      name: 'subscriptions'
    }, {
      title: 'shop',
      name: 'products'
    }, {
      title: 'b2b',
      name: 'b2b'
    }, {
      title: 'our story',
      name: 'about'
    }]
  };
}, {
  toggleMenu: function toggleMenu(_ref2) {
    var showMenu = _ref2.showMenu;
    return function () {
      return {
        showMenu: !showMenu
      };
    };
  }
}));
var SectionPageSubHeader = enhance(function (_ref3) {
  var showMenu = _ref3.showMenu,
      toggleMenu = _ref3.toggleMenu,
      A = _ref3.A,
      menu = _ref3.menu;
  return _react.default.createElement(A.Root, null, _react.default.createElement(A.GridCustom, {
    container: true,
    direction: 'row',
    justify: "center",
    alignItems: 'center',
    spacing: 0
  }, _react.default.createElement(_core.Grid, {
    item: true,
    xs: 11
  }, _react.default.createElement(_core.Grid, {
    container: true,
    alignItems: "stretch",
    direction: "row",
    justify: "space-between",
    style: {
      position: 'relative',
      height: '25px'
    }
  }, _react.default.createElement(_core.Grid, {
    item: true,
    xs: 2,
    sm: 4
  }, _react.default.createElement(_core.Hidden, {
    smDown: true
  }, _react.default.createElement(A.IconLogoWrap, {
    style: {
      fontSize: 220
    }
  }, _react.default.createElement(_routes.Link, {
    route: "index"
  }, _react.default.createElement("a", null, _react.default.createElement(_icons.IconLogo, {
    coloricon: "#4a4a4a"
  }))))), _react.default.createElement(_core.Hidden, {
    mdUp: true
  }, _react.default.createElement(A.ContainerIcon, {
    pose: !showMenu ? 'show' : 'hide',
    display: !showMenu ? 'flex' : 'none'
  }, _react.default.createElement(_core.ButtonBase, {
    disableripple: "true",
    disabletouchripple: "true",
    onClick: toggleMenu
  }, _react.default.createElement(_icons.IconMenu, null))), _react.default.createElement(A.ContainerIcon, {
    pose: showMenu ? 'show' : 'hide',
    display: showMenu ? 'flex' : 'none'
  }, _react.default.createElement(_core.ButtonBase, {
    style: {
      color: '#4a4a4a'
    },
    disableripple: "true",
    disabletouchripple: "true",
    onClick: toggleMenu
  }, _react.default.createElement(_icons2.Clear, {
    color: "inherit",
    style: {
      fontSize: 30
    }
  }))))), _react.default.createElement(_core.Grid, {
    item: true,
    xs: 8,
    sm: 6
  }, _react.default.createElement(_core.Hidden, {
    mdUp: true
  }, _react.default.createElement(A.IconLogoWrap, {
    style: {
      fontSize: '221px'
    }
  }, _react.default.createElement(_icons.IconLogo, {
    coloricon: "#4a4a4a"
  }))), _react.default.createElement(_core.Hidden, {
    smDown: true
  }, _react.default.createElement(_core.Grid, {
    container: true,
    direction: 'row',
    justify: "space-between",
    alignItems: 'center',
    spacing: 40
  }, menu.map(function (value, index) {
    return _react.default.createElement(_core.Grid, {
      key: index,
      item: true
    }, _react.default.createElement(A.ButtonMenu, null, _react.default.createElement(_routes.Link, {
      route: value.name
    }, _react.default.createElement("a", null, value.title))));
  })))), _react.default.createElement(_core.Grid, {
    item: true,
    xs: 2,
    style: {
      textAlign: 'right'
    }
  }, _react.default.createElement(_routes.Link, {
    route: "cart"
  }, _react.default.createElement("a", null, _react.default.createElement(A.IconCart, null))))))), _react.default.createElement(_core.Hidden, {
    mdUp: true
  }, _react.default.createElement(_SectionPageBurgerMenu.default, {
    showMenu: showMenu
  })));
});
var _default = SectionPageSubHeader;
exports.default = _default;
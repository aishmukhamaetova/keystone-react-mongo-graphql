"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _compose = _interopRequireDefault(require("recompose/compose"));

var _defaultProps = _interopRequireDefault(require("recompose/defaultProps"));

var _semanticUiReact = require("semantic-ui-react");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

var enhance = (0, _compose.default)((0, _defaultProps.default)({
  data: {
    'home': {
      id: "home",
      caption: "Home"
    },
    'login': {
      id: "login",
      caption: "Login"
    }
  },
  orderLeft: ["home"],
  orderRight: ["login"]
}));
var OurMenu = enhance(function (_ref) {
  var data = _ref.data,
      orderLeft = _ref.orderLeft,
      orderRight = _ref.orderRight,
      _onClick = _ref.onClick;
  return _react.default.createElement(_semanticUiReact.Menu, {
    pointing: true
  }, orderLeft.map(function (id) {
    return _react.default.createElement(_semanticUiReact.Menu.Item, {
      key: id,
      name: data[id].caption,
      onClick: function onClick() {
        return _onClick && _onClick({
          id: id
        });
      }
    });
  }), _react.default.createElement(_semanticUiReact.Menu.Menu, {
    position: "right"
  }, orderRight.map(function (id) {
    return _react.default.createElement(_semanticUiReact.Menu.Item, {
      key: id,
      name: data[id].caption,
      onClick: function onClick() {
        return _onClick && _onClick({
          id: id
        });
      }
    });
  })));
});
var _default = OurMenu;
exports.default = _default;
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _compose = _interopRequireDefault(require("recompose/compose"));

var _Logo = _interopRequireDefault(require("../Logo"));

var _withSpace = _interopRequireDefault(require("../../../../../evoke-me/space/all/withSpace"));

var _Block = _interopRequireDefault(require("../../../../../evoke-me/layout/web/Block"));

var _spacePageHeader = _interopRequireDefault(require("../../space/spacePageHeader"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

var enhance = (0, _compose.default)((0, _withSpace.default)(_spacePageHeader.default));
var BlockPageHeaderLogo = enhance(function (_ref) {
  var fromPageHeader = _ref.fromPageHeader;
  var logo = fromPageHeader.logo;
  return _react.default.createElement(_Block.default, {
    right: true,
    middle: true
  }, _react.default.createElement(_Logo.default, logo));
});
var _default = BlockPageHeaderLogo;
exports.default = _default;
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _router = require("next/router");

var _react = _interopRequireWildcard(require("react"));

var _compose = _interopRequireDefault(require("recompose/compose"));

var _Menu = _interopRequireDefault(require("../Menu"));

var _Block = _interopRequireDefault(require("../../../../../evoke-me/layout/web/Block"));

var _withSpace = _interopRequireDefault(require("../../../../../evoke-me/space/all/withSpace"));

var _spaceNav = _interopRequireDefault(require("../../../nav/space/spaceNav"));

var _spaceMenu = _interopRequireDefault(require("../../../menu/space/spaceMenu"));

var _spacePageHeader = _interopRequireDefault(require("../../space/spacePageHeader"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

var enhance = (0, _compose.default)(_router.withRouter, (0, _withSpace.default)(_spacePageHeader.default), (0, _withSpace.default)(_spaceMenu.default), (0, _withSpace.default)(_spaceNav.default));
var BlockPageHeaderMenu = enhance(function (_ref) {
  var fromMenu = _ref.fromMenu,
      fromNav = _ref.fromNav,
      fromPageHeader = _ref.fromPageHeader,
      router = _ref.router;
  var data = fromMenu.data,
      orderMap = fromMenu.orderMap;
  var height = fromPageHeader.height;
  var go = fromNav.go;
  var _orderMap$headerLeft = orderMap.headerLeft,
      headerLeft = _orderMap$headerLeft === void 0 ? [] : _orderMap$headerLeft,
      _orderMap$headerRight = orderMap.headerRight,
      headerRight = _orderMap$headerRight === void 0 ? [] : _orderMap$headerRight;
  return _react.default.createElement(_Block.default, {
    center: true,
    middle: true,
    maxWidth: true,
    style: {
      height: height
    }
  }, _react.default.createElement(_Menu.default, {
    data: data,
    orderLeft: headerLeft,
    orderRight: headerRight,
    onClick: function onClick(_ref2) {
      var id = _ref2.id;
      return go(data[id].path, router.push);
    }
  }));
});
var _default = BlockPageHeaderMenu;
exports.default = _default;
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _BlockPageFooterLinks = _interopRequireDefault(require("../block/BlockPageFooterLinks"));

var _BlockPageFooterSubscribe = _interopRequireDefault(require("../block/BlockPageFooterSubscribe"));

var _recompose = require("recompose");

var _defaultProps = _interopRequireDefault(require("recompose/defaultProps"));

var _icons = require("../../../../honey/icons");

var _core = require("@material-ui/core");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _polished = require("polished");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

var map = {
  sm: 600,
  md: 960,
  lg: 1280,
  xl: 1920
};

var mediaGrid = function mediaGrid(type) {
  return function () {
    return "@media (max-width: ".concat(map[type] - 1, "px)");
  };
};

var iconMap = {
  'instagram': _icons.IconInstagram,
  'fb': _icons.IconFacebook,
  'youtube': _icons.IconYoutube,
  'vimeo': _icons.IconVimeo
};
var enhance = (0, _recompose.compose)((0, _defaultProps.default)({
  A: {
    SocialIcon: _styledComponents.default.div.withConfig({
      displayName: "SectionPageFooter__SocialIcon",
      componentId: "sc-1jvp6so-0"
    })(["display:flex;bottom:0;svg{height:100%;}"]),
    SocialThumbnail: _styledComponents.default.a.withConfig({
      displayName: "SectionPageFooter__SocialThumbnail",
      componentId: "sc-1jvp6so-1"
    })(["position:relative;width:100%;height:100%;text-align:right;display:block;overflow:hidden;border-right:3px solid white;"]),
    Link: (0, _styledComponents.default)(_core.Typography).withConfig({
      displayName: "SectionPageFooter__Link",
      componentId: "sc-1jvp6so-2"
    })(["&&{font-size:", ";margin-bottom:", "}"], (0, _polished.rem)(16), (0, _polished.rem)(5)),
    Social: (0, _styledComponents.default)(_core.Grid).withConfig({
      displayName: "SectionPageFooter__Social",
      componentId: "sc-1jvp6so-3"
    })(["&&{", "{margin:", " 0 0;}", "{margin:", " 0;}}"], mediaGrid("md"), (0, _polished.rem)(20), mediaGrid("sm"), (0, _polished.rem)(20)),
    Menu: (0, _styledComponents.default)(_core.Grid).withConfig({
      displayName: "SectionPageFooter__Menu",
      componentId: "sc-1jvp6so-4"
    })(["&&{", "{margin:", " 0 0;}", "{margin:", " 0 0;}}"], mediaGrid("md"), (0, _polished.rem)(20), mediaGrid("sm"), (0, _polished.rem)(20)),
    Copyright: (0, _styledComponents.default)(_core.Typography).withConfig({
      displayName: "SectionPageFooter__Copyright",
      componentId: "sc-1jvp6so-5"
    })(["&&{font-size:", ";text-align:left;", "{text-align:left;margin:", " 0 0;}", "{text-align:center;margin:", " 0 0;}}"], (0, _polished.rem)(15), mediaGrid("md"), (0, _polished.rem)(20), mediaGrid("sm"), (0, _polished.rem)(20)),
    Footer: _styledComponents.default.div.withConfig({
      displayName: "SectionPageFooter__Footer",
      componentId: "sc-1jvp6so-6"
    })(["&&{font-size:1.14rem;font-weight:300;.link{padding-bottom:0.3125rem;a{color:#000;&:hover{text-decoration:underline;}}&.active{a{font-weight:bold;}}}.mdUp{margin:0 0 2rem 0;}.title{display:block;padding-bottom:0.2rem;font-size:1.3rem;font-weight:600;margin:0;}input{font-size:1.125rem;font-style:italic;}", "{.copyright{text-align:center;font-size:85%;}.copyright,.link{margin-bottom:0.6em;}.title,.link{text-align:center;}}}"], mediaGrid("sm")),
    Container: (0, _styledComponents.default)(function (_ref) {
      var background = _ref.background,
          margin = _ref.margin,
          other = _objectWithoutProperties(_ref, ["background", "margin"]);

      return _react.default.createElement(_core.Grid, other);
    }).withConfig({
      displayName: "SectionPageFooter__Container",
      componentId: "sc-1jvp6so-7"
    })(["&&{background:", ";margin:", ";padding:", ";}"], function (props) {
      return props.background;
    }, function (props) {
      return props.margin;
    }, function (props) {
      return props.padding;
    })
  }
}));
var SectionPageFooter = enhance(function (_ref2) {
  var _ref2$theme = _ref2.theme,
      theme = _ref2$theme === void 0 ? {} : _ref2$theme,
      A = _ref2.A;

  if (!_BlockPageFooterLinks.default || !_BlockPageFooterSubscribe.default) {
    return _react.default.createElement(Segment, {
      placeholder: true
    }, _react.default.createElement(Header, {
      icon: true
    }, _react.default.createElement(Icon, {
      name: "pdf file outline"
    }), "SectionPageHeader cannot find BlockPageFooterLinks or BlockPageFooterSubscribe"));
  }

  return _react.default.createElement(A.Footer, null, _react.default.createElement(A.Container, {
    background: "#f1f1f1",
    margin: "2rem 0 0",
    padding: "2.5rem 0",
    container: true,
    direction: 'row',
    justify: "center",
    alignItems: 'center',
    spacing: 0
  }, _react.default.createElement(_core.Grid, {
    xs: 10,
    item: true
  }, _react.default.createElement(_core.Grid, {
    container: true,
    direction: 'row',
    justify: "center",
    spacing: 0
  }, _react.default.createElement(_core.Hidden, {
    mdUp: true
  }, _react.default.createElement(_BlockPageFooterSubscribe.default, {
    mdUp: 'mdUp'
  })), _react.default.createElement(_core.Grid, {
    xs: 12,
    sm: 12,
    md: true,
    item: true
  }, _react.default.createElement(_core.Grid, {
    container: true,
    direction: 'row',
    spacing: 16,
    alignItems: 'center'
  }, _react.default.createElement(_BlockPageFooterLinks.default, null))), _react.default.createElement(_core.Hidden, {
    smDown: true
  }, _react.default.createElement(_BlockPageFooterSubscribe.default, null))))), _react.default.createElement(A.Container, {
    background: "#fff",
    margin: "0",
    padding: "1rem 0",
    container: true,
    direction: 'row',
    justify: "center",
    alignItems: 'center',
    spacing: 0
  }, _react.default.createElement(_core.Grid, {
    md: 10,
    xs: 12,
    item: true
  }, _react.default.createElement(_core.Grid, {
    container: true,
    direction: 'row',
    justify: "center",
    alignItems: 'center',
    spacing: 0
  }, _react.default.createElement(_core.Grid, {
    xs: 12,
    md: 5,
    item: true
  }, _react.default.createElement(A.Copyright, {
    variant: "body2",
    paragraph: true
  }, "\xA9 L\u2019Atelier du\xA0Miel 2018, all rights reserved.")), _react.default.createElement(_core.Grid, {
    md: 7,
    item: true
  }, _react.default.createElement(_core.Grid, {
    container: true,
    direction: 'row',
    justify: "center",
    alignItems: 'center',
    spacing: 0
  }, _react.default.createElement(_core.Grid, {
    md: 8,
    xs: 12,
    item: true
  }, _react.default.createElement(A.Menu, {
    container: true,
    direction: 'row',
    alignItems: 'center',
    justify: "center",
    spacing: 0
  }, _react.default.createElement(_core.Grid, {
    xs: true,
    item: true
  }, _react.default.createElement(A.Link, {
    variant: "body1",
    paragraph: true
  }, "English")), _react.default.createElement(_core.Grid, {
    xs: true,
    item: true
  }, _react.default.createElement(A.Link, {
    variant: "body1",
    paragraph: true
  }, "\u0641\u0631\u0633 \u0639\u0631\u0628\u064A")), _react.default.createElement(_core.Grid, {
    xs: true,
    item: true
  }, _react.default.createElement(A.Link, {
    variant: "body1",
    paragraph: true
  }, "Fran\xE7ais")))), _react.default.createElement(_core.Grid, {
    md: 4,
    xs: 8,
    item: true
  }, _react.default.createElement(A.Social, {
    container: true,
    direction: 'row',
    justify: "space-between",
    alignItems: 'center',
    spacing: 0
  }, Object.keys(iconMap).map(function (item, index) {
    var Icon = iconMap[item];
    return _react.default.createElement(_core.Grid, {
      xs: true,
      key: index,
      item: true,
      className: 'socialThumbnail'
    }, _react.default.createElement(A.SocialThumbnail, {
      href: "//www.instagram.com/p/Bp9tfPzDdbQ/",
      target: "_blank",
      rel: "noopener noreferrer"
    }, _react.default.createElement(A.SocialIcon, null, _react.default.createElement(Icon, {
      style: {
        fontSize: item === 'youtube' ? 24 : item === 'vimeo' ? 53 : 20,
        fill: '#0000009e'
      }
    }))));
  })))))))));
});
var _default = SectionPageFooter;
exports.default = _default;
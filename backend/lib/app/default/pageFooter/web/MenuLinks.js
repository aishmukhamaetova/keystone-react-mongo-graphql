"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _compose = _interopRequireDefault(require("recompose/compose"));

var _defaultProps = _interopRequireDefault(require("recompose/defaultProps"));

var _core = require("@material-ui/core");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _polished = require("polished");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

var map = {
  sm: 600,
  md: 960,
  lg: 1280,
  xl: 1920
};

var mediaGrid = function mediaGrid(type) {
  return function () {
    return "@media (max-width: ".concat(map[type] - 1, "px)");
  };
};

var enhance = (0, _compose.default)((0, _defaultProps.default)({
  A: {
    LinkMenu: (0, _styledComponents.default)(_core.Typography).withConfig({
      displayName: "MenuLinks__LinkMenu",
      componentId: "sc-1noybjk-0"
    })(["&&{font-size:", ";margin-bottom:", "}"], (0, _polished.rem)(14), (0, _polished.rem)(5)),
    TitleMenu: (0, _styledComponents.default)(_core.Typography).withConfig({
      displayName: "MenuLinks__TitleMenu",
      componentId: "sc-1noybjk-1"
    })(["&&{font-size:", ";margin-bottom:", ";font-weight:600;}"], (0, _polished.rem)(18), (0, _polished.rem)(10))
  }
}));
var MenuLinks = enhance(function (_ref) {
  var height = _ref.height,
      A = _ref.A;
  return ['Information', 'Customer Service', 'Legal'].map(function (value, index) {
    return _react.default.createElement(_core.Grid, {
      key: index,
      item: true,
      xs: 12,
      sm: true
    }, _react.default.createElement(A.TitleMenu, {
      noWrap: true,
      variant: "body1",
      paragraph: true
    }, value), _react.default.createElement(A.LinkMenu, {
      variant: "body1",
      paragraph: true
    }, "Our Story"), _react.default.createElement(A.LinkMenu, {
      variant: "body1",
      paragraph: true
    }, "Retail Stores"), _react.default.createElement(A.LinkMenu, {
      variant: "body1",
      paragraph: true
    }, "Business"), _react.default.createElement(A.LinkMenu, {
      variant: "body1",
      paragraph: true
    }, "Press"), _react.default.createElement(A.LinkMenu, {
      variant: "body1",
      paragraph: true
    }, "Carreers"));
  });
});
var _default = MenuLinks;
exports.default = _default;
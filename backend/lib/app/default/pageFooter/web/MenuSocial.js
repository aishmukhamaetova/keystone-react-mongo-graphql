"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _semanticUiReact = require("semantic-ui-react");

var _compose = _interopRequireDefault(require("recompose/compose"));

var _defaultProps = _interopRequireDefault(require("recompose/defaultProps"));

var _core = require("@material-ui/core");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _polished = require("polished");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

var map = {
  sm: 600,
  md: 960,
  lg: 1280,
  xl: 1920
};

var mediaGrid = function mediaGrid(type) {
  return function () {
    return "@media (max-width: ".concat(map[type] - 1, "px)");
  };
};

var enhance = (0, _compose.default)((0, _defaultProps.default)({
  A: {
    Button: (0, _styledComponents.default)(function (_ref) {
      var width = _ref.width,
          other = _objectWithoutProperties(_ref, ["width"]);

      return _react.default.createElement(_core.Button, other);
    }).withConfig({
      displayName: "MenuSocial__Button",
      componentId: "iqtklb-0"
    })(["&&{background-color:#000;color:white;font-weight:600;font-size:", ";text-transform:uppercase;width:", " || inherit;padding:", " 0;border-radius:0;}"], (0, _polished.rem)(20), function (props) {
      return props.width;
    }, (0, _polished.rem)(12.6)),
    Follow: _styledComponents.default.div.withConfig({
      displayName: "MenuSocial__Follow",
      componentId: "iqtklb-1"
    })(["&&{p{margin-bottom:1em;}", "{text-align:center;margin-bottom:1em;}}"], mediaGrid("sm")),
    Input: (0, _styledComponents.default)(function (_ref2) {
      var width = _ref2.width,
          align = _ref2.align,
          other = _objectWithoutProperties(_ref2, ["width", "align"]);

      return _react.default.createElement(_core.InputBase, other);
    }).withConfig({
      displayName: "MenuSocial__Input",
      componentId: "iqtklb-2"
    })(["&&{margin:none;border:none;box-shadow:1px 1px 0px #b1b1b1 inset,-1px -1px 0px #b1b1b1 inset;font-size:", ";width:", ";input{color:#000;text-align:", ";padding:", ";}}"], (0, _polished.rem)(18), function (props) {
      return props.width || 'auto';
    }, function (props) {
      return props.align || 'left';
    }, (0, _polished.rem)(16.8))
  }
}));
var MenuSocial = enhance(function (_ref3) {
  var height = _ref3.height,
      mdUp = _ref3.mdUp,
      children = _ref3.children,
      A = _ref3.A;
  return _react.default.createElement(_core.Grid, {
    xs: 12,
    sm: 12,
    md: true,
    item: true,
    className: mdUp
  }, _react.default.createElement(A.Follow, null, _react.default.createElement(_core.Typography, {
    className: 'title',
    noWrap: true,
    variant: "body2",
    paragraph: true
  }, "Follow our honey trail"), _react.default.createElement("p", null, "Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia, excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia")), _react.default.createElement(_core.Grid, {
    container: true,
    direction: 'row',
    justify: "center",
    spacing: 16
  }, _react.default.createElement(_core.Grid, {
    sm: 6,
    xs: 12,
    item: true
  }, _react.default.createElement(A.Input, {
    bg: "none",
    placeholder: "This is some email",
    width: "100%",
    variant: "standard",
    name: "value",
    label: "",
    id: "standard-name",
    value: '',
    margin: "none"
  })), _react.default.createElement(_core.Grid, {
    sm: 6,
    xs: 12,
    item: true
  }, _react.default.createElement(A.Button, {
    fullWidth: true,
    variant: "contained",
    color: "primary"
  }, "Subscribe"))));
});
var _default = MenuSocial;
exports.default = _default;
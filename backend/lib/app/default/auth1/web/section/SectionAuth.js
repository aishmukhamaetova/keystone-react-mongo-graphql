"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _reactGridSystem = require("react-grid-system");

var _semanticUiReact = require("semantic-ui-react");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

var SectionAuth = function SectionAuth(_ref) {
  var _ref$theme = _ref.theme,
      theme = _ref$theme === void 0 ? {} : _ref$theme;
  var BlockAuth = theme.BlockAuth;

  if (!BlockAuth) {
    return _react.default.createElement(_semanticUiReact.Segment, {
      placeholder: true
    }, _react.default.createElement(_semanticUiReact.Header, {
      icon: true
    }, _react.default.createElement(_semanticUiReact.Icon, {
      name: "pdf file outline"
    }), "SectionAuth cannot find BlockAuth"));
  }

  return _react.default.createElement(_reactGridSystem.Container, {
    fluid: true,
    style: {
      padding: 20
    }
  }, _react.default.createElement(_reactGridSystem.Row, {
    justify: "center"
  }, _react.default.createElement(_reactGridSystem.Col, {
    sm: 10,
    lg: 6
  }, _react.default.createElement(BlockAuth, null))));
};

var _default = SectionAuth;
exports.default = _default;
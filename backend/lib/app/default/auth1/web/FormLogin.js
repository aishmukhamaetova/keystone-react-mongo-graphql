"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _compose = _interopRequireDefault(require("recompose/compose"));

var _defaultProps = _interopRequireDefault(require("recompose/defaultProps"));

var _withPropsOnChange = _interopRequireDefault(require("recompose/withPropsOnChange"));

var _semanticUiReact = require("semantic-ui-react");

var _Block = _interopRequireDefault(require("../../../../evoke-me/layout/web/Block"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

var runOnceInTime = function runOnceInTime(run, timeout) {
  var time;
  return function () {
    var next = Date.now();

    if (!time || next - time >= timeout) {
      time = next;
      return run.apply(void 0, arguments);
    }
  };
};

var enhance = (0, _compose.default)((0, _defaultProps.default)({
  username: "",
  password: "",
  errors: [],
  isLoading: false
}), (0, _withPropsOnChange.default)('onLogin', function (_ref) {
  var onLogin = _ref.onLogin;
  return {
    onLogin: runOnceInTime(onLogin, 1000)
  };
}));
var FormLogin = enhance(function (props) {
  var username = props.username,
      password = props.password,
      errors = props.errors,
      isLoading = props.isLoading,
      onLogin = props.onLogin,
      onChangeUsername = props.onChangeUsername,
      onChangePassword = props.onChangePassword;
  return _react.default.createElement(_semanticUiReact.Form, {
    loading: isLoading,
    error: errors.length > 0
  }, errors.length ? _react.default.createElement(_semanticUiReact.Message, {
    error: true,
    content: errors[0].message
  }) : _react.default.createElement(_semanticUiReact.Message, {
    info: true,
    content: "Write username and password"
  }), _react.default.createElement(_Block.default, {
    center: true,
    middle: true
  }, _react.default.createElement(_semanticUiReact.Form.Field, null, _react.default.createElement("label", null, "Email"), _react.default.createElement("input", {
    placeholder: "User name",
    value: username,
    onChange: function onChange(_ref2) {
      var value = _ref2.target.value;
      return onChangeUsername && onChangeUsername(value);
    }
  })), _react.default.createElement(_semanticUiReact.Form.Field, null, _react.default.createElement("label", null, "Password"), _react.default.createElement("input", {
    placeholder: "Password",
    type: "password",
    value: password,
    onChange: function onChange(_ref3) {
      var value = _ref3.target.value;
      return onChangePassword && onChangePassword(value);
    }
  })), _react.default.createElement(_semanticUiReact.Form.Field, null, _react.default.createElement(_semanticUiReact.Checkbox, {
    label: "I agree to the Terms and Conditions"
  })), _react.default.createElement(_semanticUiReact.Button, {
    type: "submit",
    onClick: function onClick(e) {
      e.preventDefault();
      onLogin();
    }
  }, "Login")));
});
var _default = FormLogin;
exports.default = _default;
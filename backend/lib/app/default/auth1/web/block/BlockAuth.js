"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _compose = _interopRequireDefault(require("recompose/compose"));

var _lifecycle = _interopRequireDefault(require("recompose/lifecycle"));

var _FormLogin = _interopRequireDefault(require("app/default/auth/web/FormLogin"));

var _FormLogout = _interopRequireDefault(require("app/default/auth/web/FormLogout"));

var _withSpace = _interopRequireDefault(require("../../../../../evoke-me/space/all/withSpace"));

var _spaceAuth = _interopRequireDefault(require("../../space/spaceAuth"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

var enhance = (0, _compose.default)((0, _withSpace.default)(_spaceAuth.default));
var BlockAuth = enhance(function (_ref) {
  var fromAuth = _ref.fromAuth;
  var username = fromAuth.username,
      usernameSet = fromAuth.usernameSet;
  var password = fromAuth.password,
      passwordSet = fromAuth.passwordSet;
  var errors = fromAuth.errors;
  var isAuth = fromAuth.isAuth,
      isLoading = fromAuth.isLoading;
  var login = fromAuth.login,
      logout = fromAuth.logout;
  return isAuth ? _react.default.createElement(_FormLogout.default, {
    onLogout: logout
  }) : _react.default.createElement(_FormLogin.default, {
    username: username,
    password: password,
    errors: errors,
    isLoading: isLoading,
    onChangeUsername: usernameSet,
    onChangePassword: passwordSet,
    onLogin: login
  });
});
var _default = BlockAuth;
exports.default = _default;
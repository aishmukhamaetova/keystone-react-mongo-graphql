"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _router = require("next/router");

var _react = _interopRequireWildcard(require("react"));

var _reactApollo = require("react-apollo");

var _compose = _interopRequireDefault(require("recompose/compose"));

var _lifecycle = _interopRequireDefault(require("recompose/lifecycle"));

var _withSpace = _interopRequireDefault(require("../../../../../evoke-me/space/all/withSpace"));

var _spaceNav = _interopRequireDefault(require("../../../nav/space/spaceNav"));

var _spaceAuth = _interopRequireDefault(require("../../space/spaceAuth"));

var _spaceAuthGraphql = _interopRequireDefault(require("../../space/spaceAuthGraphql"));

var _FormFbLogin = _interopRequireDefault(require("../FormFbLogin"));

var _FormFbLogout = _interopRequireDefault(require("../FormFbLogout"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

// https://github.com/jaredhanson/passport-facebook/issues/12
var removeHashFromBuggyFacebook = function removeHashFromBuggyFacebook() {
  try {
    if (location.hash = '#_=_') {
      window.history.replaceState("", document.title, window.location.pathname + window.location.search);
    }
  } catch (err) {}
};

var enhance = (0, _compose.default)(_router.withRouter, _reactApollo.withApollo, (0, _withSpace.default)(_spaceAuth.default), (0, _withSpace.default)(_spaceAuthGraphql.default), (0, _withSpace.default)(_spaceNav.default), (0, _lifecycle.default)({
  componentDidMount: function componentDidMount() {
    removeHashFromBuggyFacebook();
    var client = this.props.client;
    var fromAuthGraphql = this.props.fromAuthGraphql;
    fromAuthGraphql.query(client);
  }
}));
var BlockAuth = enhance(function (_ref) {
  var fromAuth = _ref.fromAuth,
      fromNav = _ref.fromNav,
      router = _ref.router;
  var errors = fromAuth.errors;
  var isAuth = fromAuth.isAuth,
      isLoading = fromAuth.isLoading;
  var go = fromNav.go;
  return isAuth ? _react.default.createElement(_FormFbLogout.default, {
    onLogout: function onLogout() {
      return go("/api/auth/facebook/logout", router.push);
    }
  }) : _react.default.createElement(_FormFbLogin.default, {
    errors: errors,
    isLoading: isLoading,
    onLogin: function onLogin() {
      return go("/api/auth/facebook/login", router.push);
    }
  });
});
var _default = BlockAuth;
exports.default = _default;
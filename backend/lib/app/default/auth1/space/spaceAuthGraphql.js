"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _config = _interopRequireDefault(require("../../config/api/config"));

var _query = _interopRequireDefault(require("../api/graphql/query"));

var _spaceAuth = _interopRequireDefault(require("./spaceAuth"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _default = [_config.default.namespace, "authGraphql", [_spaceAuth.default], function () {
  return {
    isLoading: false
  };
}, {
  query: function query(getState, setState, methods, _ref) {
    var fromAuth = _ref.fromAuth;
    return (
      /*#__PURE__*/
      _regenerator.default.mark(function _callee(client) {
        var _ref2, data, id;

        return _regenerator.default.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.prev = 0;

                if (!client) {
                  _context.next = 20;
                  break;
                }

                _context.next = 4;
                return setState({
                  isLoading: true
                });

              case 4:
                _context.next = 6;
                return client.query({
                  query: _query.default
                });

              case 6:
                _ref2 = _context.sent;
                data = _ref2.data;

                if (!data.me) {
                  _context.next = 16;
                  break;
                }

                id = data.me.id;
                _context.next = 12;
                return fromAuth.isAuthSet(true);

              case 12:
                _context.next = 14;
                return fromAuth.idSet(id);

              case 14:
                _context.next = 18;
                break;

              case 16:
                _context.next = 18;
                return fromAuth.isAuthSet(false);

              case 18:
                _context.next = 20;
                return setState({
                  isLoading: false
                });

              case 20:
                _context.next = 27;
                break;

              case 22:
                _context.prev = 22;
                _context.t0 = _context["catch"](0);
                _context.next = 26;
                return setState({
                  isLoading: false
                });

              case 26:
                console.error(_context.t0);

              case 27:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this, [[0, 22]]);
      })
    );
  }
}];
exports.default = _default;
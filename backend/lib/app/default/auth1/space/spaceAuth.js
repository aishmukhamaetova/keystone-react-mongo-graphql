"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _config = _interopRequireDefault(require("../../config/api/config"));

var _spaceNotify = _interopRequireDefault(require("../../notify/space/spaceNotify"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _default = [_config.default.namespace, "auth", [_spaceNotify.default], function () {
  return {
    id: null,
    username: "",
    password: "",
    sessionId: null,
    errors: [],
    isAuth: false,
    isLoading: false
  };
}, {
  isAuthSet: function isAuthSet(getState, setState) {
    return (
      /*#__PURE__*/
      _regenerator.default.mark(function _callee(isAuth) {
        return _regenerator.default.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.next = 2;
                return setState({
                  isAuth: isAuth
                });

              case 2:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      })
    );
  },
  idSet: function idSet(getState, setState) {
    return (
      /*#__PURE__*/
      _regenerator.default.mark(function _callee2(id) {
        return _regenerator.default.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _context2.next = 2;
                return setState({
                  id: id
                });

              case 2:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, this);
      })
    );
  },
  usernameSet: function usernameSet(getState, setState) {
    return (
      /*#__PURE__*/
      _regenerator.default.mark(function _callee3(username) {
        return _regenerator.default.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                _context3.next = 2;
                return setState({
                  username: username
                });

              case 2:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3, this);
      })
    );
  },
  passwordSet: function passwordSet(getState, setState) {
    return (
      /*#__PURE__*/
      _regenerator.default.mark(function _callee4(password) {
        return _regenerator.default.wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                _context4.next = 2;
                return setState({
                  password: password
                });

              case 2:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4, this);
      })
    );
  },
  login: function login(getState, setState, methods, _ref) {
    var fromNotify = _ref.fromNotify;
    return (
      /*#__PURE__*/
      _regenerator.default.mark(function _callee5() {
        var _ref2, username, password, sessionId, error;

        return _regenerator.default.wrap(function _callee5$(_context5) {
          while (1) {
            switch (_context5.prev = _context5.next) {
              case 0:
                _context5.prev = 0;
                _context5.next = 3;
                return getState();

              case 3:
                _ref2 = _context5.sent;
                username = _ref2.username;
                password = _ref2.password;
                _context5.next = 8;
                return setState({
                  errors: [],
                  isAuth: false,
                  isLoading: true
                });

              case 8:
                _context5.next = 10;
                return new Promise(function (resolve, reject) {
                  setTimeout(function () {
                    if (username === "root") {
                      return resolve('SESSION_ID');
                    }

                    reject("CRASH_ME");
                  }, 200);
                });

              case 10:
                sessionId = _context5.sent;
                _context5.next = 13;
                return fromNotify.create({
                  message: "Welcome"
                }, {
                  orderName: "show"
                });

              case 13:
                _context5.next = 15;
                return setState({
                  isAuth: true,
                  isLoading: false,
                  sessionId: sessionId,
                  errors: []
                });

              case 15:
                _context5.next = 24;
                break;

              case 17:
                _context5.prev = 17;
                _context5.t0 = _context5["catch"](0);
                error = {
                  error: true,
                  message: "Wrong username or password"
                };
                _context5.next = 22;
                return fromNotify.create(error, {
                  orderName: "show"
                });

              case 22:
                _context5.next = 24;
                return setState({
                  isAuth: false,
                  isLoading: false,
                  errors: [error]
                });

              case 24:
              case "end":
                return _context5.stop();
            }
          }
        }, _callee5, this, [[0, 17]]);
      })
    );
  },
  logout: function logout(getState, setState, methods, _ref3) {
    var fromNotify = _ref3.fromNotify;
    return (
      /*#__PURE__*/
      _regenerator.default.mark(function _callee6() {
        return _regenerator.default.wrap(function _callee6$(_context6) {
          while (1) {
            switch (_context6.prev = _context6.next) {
              case 0:
                _context6.next = 2;
                return fromNotify.create({
                  message: "Bye, bye!"
                }, {
                  orderName: "show"
                });

              case 2:
                _context6.next = 4;
                return setState({
                  errors: [],
                  isAuth: false,
                  isLoading: false
                });

              case 4:
              case "end":
                return _context6.stop();
            }
          }
        }, _callee6, this);
      })
    );
  }
}];
exports.default = _default;
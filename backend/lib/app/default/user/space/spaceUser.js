"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _config = _interopRequireDefault(require("../../config/api/config"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var _default = [_config.default.namespace, "user", [], function () {
  return {
    isLoading: false,
    data: {},
    orderMap: {
      default: []
    }
  };
}, {
  create: function create(getState, setState) {
    return (
      /*#__PURE__*/
      _regenerator.default.mark(function _callee(doc) {
        var options,
            _options$orderName,
            orderName,
            _ref,
            data,
            orderMap,
            _args = arguments;

        return _regenerator.default.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                options = _args.length > 1 && _args[1] !== undefined ? _args[1] : {};
                _options$orderName = options.orderName, orderName = _options$orderName === void 0 ? "default" : _options$orderName;
                _context.next = 4;
                return getState();

              case 4:
                _ref = _context.sent;
                data = _ref.data;
                orderMap = _ref.orderMap;

                if (!data[doc.id]) {
                  _context.next = 9;
                  break;
                }

                return _context.abrupt("return");

              case 9:
                _context.next = 11;
                return setState({
                  data: _objectSpread({}, data, _defineProperty({}, doc.id, doc)),
                  orderMap: _objectSpread({}, orderMap, _defineProperty({}, orderName, _toConsumableArray(orderMap[orderName] || []).concat([doc.id])))
                });

              case 11:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      })
    );
  }
}, function (ownArgs) {
  var _ownArgs = _slicedToArray(ownArgs, 2),
      _ownArgs$ = _ownArgs[1],
      options = _ownArgs$ === void 0 ? {} : _ownArgs$;

  if (options.view) {
    return options.view;
  }

  return "default";
}];
exports.default = _default;
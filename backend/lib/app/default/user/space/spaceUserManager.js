"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _spaceUser = _interopRequireDefault(require("./spaceUser"));

var _config = _interopRequireDefault(require("../../config/api/config"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _default = ["app.default", 'userManager', [_spaceUser.default], function () {
  return {
    isSeeded: false,
    isSelectMultiple: false,
    select: {},
    animateDisappear: {}
  };
}, {
  seed: function seed(getState, setState, methods, _ref) {
    var fromUser = _ref.fromUser;
    return (
      /*#__PURE__*/
      _regenerator.default.mark(function _callee(payload, options) {
        var _ref2, isSeeded;

        return _regenerator.default.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.next = 2;
                return getState();

              case 2:
                _ref2 = _context.sent;
                isSeeded = _ref2.isSeeded;

                if (isSeeded) {
                  _context.next = 13;
                  break;
                }

                _context.next = 7;
                return fromUser.create({
                  id: "id0",
                  name: "user0"
                }, options);

              case 7:
                _context.next = 9;
                return fromUser.create({
                  id: "id1",
                  name: "user1"
                }, options);

              case 9:
                _context.next = 11;
                return fromUser.create({
                  id: "id2",
                  name: "user2"
                }, options);

              case 11:
                _context.next = 13;
                return fromUser.create({
                  id: "id3",
                  name: "user3"
                }, options);

              case 13:
                _context.next = 15;
                return setState({
                  isSeeded: true
                });

              case 15:
                return _context.abrupt("return", _context.sent);

              case 16:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      })
    );
  }
}];
exports.default = _default;
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _BlockPageFooterLinks = _interopRequireDefault(require("../../pageFooter/web/block/BlockPageFooterLinks"));

var _BlockPageFooterSocial = _interopRequireDefault(require("../../pageFooter/web/block/BlockPageFooterSocial"));

var _BlockPageHeaderLogo = _interopRequireDefault(require("../../pageHeader/web/block/BlockPageHeaderLogo"));

var _BlockPageHeaderMenu = _interopRequireDefault(require("../../pageHeader/web/block/BlockPageHeaderMenu"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// import BlockAuth            from 'app/default/auth/web/block/BlockAuth'
// import BlockFbAuth          from 'app/default/auth/web/block/BlockFbAuth'
// import BlockNotify          from 'app/default/notify/web/block/BlockNotify'
var _default = {
  default: {
    // BlockAuth,
    // BlockFbAuth,
    //
    // BlockNotify,
    BlockPageFooterLinks: _BlockPageFooterLinks.default,
    BlockPageFooterSocial: _BlockPageFooterSocial.default,
    BlockPageHeaderLogo: _BlockPageHeaderLogo.default,
    BlockPageHeaderMenu: _BlockPageHeaderMenu.default
  }
};
exports.default = _default;
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _config = _interopRequireDefault(require("../../config/api/config"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// import spaceNotify          from '../../notify/space/spaceNotify'
var _default = [_config.default.namespace, "nav", [// spaceNotify,
], function () {
  return {
    path: "/"
  };
}, {
  pathSet: function pathSet(getState, setState, methods, _ref) {
    var fromNotify = _ref.fromNotify;
    return (
      /*#__PURE__*/
      _regenerator.default.mark(function _callee(pathDirty) {
        var path;
        return _regenerator.default.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                path = pathDirty.replace("*", ""); // yield fromNotify.create({ message: `Redirected to ${path}` }, { orderName: `show` })

                _context.next = 3;
                return setState({
                  path: path
                });

              case 3:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      })
    );
  },
  go: function go(getState) {
    return (
      /*#__PURE__*/
      _regenerator.default.mark(function _callee2(destination, push) {
        var _ref2, path, pathname;

        return _regenerator.default.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _context2.prev = 0;
                _context2.next = 3;
                return getState();

              case 3:
                _ref2 = _context2.sent;
                path = _ref2.path;
                pathname = "".concat(path, "/").concat(destination).replace(/\/\/+/g, '/');
                push(pathname);
                _context2.next = 12;
                break;

              case 9:
                _context2.prev = 9;
                _context2.t0 = _context2["catch"](0);
                throw new Error("DESTINATION ".concat(destination));

              case 12:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, this, [[0, 9]]);
      })
    );
  }
}];
exports.default = _default;
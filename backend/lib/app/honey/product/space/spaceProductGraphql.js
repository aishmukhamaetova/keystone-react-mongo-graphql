"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _config = _interopRequireDefault(require("../../config/api/config"));

var _query = _interopRequireDefault(require("../api/graphql/query"));

var _queryById = _interopRequireDefault(require("../api/graphql/queryById"));

var _queryUniqueProposition = _interopRequireDefault(require("../api/graphql/queryUniqueProposition"));

var _spaceProduct = _interopRequireDefault(require("./spaceProduct"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _default = [_config.default.namespace, "productGraphql", [_spaceProduct.default], function () {
  return {
    index: 0,
    size: 10,
    isLoading: false,
    countPrev: null,
    badgeName: null,
    pairList: null,
    price: null,
    sweetness: null
  };
}, {
  badgeNameSet: function badgeNameSet(getState, setState) {
    return (
      /*#__PURE__*/
      _regenerator.default.mark(function _callee(badgeName) {
        return _regenerator.default.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.next = 2;
                return setState({
                  badgeName: badgeName
                });

              case 2:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      })
    );
  },
  paginationSet: function paginationSet(getState, setState) {
    return (
      /*#__PURE__*/
      _regenerator.default.mark(function _callee2(index, size) {
        return _regenerator.default.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _context2.next = 2;
                return setState({
                  index: index,
                  size: size
                });

              case 2:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, this);
      })
    );
  },
  applyFilter: function applyFilter(getState, setState, _ref, _ref2) {
    var badgeNameSet = _ref.badgeNameSet,
        query = _ref.query;
    var fromProduct = _ref2.fromProduct;
    return (
      /*#__PURE__*/
      _regenerator.default.mark(function _callee3(payload, options) {
        var badgeName, pairList, price, sweetness, client;
        return _regenerator.default.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                _context3.prev = 0;
                badgeName = payload.badgeName, pairList = payload.pairList, price = payload.price, sweetness = payload.sweetness;
                client = options.client;

                if (!(badgeName !== undefined)) {
                  _context3.next = 6;
                  break;
                }

                _context3.next = 6;
                return badgeNameSet(badgeName);

              case 6:
                if (!(pairList !== undefined)) {
                  _context3.next = 9;
                  break;
                }

                _context3.next = 9;
                return setState({
                  pairList: pairList
                });

              case 9:
                if (!(price !== undefined)) {
                  _context3.next = 12;
                  break;
                }

                _context3.next = 12;
                return setState({
                  price: price
                });

              case 12:
                if (!(sweetness !== undefined)) {
                  _context3.next = 15;
                  break;
                }

                _context3.next = 15;
                return setState({
                  sweetness: sweetness
                });

              case 15:
                _context3.next = 17;
                return setState({
                  index: 0,
                  size: 10
                });

              case 17:
                _context3.next = 19;
                return fromProduct.removeAll();

              case 19:
                _context3.next = 21;
                return query({}, {
                  client: client
                });

              case 21:
                _context3.next = 26;
                break;

              case 23:
                _context3.prev = 23;
                _context3.t0 = _context3["catch"](0);
                console.error(_context3.t0);

              case 26:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3, this, [[0, 23]]);
      })
    );
  },
  queryNext: function queryNext(getState, setState, _ref3) {
    var query = _ref3.query;
    return (
      /*#__PURE__*/
      _regenerator.default.mark(function _callee4(payload, options) {
        var _ref4, index;

        return _regenerator.default.wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                _context4.next = 2;
                return getState();

              case 2:
                _ref4 = _context4.sent;
                index = _ref4.index;
                _context4.next = 6;
                return setState({
                  index: index + 1
                });

              case 6:
                _context4.next = 8;
                return query(payload, options);

              case 8:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4, this);
      })
    );
  },
  query: function query(getState, setState, methods, _ref5) {
    var fromProduct = _ref5.fromProduct;
    return (
      /*#__PURE__*/
      _regenerator.default.mark(function _callee5(payload, options) {
        var client, state, index, size, badgeName, pairList, price, sweetness, _ref6, data, _iteratorNormalCompletion, _didIteratorError, _iteratorError, _iterator, _step, doc;

        return _regenerator.default.wrap(function _callee5$(_context5) {
          while (1) {
            switch (_context5.prev = _context5.next) {
              case 0:
                _context5.prev = 0;
                client = options.client;

                if (!client) {
                  _context5.next = 41;
                  break;
                }

                _context5.next = 5;
                return getState();

              case 5:
                state = _context5.sent;
                index = state.index, size = state.size, badgeName = state.badgeName, pairList = state.pairList, price = state.price, sweetness = state.sweetness;
                _context5.next = 9;
                return setState({
                  isLoading: true
                });

              case 9:
                _context5.next = 11;
                return client.query({
                  query: _query.default,
                  variables: {
                    badgeName: badgeName,
                    pairList: pairList,
                    pagination: {
                      index: index,
                      size: size
                    },
                    price: price,
                    sweetness: sweetness
                  }
                });

              case 11:
                _ref6 = _context5.sent;
                data = _ref6.data;
                // console.log(`DATA`, data)
                _iteratorNormalCompletion = true;
                _didIteratorError = false;
                _iteratorError = undefined;
                _context5.prev = 16;
                _iterator = data.product[Symbol.iterator]();

              case 18:
                if (_iteratorNormalCompletion = (_step = _iterator.next()).done) {
                  _context5.next = 25;
                  break;
                }

                doc = _step.value;
                _context5.next = 22;
                return fromProduct.create(doc);

              case 22:
                _iteratorNormalCompletion = true;
                _context5.next = 18;
                break;

              case 25:
                _context5.next = 31;
                break;

              case 27:
                _context5.prev = 27;
                _context5.t0 = _context5["catch"](16);
                _didIteratorError = true;
                _iteratorError = _context5.t0;

              case 31:
                _context5.prev = 31;
                _context5.prev = 32;

                if (!_iteratorNormalCompletion && _iterator.return != null) {
                  _iterator.return();
                }

              case 34:
                _context5.prev = 34;

                if (!_didIteratorError) {
                  _context5.next = 37;
                  break;
                }

                throw _iteratorError;

              case 37:
                return _context5.finish(34);

              case 38:
                return _context5.finish(31);

              case 39:
                _context5.next = 41;
                return setState({
                  isLoading: false,
                  countPrev: data.product.length
                });

              case 41:
                _context5.next = 48;
                break;

              case 43:
                _context5.prev = 43;
                _context5.t1 = _context5["catch"](0);
                _context5.next = 47;
                return setState({
                  isLoading: false
                });

              case 47:
                console.error(_context5.t1);

              case 48:
              case "end":
                return _context5.stop();
            }
          }
        }, _callee5, this, [[0, 43], [16, 27, 31, 39], [32,, 34, 38]]);
      })
    );
  },
  queryById: function queryById(getState, setState, methods, _ref7) {
    var fromProduct = _ref7.fromProduct;
    return (
      /*#__PURE__*/
      _regenerator.default.mark(function _callee6(_ref8, _ref9) {
        var id, client, _ref10, data, _iteratorNormalCompletion2, _didIteratorError2, _iteratorError2, _iterator2, _step2, doc;

        return _regenerator.default.wrap(function _callee6$(_context6) {
          while (1) {
            switch (_context6.prev = _context6.next) {
              case 0:
                id = _ref8.id;
                client = _ref9.client;
                _context6.prev = 2;

                if (!client) {
                  _context6.next = 38;
                  break;
                }

                _context6.next = 6;
                return setState({
                  isLoading: true
                });

              case 6:
                _context6.next = 8;
                return client.query({
                  query: _queryById.default,
                  variables: {
                    id: id
                  }
                });

              case 8:
                _ref10 = _context6.sent;
                data = _ref10.data;
                _iteratorNormalCompletion2 = true;
                _didIteratorError2 = false;
                _iteratorError2 = undefined;
                _context6.prev = 13;
                _iterator2 = data.product[Symbol.iterator]();

              case 15:
                if (_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done) {
                  _context6.next = 22;
                  break;
                }

                doc = _step2.value;
                _context6.next = 19;
                return fromProduct.create(doc);

              case 19:
                _iteratorNormalCompletion2 = true;
                _context6.next = 15;
                break;

              case 22:
                _context6.next = 28;
                break;

              case 24:
                _context6.prev = 24;
                _context6.t0 = _context6["catch"](13);
                _didIteratorError2 = true;
                _iteratorError2 = _context6.t0;

              case 28:
                _context6.prev = 28;
                _context6.prev = 29;

                if (!_iteratorNormalCompletion2 && _iterator2.return != null) {
                  _iterator2.return();
                }

              case 31:
                _context6.prev = 31;

                if (!_didIteratorError2) {
                  _context6.next = 34;
                  break;
                }

                throw _iteratorError2;

              case 34:
                return _context6.finish(31);

              case 35:
                return _context6.finish(28);

              case 36:
                _context6.next = 38;
                return setState({
                  isLoading: false
                });

              case 38:
                _context6.next = 45;
                break;

              case 40:
                _context6.prev = 40;
                _context6.t1 = _context6["catch"](2);
                _context6.next = 44;
                return setState({
                  isLoading: false
                });

              case 44:
                console.error(_context6.t1);

              case 45:
              case "end":
                return _context6.stop();
            }
          }
        }, _callee6, this, [[2, 40], [13, 24, 28, 36], [29,, 31, 35]]);
      })
    );
  },
  queryUnique: function queryUnique(getState, setState, methods, _ref11) {
    var fromProduct = _ref11.fromProduct;
    return (
      /*#__PURE__*/
      _regenerator.default.mark(function _callee7(client) {
        var _ref12, data, _iteratorNormalCompletion3, _didIteratorError3, _iteratorError3, _iterator3, _step3, doc;

        return _regenerator.default.wrap(function _callee7$(_context7) {
          while (1) {
            switch (_context7.prev = _context7.next) {
              case 0:
                _context7.prev = 0;

                if (!client) {
                  _context7.next = 38;
                  break;
                }

                _context7.next = 4;
                return setState({
                  isLoading: true
                });

              case 4:
                _context7.next = 6;
                return client.query({
                  query: _queryUniqueProposition.default
                });

              case 6:
                _ref12 = _context7.sent;
                data = _ref12.data;
                _context7.next = 10;
                return fromProduct.orderClear('unique');

              case 10:
                _iteratorNormalCompletion3 = true;
                _didIteratorError3 = false;
                _iteratorError3 = undefined;
                _context7.prev = 13;
                _iterator3 = data.product[Symbol.iterator]();

              case 15:
                if (_iteratorNormalCompletion3 = (_step3 = _iterator3.next()).done) {
                  _context7.next = 22;
                  break;
                }

                doc = _step3.value;
                _context7.next = 19;
                return fromProduct.create(doc, {
                  orderName: "unique"
                });

              case 19:
                _iteratorNormalCompletion3 = true;
                _context7.next = 15;
                break;

              case 22:
                _context7.next = 28;
                break;

              case 24:
                _context7.prev = 24;
                _context7.t0 = _context7["catch"](13);
                _didIteratorError3 = true;
                _iteratorError3 = _context7.t0;

              case 28:
                _context7.prev = 28;
                _context7.prev = 29;

                if (!_iteratorNormalCompletion3 && _iterator3.return != null) {
                  _iterator3.return();
                }

              case 31:
                _context7.prev = 31;

                if (!_didIteratorError3) {
                  _context7.next = 34;
                  break;
                }

                throw _iteratorError3;

              case 34:
                return _context7.finish(31);

              case 35:
                return _context7.finish(28);

              case 36:
                _context7.next = 38;
                return setState({
                  isLoading: false
                });

              case 38:
                _context7.next = 45;
                break;

              case 40:
                _context7.prev = 40;
                _context7.t1 = _context7["catch"](0);
                _context7.next = 44;
                return setState({
                  isLoading: false
                });

              case 44:
                console.error(_context7.t1);

              case 45:
              case "end":
                return _context7.stop();
            }
          }
        }, _callee7, this, [[0, 40], [13, 24, 28, 36], [29,, 31, 35]]);
      })
    );
  }
}];
exports.default = _default;
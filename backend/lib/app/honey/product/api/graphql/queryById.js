"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _graphqlTag = _interopRequireDefault(require("graphql-tag"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _templateObject() {
  var data = _taggedTemplateLiteral(["\nquery product($id: ID){\n\tproduct(\n\t  id: $id\n\t)\n\t{\n    id\n    sku\n    name\n    description\n    \n    sweetness\n    \n    badge {\n      id\n      name\n    }\n    \n    harvest {\n      id\n      name\n    }\n    \n    pairs {\n      id\n      name\n    }\n    \n    regions {\n      id\n      name\n    }\n    \n    categories {\n      id\n      name\n    }\n    \n    images {\n      id\n      name\n      image {\n        secure_url\n        url\n      }\n    }\n    \n    stores {\n      quantity\n      price\n      location {\n        name\n      }\n    }    \n    \n    videos {\n      id\n      name\n      videoLink\n    }\n    \n    isUniqueProposition\n  }\n}\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var _default = (0, _graphqlTag.default)(_templateObject());

exports.default = _default;
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _recompose = require("recompose");

var _polished = require("polished");

var _core = require("@material-ui/core");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _Image = _interopRequireDefault(require("../../image/web/Image"));

var _Video = _interopRequireDefault(require("../../video/web/Video"));

var _routes = require("../../page/api/routes");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

var map = {
  sm: 600,
  md: 960,
  lg: 1280,
  xl: 1920
};

var mediaGrid = function mediaGrid(type) {
  return function () {
    return "@media (max-width: ".concat(map[type] - 1, "px)");
  };
};

var enhance = (0, _recompose.compose)((0, _recompose.defaultProps)({
  A: {
    Container: _styledComponents.default.div.withConfig({
      displayName: "Product__Container",
      componentId: "sc-1hssgs7-0"
    })(["text-align:center;"]),
    ButtonBaseImg: (0, _styledComponents.default)(_core.FormControl).withConfig({
      displayName: "Product__ButtonBaseImg",
      componentId: "sc-1hssgs7-1"
    })(["&&{text-align:center;margin:", " auto;width:", ";justify:center;img{width:", ";height:auto;margin:auto;}", "{text-align:left;img{width:", ";height:auto;}}", "{text-align:center;img{width:", ";height:auto;}}}"], (0, _polished.rem)(20), (0, _polished.rem)(100), (0, _polished.rem)(100), mediaGrid("md"), (0, _polished.rem)(100), mediaGrid("sm"), (0, _polished.rem)(100)),
    ReadMore: (0, _styledComponents.default)(_core.Typography).withConfig({
      displayName: "Product__ReadMore",
      componentId: "sc-1hssgs7-2"
    })(["&&{text-align:center;color:#000;font-size:", ";line-height:1.4;font-weight:600;margin-bottom:", ";margin:", " auto;padding:inherit;a{text-decoration:underline;color:#000;font-weight:600;&:hover{text-decoration:none;}}}"], (0, _polished.rem)(10), (0, _polished.rem)(18), (0, _polished.rem)(15)),
    SubTitle: (0, _styledComponents.default)(_core.Typography).withConfig({
      displayName: "Product__SubTitle",
      componentId: "sc-1hssgs7-3"
    })(["&&{font-size:", ";text-align:center;width:", ";}"], (0, _polished.rem)(14), (0, _polished.rem)(180)),
    Title: (0, _styledComponents.default)(_core.Typography).withConfig({
      displayName: "Product__Title",
      componentId: "sc-1hssgs7-4"
    })(["&&{font-weight:700;font-size:", ";line-height:normal;border-bottom:1px solid;padding-bottom:", ";margin-bottom:", ";color:rgba(0,0,0,0.87);border-color:rgba(0,0,0,0.87);text-transform:uppercase;a{color:#000;}}"], (0, _polished.rem)(17), (0, _polished.rem)(7), (0, _polished.rem)(13))
  }
}), (0, _recompose.withProps)(function (_ref) {
  var _ref$description = _ref.description,
      description = _ref$description === void 0 ? '' : _ref$description;
  return {
    descriptionSplitted: description.split(' ', 10).join(' ') + '...'
  };
}));
var Product = enhance(function (_ref2) {
  var id = _ref2.id,
      children = _ref2.children,
      sku = _ref2.sku,
      name = _ref2.name,
      description = _ref2.description,
      _ref2$images = _ref2.images,
      images = _ref2$images === void 0 ? [] : _ref2$images,
      _ref2$videos = _ref2.videos,
      videos = _ref2$videos === void 0 ? [] : _ref2$videos,
      A = _ref2.A,
      key = _ref2.key,
      descriptionSplitted = _ref2.descriptionSplitted;
  return _react.default.createElement(A.Container, null, _react.default.createElement(A.Title, null, _react.default.createElement(_routes.Link, {
    route: "product",
    params: {
      id: id
    }
  }, _react.default.createElement("a", null, name))), _react.default.createElement(_core.FormControl, null, _react.default.createElement(A.SubTitle, {
    variant: "caption"
  }, descriptionSplitted), _react.default.createElement(A.ReadMore, {
    variant: "button",
    gutterBottom: true
  }, _react.default.createElement(_routes.Link, {
    route: "product",
    params: {
      id: id
    }
  }, _react.default.createElement("a", null, "Read more"))), _react.default.createElement(A.ButtonBaseImg, null, images.map(function (doc, index) {
    return index === 0 ? _react.default.createElement(_Image.default, _extends({
      key: doc.id
    }, doc)) : null;
  })), children));
});
var _default = Product;
exports.default = _default;
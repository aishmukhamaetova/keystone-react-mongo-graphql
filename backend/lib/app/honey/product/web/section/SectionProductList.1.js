"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _core = require("@material-ui/core");

var _recompose = require("recompose");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _Block = _interopRequireDefault(require("../../../../../evoke-me/layout/web/Block"));

var _BlockProductList = _interopRequireDefault(require("../block/BlockProductList"));

var _polished = require("polished");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

var enhance = (0, _recompose.compose)((0, _recompose.defaultProps)({
  A: {
    Container: _styledComponents.default.div.withConfig({
      displayName: "SectionProductList1__Container",
      componentId: "qjkzkl-0"
    })(["&&{margin-top:", ";}"], (0, _polished.rem)(40))
  }
}));
var SectionProductList = enhance(function (_ref) {
  var A = _ref.A;
  return _react.default.createElement(_react.default.Fragment, null, _react.default.createElement(SectionFilter, null), _react.default.createElement(A.Container, null, _react.default.createElement(_core.Grid, {
    container: true,
    direction: 'row',
    justify: "center",
    alignItems: 'center',
    spacing: 0
  }, _react.default.createElement(_core.Grid, {
    xs: 10,
    item: true
  }, _react.default.createElement(_core.Grid, {
    container: true,
    direction: 'row',
    justify: "center",
    alignItems: 'center',
    spacing: 40
  })))));
});
var _default = SectionProductList;
exports.default = _default;
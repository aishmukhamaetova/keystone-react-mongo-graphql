"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _core = require("@material-ui/core");

var _polished = require("polished");

var _react = _interopRequireDefault(require("react"));

var _compose = _interopRequireDefault(require("recompose/compose"));

var _defaultProps = _interopRequireDefault(require("recompose/defaultProps"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _BlockProductList = _interopRequireDefault(require("../block/BlockProductList"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _default = (0, _compose.default)((0, _defaultProps.default)({
  A: {
    SectionProductList: _styledComponents.default.div.withConfig({
      displayName: "SectionProductList",
      componentId: "sc-12f1qan-0"
    })(["margin-top:", ";"], (0, _polished.rem)(40))
  }
}))(function (props) {
  var A = props.A;
  return _react.default.createElement(A.SectionProductList, null, _react.default.createElement(_core.Grid, {
    container: true,
    direction: "row",
    justify: "center",
    alignItems: "center",
    spacing: 0
  }, _react.default.createElement(_core.Grid, {
    xs: 10,
    item: true
  }, _react.default.createElement(_core.Grid, {
    container: true,
    direction: "row",
    justify: "center",
    alignItems: "flex-start",
    spacing: 40
  }, _react.default.createElement(_BlockProductList.default, null)))));
});

exports.default = _default;
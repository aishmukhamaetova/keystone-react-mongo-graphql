"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _compose = _interopRequireDefault(require("recompose/compose"));

var _defaultProps = _interopRequireDefault(require("recompose/defaultProps"));

var _withStateHandlers = _interopRequireDefault(require("recompose/withStateHandlers"));

var _core = require("@material-ui/core");

var _Slider = _interopRequireDefault(require("@material-ui/lab/Slider"));

var _icons = require("../../icons");

var _polished = require("polished");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _customUI = require("../../customUI");

var _reactPose = _interopRequireDefault(require("react-pose"));

var _posed$div;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

function _objectDestructuringEmpty(obj) { if (obj == null) throw new TypeError("Cannot destructure undefined"); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var poseMap = {
  OPEN: "open",
  CLOSE: "close"
};

var PairingFieldSet = _styledComponents.default.div.withConfig({
  displayName: "SectionFilter1__PairingFieldSet",
  componentId: "sc-3boolk-0"
})(["&&{display:flex;flex-direction:row;justify-content:space-between;align-items:center;width:100%;font-size:", ";margin-top:", ";lable{font-size:inherit;}span{font-size:inherit;}}"], (0, _polished.rem)(13), (0, _polished.rem)(-14));

var SliderFooter = _styledComponents.default.div.withConfig({
  displayName: "SectionFilter1__SliderFooter",
  componentId: "sc-3boolk-1"
})(["&&{display:flex;flex-direction:row;justify-content:space-between;align-items:center;width:100%;font-size:", ";p{font-size:inherit;&.left{font-style:italic;font-weight:normal;font-stretch:normal;line-height:normal;letter-spacing:normal;padding-top:0.40rem;font-size:", ";}&.right{text-align:right;font-style:italic;font-weight:normal;font-stretch:normal;line-height:normal;letter-spacing:normal;padding-top:0.40rem;font-size:", ";}}}"], (0, _polished.rem)(13), (0, _polished.rem)(10), (0, _polished.rem)(10));

var Content = (0, _styledComponents.default)(_reactPose.default.div((_posed$div = {}, _defineProperty(_posed$div, poseMap.CLOSE, {
  height: 0
}), _defineProperty(_posed$div, poseMap.OPEN, {
  height: 'auto'
}), _posed$div))).withConfig({
  displayName: "SectionFilter1__Content",
  componentId: "sc-3boolk-2"
})(["height:0;overflow:hidden;"]);
var enhance = (0, _compose.default)((0, _withStateHandlers.default)(function () {
  return {
    isOpen: false,
    sweetness: 0,
    texture: 0,
    price: 0,
    tabValue: 0,
    pairing: {},
    error: null,
    country: {
      'Lebanon': true
    }
  };
}, {
  onClickFilter: function onClickFilter(_ref) {
    var isOpen = _ref.isOpen;
    return function () {
      return {
        isOpen: !isOpen
      };
    };
  },
  handleSweetnessChange: function handleSweetnessChange(_ref2) {
    _objectDestructuringEmpty(_ref2);

    return function (sweetness) {
      return {
        sweetness: sweetness
      };
    };
  },
  handlePriceChange: function handlePriceChange(_ref3) {
    _objectDestructuringEmpty(_ref3);

    return function (price) {
      return {
        price: price
      };
    };
  },
  handleTextureChange: function handleTextureChange(_ref4) {
    _objectDestructuringEmpty(_ref4);

    return function (texture) {
      return {
        texture: texture
      };
    };
  },
  handleTabsChange: function handleTabsChange() {
    return function (tabValue) {
      return {
        tabValue: tabValue
      };
    };
  },
  handleChangePairing: function handleChangePairing() {
    return function (event, name) {
      var pairing = {};
      pairing[name] = event.target && event.target.checked;
      return {
        pairing: pairing
      };
    };
  }
}), (0, _defaultProps.default)({
  A: {
    FormControlLabel: (0, _styledComponents.default)(_core.FormControlLabel).withConfig({
      displayName: "SectionFilter1__FormControlLabel",
      componentId: "sc-3boolk-3"
    })(["&&{margin:auto;.label{font-size:", ";display:flex;}},"], (0, _polished.rem)(14)),
    Tab: (0, _styledComponents.default)(_core.Tab).withConfig({
      displayName: "SectionFilter1__Tab",
      componentId: "sc-3boolk-4"
    })(["&&{font-weight:normal;font-size:", ";text-transform:uppercase;opacity:inherit;min-width:inherit;width:50%;height:", ";&:visited{color:#000000;font-weight:bold;}&:hover{color:#000000;font-weight:bold;background-color:#d8d8d8;}&:active{font-weight:bold;color:#000000;}&:first-child{border-right:1px solid rgba(0,0,0,0.2);}}"], (0, _polished.rem)(14), (0, _polished.rem)(48)),
    Tabs: (0, _styledComponents.default)(_core.Tabs).withConfig({
      displayName: "SectionFilter1__Tabs",
      componentId: "sc-3boolk-5"
    })(["&&{width:100%;margin-top:10px;margin-bottom:10px;background-color:#d8d8d8;opacity:0.21;}"]),
    Search: (0, _styledComponents.default)(_core.FormGroup).withConfig({
      displayName: "SectionFilter1__Search",
      componentId: "sc-3boolk-6"
    })(["&&{margin:auto ", ";", "{margin:auto ", ";}", "{margin:auto ", ";}", "{margin:auto ", ";}", "{margin:", " 30%;}}"], (0, _polished.rem)(30), (0, _customUI.mediaGrid)("xl"), (0, _polished.rem)(20), (0, _customUI.mediaGrid)("lg"), (0, _polished.rem)(30), (0, _customUI.mediaGrid)("md"), (0, _polished.rem)(80), (0, _customUI.mediaGrid)("sm"), (0, _polished.rem)(20)),
    InputBase: (0, _styledComponents.default)(function (_ref5) {
      var fontSize = _ref5.fontSize,
          margin = _ref5.margin,
          other = _objectWithoutProperties(_ref5, ["fontSize", "margin"]);

      return _react.default.createElement(_core.InputBase, other);
    }).withConfig({
      displayName: "SectionFilter1__InputBase",
      componentId: "sc-3boolk-7"
    })(["&&{font-size:", ";margin:auto;background:none;", "{background:#d8d8d8;height:", ";padding:", ";}button{margin-right:", ";border-radius:100px;overflow:hiddne;padding:", ";}}"], (0, _polished.rem)(15), (0, _customUI.mediaGrid)("sm"), (0, _polished.rem)(48), (0, _polished.rem)(5), (0, _polished.rem)(5), (0, _polished.rem)(5)),
    Checkbox: (0, _styledComponents.default)(function (_ref6) {
      var fontSize = _ref6.fontSize,
          margin = _ref6.margin,
          other = _objectWithoutProperties(_ref6, ["fontSize", "margin"]);

      return _react.default.createElement(_core.Checkbox, other);
    }).withConfig({
      displayName: "SectionFilter1__Checkbox",
      componentId: "sc-3boolk-8"
    })(["&&{&.checkbox_color{color:black;}}"]),
    Typography: _styledComponents.default.div.withConfig({
      displayName: "SectionFilter1__Typography",
      componentId: "sc-3boolk-9"
    })(["&&{&.sweetness{font-size:", ";font-weight:bold;padding-bottom:0.5rem;padding-top:0.5rem;font-family:Open Sans,sans-serif;}&.texture{font-size:", ";font-weight:bold;padding-bottom:0.5rem;padding-top:0.5rem;font-family:Open Sans,sans-serif;}&.price{font-size:", ";font-weight:bold;padding-bottom:0.5rem;padding-top:0.5rem;font-family:Open Sans,sans-serif;}&.pairing{font-size:", ";font-weight:bold;padding-top:0.5rem;font-family:Open Sans,sans-serif;}&.country{font-size:", ";font-weight:bold;padding-top:0.5rem;font-family:Open Sans,sans-serif;}}"], (0, _polished.rem)(10), (0, _polished.rem)(10), (0, _polished.rem)(10), (0, _polished.rem)(10), (0, _polished.rem)(10)),
    IconSearch: (0, _styledComponents.default)(function (_ref7) {
      var other = _extends({}, _ref7);

      return _react.default.createElement(_icons.IconSearch, other);
    }).withConfig({
      displayName: "SectionFilter1__IconSearch",
      componentId: "sc-3boolk-10"
    })(["font-size:inherit;"])
  }
}));
var Filter = enhance(function (_ref8) {
  var isOpen = _ref8.isOpen,
      A = _ref8.A,
      onClickFilter = _ref8.onClickFilter,
      handleSweetnessChange = _ref8.handleSweetnessChange,
      handleTextureChange = _ref8.handleTextureChange,
      handlePriceChange = _ref8.handlePriceChange,
      sweetness = _ref8.sweetness,
      texture = _ref8.texture,
      price = _ref8.price,
      tabValue = _ref8.tabValue,
      handleTabsChange = _ref8.handleTabsChange,
      handleChangePairing = _ref8.handleChangePairing,
      pairing = _ref8.pairing,
      error = _ref8.error,
      country = _ref8.country;
  return _react.default.createElement(_react.default.Fragment, null, _react.default.createElement(_core.Grid, {
    container: true,
    direction: 'row',
    justify: 'center',
    alignItems: 'center',
    spacing: 0
  }, _react.default.createElement(_core.Grid, {
    xs: 10,
    item: true
  }, _react.default.createElement(_core.Grid, {
    container: true,
    direction: "row",
    justify: "center",
    alignItems: "center",
    spacing: 0
  }, _react.default.createElement(_core.Grid, {
    xs: 12,
    sm: 4,
    md: 4,
    item: true
  }, _react.default.createElement(_core.FormGroup, {
    row: true
  }, _react.default.createElement(A.FormControlLabel, {
    control: _react.default.createElement(_core.Switch, {
      value: isOpen,
      checked: isOpen,
      onChange: onClickFilter,
      color: "default"
    }),
    label: _react.default.createElement("span", {
      className: 'label'
    }, "Filter")
  }))), _react.default.createElement(_core.Grid, {
    xs: 12,
    sm: 4,
    md: 4,
    item: true
  }, _react.default.createElement(A.Tabs, {
    fullWidth: true,
    value: tabValue,
    onChange: function onChange(e, tabValue) {
      return handleTabsChange(tabValue);
    }
  }, _react.default.createElement(A.Tab, {
    fullWidth: true,
    label: "must have"
  }), _react.default.createElement(A.Tab, {
    fullWidth: true,
    label: "list all"
  }))), _react.default.createElement(_core.Grid, {
    xs: 12,
    sm: 4,
    md: 4,
    item: true
  }, _react.default.createElement(A.Search, {
    row: true
  }, _react.default.createElement(A.InputBase, {
    autoFocus: true,
    startAdornment: _react.default.createElement(A.IconSearch, {
      disableripple: "true",
      disabletouchripple: "true",
      style: {
        fontSize: 15
      }
    }),
    placeholder: "Search"
  })))))), _react.default.createElement(Content, {
    pose: isOpen ? 'open' : 'close'
  }, _react.default.createElement(_core.Grid, {
    container: true,
    direction: 'row',
    justify: 'center',
    alignItems: 'center',
    spacing: 0
  }, _react.default.createElement(_core.Grid, {
    xs: 8,
    item: true
  }, _react.default.createElement(_core.Grid, {
    container: true,
    direction: "row",
    justify: "center",
    alignItems: "flex-start",
    spacing: 0
  }, _react.default.createElement(_core.Grid, {
    xs: 4,
    item: true
  }, _react.default.createElement(_core.FormGroup, {
    row: true
  }, _react.default.createElement(_core.Typography, {
    id: "sweetness"
  }, "Sweetness"), _react.default.createElement(_Slider.default, {
    value: sweetness,
    "aria-labelledby": "sweetness",
    onChange: function onChange(event, value) {
      return handleSweetnessChange(value);
    }
  }), _react.default.createElement(SliderFooter, null, _react.default.createElement(_core.Typography, {
    className: 'left',
    id: "sweetness"
  }, "Least"), _react.default.createElement(_core.Typography, {
    className: 'right',
    id: "sweetness"
  }, "Most"))), _react.default.createElement(_core.FormGroup, {
    row: true
  }, _react.default.createElement(A.Typography, {
    className: 'texture',
    id: "texture"
  }, "TEXTURE"), _react.default.createElement(_Slider.default, {
    value: texture,
    "aria-labelledby": "texture",
    onChange: function onChange(event, value) {
      return handleTextureChange(value);
    }
  }), _react.default.createElement(SliderFooter, null, _react.default.createElement(_core.Typography, {
    className: 'left',
    id: "sweetness"
  }, "Fine"), _react.default.createElement(_core.Typography, {
    className: 'right',
    id: "sweetness"
  }, "Thick"))), _react.default.createElement(_core.FormGroup, {
    row: true
  }, _react.default.createElement(A.Typography, {
    className: 'price',
    id: "price"
  }, "PRICE"), _react.default.createElement(_Slider.default, {
    value: price,
    "aria-labelledby": "price",
    onChange: function onChange(event, value) {
      return handlePriceChange(value);
    }
  }), _react.default.createElement(SliderFooter, null, _react.default.createElement(_core.Typography, {
    className: 'left',
    id: "sweetness"
  }, "$"), _react.default.createElement(_core.Typography, {
    className: 'right',
    id: "sweetness"
  }, "$$$")))), _react.default.createElement(_core.Grid, {
    xs: 12,
    sm: 10,
    md: 4,
    item: true
  }, _react.default.createElement(A.Typography, {
    className: "pairing",
    id: "price"
  }, "PAIRING"), _react.default.createElement(PairingFieldSet, null, _react.default.createElement(_core.FormControl, {
    component: "fieldset",
    required: true,
    error: error
  }, _react.default.createElement(_core.FormGroup, null, _react.default.createElement(_core.FormControlLabel, {
    control: _react.default.createElement(A.Checkbox, {
      className: "checkbox_color",
      checked: pairing['cheese'],
      onChange: function onChange(event) {
        return handleChangePairing(event, 'cheese');
      },
      value: "cheese"
    }),
    label: "Cheese"
  }), _react.default.createElement(_core.FormControlLabel, {
    control: _react.default.createElement(A.Checkbox, {
      className: "checkbox_color",
      checked: pairing['food'],
      onChange: function onChange(event) {
        return handleChangePairing(event, 'food');
      },
      value: "food"
    }),
    label: "Food"
  }), _react.default.createElement(_core.FormControlLabel, {
    control: _react.default.createElement(A.Checkbox, {
      className: "checkbox_color",
      checked: pairing['breakfast'],
      onChange: function onChange(event) {
        return handleChangePairing(event, 'breakfast');
      },
      value: "breakfast"
    }),
    label: "Breakfast"
  }))), _react.default.createElement(_core.FormControl, {
    component: "fieldset",
    required: true,
    error: error
  }, _react.default.createElement(_core.FormGroup, null, _react.default.createElement(_core.FormControlLabel, {
    control: _react.default.createElement(A.Checkbox, {
      className: "checkbox_color",
      checked: pairing['cheese'],
      onChange: function onChange(event) {
        return handleChangePairing(event, 'cheese');
      },
      value: "cheese"
    }),
    label: "Cheese"
  }), _react.default.createElement(_core.FormControlLabel, {
    control: _react.default.createElement(A.Checkbox, {
      className: "checkbox_color",
      checked: pairing['food'],
      onChange: function onChange(event) {
        return handleChangePairing(event, 'food');
      },
      value: "food"
    }),
    label: "Food"
  }), _react.default.createElement(_core.FormControlLabel, {
    control: _react.default.createElement(A.Checkbox, {
      className: "checkbox_color",
      checked: pairing['breakfast'],
      onChange: function onChange(event) {
        return handleChangePairing(event, 'breakfast');
      },
      value: "breakfast"
    }),
    label: "Breakfast"
  }))))), _react.default.createElement(_core.Grid, {
    xs: 12,
    sm: 10,
    dm: 4,
    item: true
  }, _react.default.createElement(A.Typography, {
    className: "country",
    id: "country"
  }, "COUNTRY"), _react.default.createElement(PairingFieldSet, null, _react.default.createElement(_core.FormControl, {
    component: "fieldset",
    required: true,
    error: error
  }, _react.default.createElement(_core.FormGroup, null, _react.default.createElement(_core.FormControlLabel, {
    value: country['Lebanon'],
    control: _react.default.createElement(_core.Radio, {
      color: "primary"
    }),
    label: "Lebanon  \uD83C\uDDF1\uD83C\uDDE7",
    labelPlacement: "end"
  }), _react.default.createElement(_core.FormControlLabel, {
    value: "disabled",
    disabled: true,
    control: _react.default.createElement(_core.Radio, null),
    label: "France  \uD83C\uDDEB\uD83C\uDDF7",
    labelPlacement: "end"
  }), _react.default.createElement(_core.FormControlLabel, {
    value: "disabled",
    disabled: true,
    control: _react.default.createElement(_core.Radio, null),
    label: "Italy  \uD83C\uDDEE\uD83C\uDDF9",
    labelPlacement: "end"
  }))), _react.default.createElement(_core.FormControl, {
    component: "fieldset",
    required: true,
    error: error
  }, _react.default.createElement(_core.FormGroup, null, _react.default.createElement(_core.FormControlLabel, {
    value: "disabled",
    disabled: true,
    control: _react.default.createElement(_core.Radio, null),
    label: "Kuwait  \uD83C\uDDF1\uD83C\uDDE7",
    labelPlacement: "end"
  }), _react.default.createElement(_core.FormControlLabel, {
    value: "disabled",
    disabled: true,
    control: _react.default.createElement(_core.Radio, null),
    label: "Spain  \uD83C\uDDEA\uD83C\uDDF8",
    labelPlacement: "end"
  }), _react.default.createElement(_core.FormControlLabel, {
    value: "disabled",
    disabled: true,
    control: _react.default.createElement(_core.Radio, null),
    label: "Scotland  \uD83C\uDFF4\uDB40\uDC67\uDB40\uDC62\uDB40\uDC73\uDB40\uDC63\uDB40\uDC74\uDB40\uDC7F",
    labelPlacement: "end"
  }))))))))));
});
var _default = Filter;
exports.default = _default;
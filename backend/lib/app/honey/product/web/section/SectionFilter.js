"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _core = require("@material-ui/core");

var _styles = require("@material-ui/core/styles");

var _polished = require("polished");

var _react = _interopRequireDefault(require("react"));

var _reactApollo = require("react-apollo");

var _compose = _interopRequireDefault(require("recompose/compose"));

var _defaultProps = _interopRequireDefault(require("recompose/defaultProps"));

var _withHandlers = _interopRequireDefault(require("recompose/withHandlers"));

var _withStateHandlers = _interopRequireDefault(require("recompose/withStateHandlers"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _icons = require("../../../icons");

var _spaceProductGraphql = _interopRequireDefault(require("../../space/spaceProductGraphql"));

var _withSpace = _interopRequireDefault(require("../../../../../evoke-me/space/all/withSpace"));

var _SectionExpandedFilter = _interopRequireDefault(require("./SectionExpandedFilter"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

var map = {
  sm: 600,
  md: 960,
  lg: 1280,
  xl: 1920
};

var mediaGrid = function mediaGrid(type) {
  return function () {
    return "@media (max-width: ".concat(map[type] - 1, "px)");
  };
};

var _default = (0, _compose.default)(_reactApollo.withApollo, (0, _withSpace.default)(_spaceProductGraphql.default), (0, _withStateHandlers.default)(function () {
  return {
    isOpen: false,
    tabValue: 1,
    error: null
  };
}, {
  onClickFilter: function onClickFilter(_ref) {
    var isOpen = _ref.isOpen;
    return function () {
      return {
        isOpen: !isOpen
      };
    };
  },
  tabValueSet: function tabValueSet() {
    return function (tabValue) {
      return {
        tabValue: tabValue
      };
    };
  }
}), (0, _withHandlers.default)({
  onTabChange: function onTabChange(_ref2) {
    var client = _ref2.client,
        tabValueSet = _ref2.tabValueSet,
        fromProductGraphql = _ref2.fromProductGraphql;
    return function (e, tabValue) {
      tabValueSet(tabValue);

      if (tabValue === 0) {
        fromProductGraphql.applyFilter({
          badgeName: "MUST HAVE"
        }, {
          client: client
        });
      } else {
        fromProductGraphql.applyFilter({
          badgeName: null
        }, {
          client: client
        });
      }
    };
  }
}), (0, _defaultProps.default)({
  A: {
    Container: _styledComponents.default.div.withConfig({
      displayName: "SectionFilter__Container",
      componentId: "sc-1x8kcn2-0"
    })(["position:relative;"]),
    FormControlLabel: (0, _styledComponents.default)(_core.FormControlLabel).withConfig({
      displayName: "SectionFilter__FormControlLabel",
      componentId: "sc-1x8kcn2-1"
    })(["&&{margin:auto;.label{font-size:", ";display:flex;}},"], (0, _polished.rem)(14)),
    Tab: (0, _styledComponents.default)(_core.Tab).withConfig({
      displayName: "SectionFilter__Tab",
      componentId: "sc-1x8kcn2-2"
    })(["&&{font-weight:normal;font-size:", ";text-transform:uppercase;min-width:inherit;width:50%;height:", ";&:first-child{border-right:1px solid rgba(0,0,0,0.2);}}"], (0, _polished.rem)(14), (0, _polished.rem)(48)),
    Tabs: (0, _styledComponents.default)(_core.Tabs).withConfig({
      displayName: "SectionFilter__Tabs",
      componentId: "sc-1x8kcn2-3"
    })(["&&{width:100%;margin-top:", ";margin-bottom:", ";background-color:#eaeaea;}"], (0, _polished.rem)(10), (0, _polished.rem)(10)),
    Search: (0, _styledComponents.default)(_core.FormGroup).withConfig({
      displayName: "SectionFilter__Search",
      componentId: "sc-1x8kcn2-4"
    })(["&&{margin:auto ", ";", "{margin:auto ", ";}", "{margin:auto ", ";}", "{margin:auto ", ";}", "{margin:", " 30%;}}"], (0, _polished.rem)(30), mediaGrid("xl"), (0, _polished.rem)(20), mediaGrid("lg"), (0, _polished.rem)(30), mediaGrid("md"), (0, _polished.rem)(80), mediaGrid("sm"), (0, _polished.rem)(20)),
    InputBase: (0, _styledComponents.default)(function (_ref3) {
      var fontSize = _ref3.fontSize,
          margin = _ref3.margin,
          other = _objectWithoutProperties(_ref3, ["fontSize", "margin"]);

      return _react.default.createElement(_core.InputBase, other);
    }).withConfig({
      displayName: "SectionFilter__InputBase",
      componentId: "sc-1x8kcn2-5"
    })(["&&{font-size:", ";margin:auto;background:none;", "{background:#d8d8d8;height:", ";padding:", ";}button{margin-right:", ";border-radius:100px;overflow:hidden;padding:", ";}}"], (0, _polished.rem)(15), mediaGrid("sm"), (0, _polished.rem)(48), (0, _polished.rem)(5), (0, _polished.rem)(5), (0, _polished.rem)(5)),
    IconSearch: (0, _styledComponents.default)(function (_ref4) {
      var other = _extends({}, _ref4);

      return _react.default.createElement(_icons.IconSearch, other);
    }).withConfig({
      displayName: "SectionFilter__IconSearch",
      componentId: "sc-1x8kcn2-6"
    })(["font-size:inherit;"])
  }
}), (0, _styles.withStyles)(function (theme) {
  return {
    indicator: {
      backgroundColor: 'rgba(0,0,0,0)'
    },
    tabSelected: {},
    tabRoot: {
      textTransform: 'initial',
      fontWeight: 300,
      '&:hover': {
        color: '#000',
        opacity: 1,
        fontWeight: 600
      },
      '&$tabSelected': {
        color: '#000',
        opacity: 1,
        fontWeight: 600
      },
      '&:focus': {
        opacity: 1,
        color: '#000',
        fontWeight: 600
      }
    }
  };
}))(function (_ref5) {
  var isOpen = _ref5.isOpen,
      A = _ref5.A,
      onClickFilter = _ref5.onClickFilter,
      tabValue = _ref5.tabValue,
      onTabChange = _ref5.onTabChange,
      classes = _ref5.classes;
  return _react.default.createElement(A.Container, null, _react.default.createElement(_core.Grid, {
    container: true,
    direction: 'row',
    justify: 'center',
    alignItems: 'center',
    spacing: 0
  }, _react.default.createElement(_core.Grid, {
    xs: 10,
    item: true
  }, _react.default.createElement(_core.Grid, {
    container: true,
    direction: "row",
    justify: "center",
    alignItems: "center",
    spacing: 0
  }, _react.default.createElement(_core.Grid, {
    xs: 12,
    sm: 4,
    md: 4,
    item: true
  }, _react.default.createElement(_core.FormGroup, {
    row: true
  }, _react.default.createElement(A.FormControlLabel, {
    control: _react.default.createElement(_core.Switch, {
      value: isOpen,
      checked: isOpen,
      onChange: onClickFilter,
      color: "default"
    }),
    label: _react.default.createElement("span", {
      className: 'label'
    }, "Filter")
  }))), _react.default.createElement(_core.Grid, {
    xs: 12,
    sm: 4,
    md: 4,
    item: true
  }, _react.default.createElement(A.Tabs, {
    fullWidth: true,
    value: tabValue,
    onChange: onTabChange,
    classes: {
      indicator: classes.indicator
    }
  }, _react.default.createElement(A.Tab, {
    fullWidth: true,
    classes: {
      selected: classes.tabSelected,
      root: classes.tabRoot
    },
    label: "must have"
  }), _react.default.createElement(A.Tab, {
    fullWidth: true,
    classes: {
      selected: classes.tabSelected,
      root: classes.tabRoot
    },
    label: "list all"
  }))), _react.default.createElement(_core.Grid, {
    xs: 12,
    sm: 4,
    md: 4,
    item: true
  }, _react.default.createElement(A.Search, {
    row: true
  }, _react.default.createElement(A.InputBase, {
    autoFocus: true,
    startAdornment: _react.default.createElement(A.IconSearch, {
      disableripple: "true",
      disabletouchripple: "true",
      style: {
        fontSize: 15
      }
    }),
    placeholder: "Search"
  })))))), _react.default.createElement(_SectionExpandedFilter.default, {
    isOpen: isOpen
  }));
});

exports.default = _default;
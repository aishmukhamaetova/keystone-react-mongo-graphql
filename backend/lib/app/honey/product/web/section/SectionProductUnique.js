"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _reactGridSystem = require("react-grid-system");

var _compose = _interopRequireDefault(require("recompose/compose"));

var _defaultProps = _interopRequireDefault(require("recompose/defaultProps"));

var _withPropsOnChange = _interopRequireDefault(require("recompose/withPropsOnChange"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _core = require("@material-ui/core");

var _polished = require("polished");

var _BlockProductUnique = _interopRequireDefault(require("../block/BlockProductUnique"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

var enhance = (0, _compose.default)((0, _defaultProps.default)({
  A: {
    Container: _styledComponents.default.div.withConfig({
      displayName: "SectionProductUnique__Container",
      componentId: "sc-16h3epw-0"
    })(["display:inline-flex;text-align:center;margin:0 auto;"])
  }
}));
var SectionProductList = enhance(function (_ref) {
  var A = _ref.A;
  return _react.default.createElement(_core.Grid, {
    container: true,
    direction: 'row',
    justify: 'center',
    alignItems: 'center',
    spacing: 0
  }, _react.default.createElement(_core.Grid, {
    item: true,
    xs: 8,
    sm: 5
  }, _react.default.createElement(A.Container, null, _react.default.createElement(_BlockProductUnique.default, null))));
});
var _default = SectionProductList;
exports.default = _default;
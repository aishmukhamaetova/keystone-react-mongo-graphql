"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _reactApollo = require("react-apollo");

var _compose = _interopRequireDefault(require("recompose/compose"));

var _defaultProps = _interopRequireDefault(require("recompose/defaultProps"));

var _withHandlers = _interopRequireDefault(require("recompose/withHandlers"));

var _withStateHandlers = _interopRequireDefault(require("recompose/withStateHandlers"));

var _core = require("@material-ui/core");

var _Slider = _interopRequireDefault(require("@material-ui/lab/Slider"));

var _polished = require("polished");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _reactPose = _interopRequireDefault(require("react-pose"));

var _styles = require("@material-ui/core/styles");

var _spaceProductGraphql = _interopRequireDefault(require("../../space/spaceProductGraphql"));

var _withSpace = _interopRequireDefault(require("../../../../../evoke-me/space/all/withSpace"));

var _posed$div;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var poseMap = {
  OPEN: "open",
  CLOSE: "close"
};
var cntrMap = {
  Lebanon: "\uD83C\uDDF1\uD83C\uDDE7",
  France: "\uD83C\uDDEB\uD83C\uDDF7",
  Italy: "\uD83C\uDDEE\uD83C\uDDF9",
  Kuwait: "\uD83C\uDDF1\uD83C\uDDE7",
  Spain: "\uD83C\uDDEA\uD83C\uDDF8",
  Scotland: "\uD83C\uDFF4"
};
var Content = (0, _styledComponents.default)(_reactPose.default.div((_posed$div = {}, _defineProperty(_posed$div, poseMap.CLOSE, {
  height: 0,
  paddingBottom: 0
}), _defineProperty(_posed$div, poseMap.OPEN, {
  height: 'auto',
  paddingBottom: 30
}), _posed$div))).withConfig({
  displayName: "SectionExpandedFilter__Content",
  componentId: "q7fg7y-0"
})(["height:0;overflow:hidden;padding-bottom:0;"]);
var enhance = (0, _compose.default)(_reactApollo.withApollo, (0, _withSpace.default)(_spaceProductGraphql.default), (0, _withStateHandlers.default)(function () {
  return {
    slider: {},
    pairing: {},
    country: {
      'Lebanon': true
    }
  };
}, {
  sliderSetItem: function sliderSetItem(_ref) {
    var slider = _ref.slider;
    return function (name, value) {
      return {
        slider: _objectSpread({}, slider, _defineProperty({}, name, value))
      };
    };
  },
  pairingSetItem: function pairingSetItem(_ref2) {
    var pairing = _ref2.pairing;
    return function (name, value) {
      return console.log("pairingSetItem", name, value), {
        pairing: _objectSpread({}, pairing, _defineProperty({}, name, value))
      };
    };
  }
}), (0, _withHandlers.default)({
  onSliderChange: function onSliderChange(_ref3) {
    var client = _ref3.client,
        sliderSetItem = _ref3.sliderSetItem,
        fromProductGraphql = _ref3.fromProductGraphql;
    return function (value, label) {
      if (label === "sweetness") {
        fromProductGraphql.applyFilter({
          sweetness: value
        }, {
          client: client
        });
      }

      if (label === "price") {
        fromProductGraphql.applyFilter({
          price: value * 10
        }, {
          client: client
        });
      }

      sliderSetItem(label, value);
    };
  },
  onPairChange: function onPairChange(_ref4) {
    var client = _ref4.client,
        fromProductGraphql = _ref4.fromProductGraphql,
        pairing = _ref4.pairing,
        pairingSetItem = _ref4.pairingSetItem;
    return function (event, name) {
      var pairingNext = _objectSpread({}, pairing, _defineProperty({}, name, event.target && event.target.checked));

      var pairList = Object.keys(pairingNext).reduce(function (acc, name) {
        return pairingNext[name] && acc.push(name), acc;
      }, []);
      fromProductGraphql.applyFilter({
        pairList: pairList.length ? pairList : null
      }, {
        client: client
      });
      pairingSetItem(name, event.target && event.target.checked);
    };
  }
}), (0, _defaultProps.default)({
  A: {
    FieldSetContainer: _styledComponents.default.div.withConfig({
      displayName: "SectionExpandedFilter__FieldSetContainer",
      componentId: "q7fg7y-1"
    })(["&&{align-items:center;width:100%;font-size:", ";lable{font-size:inherit;}span{font-size:inherit;}}"], (0, _polished.rem)(13)),
    SliderFooter: _styledComponents.default.div.withConfig({
      displayName: "SectionExpandedFilter__SliderFooter",
      componentId: "q7fg7y-2"
    })(["&&{display:flex;flex-direction:row;justify-content:space-between;align-items:center;width:100%;font-size:", ";p{font-size:inherit;&.left{font-style:italic;font-weight:normal;font-stretch:normal;line-height:normal;letter-spacing:normal;padding-top:0.40rem;font-size:", ";}&.right{text-align:right;font-style:italic;font-weight:normal;font-stretch:normal;line-height:normal;letter-spacing:normal;padding-top:0.40rem;font-size:", ";}}}"], (0, _polished.rem)(13), (0, _polished.rem)(10), (0, _polished.rem)(10)),
    FormControlLabel: (0, _styledComponents.default)(_core.FormControlLabel).withConfig({
      displayName: "SectionExpandedFilter__FormControlLabel",
      componentId: "q7fg7y-3"
    })(["&&{margin:auto;.label{font-size:", ";display:flex;}},"], (0, _polished.rem)(14)),
    Radio: (0, _styledComponents.default)(function (_ref5) {
      var other = _extends({}, _ref5);

      return _react.default.createElement(_core.Radio, other);
    }).withConfig({
      displayName: "SectionExpandedFilter__Radio",
      componentId: "q7fg7y-4"
    })(["&&{padding:0px ", " ", " ", ";&.primary{color:black;}}"], (0, _polished.rem)(5), (0, _polished.rem)(5), (0, _polished.rem)(5)),
    Checkbox: (0, _styledComponents.default)(function (_ref6) {
      var other = _extends({}, _ref6);

      return _react.default.createElement(_core.Checkbox, other);
    }).withConfig({
      displayName: "SectionExpandedFilter__Checkbox",
      componentId: "q7fg7y-5"
    })(["&&{padding:0px ", " ", " ", ";&.primary{color:black;}}"], (0, _polished.rem)(5), (0, _polished.rem)(5), (0, _polished.rem)(5)),
    Typography: _styledComponents.default.div.withConfig({
      displayName: "SectionExpandedFilter__Typography",
      componentId: "q7fg7y-6"
    })(["&&{text-transform:uppercase;&.title{font-size:", ";font-weight:bold;padding-bottom:0.5rem;padding-top:0.5rem;font-family:Open Sans,sans-serif;}}"], (0, _polished.rem)(10))
  }
}));
var ExpandedFilter = enhance(function (_ref7) {
  var isOpen = _ref7.isOpen,
      A = _ref7.A,
      slider = _ref7.slider,
      handleChangePairing = _ref7.handleChangePairing,
      pairing = _ref7.pairing,
      error = _ref7.error,
      country = _ref7.country,
      classes = _ref7.classes,
      onSliderChange = _ref7.onSliderChange,
      onPairChange = _ref7.onPairChange;
  return _react.default.createElement(Content, {
    pose: isOpen ? 'open' : 'close'
  }, _react.default.createElement(_core.Grid, {
    container: true,
    direction: 'row',
    justify: 'center',
    alignItems: 'center',
    spacing: 0
  }, _react.default.createElement(_core.Grid, {
    xs: 8,
    item: true
  }, _react.default.createElement(_core.Grid, {
    container: true,
    direction: "row",
    justify: "center",
    alignItems: "flex-start",
    spacing: 16
  }, _react.default.createElement(_core.Grid, {
    xs: 12,
    sm: 10,
    md: 4,
    item: true
  }, _react.default.createElement("div", {
    style: {
      width: '90%'
    }
  }, ['sweetness', 'texture', 'price'].map(function (item, index) {
    return _react.default.createElement(_core.FormGroup, {
      key: index,
      row: true
    }, _react.default.createElement(A.Typography, {
      className: "title",
      id: item
    }, item), _react.default.createElement(_Slider.default, {
      classes: {
        track: classes.track,
        thumb: classes.thumb
      },
      value: slider[item] || 0,
      "aria-labelledby": item,
      onChange: function onChange(event, value) {
        return onSliderChange(value, item);
      }
    }), _react.default.createElement(A.SliderFooter, null, _react.default.createElement(_core.Typography, {
      className: 'left',
      id: item
    }, "Least"), _react.default.createElement(_core.Typography, {
      className: 'right',
      id: item
    }, "Most")));
  }))), _react.default.createElement(_core.Grid, {
    xs: 12,
    sm: 10,
    md: 4,
    item: true
  }, _react.default.createElement(A.Typography, {
    className: "title",
    id: "price"
  }, "pairing"), _react.default.createElement(A.FieldSetContainer, null, _react.default.createElement(_core.Grid, {
    container: true,
    alignItems: "stretch",
    justify: "flex-start"
  }, ['Cheese', 'Cookies', 'Meat', 'Red wine'].map(function (label, index) {
    return _react.default.createElement(_core.Grid, {
      key: index,
      item: true,
      xs: 3,
      sx: 6,
      md: 6
    }, _react.default.createElement(_core.FormControl, {
      component: "fieldset",
      required: true,
      error: error
    }, _react.default.createElement(_core.FormControlLabel, {
      control: _react.default.createElement(A.Checkbox, {
        className: "primary",
        checked: pairing[label] || false,
        onChange: function onChange(event) {
          return onPairChange(event, label);
        },
        value: label
      }),
      label: label
    })));
  })))), _react.default.createElement(_core.Grid, {
    xs: 12,
    sm: 10,
    md: 4,
    item: true
  }, _react.default.createElement(A.Typography, {
    className: "title",
    id: "country"
  }, "country"), _react.default.createElement(A.FieldSetContainer, null, _react.default.createElement(_core.Grid, {
    container: true,
    alignItems: "stretch",
    justify: "flex-start"
  }, ['Lebanon', 'France', 'Italy', 'Kuwait', 'Spain', 'Scotland'].map(function (cntr, index) {
    return _react.default.createElement(_core.Grid, {
      key: index,
      item: true,
      xs: 3,
      sx: 6,
      md: 6
    }, _react.default.createElement(_core.FormControl, {
      component: "fieldset",
      required: true,
      error: error
    }, _react.default.createElement(_core.FormControlLabel, {
      disabled: index !== 0,
      value: country[cntr],
      control: _react.default.createElement(A.Radio, {
        checked: index === 0,
        className: "primary",
        color: "primary"
      }),
      label: _react.default.createElement("nobr", null, "".concat(cntr, " ").concat(cntrMap[cntr])),
      labelPlacement: "end"
    })));
  }))))))));
});

var _default = (0, _styles.withStyles)(function (theme) {
  return {
    thumb: {
      backgroundColor: '#000'
    },
    track: {
      backgroundColor: '#000'
    }
  };
})(ExpandedFilter);

exports.default = _default;
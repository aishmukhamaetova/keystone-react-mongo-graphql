"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _recompose = require("recompose");

var _Product = _interopRequireDefault(require("./Product"));

var _CartProductQuantity = _interopRequireDefault(require("../../../../box/eshop/cart/web/CartProductQuantity"));

var _core = require("@material-ui/core");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _polished = require("polished");

var _routes = require("../../page/api/routes");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

var enhance = (0, _recompose.compose)((0, _recompose.defaultProps)({
  data: {},
  order: [],
  A: {
    ButtonBuy: (0, _styledComponents.default)(_core.Button).withConfig({
      displayName: "ProductList__ButtonBuy",
      componentId: "sc-1en0ppb-0"
    })(["&&{border-radius:0;background-color:rgba(255,255,255,0.05);color:#000;box-shadow:1px 1px 0px  black inset,-1px -1px 0px  black inset;font-size:", ";&.cart{margin:", " auto;}font-weight:600;text-transform:uppercase;line-height:2.07;a{color:#000;&:hover{color:white;}}&:hover{a{color:white;}color:white;background:#000;}}"], (0, _polished.rem)(18), (0, _polished.rem)(10))
  }
}));
var ProductList = enhance(function (_ref) {
  var data = _ref.data,
      order = _ref.order,
      _ref$dataCart = _ref.dataCart,
      dataCart = _ref$dataCart === void 0 ? {} : _ref$dataCart,
      onCartAdd = _ref.onCartAdd,
      onCartMinus = _ref.onCartMinus,
      onCartPlus = _ref.onCartPlus,
      A = _ref.A;
  return order.map(function (id, index) {
    var stores = data[id].stores;
    var price = stores.length ? stores[0].price : null;
    return _react.default.createElement(_core.Grid, {
      xs: 12,
      sm: 6,
      md: 4,
      item: true,
      key: id
    }, _react.default.createElement(_Product.default, data[id], price && dataCart[id] ? _react.default.createElement(_react.default.Fragment, null, _react.default.createElement(_CartProductQuantity.default, _extends({}, dataCart[id], {
      onMinus: function onMinus() {
        return onCartMinus({
          id: id
        });
      },
      onPlus: function onPlus() {
        return onCartPlus({
          id: id
        });
      }
    })), _react.default.createElement(A.ButtonBuy, {
      className: 'cart',
      fullWidth: true,
      variant: "contained",
      align: "center",
      color: "primary"
    }, _react.default.createElement(_routes.Link, {
      route: "cart"
    }, _react.default.createElement("a", null, "go to cart")))) : _react.default.createElement(A.ButtonBuy, {
      disabled: !price,
      onClick: function onClick() {
        return onCartAdd(_objectSpread({}, data[id], {
          price: price
        }));
      },
      fullWidth: true,
      variant: "contained",
      align: "center",
      color: "primary"
    }, "Buy Now")));
  });
});
var _default = ProductList;
exports.default = _default;
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _recompose = require("recompose");

var _polished = require("polished");

var _core = require("@material-ui/core");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _routes = require("../../page/api/routes");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

var map = {
  sm: 600,
  md: 960,
  lg: 1280,
  xl: 1920
};

var mediaGrid = function mediaGrid(type) {
  return function () {
    return "@media (max-width: ".concat(map[type] - 1, "px)");
  };
};

var enhance = (0, _recompose.compose)((0, _recompose.defaultProps)({
  A: {
    Occasion: (0, _styledComponents.default)(function (_ref) {
      var bgurl = _ref.bgurl,
          _ref$image = _ref.image,
          image = _ref$image === void 0 ? {} : _ref$image,
          other = _objectWithoutProperties(_ref, ["bgurl", "image"]);

      return _react.default.createElement(_core.Grid, other);
    }).withConfig({
      displayName: "ProductPromotionalBar__Occasion",
      componentId: "sc-346pmg-0"
    })(["&&{color:", ";text-align:center;background-image:url(", ");background-position:center center;background-repeat:no-repeat;background-size:cover;button{margin-bottom:", ";}.Top{height:48%;}.Bottom{height:52%;}height:", ";margin:0 auto;", "{width:100%;height:", ";}", "{width:100%;height:", ";}", "{width:80%;height:", ";}", "{width:100%;height:", ";.Title{font-size:1.2rem;white-space:nowrap;}.TagLine{font-size:1.0rem;}}}"], function (props) {
      return props.color;
    }, function (props) {
      return props.image.secure_url;
    }, (0, _polished.rem)(40), (0, _polished.rem)(720), mediaGrid("xl"), (0, _polished.rem)(720), mediaGrid("lg"), (0, _polished.rem)(720), mediaGrid("md"), (0, _polished.rem)(720), mediaGrid("sm"), (0, _polished.rem)(720)),
    Title: (0, _styledComponents.default)(function (_ref2) {
      var color = _ref2.color,
          transform = _ref2.transform,
          border = _ref2.border,
          margin = _ref2.margin,
          fweight = _ref2.fweight,
          other = _objectWithoutProperties(_ref2, ["color", "transform", "border", "margin", "fweight"]);

      return _react.default.createElement(_core.Typography, other);
    }).withConfig({
      displayName: "ProductPromotionalBar__Title",
      componentId: "sc-346pmg-1"
    })(["&&{color:", ";border-bottom:", ";text-transform:", ";border-color:", ";margin:", ";font-weight:", ";min-width:auto;width:100%;font-size:", ";text-align:center;a{color:inherit;}&:hover{background:none;},}"], function (props) {
      return props.color || 'inherit';
    }, function (props) {
      return props.border;
    }, function (props) {
      return props.transform || 'inherit';
    }, function (props) {
      return props.color;
    }, function (props) {
      return props.margin;
    }, function (props) {
      return props.fweight || '700';
    }, function (props) {
      return props.size;
    }),
    TagLine: (0, _styledComponents.default)(function (_ref3) {
      var color = _ref3.color,
          transform = _ref3.transform,
          border = _ref3.border,
          margin = _ref3.margin,
          fweight = _ref3.fweight,
          other = _objectWithoutProperties(_ref3, ["color", "transform", "border", "margin", "fweight"]);

      return _react.default.createElement(_core.Typography, other);
    }).withConfig({
      displayName: "ProductPromotionalBar__TagLine",
      componentId: "sc-346pmg-2"
    })(["&&{font-size:", ";font-weight:", ";color:", ";margin:", ";text-align:center;font-style:normal;line-height:normal;}"], function (props) {
      return props.size || 16;
    }, function (props) {
      return props.fweight || 400;
    }, function (props) {
      return props.color;
    }, function (props) {
      return props.margin;
    }),
    Button: (0, _styledComponents.default)(_core.Button).withConfig({
      displayName: "ProductPromotionalBar__Button",
      componentId: "sc-346pmg-3"
    })(["&&{background-color:rgba(255,255,255,0.05);color:#fff;box-shadow:1px 1px 0px  white inset,-1px -1px 0px  white inset;font-size:", ";font-weight:600;text-transform:uppercase;min-width:", ";margin:1.312em;line-height:1.321;&:hover{color:black;}}"], (0, _polished.rem)(20), (0, _polished.rem)(150))
  }
}), (0, _recompose.withProps)(function (_ref4) {
  var _ref4$description = _ref4.description,
      description = _ref4$description === void 0 ? '' : _ref4$description;
  return {
    descriptionSplitted: description.split(' ', 10).join(' ') + '...'
  };
}));
var ProductPromotionalBar = enhance(function (_ref5) {
  var id = _ref5.id,
      sku = _ref5.sku,
      name = _ref5.name,
      description = _ref5.description,
      _ref5$images = _ref5.images,
      images = _ref5$images === void 0 ? [] : _ref5$images,
      _ref5$videos = _ref5.videos,
      videos = _ref5$videos === void 0 ? [] : _ref5$videos,
      A = _ref5.A,
      key = _ref5.key,
      descriptionSplitted = _ref5.descriptionSplitted,
      content = _ref5.content,
      children = _ref5.children;
  return _react.default.createElement(_core.Grid, {
    item: true,
    xs: 9,
    sm: 7,
    md: 4
  }, images ? images.map(function (doc, index) {
    return index === 0 ? _react.default.createElement(A.Occasion, _extends({
      key: doc.id
    }, doc, {
      alt: doc.name,
      container: true,
      direction: "column",
      justify: "space-between",
      alignItems: "center",
      spacing: 0,
      color: "#fff"
    }), _react.default.createElement(_core.Grid, {
      item: true
    }, "\xA0"), _react.default.createElement(_core.Grid, {
      item: true
    }, _react.default.createElement(A.Title, {
      className: 'Title',
      size: "1.875rem",
      fweight: "700",
      color: "#fff",
      transform: "uppercase",
      margin: "0 auto"
    }, _react.default.createElement(_routes.Link, {
      route: "product",
      params: {
        id: id
      }
    }, _react.default.createElement("a", null, name)))), _react.default.createElement(_core.Grid, {
      item: true
    }, _react.default.createElement(A.TagLine, {
      className: 'TagLine',
      margin: "0 auto",
      color: "#fff",
      fweight: "300",
      size: "1.25rem"
    }, _react.default.createElement("div", {
      dangerouslySetInnerHTML: {
        __html: content
      }
    }))), _react.default.createElement(_core.Grid, {
      item: true,
      justify: "center",
      container: true
    }, _react.default.createElement(A.Button, {
      variant: "contained"
    }, children))) : null;
  }) : null);
});
var _default = ProductPromotionalBar;
exports.default = _default;
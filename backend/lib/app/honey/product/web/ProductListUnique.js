"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _recompose = require("recompose");

var _routes = require("../../page/api/routes");

var _ProductUnique = _interopRequireDefault(require("./ProductUnique"));

var _CartProductQuantity = _interopRequireDefault(require("../../../../box/eshop/cart/web/CartProductQuantity"));

var _core = require("@material-ui/core");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _polished = require("polished");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

var enhance = (0, _recompose.compose)((0, _recompose.defaultProps)({
  data: {},
  order: [],
  A: {
    Button: (0, _styledComponents.default)(function (_ref) {
      var width = _ref.width,
          other = _objectWithoutProperties(_ref, ["width"]);

      return _react.default.createElement(_core.Button, other);
    }).withConfig({
      displayName: "ProductListUnique__Button",
      componentId: "sc-1bux7yn-0"
    })(["&&{background-color:#000;color:white;font-weight:600;font-size:", ";text-transform:uppercase;width:", " || inherit;padding-top:", ";padding-bottom:", ";margin-top:", ";a{color:white;}&:hover{a{color:white;}}}"], (0, _polished.rem)(20), function (props) {
      return props.width;
    }, (0, _polished.rem)(10), (0, _polished.rem)(10), (0, _polished.rem)(15))
  }
}));
var ProductListUnique = enhance(function (_ref2) {
  var data = _ref2.data,
      order = _ref2.order,
      _ref2$dataCart = _ref2.dataCart,
      dataCart = _ref2$dataCart === void 0 ? {} : _ref2$dataCart,
      onCartAdd = _ref2.onCartAdd,
      onCartMinus = _ref2.onCartMinus,
      onCartPlus = _ref2.onCartPlus,
      A = _ref2.A;
  return order.map(function (id) {
    var stores = data[id].stores;
    var price = stores.length ? stores[0].price : null;
    return _react.default.createElement(_ProductUnique.default, _extends({
      id: id,
      key: id
    }, data[id]), price && dataCart[id] ? _react.default.createElement("div", {
      style: {
        marginTop: 20
      }
    }, _react.default.createElement(_CartProductQuantity.default, _extends({}, dataCart[id], {
      onMinus: function onMinus() {
        return onCartMinus({
          id: id
        });
      },
      onPlus: function onPlus() {
        return onCartPlus({
          id: id
        });
      }
    })), _react.default.createElement(A.Button, {
      fullWidth: true,
      variant: "contained",
      align: "center",
      color: "primary"
    }, _react.default.createElement(_routes.Link, {
      route: "cart"
    }, _react.default.createElement("a", null, "go to cart")))) : null, price && !dataCart[id] ? _react.default.createElement("div", null, _react.default.createElement(A.Button, {
      fullWidth: true,
      variant: "contained",
      color: "primary",
      onClick: function onClick() {
        return onCartAdd(_objectSpread({}, data[id], {
          price: price
        }));
      }
    }, "Add to cart")) : null);
  });
});
var _default = ProductListUnique;
exports.default = _default;
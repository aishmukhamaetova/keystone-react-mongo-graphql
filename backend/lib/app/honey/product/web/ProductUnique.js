"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _compose = _interopRequireDefault(require("recompose/compose"));

var _defaultProps = _interopRequireDefault(require("recompose/defaultProps"));

var _semanticUiReact = require("semantic-ui-react");

var _routes = require("../../page/api/routes");

var _Image = _interopRequireDefault(require("../../image/web/Image"));

var _Video = _interopRequireDefault(require("../../video/web/Video"));

var _Emojis = _interopRequireDefault(require("../../image/web/Emojis"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _polished = require("polished");

var _core = require("@material-ui/core");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

var map = {
  sm: 600,
  md: 960,
  lg: 1280,
  xl: 1920
};

var mediaGrid = function mediaGrid(type) {
  return function () {
    return "@media (max-width: ".concat(map[type] - 1, "px)");
  };
};

var enhance = (0, _compose.default)((0, _defaultProps.default)({
  A: {
    Image: _styledComponents.default.div.withConfig({
      displayName: "ProductUnique__Image",
      componentId: "sc-1lqdwr0-0"
    })(["&&{text-align:center;img{width:", ";margin-right:", ";height:auto;}", "{text-align:left;img{width:", ";height:auto;}}", "{text-align:center;img{width:", ";height:auto;}}}"], (0, _polished.rem)(200), (0, _polished.rem)(40), mediaGrid("md"), (0, _polished.rem)(200), mediaGrid("sm"), (0, _polished.rem)(200)),
    Description: (0, _styledComponents.default)(_core.Typography).withConfig({
      displayName: "ProductUnique__Description",
      componentId: "sc-1lqdwr0-1"
    })(["&&{font-size:", ";margin-bottom:", ";}"], (0, _polished.rem)(16), (0, _polished.rem)(15)),
    Details: (0, _styledComponents.default)(_core.FormControl).withConfig({
      displayName: "ProductUnique__Details",
      componentId: "sc-1lqdwr0-2"
    })(["text-align:left;width:80%;"]),
    Detail: (0, _styledComponents.default)(_core.Typography).withConfig({
      displayName: "ProductUnique__Detail",
      componentId: "sc-1lqdwr0-3"
    })(["&&{font-size:", ";line-height:", ";font-style:normal;font-stretch:normal;line-height:normal;letter-spacing:normal;margin-bottom:", ";}"], (0, _polished.rem)(16), (0, _polished.rem)(23), (0, _polished.rem)(5)),
    Title: (0, _styledComponents.default)(_core.Typography).withConfig({
      displayName: "ProductUnique__Title",
      componentId: "sc-1lqdwr0-4"
    })(["&&{font-weight:700;font-size:", ";line-height:normal;border-bottom:1px solid;padding-bottom:", ";margin-bottom:", ";color:#000;border-color:#000;text-transform:uppercase;white-space:nowrap;a{color:#000;}}"], (0, _polished.rem)(30), (0, _polished.rem)(7), (0, _polished.rem)(13))
  }
}));
var ProductUnique = enhance(function (_ref) {
  var children = _ref.children,
      sku = _ref.sku,
      name = _ref.name,
      description = _ref.description,
      _ref$images = _ref.images,
      images = _ref$images === void 0 ? [] : _ref$images,
      _ref$videos = _ref.videos,
      videos = _ref$videos === void 0 ? [] : _ref$videos,
      id = _ref.id,
      pairs = _ref.pairs,
      regions = _ref.regions,
      sweetness = _ref.sweetness,
      stores = _ref.stores,
      A = _ref.A;
  return _react.default.createElement(_react.default.Fragment, null, _react.default.createElement(A.Image, null, images.map(function (doc, index) {
    return index === 0 ? _react.default.createElement(_Image.default, _extends({
      key: doc.id
    }, doc)) : null;
  })), _react.default.createElement(A.Details, null, _react.default.createElement(_core.Grid, {
    container: true,
    direction: 'column',
    alignItems: 'stretch',
    justify: 'space-around',
    spacing: 0
  }, _react.default.createElement(_core.Grid, {
    item: true,
    xs: true
  }, _react.default.createElement(A.Title, null, _react.default.createElement(_routes.Link, {
    route: 'product',
    params: {
      id: id
    }
  }, "".concat(name))), _react.default.createElement(A.Description, {
    variant: "caption"
  }, description), _react.default.createElement("div", {
    style: {
      margin: '20px auto'
    }
  }, _react.default.createElement(_Emojis.default, null)), _react.default.createElement(A.Detail, {
    paragraph: true
  }, _react.default.createElement("b", null, "Sweetness:"), " ", sweetness), _react.default.createElement(A.Detail, {
    paragraph: true
  }, _react.default.createElement("b", null, "Regions:"), " ", regions.map(function (region, i) {
    return region.name + (i < regions.length && regions.length !== 1 ? ', ' : '');
  })), _react.default.createElement(A.Detail, {
    paragraph: true
  }, _react.default.createElement("b", null, "Food pairing:"), " ", pairs.map(function (pair, i) {
    return pair.name + (i < pairs.length && pairs.length !== 1 ? ', ' : '');
  })), _react.default.createElement(A.Detail, {
    paragraph: true
  }, _react.default.createElement("b", null, "Stores:"), " ", stores.map(function (store, i) {
    return store.location && store.location.name + (i < stores.length && stores.length !== 1 ? ', ' : '');
  }))), _react.default.createElement(_core.Grid, {
    item: true,
    xs: true
  }, children))));
});
var _default = ProductUnique;
exports.default = _default;
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _recompose = require("recompose");

var _polished = require("polished");

var _core = require("@material-ui/core");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _ThumbImage = _interopRequireDefault(require("../../image/web/ThumbImage"));

var _Image = _interopRequireDefault(require("../../image/web/Image"));

var _Emojis = _interopRequireDefault(require("../../image/web/Emojis"));

var _routes = require("../../page/api/routes");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var map = {
  sm: 600,
  md: 960,
  lg: 1280,
  xl: 1920
};

var mediaGrid = function mediaGrid(type) {
  return function () {
    return "@media (max-width: ".concat(map[type] - 1, "px)");
  };
};

var enhance = (0, _recompose.compose)((0, _recompose.defaultProps)({
  A: _defineProperty({
    FormGroup: (0, _styledComponents.default)(_core.FormGroup).withConfig({
      displayName: "ProductView__FormGroup",
      componentId: "gr5dvx-0"
    })(["margin-top:", ";"], (0, _polished.rem)(10)),
    ProductDesc: _styledComponents.default.div.withConfig({
      displayName: "ProductView__ProductDesc",
      componentId: "gr5dvx-1"
    })(["&&{font-size:", ";padding:", ";margin-top:", ";font-style:italic;line-height:normal;box-shadow:0 0 ", " rgba(34,25,25,0.4);box-sizing:border-box;margin-bottom:", ";margin-top:", ";}"], (0, _polished.rem)(12), (0, _polished.rem)(4), (0, _polished.rem)(5), (0, _polished.rem)(1), (0, _polished.rem)(19), (0, _polished.rem)(19)),
    Detail: (0, _styledComponents.default)(_core.Typography).withConfig({
      displayName: "ProductView__Detail",
      componentId: "gr5dvx-2"
    })(["&&{font-size:", ";font-style:normal;font-stretch:normal;line-height:normal;letter-spacing:normal;margin-bottom:", ";}"], (0, _polished.rem)(16), (0, _polished.rem)(5)),
    SubTitle: (0, _styledComponents.default)(_core.Typography).withConfig({
      displayName: "ProductView__SubTitle",
      componentId: "gr5dvx-3"
    })(["&&{font-size:", ";margin-bottom:", ";text-align:left;}"], (0, _polished.rem)(16), (0, _polished.rem)(18)),
    Description: _styledComponents.default.div.withConfig({
      displayName: "ProductView__Description",
      componentId: "gr5dvx-4"
    })(["text-align:left;"]),
    Title: (0, _styledComponents.default)(_core.Typography).withConfig({
      displayName: "ProductView__Title",
      componentId: "gr5dvx-5"
    })(["&&{font-weight:700;font-size:", ";line-height:normal;border-bottom:", " solid;padding-bottom:", ";margin-bottom:", ";color:rgba(0,0,0,0.87);border-color:rgba(0,0,0,0.87);text-transform:uppercase;", "{font-size:", ";}}"], (0, _polished.rem)(30), (0, _polished.rem)(1), (0, _polished.rem)(7), (0, _polished.rem)(13), mediaGrid("sm"), (0, _polished.rem)(20)),
    Container: _styledComponents.default.div.withConfig({
      displayName: "ProductView__Container",
      componentId: "gr5dvx-6"
    })(["display:inline-flex;margin:0;"]),
    Img: (0, _styledComponents.default)(_core.ButtonBase).withConfig({
      displayName: "ProductView__Img",
      componentId: "gr5dvx-7"
    })(["&&{text-align:left;margin-right:", ";img{width:", ";margin-bottom:", ";height:auto;}&.fullImg{img{width:", ";height:auto;}}", "{img{width:", ";height:auto;}&.fullImg{img{width:", ";height:auto;}}}"], (0, _polished.rem)(58), (0, _polished.rem)(200), (0, _polished.rem)(20), (0, _polished.rem)(350), mediaGrid("sm"), (0, _polished.rem)(180), (0, _polished.rem)(350)),
    ReadMore: (0, _styledComponents.default)(_core.Typography).withConfig({
      displayName: "ProductView__ReadMore",
      componentId: "gr5dvx-8"
    })(["&&{text-align:center;color:#000;font-size:", ";line-height:1.4;font-weight:600;margin-bottom:", ";margin:", " auto;padding:inherit;a{text-decoration:underline;color:#000;font-weight:600;&:hover{text-decoration:none;}}}"], (0, _polished.rem)(10), (0, _polished.rem)(18), (0, _polished.rem)(15))
  }, "Title", (0, _styledComponents.default)(_core.Typography).withConfig({
    displayName: "ProductView",
    componentId: "gr5dvx-9"
  })(["&&{font-weight:700;font-size:", ";line-height:normal;border-bottom:1px solid;padding-bottom:", ";margin-bottom:", ";color:rgba(0,0,0,0.87);border-color:rgba(0,0,0,0.87);text-transform:uppercase;a{color:#000;}}"], (0, _polished.rem)(17), (0, _polished.rem)(7), (0, _polished.rem)(13)))
}), (0, _recompose.withProps)(function (_ref) {
  var _ref$description = _ref.description,
      description = _ref$description === void 0 ? '' : _ref$description,
      _ref$images = _ref.images,
      images = _ref$images === void 0 ? [] : _ref$images;
  return {
    descriptionSplitted: description.split(' ', 10).join(' ') + '...',
    imgMain: images[0]
  };
}));
var ProductView = enhance(function (props) {
  var id = props.id,
      children = props.children,
      sku = props.sku,
      name = props.name,
      description = props.description,
      _props$images = props.images,
      images = _props$images === void 0 ? [] : _props$images,
      _props$videos = props.videos,
      videos = _props$videos === void 0 ? [] : _props$videos,
      A = props.A,
      key = props.key,
      descriptionSplitted = props.descriptionSplitted,
      setActiveMedia = props.setActiveMedia,
      imgMain = props.imgMain,
      pairs = props.pairs,
      regions = props.regions,
      sweetness = props.sweetness,
      stores = props.stores;
  return _react.default.createElement(_core.Grid, {
    container: true,
    justify: "center",
    alignItems: "center"
  }, _react.default.createElement(_core.Grid, {
    item: true,
    xs: 10,
    sm: 8
  }, _react.default.createElement(_core.Grid, {
    container: true,
    direction: "row",
    justify: "flex-start",
    alignItems: "flex-start"
  }, _react.default.createElement(_core.Hidden, {
    smDown: true
  }, _react.default.createElement(_core.Grid, {
    item: true,
    sm: 2
  }, _react.default.createElement(_core.FormControl, null, images.map(function (image, index) {
    return _react.default.createElement(_core.ButtonBase, {
      disableRipple: true,
      disabletouchripple: true,
      onClick: function onClick() {
        setActiveMedia(image);
      },
      key: index
    }, _react.default.createElement(_ThumbImage.default, image));
  })))), _react.default.createElement(_core.Grid, {
    item: true,
    xs: 12,
    sm: 3
  }, _react.default.createElement(A.Img, null, _react.default.createElement(_Image.default, _extends({
    name: name
  }, imgMain)))), _react.default.createElement(_core.Grid, {
    item: true,
    xs: 12,
    sm: 7
  }, _react.default.createElement(A.Description, null, _react.default.createElement(A.Title, null, name), _react.default.createElement(A.SubTitle, {
    variant: "caption"
  }, description), _react.default.createElement("div", {
    style: {
      margin: '20px auto'
    }
  }, _react.default.createElement(_Emojis.default, null)), _react.default.createElement(A.Detail, {
    paragraph: true
  }, _react.default.createElement("b", null, "Sweetness:"), " ", sweetness), _react.default.createElement(A.Detail, {
    paragraph: true
  }, _react.default.createElement("b", null, "Regions:"), " ", regions.map(function (region, i) {
    return region.name + (i < regions.length && regions.length !== 1 ? ', ' : '');
  })), _react.default.createElement(A.Detail, {
    paragraph: true
  }, _react.default.createElement("b", null, "Food Pairing:"), " ", pairs.map(function (pair, i) {
    return pair.name + (i < pairs.length && pairs.length !== 1 ? ', ' : '');
  })), _react.default.createElement(A.Detail, {
    paragraph: true
  }, _react.default.createElement("b", null, "Stores:"), " ", stores.map(function (store, i) {
    return store.location && store.location.name + (i < stores.length && stores.length !== 1 ? ', ' : '');
  })), _react.default.createElement(A.ProductDesc, null, _react.default.createElement("p", null, " \u2714\uFE0E This honey contains potassium, vitamin A and magnesium, and has been "), _react.default.createElement("p", null, " Shown to have excellent diuretic properties.")), _react.default.createElement(A.FormGroup, {
    row: true
  }, children))))));
});
var _default = ProductView;
exports.default = _default;
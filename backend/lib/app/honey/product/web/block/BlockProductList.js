"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _reactApollo = require("react-apollo");

var _compose = _interopRequireDefault(require("recompose/compose"));

var _lifecycle = _interopRequireDefault(require("recompose/lifecycle"));

var _withHandlers = _interopRequireDefault(require("recompose/withHandlers"));

var _withStateHandlers = _interopRequireDefault(require("recompose/withStateHandlers"));

var _spaceBoxEshopCart = _interopRequireDefault(require("../../../../../box/eshop/cart/space/spaceBoxEshopCart"));

var _withSpace = _interopRequireDefault(require("../../../../../evoke-me/space/all/withSpace"));

var _spaceProduct = _interopRequireDefault(require("../../space/spaceProduct"));

var _spaceProductGraphql = _interopRequireDefault(require("../../space/spaceProductGraphql"));

var _ProductList = _interopRequireDefault(require("../ProductList"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _default = (0, _compose.default)(_reactApollo.withApollo, (0, _withSpace.default)(_spaceBoxEshopCart.default), (0, _withSpace.default)(_spaceProduct.default), (0, _withSpace.default)(_spaceProductGraphql.default), (0, _withHandlers.default)({
  fetchMore: function fetchMore(_ref) {
    var client = _ref.client,
        fromProduct = _ref.fromProduct,
        fromProductGraphql = _ref.fromProductGraphql,
        paginationSet = _ref.paginationSet;
    return function () {
      var orderMap = fromProduct.orderMap;
      var index = fromProductGraphql.index,
          size = fromProductGraphql.size;

      if (orderMap.default.length === (index + 1) * size) {
        fromProductGraphql.queryNext({}, {
          client: client
        });
      }
    };
  }
}), (0, _withHandlers.default)({
  onScroll: function onScroll(_ref2) {
    var fetchMore = _ref2.fetchMore;
    return function (e) {
      if (window.innerHeight + window.pageYOffset >= document.body.offsetHeight - 2) {
        fetchMore();
      }
    };
  }
}), (0, _withHandlers.default)({
  mount: function mount(_ref3) {
    var index = _ref3.index,
        client = _ref3.client,
        size = _ref3.size,
        fromProduct = _ref3.fromProduct,
        fromProductGraphql = _ref3.fromProductGraphql,
        onScroll = _ref3.onScroll;
    return function () {
      var orderMap = fromProduct.orderMap;

      if (orderMap.default.length < 3) {
        fromProductGraphql.query({
          pagination: {
            index: index,
            size: size
          }
        }, {
          client: client
        });
      }

      if (window) {
        window.addEventListener("scroll", onScroll);
      }
    };
  },
  unmount: function unmount(_ref4) {
    var onScroll = _ref4.onScroll;
    return function () {
      if (window) {
        window.removeEventListener("scroll", onScroll);
      }
    };
  }
}), (0, _lifecycle.default)({
  componentDidMount: function componentDidMount() {
    this.props.mount();
  },
  componentWillUnmount: function componentWillUnmount() {
    this.props.unmount();
  }
}))(function (_ref5) {
  var fromBoxEshopCart = _ref5.fromBoxEshopCart,
      fromProduct = _ref5.fromProduct,
      fromProductGraphql = _ref5.fromProductGraphql;
  var data = fromProduct.data,
      orderMap = fromProduct.orderMap;
  var isLoading = fromProductGraphql.isLoading;
  return _react.default.createElement(_ProductList.default, {
    data: data,
    order: orderMap.default,
    dataCart: fromBoxEshopCart.data,
    onCartAdd: function onCartAdd(payload) {
      var id = payload.id,
          name = payload.name,
          price = payload.price,
          sku = payload.sku;
      fromBoxEshopCart.create({
        id: id,
        name: name,
        price: price,
        sku: sku,
        quantity: 1
      });
    },
    onCartMinus: function onCartMinus(_ref6) {
      var id = _ref6.id;
      var quantity = fromBoxEshopCart.data[id].quantity;

      if (quantity === 1) {
        fromBoxEshopCart.remove({
          id: id
        });
      } else {
        fromBoxEshopCart.update({
          id: id,
          quantity: quantity > 0 ? quantity - 1 : quantity
        });
      }
    },
    onCartPlus: function onCartPlus(_ref7) {
      var id = _ref7.id;
      var quantity = fromBoxEshopCart.data[id].quantity;
      fromBoxEshopCart.update({
        id: id,
        quantity: quantity + 1
      });
    }
  });
});

exports.default = _default;
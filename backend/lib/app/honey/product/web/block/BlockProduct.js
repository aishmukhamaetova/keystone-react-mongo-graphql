"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _router = require("next/router");

var _react = _interopRequireWildcard(require("react"));

var _reactApollo = require("react-apollo");

var _recompose = require("recompose");

var _spaceBoxEshopCart = _interopRequireDefault(require("../../../../../box/eshop/cart/space/spaceBoxEshopCart"));

var _withSpace = _interopRequireDefault(require("../../../../../evoke-me/space/all/withSpace"));

var _spaceProduct = _interopRequireDefault(require("../../space/spaceProduct"));

var _spaceProductGraphql = _interopRequireDefault(require("../../space/spaceProductGraphql"));

var _ProductListView = _interopRequireDefault(require("../ProductListView"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

var enhance = (0, _recompose.compose)(_router.withRouter, _reactApollo.withApollo, (0, _withSpace.default)(_spaceBoxEshopCart.default), (0, _withSpace.default)(_spaceProduct.default), (0, _withSpace.default)(_spaceProductGraphql.default), (0, _recompose.lifecycle)({
  componentDidMount: function componentDidMount() {
    var client = this.props.client;
    var fromProductGraphql = this.props.fromProductGraphql;
    var router = this.props.router;
    var id = router.query.id;
    var fromProduct = this.props.fromProduct;
    var data = fromProduct.data,
        orderMap = fromProduct.orderMap;

    if (!data[id]) {
      fromProductGraphql.queryById({
        id: id
      }, {
        client: client
      });
    }
  }
}));
var BlockProduct = enhance(function (_ref) {
  var fromBoxEshopCart = _ref.fromBoxEshopCart,
      fromProduct = _ref.fromProduct,
      fromProductGraphql = _ref.fromProductGraphql,
      router = _ref.router;
  var id = router.query.id;
  var data = fromProduct.data;
  var isLoading = fromProductGraphql.isLoading;
  if (isLoading || !data[id]) return null;
  return _react.default.createElement(_ProductListView.default, {
    data: data,
    order: [id],
    dataCart: fromBoxEshopCart.data,
    onCartAdd: function onCartAdd(payload) {
      var id = payload.id,
          name = payload.name,
          price = payload.price,
          sku = payload.sku;
      fromBoxEshopCart.create({
        id: id,
        name: name,
        price: price,
        sku: sku,
        quantity: 1
      });
    },
    onCartMinus: function onCartMinus(_ref2) {
      var id = _ref2.id;
      var quantity = fromBoxEshopCart.data[id].quantity;

      if (quantity === 1) {
        fromBoxEshopCart.remove({
          id: id
        });
      } else {
        fromBoxEshopCart.update({
          id: id,
          quantity: quantity > 0 ? quantity - 1 : quantity
        });
      }
    },
    onCartPlus: function onCartPlus(_ref3) {
      var id = _ref3.id;
      var quantity = fromBoxEshopCart.data[id].quantity;
      fromBoxEshopCart.update({
        id: id,
        quantity: quantity + 1
      });
    }
  });
});
var _default = BlockProduct;
exports.default = _default;
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _recompose = require("recompose");

var _ProductView = _interopRequireDefault(require("./ProductView"));

var _CartProductQuantity = _interopRequireDefault(require("../../../../box/eshop/cart/web/CartProductQuantity"));

var _core = require("@material-ui/core");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _polished = require("polished");

var _routes = require("../../page/api/routes");

var _MediaView = _interopRequireDefault(require("../../image/web/MediaView"));

var _sample = _interopRequireDefault(require("../../media/sample3.jpg"));

var _icons = require("../../icons");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

function _objectDestructuringEmpty(obj) { if (obj == null) throw new TypeError("Cannot destructure undefined"); }

var map = {
  sm: 600,
  md: 960,
  lg: 1280,
  xl: 1920
};

var mediaGrid = function mediaGrid(type) {
  return function () {
    return "@media (max-width: ".concat(map[type] - 1, "px)");
  };
};

var enhance = (0, _recompose.compose)((0, _recompose.withStateHandlers)(function () {
  return {
    activeMedia: null,
    indexMedia: 0,
    staticBlock: {
      image: {
        secure_url: _sample.default
      }
    }
  };
}, {
  setActiveMedia: function setActiveMedia(_ref) {
    _objectDestructuringEmpty(_ref);

    return function (activeMedia) {
      return {
        activeMedia: activeMedia
      };
    };
  }
}), (0, _recompose.defaultProps)({
  data: {},
  order: [],
  A: {
    MediaContainer: _styledComponents.default.div.withConfig({
      displayName: "ProductListView__MediaContainer",
      componentId: "sc-1q6bse1-0"
    })(["margin-bottom:", ";"], (0, _polished.rem)(30)),
    StarSpan: (0, _styledComponents.default)(function (_ref2) {
      var width = _ref2.width,
          other = _objectWithoutProperties(_ref2, ["width"]);

      return _react.default.createElement("p", other);
    }).withConfig({
      displayName: "ProductListView__StarSpan",
      componentId: "sc-1q6bse1-1"
    })(["margin-right:", ";margin-bottom:-", ";}"], (0, _polished.rem)(10), (0, _polished.rem)(6)),
    Rating: (0, _styledComponents.default)(_core.Typography).withConfig({
      displayName: "ProductListView__Rating",
      componentId: "sc-1q6bse1-2"
    })(["&&{font-size:", ";line-height:", ";font-style:italic;line-height:normal;}"], (0, _polished.rem)(12), (0, _polished.rem)(23)),
    ButtonBlock: (0, _styledComponents.default)(_core.ButtonBase).withConfig({
      displayName: "ProductListView__ButtonBlock",
      componentId: "sc-1q6bse1-3"
    })(["&&{margin-left:", ";"], (0, _polished.rem)(20)),
    TagLineContainer: _styledComponents.default.div.withConfig({
      displayName: "ProductListView__TagLineContainer",
      componentId: "sc-1q6bse1-4"
    })(["&&{text-align:center;&.dark{padding:", " ", ";background-color:rgba(0,0,0,0.2);}}"], (0, _polished.rem)(70), (0, _polished.rem)(45)),
    TagLine: (0, _styledComponents.default)(function (_ref3) {
      var other = _extends({}, _ref3);

      return _react.default.createElement(_core.Typography, other);
    }).withConfig({
      displayName: "ProductListView__TagLine",
      componentId: "sc-1q6bse1-5"
    })(["&&{font-size:", ";font-weight:300;color:#fff;text-align:center;font-style:normal;line-height:normal;&.dark{padding:", " ", ";background-color:rgba(0,0,0,0.2);}", "{font-size:", ";}", "{font-size:", ";}", "{font-size:", ";}", "{font-size:", ";}}"], (0, _polished.rem)(35), (0, _polished.rem)(70), (0, _polished.rem)(45), mediaGrid("xl"), (0, _polished.rem)(33.6), mediaGrid("lg"), (0, _polished.rem)(30), mediaGrid("md"), (0, _polished.rem)(24), mediaGrid("sm"), (0, _polished.rem)(20.8)),
    ButtonBuy: (0, _styledComponents.default)(_core.Button).withConfig({
      displayName: "ProductListView__ButtonBuy",
      componentId: "sc-1q6bse1-6"
    })(["&&{border-radius:0;background-color:rgba(255,255,255,0.05);color:#000;box-shadow:1px 1px 0px  black inset,-1px -1px 0px  black inset;font-size:", ";font-weight:600;text-transform:uppercase;line-height:2.07;a{color:#000;&:hover{color:white;}}&:hover{color:white;a{color:white;}background:#000;}}"], (0, _polished.rem)(18)),
    MediaTitle: _styledComponents.default.div.withConfig({
      displayName: "ProductListView__MediaTitle",
      componentId: "sc-1q6bse1-7"
    })(["text-transform:uppercase;background-color:rgba(0,0,0,0.2);font-size:", ";font-weight:600;padding:", " ", ";color:#fff;margin:", " auto;"], (0, _polished.rem)(30), (0, _polished.rem)(30), (0, _polished.rem)(150), (0, _polished.rem)(30))
  }
}), (0, _recompose.withProps)(function (_ref4) {
  var order = _ref4.order,
      data = _ref4.data;
  return {};
}));
var ProductList = enhance(function (_ref5) {
  var data = _ref5.data,
      order = _ref5.order,
      _ref5$dataCart = _ref5.dataCart,
      dataCart = _ref5$dataCart === void 0 ? {} : _ref5$dataCart,
      onCartAdd = _ref5.onCartAdd,
      onCartMinus = _ref5.onCartMinus,
      onCartPlus = _ref5.onCartPlus,
      A = _ref5.A,
      activeMedia = _ref5.activeMedia,
      indexMedia = _ref5.indexMedia,
      staticBlock = _ref5.staticBlock,
      setActiveMedia = _ref5.setActiveMedia;
  return order.map(function (id, index) {
    var stores = data[id].stores;
    var price = stores.length ? stores[0].price : null;
    var images = data[id].images;
    return _react.default.createElement(_react.default.Fragment, {
      key: id
    }, _react.default.createElement(A.MediaContainer, null, activeMedia ? _react.default.createElement(_MediaView.default, activeMedia, _react.default.createElement(_core.Grid, {
      item: true,
      xs: 10,
      sm: 8
    }, _react.default.createElement(A.MediaTitle, null, activeMedia.name))) : staticBlock ? _react.default.createElement(_MediaView.default, staticBlock, _react.default.createElement(_core.Grid, {
      item: true,
      xs: 10,
      sm: 8
    }, _react.default.createElement(A.TagLineContainer, {
      className: 'dark'
    }, _react.default.createElement(A.TagLine, null, "In winter, on the southern coast of Lebanon, the Alman Plateau outside Saida provides local floral diversity including loquat, inula and wild flowers.")))) : Array.isArray(images) && images.length ? _react.default.createElement(_MediaView.default, images[indexMedia], _react.default.createElement(_core.Grid, {
      item: true,
      xs: 10,
      sm: 8
    }, _react.default.createElement(A.MediaTitle, null, images[indexMedia].name))) : null), _react.default.createElement(_ProductView.default, _extends({
      setActiveMedia: setActiveMedia
    }, data[id]), price && dataCart[id] ? _react.default.createElement(_react.default.Fragment, null, _react.default.createElement(_CartProductQuantity.default, _extends({}, dataCart[id], {
      onMinus: function onMinus() {
        return onCartMinus({
          id: id
        });
      },
      onPlus: function onPlus() {
        return onCartPlus({
          id: id
        });
      }
    })), _react.default.createElement(A.ButtonBlock, null, _react.default.createElement(A.ButtonBuy, {
      variant: "contained",
      align: "center",
      color: "primary"
    }, _react.default.createElement(_routes.Link, {
      route: "cart"
    }, _react.default.createElement("a", null, "go to cart"))))) : _react.default.createElement(_react.default.Fragment, null, _react.default.createElement(A.ButtonBuy, {
      disabled: !price,
      onClick: function onClick() {
        return onCartAdd(_objectSpread({}, data[id], {
          price: price
        }));
      },
      variant: "contained",
      align: "center",
      color: "primary"
    }, "Buy Now"), _react.default.createElement(A.ButtonBlock, null, Array(5).fill(null).map(function (i, idx) {
      return _react.default.createElement(A.StarSpan, {
        key: idx
      }, _react.default.createElement(_icons.IconStar, null));
    }), _react.default.createElement(A.Rating, null, "Rating 5/5")))));
  });
});
var _default = ProductList;
exports.default = _default;
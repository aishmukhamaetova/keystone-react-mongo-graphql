"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _router = require("next/router");

var _react = _interopRequireDefault(require("react"));

var _reactApollo = require("react-apollo");

var _recompose = require("recompose");

var _withSpace = _interopRequireDefault(require("../../../../../evoke-me/space/all/withSpace"));

var _spacePost = _interopRequireDefault(require("../../space/spacePost"));

var _spacePostGraphql = _interopRequireDefault(require("../../space/spacePostGraphql"));

var _PostView = _interopRequireDefault(require("../PostView"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var enhance = (0, _recompose.compose)(_router.withRouter, _reactApollo.withApollo, (0, _withSpace.default)(_spacePost.default), (0, _withSpace.default)(_spacePostGraphql.default), (0, _recompose.lifecycle)({
  componentDidMount: function componentDidMount() {
    var client = this.props.client;
    var fromPostGraphql = this.props.fromPostGraphql;
    var router = this.props.router;
    var id = router.query.id;
    var fromPost = this.props.fromPost;
    var data = fromPost.data;

    if (!data[id]) {
      fromPostGraphql.queryById({
        id: id
      }, {
        client: client
      });
    }
  }
}));
var BlockPost = enhance(function (_ref) {
  var fromPost = _ref.fromPost,
      fromPostGraphql = _ref.fromPostGraphql,
      router = _ref.router;
  var id = router.query.id;
  var data = fromPost.data;
  var isLoading = fromPostGraphql.isLoading;
  if (isLoading || !data[id]) return null;
  return _react.default.createElement(_PostView.default, data[id]);
});
var _default = BlockPost;
exports.default = _default;
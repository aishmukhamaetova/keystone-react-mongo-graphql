"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _reactApollo = require("react-apollo");

var _recompose = require("recompose");

var _semanticUiReact = require("semantic-ui-react");

var _withSpace = _interopRequireDefault(require("../../../../../evoke-me/space/all/withSpace"));

var _spacePost = _interopRequireDefault(require("../../space/spacePost"));

var _spacePostGraphql = _interopRequireDefault(require("../../space/spacePostGraphql"));

var _PostList = _interopRequireDefault(require("../PostList"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

var enhance = (0, _recompose.compose)(_reactApollo.withApollo, (0, _withSpace.default)(_spacePost.default), (0, _withSpace.default)(_spacePostGraphql.default), (0, _recompose.lifecycle)({
  componentDidMount: function componentDidMount() {
    var client = this.props.client;
    var fromPostGraphql = this.props.fromPostGraphql;
    var fromPost = this.props.fromPost;
    var orderMap = fromPost.orderMap;

    if (orderMap.default.length < 3) {
      fromPostGraphql.query({}, {
        client: client
      });
    }
  }
}));
var BlockPostList = enhance(function (_ref) {
  var fromPost = _ref.fromPost,
      fromPostGraphql = _ref.fromPostGraphql;
  var data = fromPost.data,
      orderMap = fromPost.orderMap;
  var isLoading = fromPostGraphql.isLoading;

  if (!orderMap.default.length) {
    return _react.default.createElement(_semanticUiReact.Segment, {
      placeholder: true,
      loading: isLoading
    }, _react.default.createElement(_semanticUiReact.Header, {
      icon: true
    }, _react.default.createElement(_semanticUiReact.Icon, {
      name: "pdf file outline"
    }), "BlockPostList is empty"));
  }

  return _react.default.createElement(_PostList.default, {
    data: data,
    order: orderMap.default
  });
});
var _default = BlockPostList;
exports.default = _default;
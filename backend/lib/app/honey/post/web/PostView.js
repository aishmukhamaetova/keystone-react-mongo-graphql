"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _compose = _interopRequireDefault(require("recompose/compose"));

var _defaultProps = _interopRequireDefault(require("recompose/defaultProps"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _polished = require("polished");

var _core = require("@material-ui/core");

var _Image = _interopRequireDefault(require("../../image/web/Image"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

// import Video                from 'app/honey/video/web/Video'
var enhance = (0, _compose.default)((0, _defaultProps.default)({
  A: {
    Container: (0, _styledComponents.default)(_core.Grid).withConfig({
      displayName: "PostView__Container",
      componentId: "dg4mr0-0"
    })(["&&{text-align:center;color:#000;img{width:50%;height:auto;margin:auto;}}"]),
    P: (0, _styledComponents.default)(function (_ref) {
      var color = _ref.color,
          transform = _ref.transform,
          border = _ref.border,
          fweight = _ref.fweight,
          other = _objectWithoutProperties(_ref, ["color", "transform", "border", "fweight"]);

      return _react.default.createElement(_core.Typography, other);
    }).withConfig({
      displayName: "PostView__P",
      componentId: "dg4mr0-1"
    })(["&&{font-size:", ";text-align:left;}"], (0, _polished.rem)(20)),
    Title: (0, _styledComponents.default)(_core.Typography).withConfig({
      displayName: "PostView__Title",
      componentId: "dg4mr0-2"
    })(["&&{color:#000;margin:", ";font-weight:", ";min-width:auto;width:100%;font-size:", ";text-align:center;font-size:", ";text-transform:uppercase;}"], function (props) {
      return props.margin;
    }, function (props) {
      return props.fweight || '700';
    }, (0, _polished.rem)(35), (0, _polished.rem)(38))
  }
}));
var PostView = enhance(function (_ref2) {
  var content = _ref2.content,
      images = _ref2.images,
      link = _ref2.link,
      title = _ref2.title,
      videos = _ref2.videos,
      A = _ref2.A;
  return images.map(function (doc, index) {
    return index === 0 && // {videos.map((doc) => (<Video key={doc.id} {...doc} />))}
    _react.default.createElement(A.Container, {
      container: true,
      direction: 'row',
      justify: 'center',
      alignItems: 'center',
      spacing: 0
    }, _react.default.createElement(_core.Grid, {
      item: true,
      xs: 10,
      sm: 8
    }, _react.default.createElement(_Image.default, _extends({
      key: doc.id
    }, doc))), _react.default.createElement(_core.Grid, {
      item: true,
      xs: 10,
      sm: 8
    }, _react.default.createElement(A.Title, {
      fweight: "700",
      border: "0",
      margin: "0 0 1.25rem 0",
      transform: "uppercase"
    }, title), _react.default.createElement(A.P, {
      fweight: "300",
      dangerouslySetInnerHTML: {
        __html: content.brief
      }
    }), _react.default.createElement(A.P, {
      fweight: "300",
      dangerouslySetInnerHTML: {
        __html: content.extended
      }
    })));
  });
});
var _default = PostView;
exports.default = _default;
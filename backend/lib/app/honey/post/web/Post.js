"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _compose = _interopRequireDefault(require("recompose/compose"));

var _defaultProps = _interopRequireDefault(require("recompose/defaultProps"));

var _routes = require("../../page/api/routes");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _polished = require("polished");

var _core = require("@material-ui/core");

var _Poster = _interopRequireDefault(require("../../image/web/Poster"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

// import Video                from 'app/honey/video/web/Video'
var enhance = (0, _compose.default)((0, _defaultProps.default)({
  A: {
    Container: _styledComponents.default.div.withConfig({
      displayName: "Post__Container",
      componentId: "sc-1wc6h4j-0"
    })(["&&{text-align:center;&.dark{padding:", " ", ";background-color:rgba(0,0,0,0.2);}}"], (0, _polished.rem)(70), (0, _polished.rem)(45)),
    TagLine: (0, _styledComponents.default)(function (_ref) {
      var color = _ref.color,
          transform = _ref.transform,
          border = _ref.border,
          fweight = _ref.fweight,
          other = _objectWithoutProperties(_ref, ["color", "transform", "border", "fweight"]);

      return _react.default.createElement(_core.Typography, other);
    }).withConfig({
      displayName: "Post__TagLine",
      componentId: "sc-1wc6h4j-1"
    })(["&&{font-size:", ";font-weight:", ";color:", ";text-align:center;font-style:normal;line-height:normal;&.dark{padding:", " ", ";background-color:rgba(0,0,0,0.2);}}"], (0, _polished.rem)(35), function (props) {
      return props.fweight || 400;
    }, function (props) {
      return props.color;
    }, (0, _polished.rem)(70), (0, _polished.rem)(45)),
    Title: (0, _styledComponents.default)(_core.Typography).withConfig({
      displayName: "Post__Title",
      componentId: "sc-1wc6h4j-2"
    })(["&&{color:", ";margin:", ";font-weight:", ";min-width:auto;width:100%;font-size:", ";text-align:center;font-size:", ";text-transform:uppercase;}"], function (props) {
      return props.textColor || '#fff';
    }, function (props) {
      return props.margin;
    }, function (props) {
      return props.fweight || '700';
    }, (0, _polished.rem)(35), (0, _polished.rem)(38)),
    Link: _styledComponents.default.div.withConfig({
      displayName: "Post__Link",
      componentId: "sc-1wc6h4j-3"
    })(["&&{text-align:center;color:", ";font-size:", ";line-height:1.4;font-weight:", ";font-style:italic;margin:", ";padding:", ";a{text-decoration:underline;color:", ";font-weight:", ";&:hover{text-decoration:none;}}}"], function (props) {
      return props.color || '#fff';
    }, function (props) {
      return props.size;
    }, function (props) {
      return props.bold || 'normal';
    }, function (props) {
      return props.margin || '0';
    }, function (props) {
      return props.padding || '0';
    }, function (props) {
      return props.color || '#fff';
    }, function (props) {
      return props.bold || 'normal';
    })
  }
}));
var Post = enhance(function (_ref2) {
  var content = _ref2.content,
      images = _ref2.images,
      link = _ref2.link,
      title = _ref2.title,
      videos = _ref2.videos,
      A = _ref2.A,
      id = _ref2.id;
  return images.map(function (doc, index) {
    return index === 0 && _react.default.createElement(_Poster.default, _extends({
      key: doc.id
    }, doc), _react.default.createElement(_core.Grid, {
      item: true,
      xs: 10,
      sm: 8
    }, _react.default.createElement(A.Container, {
      className: 'dark'
    }, _react.default.createElement(A.Title, {
      fweight: "700",
      border: "0",
      margin: "0 0 1.25rem 0",
      transform: "uppercase"
    }, title), _react.default.createElement(A.TagLine, {
      color: "#fff",
      fweight: "300",
      dangerouslySetInnerHTML: {
        __html: content.brief
      }
    }), _react.default.createElement(A.Link, {
      bold: "600",
      size: "0.875rem",
      margin: "1.875rem auto 0.9375rem",
      variant: "button",
      gutterBottom: true
    }, _react.default.createElement(_routes.Link, {
      route: "post",
      params: {
        id: id
      }
    }, _react.default.createElement("a", null, "read more"))))));
  }); // {videos.map((doc) => (<Video key={doc.id} {...doc} />))}
});
var _default = Post;
exports.default = _default;
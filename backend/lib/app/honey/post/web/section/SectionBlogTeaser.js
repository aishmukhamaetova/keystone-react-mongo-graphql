"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _reactGridSystem = require("react-grid-system");

var _Block = _interopRequireDefault(require("../../../../../evoke-me/layout/web/Block"));

var _BlockBlogTeaser = _interopRequireDefault(require("../block/BlockBlogTeaser"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

var SectionBlogTeaser = function SectionBlogTeaser() {
  return _react.default.createElement(_BlockBlogTeaser.default, null);
};

var _default = SectionBlogTeaser;
exports.default = _default;
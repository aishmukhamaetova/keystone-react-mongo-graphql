"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _graphqlTag = _interopRequireDefault(require("graphql-tag"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _templateObject() {
  var data = _taggedTemplateLiteral(["\nquery {\n  post {\n    id\n    title\n    state\n    \n    author {\n      id\n      name {\n        first\n        last\n      }\n    }\n    \n    publishedDate\n    \n    content {\n    \tbrief\n      extended\n  \t}\n    \n    categories {\n      id\n      name\n    }\n    \n    images {\n      id\n      image {\n        secure_url\n        url\n      }\n    }\n    \n    videos {\n    \tid\n      videoLink\n  \t}\n    \n    isPinnedToIndex\n  }\n}\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var _default = (0, _graphqlTag.default)(_templateObject());

exports.default = _default;
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _config = _interopRequireDefault(require("../../config/api/config"));

var _query = _interopRequireDefault(require("../api/graphql/query"));

var _queryById = _interopRequireDefault(require("../api/graphql/queryById"));

var _queryBlogTeaser = _interopRequireDefault(require("../api/graphql/queryBlogTeaser"));

var _spacePost = _interopRequireDefault(require("./spacePost"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _default = [_config.default.namespace, "postGraphql", [_spacePost.default], function () {
  return {
    isLoading: false
  };
}, {
  query: function query(getState, setState, methods, _ref) {
    var fromPost = _ref.fromPost;
    return (
      /*#__PURE__*/
      _regenerator.default.mark(function _callee(payload, _ref2) {
        var client, _ref3, data, _iteratorNormalCompletion, _didIteratorError, _iteratorError, _iterator, _step, doc;

        return _regenerator.default.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                client = _ref2.client;
                _context.prev = 1;

                if (!client) {
                  _context.next = 37;
                  break;
                }

                _context.next = 5;
                return setState({
                  isLoading: true
                });

              case 5:
                _context.next = 7;
                return client.query({
                  query: _query.default
                });

              case 7:
                _ref3 = _context.sent;
                data = _ref3.data;
                _iteratorNormalCompletion = true;
                _didIteratorError = false;
                _iteratorError = undefined;
                _context.prev = 12;
                _iterator = data.post[Symbol.iterator]();

              case 14:
                if (_iteratorNormalCompletion = (_step = _iterator.next()).done) {
                  _context.next = 21;
                  break;
                }

                doc = _step.value;
                _context.next = 18;
                return fromPost.create(doc);

              case 18:
                _iteratorNormalCompletion = true;
                _context.next = 14;
                break;

              case 21:
                _context.next = 27;
                break;

              case 23:
                _context.prev = 23;
                _context.t0 = _context["catch"](12);
                _didIteratorError = true;
                _iteratorError = _context.t0;

              case 27:
                _context.prev = 27;
                _context.prev = 28;

                if (!_iteratorNormalCompletion && _iterator.return != null) {
                  _iterator.return();
                }

              case 30:
                _context.prev = 30;

                if (!_didIteratorError) {
                  _context.next = 33;
                  break;
                }

                throw _iteratorError;

              case 33:
                return _context.finish(30);

              case 34:
                return _context.finish(27);

              case 35:
                _context.next = 37;
                return setState({
                  isLoading: false
                });

              case 37:
                _context.next = 44;
                break;

              case 39:
                _context.prev = 39;
                _context.t1 = _context["catch"](1);
                _context.next = 43;
                return setState({
                  isLoading: false
                });

              case 43:
                console.error(_context.t1);

              case 44:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this, [[1, 39], [12, 23, 27, 35], [28,, 30, 34]]);
      })
    );
  },
  queryById: function queryById(getState, setState, methods, _ref4) {
    var fromPost = _ref4.fromPost;
    return (
      /*#__PURE__*/
      _regenerator.default.mark(function _callee2(_ref5, _ref6) {
        var id, client, _ref7, data, _iteratorNormalCompletion2, _didIteratorError2, _iteratorError2, _iterator2, _step2, doc;

        return _regenerator.default.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                id = _ref5.id;
                client = _ref6.client;
                _context2.prev = 2;

                if (!client) {
                  _context2.next = 38;
                  break;
                }

                _context2.next = 6;
                return setState({
                  isLoading: true
                });

              case 6:
                _context2.next = 8;
                return client.query({
                  query: _queryById.default,
                  variables: {
                    id: id
                  }
                });

              case 8:
                _ref7 = _context2.sent;
                data = _ref7.data;
                _iteratorNormalCompletion2 = true;
                _didIteratorError2 = false;
                _iteratorError2 = undefined;
                _context2.prev = 13;
                _iterator2 = data.post[Symbol.iterator]();

              case 15:
                if (_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done) {
                  _context2.next = 22;
                  break;
                }

                doc = _step2.value;
                _context2.next = 19;
                return fromPost.create(doc);

              case 19:
                _iteratorNormalCompletion2 = true;
                _context2.next = 15;
                break;

              case 22:
                _context2.next = 28;
                break;

              case 24:
                _context2.prev = 24;
                _context2.t0 = _context2["catch"](13);
                _didIteratorError2 = true;
                _iteratorError2 = _context2.t0;

              case 28:
                _context2.prev = 28;
                _context2.prev = 29;

                if (!_iteratorNormalCompletion2 && _iterator2.return != null) {
                  _iterator2.return();
                }

              case 31:
                _context2.prev = 31;

                if (!_didIteratorError2) {
                  _context2.next = 34;
                  break;
                }

                throw _iteratorError2;

              case 34:
                return _context2.finish(31);

              case 35:
                return _context2.finish(28);

              case 36:
                _context2.next = 38;
                return setState({
                  isLoading: false
                });

              case 38:
                _context2.next = 45;
                break;

              case 40:
                _context2.prev = 40;
                _context2.t1 = _context2["catch"](2);
                _context2.next = 44;
                return setState({
                  isLoading: false
                });

              case 44:
                console.error(_context2.t1);

              case 45:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, this, [[2, 40], [13, 24, 28, 36], [29,, 31, 35]]);
      })
    );
  },
  queryBlogTeaser: function queryBlogTeaser(getState, setState, methods, _ref8) {
    var fromPost = _ref8.fromPost;
    return (
      /*#__PURE__*/
      _regenerator.default.mark(function _callee3(payload, _ref9) {
        var client, _ref10, data, _iteratorNormalCompletion3, _didIteratorError3, _iteratorError3, _iterator3, _step3, doc;

        return _regenerator.default.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                client = _ref9.client;
                _context3.prev = 1;

                if (!client) {
                  _context3.next = 39;
                  break;
                }

                _context3.next = 5;
                return setState({
                  isLoading: true
                });

              case 5:
                _context3.next = 7;
                return client.query({
                  query: _queryBlogTeaser.default
                });

              case 7:
                _ref10 = _context3.sent;
                data = _ref10.data;
                _context3.next = 11;
                return fromPost.orderClear('blogTeaser');

              case 11:
                _iteratorNormalCompletion3 = true;
                _didIteratorError3 = false;
                _iteratorError3 = undefined;
                _context3.prev = 14;
                _iterator3 = data.post[Symbol.iterator]();

              case 16:
                if (_iteratorNormalCompletion3 = (_step3 = _iterator3.next()).done) {
                  _context3.next = 23;
                  break;
                }

                doc = _step3.value;
                _context3.next = 20;
                return fromPost.create(doc, {
                  orderName: "blogTeaser"
                });

              case 20:
                _iteratorNormalCompletion3 = true;
                _context3.next = 16;
                break;

              case 23:
                _context3.next = 29;
                break;

              case 25:
                _context3.prev = 25;
                _context3.t0 = _context3["catch"](14);
                _didIteratorError3 = true;
                _iteratorError3 = _context3.t0;

              case 29:
                _context3.prev = 29;
                _context3.prev = 30;

                if (!_iteratorNormalCompletion3 && _iterator3.return != null) {
                  _iterator3.return();
                }

              case 32:
                _context3.prev = 32;

                if (!_didIteratorError3) {
                  _context3.next = 35;
                  break;
                }

                throw _iteratorError3;

              case 35:
                return _context3.finish(32);

              case 36:
                return _context3.finish(29);

              case 37:
                _context3.next = 39;
                return setState({
                  isLoading: false
                });

              case 39:
                _context3.next = 46;
                break;

              case 41:
                _context3.prev = 41;
                _context3.t1 = _context3["catch"](1);
                _context3.next = 45;
                return setState({
                  isLoading: false
                });

              case 45:
                console.error(_context3.t1);

              case 46:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3, this, [[1, 41], [14, 25, 29, 37], [30,, 32, 36]]);
      })
    );
  }
}];
exports.default = _default;
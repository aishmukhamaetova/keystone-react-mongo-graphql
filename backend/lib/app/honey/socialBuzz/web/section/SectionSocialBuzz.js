"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _BlockSocialBuzz = _interopRequireDefault(require("../block/BlockSocialBuzz"));

var _react = _interopRequireWildcard(require("react"));

var _core = require("@material-ui/core");

var _compose = _interopRequireDefault(require("recompose/compose"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _polished = require("polished");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

var enhance = (0, _compose.default)();
var Title = (0, _styledComponents.default)(function (_ref) {
  var color = _ref.color,
      transform = _ref.transform,
      border = _ref.border,
      margin = _ref.margin,
      fweight = _ref.fweight,
      other = _objectWithoutProperties(_ref, ["color", "transform", "border", "margin", "fweight"]);

  return _react.default.createElement(_core.Typography, other);
}).withConfig({
  displayName: "SectionSocialBuzz__Title",
  componentId: "llyjm0-0"
})(["&&{color:", ";border-bottom:", ";text-transform:", ";border-color:#000;margin:", ";font-weight:", ";min-width:auto;width:100%;text-align:center;font-size:", ";}"], function (props) {
  return props.color;
}, function (props) {
  return props.border;
}, function (props) {
  return props.transform || 'inherit';
}, function (props) {
  return props.margin;
}, function (props) {
  return props.fweight || '700';
}, (0, _polished.rem)(35));
var SectionSocialBuzz = enhance(function () {
  return _react.default.createElement(_react.default.Fragment, null, _react.default.createElement(Title, {
    transform: "uppercase",
    margin: "0 0 2.25rem 0"
  }, "Sozial Buzz"), _react.default.createElement(_BlockSocialBuzz.default, null));
});
var _default = SectionSocialBuzz;
exports.default = _default;
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _reactApollo = require("react-apollo");

var _recompose = require("recompose");

var _semanticUiReact = require("semantic-ui-react");

var _withSpace = _interopRequireDefault(require("../../../../../evoke-me/space/all/withSpace"));

var _spaceSocialBuzz = _interopRequireDefault(require("../../space/spaceSocialBuzz"));

var _spaceSocialBuzzGraphql = _interopRequireDefault(require("../../space/spaceSocialBuzzGraphql"));

var _SocialBuzzList = _interopRequireDefault(require("../SocialBuzzList"));

var _core = require("@material-ui/core");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

var enhance = (0, _recompose.compose)(_reactApollo.withApollo, (0, _withSpace.default)(_spaceSocialBuzz.default), (0, _withSpace.default)(_spaceSocialBuzzGraphql.default), (0, _recompose.lifecycle)({
  componentDidMount: function componentDidMount() {
    var client = this.props.client;
    var fromSocialBuzzGraphql = this.props.fromSocialBuzzGraphql;
    var fromSocialBuzz = this.props.fromSocialBuzz;
    var orderMap = fromSocialBuzz.orderMap;

    if (!orderMap.default.length) {
      fromSocialBuzzGraphql.query(client);
    }
  }
}));
var BlockSocialBuzz = enhance(function (_ref) {
  var fromSocialBuzz = _ref.fromSocialBuzz,
      fromSocialBuzzGraphql = _ref.fromSocialBuzzGraphql;
  var data = fromSocialBuzz.data,
      orderMap = fromSocialBuzz.orderMap;
  var isLoading = fromSocialBuzzGraphql.isLoading;

  if (!orderMap.default || !orderMap.default.length) {
    return _react.default.createElement(_semanticUiReact.Segment, {
      placeholder: true,
      loading: isLoading
    }, _react.default.createElement(_semanticUiReact.Header, {
      icon: true
    }, _react.default.createElement(_semanticUiReact.Icon, {
      name: "hand peace"
    }), "BlockSocialBuzz is empty!"));
  }

  return _react.default.createElement(_core.Grid, {
    container: true,
    direction: 'row',
    justify: "space-evenly",
    alignItems: 'center',
    spacing: 0
  }, _react.default.createElement(_SocialBuzzList.default, {
    data: data,
    order: orderMap.default.slice(0, 5)
  }));
});
var _default = BlockSocialBuzz;
exports.default = _default;
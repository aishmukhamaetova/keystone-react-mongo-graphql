"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _compose = _interopRequireDefault(require("recompose/compose"));

var _defaultProps = _interopRequireDefault(require("recompose/defaultProps"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _core = require("@material-ui/core");

var _polished = require("polished");

var _icons = require("../../icons");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

var enhance = (0, _compose.default)((0, _defaultProps.default)({
  A: {
    SocialThumbnail: (0, _styledComponents.default)(_core.Grid).withConfig({
      displayName: "SocialBuzz__SocialThumbnail",
      componentId: "sc-189z26w-0"
    })(["position:relative;background:rgba(0,0,0,0.2);"]),
    SocialThumbnailLink: _styledComponents.default.a.withConfig({
      displayName: "SocialBuzz__SocialThumbnailLink",
      componentId: "sc-189z26w-1"
    })(["position:relative;width:100%;height:100%;text-align:right;display:block;overflow:hidden;border-right:3px solid white;"]),
    SocialThumbnailImg: _styledComponents.default.img.withConfig({
      displayName: "SocialBuzz__SocialThumbnailImg",
      componentId: "sc-189z26w-2"
    })(["width:100%;height:100%;text-align:right;display:block;overflow:hidden;border-right:3px solid white;"]),
    SocialIcon: _styledComponents.default.div.withConfig({
      displayName: "SocialBuzz__SocialIcon",
      componentId: "sc-189z26w-3"
    })(["display:flex;position:absolute;bottom:0;right:", ";height:", ";svg{height:100%;}"], (0, _polished.rem)(20), (0, _polished.rem)(45))
  }
}));
var SocialBuzz = enhance(function (_ref) {
  var _ref$imageLink = _ref.imageLink,
      imageLink = _ref$imageLink === void 0 ? '' : _ref$imageLink,
      _ref$linkInstagram = _ref.linkInstagram,
      linkInstagram = _ref$linkInstagram === void 0 ? '' : _ref$linkInstagram,
      _ref$name = _ref.name,
      name = _ref$name === void 0 ? '' : _ref$name,
      A = _ref.A,
      props = _objectWithoutProperties(_ref, ["imageLink", "linkInstagram", "name", "A"]);

  return _react.default.createElement(A.SocialThumbnail, {
    md: 3,
    sm: 6,
    xs: 12,
    item: true
  }, _react.default.createElement(A.SocialThumbnailLink, {
    href: linkInstagram,
    target: "_blank",
    rel: "noopener noreferrer"
  }, _react.default.createElement(A.SocialThumbnailImg, {
    src: imageLink,
    alt: name
  }), _react.default.createElement(A.SocialIcon, null, _react.default.createElement(_icons.IconInstagram, {
    style: {
      fontSize: 22,
      fill: '#fff'
    }
  }))));
});
var _default = SocialBuzz;
exports.default = _default;
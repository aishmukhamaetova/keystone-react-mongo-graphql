"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _config = _interopRequireDefault(require("../../config/api/config"));

var _query = _interopRequireDefault(require("../api/graphql/query"));

var _spaceSocialBuzz = _interopRequireDefault(require("./spaceSocialBuzz"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var _default = [_config.default.namespace, "socialBuzzGraphql", [_spaceSocialBuzz.default], function () {
  return {
    isLoading: false
  };
}, {
  query: function query(getState, setState, methods, _ref) {
    var fromSocialBuzz = _ref.fromSocialBuzz;
    return (
      /*#__PURE__*/
      _regenerator.default.mark(function _callee(client) {
        var _ref2, data, _iteratorNormalCompletion, _didIteratorError, _iteratorError, _iterator, _step, doc, linkInstagram, result, imageLink;

        return _regenerator.default.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.prev = 0;

                if (!client) {
                  _context.next = 42;
                  break;
                }

                _context.next = 4;
                return setState({
                  isLoading: true
                });

              case 4:
                _context.next = 6;
                return client.query({
                  query: _query.default
                });

              case 6:
                _ref2 = _context.sent;
                data = _ref2.data;
                _iteratorNormalCompletion = true;
                _didIteratorError = false;
                _iteratorError = undefined;
                _context.prev = 11;
                _iterator = data.socialBuzz[Symbol.iterator]();

              case 13:
                if (_iteratorNormalCompletion = (_step = _iterator.next()).done) {
                  _context.next = 26;
                  break;
                }

                doc = _step.value;
                linkInstagram = doc.linkInstagram;
                _context.next = 18;
                return fetch("".concat(linkInstagram, "media"));

              case 18:
                result = _context.sent;
                imageLink = null;

                if (result.status === 200) {
                  imageLink = result.url;
                }

                _context.next = 23;
                return fromSocialBuzz.create(_objectSpread({}, doc, {
                  imageLink: imageLink
                }));

              case 23:
                _iteratorNormalCompletion = true;
                _context.next = 13;
                break;

              case 26:
                _context.next = 32;
                break;

              case 28:
                _context.prev = 28;
                _context.t0 = _context["catch"](11);
                _didIteratorError = true;
                _iteratorError = _context.t0;

              case 32:
                _context.prev = 32;
                _context.prev = 33;

                if (!_iteratorNormalCompletion && _iterator.return != null) {
                  _iterator.return();
                }

              case 35:
                _context.prev = 35;

                if (!_didIteratorError) {
                  _context.next = 38;
                  break;
                }

                throw _iteratorError;

              case 38:
                return _context.finish(35);

              case 39:
                return _context.finish(32);

              case 40:
                _context.next = 42;
                return setState({
                  isLoading: false
                });

              case 42:
                _context.next = 49;
                break;

              case 44:
                _context.prev = 44;
                _context.t1 = _context["catch"](0);
                _context.next = 48;
                return setState({
                  isLoading: false
                });

              case 48:
                console.error(_context.t1);

              case 49:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this, [[0, 44], [11, 28, 32, 40], [33,, 35, 39]]);
      })
    );
  }
}];
exports.default = _default;
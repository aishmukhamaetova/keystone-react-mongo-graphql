"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _core = require("@material-ui/core");

var _icons = require("../../../icons");

var _compose = _interopRequireDefault(require("recompose/compose"));

var _defaultProps = _interopRequireDefault(require("recompose/defaultProps"));

var _polished = require("polished");

var _styledComponents = _interopRequireDefault(require("styled-components"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

var TypographyItalic = (0, _styledComponents.default)(function (_ref) {
  var size = _ref.size,
      align = _ref.align,
      margin = _ref.margin,
      bold = _ref.bold,
      color = _ref.color,
      other = _objectWithoutProperties(_ref, ["size", "align", "margin", "bold", "color"]);

  return _react.default.createElement(_core.Typography, other);
}).withConfig({
  displayName: "SectionTestimonial__TypographyItalic",
  componentId: "pgvysr-0"
})(["&&{color:", ";font-size:", ";text-align:", ";line-height:1.4;font-weight:", ";font-style:italic;margin:", ";padding:", ";a{text-decoration:underline;color:", ";font-weight:", ";&:hover{text-decoration:none;}}}"], function (props) {
  return props.color || '#fff';
}, function (props) {
  return props.size;
}, function (props) {
  return props.align || 'left';
}, function (props) {
  return props.bold || 'normal';
}, function (props) {
  return props.margin || '0';
}, function (props) {
  return props.padding || '0';
}, function (props) {
  return props.color || '#fff';
}, function (props) {
  return props.bold || 'normal';
});
var AvatarUI = (0, _styledComponents.default)(function (_ref2) {
  var width = _ref2.width,
      height = _ref2.height,
      margin = _ref2.margin,
      other = _objectWithoutProperties(_ref2, ["width", "height", "margin"]);

  return _react.default.createElement(_core.Avatar, other);
}).withConfig({
  displayName: "SectionTestimonial__AvatarUI",
  componentId: "pgvysr-1"
})(["&&{width:", ";height:", ";margin:", ";}"], function (props) {
  return props.width || 'auto';
}, function (props) {
  return props.height || 'auto';
}, function (props) {
  return props.margin || 'auto';
});
var Title = (0, _styledComponents.default)(function (_ref3) {
  var color = _ref3.color,
      transform = _ref3.transform,
      border = _ref3.border,
      margin = _ref3.margin,
      fweight = _ref3.fweight,
      other = _objectWithoutProperties(_ref3, ["color", "transform", "border", "margin", "fweight"]);

  return _react.default.createElement(_core.Typography, other);
}).withConfig({
  displayName: "SectionTestimonial__Title",
  componentId: "pgvysr-2"
})(["&&{color:", ";border-bottom:", ";text-transform:", ";border-color:", ";margin:", ";font-weight:", ";min-width:auto;width:100%;text-align:center;font-size:", ";}"], function (props) {
  return props.color;
}, function (props) {
  return props.border;
}, function (props) {
  return props.transform || 'inherit';
}, function (props) {
  return props.color;
}, function (props) {
  return props.margin;
}, function (props) {
  return props.fweight || '700';
}, (0, _polished.rem)(35));

var styles = function styles(theme) {
  return {};
};

var GridCustom = (0, _styledComponents.default)(function (_ref4) {
  var background = _ref4.background,
      margin = _ref4.margin,
      other = _objectWithoutProperties(_ref4, ["background", "margin"]);

  return _react.default.createElement(_core.Grid, other);
}).withConfig({
  displayName: "SectionTestimonial__GridCustom",
  componentId: "pgvysr-3"
})(["&&{background:", ";margin:", ";padding:", ";}"], function (props) {
  return props.background;
}, function (props) {
  return props.margin;
}, function (props) {
  return props.padding;
});
var enhance = (0, _compose.default)((0, _defaultProps.default)({
  A: {
    PaperCustom: (0, _styledComponents.default)(_core.Paper).withConfig({
      displayName: "SectionTestimonial__PaperCustom",
      componentId: "pgvysr-4"
    })(["&&{box-shadow:none;background:none;padding:", " 0 ", ";}"], (0, _polished.rem)(40), (0, _polished.rem)(20))
  }
}));
var Testimonial = enhance(function (_ref5) {
  var A = _ref5.A;
  return _react.default.createElement(_core.Grid, {
    container: true,
    alignItems: "stretch",
    direction: "row",
    justify: "center",
    spacing: 0
  }, _react.default.createElement(_core.Grid, {
    container: true,
    spacing: 0,
    justify: "center"
  }, _react.default.createElement(_core.Grid, {
    item: true
  }, _react.default.createElement(Title, {
    size: "1.875em",
    color: "#000",
    transform: "uppercase",
    justify: "center",
    margin: "0 0 1.5rem 0"
  }, "Testimonial"))), _react.default.createElement(GridCustom, {
    background: "#ececec",
    container: true,
    spacing: 0,
    justify: "center",
    alignItems: "stretch",
    direction: "row"
  }, _react.default.createElement(_core.Grid, {
    item: true,
    xs: 10,
    sm: 8
  }, _react.default.createElement(A.PaperCustom, {
    square: true
  }, _react.default.createElement(_core.Grid, {
    container: true,
    spacing: 40,
    justify: "center",
    alignItems: "center"
  }, _react.default.createElement(_core.Grid, {
    item: true
  }, _react.default.createElement(AvatarUI, {
    width: "150px",
    height: "150px",
    src: "https://via.placeholder.com/150",
    alt: "avatar"
  })), _react.default.createElement(_core.Grid, {
    item: true,
    xs: 12,
    sm: true,
    container: true,
    justify: "center"
  }, _react.default.createElement(TypographyItalic, {
    color: "#000",
    bold: "600",
    align: "center",
    size: "1.125rem",
    variant: "body1",
    gutterBottom: true
  }, "Honey, just allow me one more chance to get along with you Honey, just allow me one more chance Ah'll do anything with you. Well, I'm a-walkin' down the road with my head in my hand I'm lookin' for a woman needs a worried man Just-a one kind favor I ask you Allow me just-a one more chance."), _react.default.createElement(TypographyItalic, {
    color: "#000",
    margin: "1rem 0 0;",
    bold: "300",
    align: "center",
    size: "1.125rem",
    variant: "body1",
    gutterBottom: true
  }, "\u2014\xA0Bob Dylan \xA0", _react.default.createElement(_core.ButtonBase, null, Array(4).fill(null).map(function (i, idx) {
    return _react.default.createElement("span", {
      key: idx
    }, _react.default.createElement(_icons.IconStar, null));
  })))))))));
});

var _default = (0, _core.withStyles)(styles)(Testimonial);

exports.default = _default;
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.PlayVidIcon = exports.IconStarz = exports.SwipeRightIcon = exports.SwipeLeftIcon = exports.IconStar = exports.IconVimeo = exports.IconYoutube = exports.IconInstagram = exports.IconSearch = exports.IconFacebook = exports.IconMinus = exports.IconPlus = exports.IconCart = exports.IconMenu = exports.IconLogo = void 0;

var _react = _interopRequireDefault(require("react"));

var _SvgIcon = _interopRequireDefault(require("@material-ui/core/SvgIcon"));

var _core = require("@material-ui/core");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

var IconMinus = function IconMinus(props) {
  return _react.default.createElement(_core.IconButton, {
    disableripple: "true"
  }, _react.default.createElement(_SvgIcon.default, _extends({
    width: "20px",
    height: "1px",
    viewBox: "0 0 20 1",
    version: "1.1",
    xmlns: "http://www.w3.org/2000/svg"
  }, props), _react.default.createElement("g", {
    id: "Page-1",
    stroke: "none",
    strokeWidth: "1",
    fill: "none",
    fillRule: "evenodd",
    strokeLinecap: "square"
  }, _react.default.createElement("g", {
    id: "Desktop-HD",
    transform: "translate(-596.000000, -2435.000000)",
    stroke: "#000000"
  }, _react.default.createElement("path", {
    d: "M616,2435.5 L596,2435.5 L616,2435.5 Z",
    id: "Line-Copy"
  })))));
};

exports.IconMinus = IconMinus;

var IconPlus = function IconPlus(props) {
  return _react.default.createElement(_core.IconButton, {
    disableripple: "true"
  }, _react.default.createElement(_SvgIcon.default, _extends({
    width: "21px",
    height: "20px",
    viewBox: "0 0 21 20",
    version: "1.1",
    xmlns: "http://www.w3.org/2000/svg"
  }, props), _react.default.createElement("g", {
    id: "Page-1",
    stroke: "none",
    strokeWidth: "1",
    fill: "none",
    fillRule: "evenodd",
    strokeLinecap: "square"
  }, _react.default.createElement("g", {
    id: "Desktop-HD",
    transform: "translate(-837.000000, -2425.000000)",
    stroke: "#000000"
  }, _react.default.createElement("g", {
    id: "Group-10",
    transform: "translate(837.000000, 2425.000000)"
  }, _react.default.createElement("path", {
    d: "M10.5,0.5 L10.5,19.5",
    id: "Line"
  }), _react.default.createElement("path", {
    d: "M20.4101563,10 L0.58984375,10 L20.4101563,10 Z",
    id: "Line-Copy"
  }))))));
};

exports.IconPlus = IconPlus;

var IconCart = function IconCart(props) {
  return _react.default.createElement(_core.IconButton, {
    disableripple: "true",
    style: {
      marginTop: '-10px',
      position: 'relative'
    }
  }, _react.default.createElement(_SvgIcon.default, _extends({
    width: "14px",
    height: "17px",
    viewBox: "0 0 14 17",
    version: "1.1",
    xmlns: "http://www.w3.org/2000/svg"
  }, props), _react.default.createElement("g", {
    id: "Page-1",
    stroke: "none",
    strokeWidth: "1",
    fill: "none",
    fillRule: "evenodd"
  }, _react.default.createElement("g", {
    id: "Artboard",
    transform: "translate(-181.000000, -118.000000)",
    fill: "#4a4a4a",
    fillRule: "nonzero"
  }, _react.default.createElement("g", {
    id: "image_large_shop",
    transform: "translate(181.000000, 118.000000)"
  }, _react.default.createElement("path", {
    d: "M12.5,3 L10.69,3 C10.41,1.14 8.65,0 7,0 C5.35,0 3.58,1.14 3.31,3 L1.5,3 C0.67,3 0,3.67 0,4.5 L0,15.5 C0,16.33 0.67,17 1.5,17 L12.5,17 C13.33,17 14,16.33 14,15.5 L14,4.5 C14,3.67 13.33,3 12.5,3 Z M7,1 C8.18,1 9.43,1.75 9.7,3 L4.3,3 C4.57,1.75 5.82,1 7,1 Z M13,15.5 C13,15.78 12.78,16 12.5,16 L1.5,16 C1.22,16 1,15.78 1,15.5 L1,4.5 C1,4.22 1.22,4 1.5,4 L12.5,4 C12.78,4 13,4.22 13,4.5 L13,15.5 Z",
    id: "Shape"
  }))))));
};

exports.IconCart = IconCart;

var IconMenu = function IconMenu(props) {
  return _react.default.createElement(_SvgIcon.default, props, _react.default.createElement("g", {
    id: "Page-1",
    stroke: "none",
    strokeWidth: "1",
    fill: "none",
    fillRule: "evenodd"
  }, _react.default.createElement("g", {
    id: "Desktop-HD",
    transform: "translate(-1362.000000, -654.000000)",
    fill: "#4A4A4A"
  }, _react.default.createElement("g", {
    id: "Group-3",
    transform: "translate(53.000000, 648.000000)"
  }, _react.default.createElement("g", {
    id: "Rectangle-3",
    transform: "translate(1309.000000, 6.000000)"
  }, _react.default.createElement("rect", {
    x: "0",
    y: "12",
    width: "28",
    height: "1"
  }), _react.default.createElement("rect", {
    id: "Rectangle-3-Copy",
    x: "0",
    y: "0",
    width: "28",
    height: "1"
  }), _react.default.createElement("rect", {
    id: "Rectangle-3-Copy-2",
    x: "0",
    y: "23",
    width: "28",
    height: "1"
  }))))));
};

exports.IconMenu = IconMenu;

var IconLogo = function IconLogo(props) {
  var coloricon = props.coloricon;
  return _react.default.createElement(_SvgIcon.default, _extends({
    fontSize: "inherit",
    width: "221px",
    height: "36px",
    viewBox: "0 0 221 36",
    version: "1.1",
    xmlns: "http://www.w3.org/2000/svg"
  }, props), _react.default.createElement("title", null, "symbol"), _react.default.createElement("desc", null, "Created with Sketch."), _react.default.createElement("defs", null, _react.default.createElement("polygon", {
    id: "path-1",
    points: "0 36 220.958884 36 220.958884 0.249636364 0 0.249636364"
  })), _react.default.createElement("g", {
    id: "Page-1",
    stroke: "none",
    strokeWidth: "1",
    fill: coloricon || '#ffffff',
    fillRule: "evenodd"
  }, _react.default.createElement("g", {
    id: "Desktop-HD",
    transform: "translate(-343.000000, -108.000000)"
  }, _react.default.createElement("g", {
    id: "symbol",
    transform: "translate(343.000000, 108.000000)"
  }, _react.default.createElement("g", {
    id: "Group-24"
  }, _react.default.createElement("polygon", {
    id: "Fill-1",
    fill: coloricon || '#ffffff',
    points: "24 25 24 11 25.4052916 11 25.4052916 23.8594569 33 23.8594569 33 25"
  }), _react.default.createElement("polygon", {
    id: "Fill-2",
    fill: coloricon || '#ffffff',
    points: "33.1703638 11 34.7657498 11 35 13.3697449 33.5323869 16 33 15.4610133 33.9370009 13.283507 33.1703638 13.283507"
  }), _react.default.createElement("path", {
    d: "M46.0925676,21.3599333 L38.9074324,21.3599333 L37.4756757,25 L36,25 L41.7744595,11 L43.2272973,11 L49,25 L47.5243243,25 L46.0925676,21.3599333 Z M39.3501351,20.2393997 L45.6498649,20.2393997 L42.4894595,12.2606003 L39.3501351,20.2393997 Z",
    id: "Fill-3",
    fill: coloricon || '#ffffff'
  }), _react.default.createElement("polygon", {
    id: "Fill-4",
    fill: coloricon || '#ffffff',
    points: "54.2056772 12.1405431 54.2056772 25 52.8139497 25 52.8139497 12.1405431 48 12.1405431 48 11 59 11 59 12.1405431"
  }), _react.default.createElement("polygon", {
    id: "Fill-5",
    fill: coloricon || '#ffffff',
    points: "62 25 62 11 70.8752166 11 70.8752166 12.1405431 63.3500867 12.1405431 63.3500867 17.4397332 69.5857886 17.4397332 69.5857886 18.5602668 63.3500867 18.5602668 63.3500867 23.8594569 71 23.8594569 71 25"
  }), _react.default.createElement("polygon", {
    id: "Fill-6",
    fill: coloricon || '#ffffff',
    points: "74 25 74 11 75.4052916 11 75.4052916 23.8594569 83 23.8594569 83 25"
  }), _react.default.createElement("polygon", {
    id: "Fill-7",
    fill: coloricon || '#ffffff',
    points: "85.221 25 86.779 25 86.779 11 85.221 11"
  }), _react.default.createElement("polygon", {
    id: "Fill-8",
    fill: coloricon || '#ffffff',
    points: "90 25 90 11 98.8752166 11 98.8752166 12.1405431 91.3500867 12.1405431 91.3500867 17.4397332 97.5857886 17.4397332 97.5857886 18.5602668 91.3500867 18.5602668 91.3500867 23.8594569 99 23.8594569 99 25"
  }), _react.default.createElement("g", {
    id: "Group-23"
  }, _react.default.createElement("path", {
    d: "M108.0027,19.1149495 L104.118925,19.1149495 L104.118925,25.2485657 L102.784359,25.2485657 L102.784359,11.172101 L108.126049,11.172101 C111.043591,11.172101 113.364948,12.1580404 113.364948,15.1326263 C113.364948,17.7081414 111.619219,18.8131313 109.337266,19.0545859 L113.385506,25.2485657 L111.864204,25.2485657 L108.0027,19.1149495 Z M108.208281,12.2988889 L104.118925,12.2988889 L104.118925,17.9898384 L108.105491,17.9898384 C110.365173,17.9898384 111.989266,17.2051111 111.989266,15.1326263 C111.989266,13.0618182 110.365173,12.2988889 108.208281,12.2988889 Z",
    id: "Fill-9",
    fill: coloricon || '#ffffff'
  }), _react.default.createElement("path", {
    d: "M126.609358,25.248901 L122.398366,25.248901 L122.398366,11.1707596 L126.609358,11.1707596 C130.678157,11.1707596 133.246211,13.1828808 133.246211,18.2098303 C133.246211,23.2384566 130.678157,25.248901 126.609358,25.248901 M126.568242,12.3176687 L123.731219,12.3176687 L123.731219,24.1019919 L126.568242,24.1019919 C130.224164,24.1019919 131.827699,22.232396 131.827699,18.2098303 C131.827699,14.1889414 130.224164,12.3176687 126.568242,12.3176687",
    id: "Fill-11",
    fill: coloricon || '#ffffff'
  }), _react.default.createElement("path", {
    d: "M146.958319,20.2816444 C146.958319,23.1975434 145.704272,25.4695636 141.781094,25.4695636 C137.815086,25.4695636 136.56104,23.1975434 136.56104,20.2816444 L136.56104,11.1717657 L137.895605,11.1717657 L137.895605,20.2816444 C137.895605,22.515099 138.596295,24.3042101 141.781094,24.3042101 C144.924776,24.3042101 145.62204,22.515099 145.62204,20.2816444 L145.62204,11.1717657 L146.958319,11.1717657 L146.958319,20.2816444 Z",
    id: "Fill-13",
    fill: coloricon || '#ffffff'
  }), _react.default.createElement("polygon", {
    id: "Fill-15",
    fill: coloricon || '#ffffff',
    points: "168.378016 13.3837576 163.673628 22.8960606 162.727953 22.8960606 158.021853 13.4038788 158.021853 25.2485657 156.689 25.2485657 156.689 11.172101 158.105798 11.172101 163.202504 21.4272121 168.275225 11.172101 169.712581 11.172101 169.712581 25.2485657 168.378016 25.2485657"
  }), _react.default.createElement("mask", {
    id: "mask-2",
    fill: "white"
  }, _react.default.createElement("use", {
    xlinkHref: "#path-1"
  })), _react.default.createElement("g", {
    id: "Clip-18"
  }), _react.default.createElement("polygon", {
    id: "Fill-17",
    fill: coloricon || '#ffffff',
    mask: "url(#mask-2)",
    points: "173.873891 25.2485657 175.206744 25.2485657 175.206744 11.1704242 173.873891 11.1704242"
  }), _react.default.createElement("polygon", {
    id: "Fill-19",
    fill: coloricon || '#ffffff',
    mask: "url(#mask-2)",
    points: "179.383302 25.248901 179.383302 11.1707596 188.156488 11.1707596 188.156488 12.3176687 180.717867 12.3176687 180.717867 17.6464364 186.883596 17.6464364 186.883596 18.7732242 180.717867 18.7732242 180.717867 24.1019919 188.279836 24.1019919 188.279836 25.248901"
  }), _react.default.createElement("polygon", {
    id: "Fill-20",
    fill: coloricon || '#ffffff',
    mask: "url(#mask-2)",
    points: "191.489305 25.248901 191.489305 11.1707596 192.827297 11.1707596 192.827297 24.1019919 200.038064 24.1019919 200.038064 25.248901"
  }), _react.default.createElement("polygon", {
    id: "Stroke-21",
    fill: coloricon || '#ffffff',
    fillRule: "nonzero",
    mask: "url(#mask-2)",
    points: "-0.000342635659 35.8304727 -0.000342635659 35.3304727 220.891727 35.3304727 220.891727 35.8304727"
  }), _react.default.createElement("polygon", {
    id: "Stroke-22",
    fill: coloricon || '#ffffff',
    fillRule: "nonzero",
    mask: "url(#mask-2)",
    points: "0.0616744186 0.918828283 0.0616744186 0.418828283 220.958884 0.418828283 220.958884 0.918828283"
  })))))));
};

exports.IconLogo = IconLogo;

var IconSearch = function IconSearch(props) {
  return _react.default.createElement(_core.ButtonBase, null, _react.default.createElement(_SvgIcon.default, _extends({
    width: "16px",
    height: "16px",
    viewBox: "0 0 16 16",
    version: "1.1",
    xmlns: "http://www.w3.org/2000/svg"
  }, props), _react.default.createElement("g", {
    id: "Welcome",
    stroke: "none",
    strokeWidth: "1",
    fill: "none",
    fillRule: "evenodd"
  }, _react.default.createElement("g", {
    id: "Store-List-View",
    transform: "translate(-1019.000000, -413.000000)",
    fill: "#000000",
    fillRule: "nonzero"
  }, _react.default.createElement("g", {
    id: "image_large",
    transform: "translate(1019.000000, 413.000000)"
  }, _react.default.createElement("path", {
    d: "M15.27,14.29 L11.21,10.23 C12.0832967,9.14447249 12.5595924,7.79320381 12.56,6.4 C12.56,3.01 9.8,0.25 6.41,0.25 C3.02,0.25 0.26,3.01 0.26,6.4 C0.26,9.79 3.02,12.55 6.41,12.55 C7.84,12.55 9.16,12.05 10.21,11.22 L14.27,15.28 L15.27,14.29 Z M6.4,11.33 C3.68504107,11.3244985 1.48550145,9.12495893 1.48,6.41 C1.48,3.7 3.68,1.49 6.4,1.49 C9.12,1.49 11.32,3.7 11.32,6.41 C11.33,9.12 9.12,11.33 6.4,11.33 Z",
    id: "Shape"
  }))))));
};

exports.IconSearch = IconSearch;

var IconInstagram = function IconInstagram(props) {
  return _react.default.createElement(_SvgIcon.default, _extends({
    xmlns: "http://www.w3.org/2000/svg",
    viewBox: "0 -10 110 110"
  }, props), _react.default.createElement("path", {
    d: "M50,9c13.35,0,14.93.05,20.2.29A27.74,27.74,0,0,1,79.49,11a15.56,15.56,0,0,1,5.75,3.74A15.56,15.56,0,0,1,89,20.51,27.74,27.74,0,0,1,90.7,29.8c.24,5.27.29,6.85.29,20.2s0,14.93-.29,20.2A27.74,27.74,0,0,1,89,79.49,16.55,16.55,0,0,1,79.49,89,27.74,27.74,0,0,1,70.2,90.7c-5.27.24-6.85.29-20.2.29s-14.93,0-20.2-.29A27.74,27.74,0,0,1,20.51,89a15.56,15.56,0,0,1-5.75-3.74A15.56,15.56,0,0,1,11,79.49,27.74,27.74,0,0,1,9.3,70.2C9.06,64.93,9,63.35,9,50s.05-14.93.29-20.2A27.74,27.74,0,0,1,11,20.51a15.56,15.56,0,0,1,3.74-5.75A15.56,15.56,0,0,1,20.51,11,27.74,27.74,0,0,1,29.8,9.3C35.07,9.06,36.65,9,50,9m0-9C36.42,0,34.72.06,29.39.3A36.77,36.77,0,0,0,17.25,2.62,24.54,24.54,0,0,0,8.39,8.39a24.54,24.54,0,0,0-5.77,8.86A36.77,36.77,0,0,0,.3,29.39C.06,34.72,0,36.42,0,50S.06,65.28.3,70.61A36.77,36.77,0,0,0,2.62,82.75a24.54,24.54,0,0,0,5.77,8.86,24.54,24.54,0,0,0,8.86,5.77A36.77,36.77,0,0,0,29.39,99.7c5.33.24,7,.3,20.61.3s15.28-.06,20.61-.3a36.77,36.77,0,0,0,12.14-2.32A25.58,25.58,0,0,0,97.38,82.75,36.77,36.77,0,0,0,99.7,70.61c.24-5.33.3-7,.3-20.61s-.06-15.28-.3-20.61a36.77,36.77,0,0,0-2.32-12.14,24.54,24.54,0,0,0-5.77-8.86,24.54,24.54,0,0,0-8.86-5.77A36.77,36.77,0,0,0,70.61.3C65.28.06,63.58,0,50,0Z"
  }), _react.default.createElement("path", {
    d: "M50,24.32A25.68,25.68,0,1,0,75.68,50,25.68,25.68,0,0,0,50,24.32Zm0,42.35A16.67,16.67,0,1,1,66.67,50,16.68,16.68,0,0,1,50,66.67Z"
  }), _react.default.createElement("circle", {
    cx: "76.69",
    cy: "23.31",
    r: "6"
  }));
};

exports.IconInstagram = IconInstagram;

var IconFacebook = function IconFacebook(props) {
  return _react.default.createElement(_SvgIcon.default, _extends({
    xmlns: "http://www.w3.org/2000/svg",
    viewBox: "0 -10 110 110"
  }, props), _react.default.createElement("path", {
    d: "M94.48,0h-89A5.52,5.52,0,0,0,0,5.52v89A5.52,5.52,0,0,0,5.52,100h47.9V61.33h-13V46.19h13V35.06c0-12.91,7.89-20,19.41-20a102.89,102.89,0,0,1,11.64.6V29.2h-8c-6.25,0-7.46,3-7.46,7.35v9.64H84L82,61.33H69V100H94.48A5.52,5.52,0,0,0,100,94.48v-89A5.52,5.52,0,0,0,94.48,0Z"
  }));
};

exports.IconFacebook = IconFacebook;

var IconYoutube = function IconYoutube(props) {
  return _react.default.createElement(_SvgIcon.default, _extends({
    xmlns: "http://www.w3.org/2000/svg",
    viewBox: "0 0 100 100"
  }, props), _react.default.createElement("path", {
    d: "M100,50s0,16.38-2.09,24.23a12.54,12.54,0,0,1-8.84,8.89C81.27,85.23,50,85.23,50,85.23s-31.27,0-39.07-2.11a12.54,12.54,0,0,1-8.84-8.89C0,66.38,0,50,0,50S0,33.62,2.09,25.77a12.54,12.54,0,0,1,8.84-8.89C18.73,14.77,50,14.77,50,14.77s31.27,0,39.07,2.11a12.54,12.54,0,0,1,8.84,8.89C100,33.62,100,50,100,50ZM39.77,64.87,65.91,50,39.77,35.13Z"
  }));
};

exports.IconYoutube = IconYoutube;

var IconVimeo = function IconVimeo(props) {
  return _react.default.createElement(_SvgIcon.default, _extends({
    xmlns: "http://www.w3.org/2000/svg",
    viewBox: "0 28 100 50"
  }, props), _react.default.createElement("path", {
    d: "M22.35,49.17c-.1,2.17-1.62,5.16-4.56,8.94Q13.23,64.05,10.07,64q-2,0-3.3-3.61C6.16,58.22,5.56,56,5,53.81,4.29,51.4,3.57,50.2,2.81,50.2a6.78,6.78,0,0,0-1.76,1L0,49.89Q1.65,48.44,3.26,47C4.74,45.71,5.84,45,6.58,45q2.61-.24,3.21,3.57.66,4.13.91,5.13.75,3.42,1.65,3.42c.47,0,1.17-.74,2.11-2.22A8.72,8.72,0,0,0,16,51.49c.13-1.27-.37-1.91-1.51-1.91a4.21,4.21,0,0,0-1.65.36q1.65-5.4,6.3-5.25C21.4,44.76,22.49,46.25,22.35,49.17Z"
  }), _react.default.createElement("path", {
    d: "M36.16,56.22A17.06,17.06,0,0,1,32.29,61,10.32,10.32,0,0,1,25.56,64a2.9,2.9,0,0,1-2.38-1,3.68,3.68,0,0,1-.78-2.61,23,23,0,0,1,1-5.11c.65-2.31,1-3.55,1-3.72,0-.87-.3-1.3-.91-1.3a5.47,5.47,0,0,0-1.7,1l-1.16-1.36L23.85,47c1.44-1.27,2.52-1.94,3.22-2a2.45,2.45,0,0,1,2.43,1,4.16,4.16,0,0,1,.58,2.93c-.43,2-.9,4.62-1.4,7.75,0,1.44.48,2.15,1.55,2.15a5.48,5.48,0,0,0,2.51-1.48A22,22,0,0,0,35.21,55ZM31.79,38.36a3.48,3.48,0,0,1-1.26,2.4,4.46,4.46,0,0,1-3.21,1.3c-1.91,0-2.83-.83-2.76-2.5a3.49,3.49,0,0,1,1.53-2.5,5.1,5.1,0,0,1,3.29-1.2,2.23,2.23,0,0,1,1.76.83A2.36,2.36,0,0,1,31.79,38.36Z"
  }), _react.default.createElement("path", {
    d: "M67.52,56.22A17,17,0,0,1,63.66,61,10.36,10.36,0,0,1,56.93,64q-3.27,0-3.17-3.62a17.82,17.82,0,0,1,.63-3.86A19.34,19.34,0,0,0,55,53c0-1.27-.36-1.91-1.16-1.91s-1.92,1-3.13,3.11a14,14,0,0,0-2.06,6.33,7.41,7.41,0,0,0,.42,3.32c-2.34.06-4-.32-4.91-1.16A4.44,4.44,0,0,1,43,59a13.72,13.72,0,0,1,.49-3.21,13,13,0,0,0,.5-2.86c.07-1.24-.39-1.86-1.36-1.86s-1.74,1-2.72,2.86a14.62,14.62,0,0,0-1.61,6,10.79,10.79,0,0,0,.36,3.92c-2.29.06-3.93-.42-4.89-1.46a5.32,5.32,0,0,1-1.1-4,26.92,26.92,0,0,1,.46-3.63,27.3,27.3,0,0,0,.46-3.63c.07-.6-.08-.91-.45-.91a5.61,5.61,0,0,0-1.71,1l-1.2-1.36c.16-.13,1.22-1.1,3.16-2.91,1.41-1.3,2.36-2,2.86-2a2.24,2.24,0,0,1,2.11,1.08,4.91,4.91,0,0,1,.71,3.69,9.11,9.11,0,0,1,1.75-2A8.89,8.89,0,0,1,46,45.48a3.75,3.75,0,0,1,3.51,1.15,4.9,4.9,0,0,1,.81,3.16l.75-.65A13.59,13.59,0,0,1,53.36,47a7.48,7.48,0,0,1,3.87-1.5,3.67,3.67,0,0,1,3.46,1.15,4.52,4.52,0,0,1,.81,3.15,23.81,23.81,0,0,1-.68,4.08,26.12,26.12,0,0,0-.63,3.07,2.7,2.7,0,0,0,.2,1.45c.17.27.57.4,1.21.4a5.58,5.58,0,0,0,2.51-1.48A22.72,22.72,0,0,0,66.57,55Z"
  }), _react.default.createElement("path", {
    d: "M86.36,56.17c-1,1.61-2.88,3.21-5.73,4.81A21.46,21.46,0,0,1,69.83,64Q65.77,64,64,61.33a7.44,7.44,0,0,1-1.21-4.42,11.21,11.21,0,0,1,3.62-7.73,12.48,12.48,0,0,1,9.24-4q4.92,0,5.22,4c.13,1.7-.8,3.46-2.81,5.26A15.38,15.38,0,0,1,70,58.19a3.14,3.14,0,0,0,2.71,1.25,20,20,0,0,0,7.89-1.84,21.21,21.21,0,0,0,4.82-2.68ZM75,50.93c0-.9-.33-1.35-1.1-1.35q-1.5,0-3.06,2.07a7,7,0,0,0-1.6,4,2.67,2.67,0,0,0,0,.65,11.28,11.28,0,0,0,4.26-2.74A4.84,4.84,0,0,0,75,50.93Z"
  }), _react.default.createElement("path", {
    d: "M100,52.8a11.94,11.94,0,0,1-3.42,8,10.59,10.59,0,0,1-8,3.36,7.29,7.29,0,0,1-6-2.56,8.2,8.2,0,0,1-1.66-4.76A10.46,10.46,0,0,1,84,48.89a11.73,11.73,0,0,1,9.09-4,6.29,6.29,0,0,1,5.32,2.36A8.08,8.08,0,0,1,100,52.8Zm-5.68-.19a6.32,6.32,0,0,0-.37-2.49c-.29-.75-.7-1.13-1.23-1.13q-2.56,0-4.22,2.77A9.76,9.76,0,0,0,87,56.59a4.36,4.36,0,0,0,.45,2.21A1.71,1.71,0,0,0,89,59.91q2.27,0,3.87-2.67A9.46,9.46,0,0,0,94.31,52.61Z"
  }));
};

exports.IconVimeo = IconVimeo;

var IconStar = function IconStar(props) {
  return _react.default.createElement(_SvgIcon.default, _extends({
    xmlns: "http://www.w3.org/2000/svg",
    viewBox: "0 0 20 20"
  }, props), _react.default.createElement("g", {
    id: "Page-1",
    stroke: "none",
    strokeWidth: "1",
    fill: "none",
    fillRule: "evenodd"
  }, _react.default.createElement("g", {
    id: "Desktop-HD",
    transform: "translate(-871.000000, -6772.000000)",
    fill: "#F5A623"
  }, _react.default.createElement("g", {
    id: "Group-19",
    transform: "translate(0.000000, 6535.000000)"
  }, _react.default.createElement("g", {
    id: "Group-14",
    transform: "translate(871.000000, 237.000000)"
  }, _react.default.createElement("polygon", {
    id: "Star",
    points: "9.5 14.25 3.9160401 17.1856614 4.98248155 10.9678307 0.464963095 6.56433855 6.70802005 5.65716928 9.5 0 12.2919799 5.65716928 18.5350369 6.56433855 14.0175185 10.9678307 15.0839599 17.1856614"
  }))))));
};

exports.IconStar = IconStar;

var IconStarz = function IconStarz(props) {
  return _react.default.createElement(_SvgIcon.default, _extends({
    xmlns: "http://www.w3.org/2000/svg",
    viewBox: "0 0 20 20"
  }, props), _react.default.createElement("g", {
    id: "Page-1",
    stroke: "none",
    strokeWidth: "1",
    fill: "none",
    fillRule: "evenodd"
  }, _react.default.createElement("g", {
    id: "Desktop-HD",
    transform: "translate(-871.000000, -6772.000000)",
    fill: "#F5A623"
  }, _react.default.createElement("g", {
    id: "Group-19",
    transform: "translate(0.000000, 6535.000000)"
  }, _react.default.createElement("g", {
    id: "Group-14",
    transform: "translate(871.000000, 237.000000)"
  }, _react.default.createElement("polygon", {
    id: "Star",
    points: "9.5 14.25 3.9160401 17.1856614 4.98248155 10.9678307 0.464963095 6.56433855 6.70802005 5.65716928 9.5 0 12.2919799 5.65716928 18.5350369 6.56433855 14.0175185 10.9678307 15.0839599 17.1856614"
  }))))));
};

exports.IconStarz = IconStarz;

var SwipeLeftIcon = function SwipeLeftIcon(props) {
  return _react.default.createElement(_SvgIcon.default, _extends({
    xmlns: "http://www.w3.org/2000/svg",
    width: "24",
    height: "24",
    viewBox: "0 0 24 24"
  }, props), _react.default.createElement("path", {
    d: "M15.41 16.59L10.83 12l4.58-4.59L14 6l-6 6 6 6 1.41-1.41z"
  }), _react.default.createElement("path", {
    fill: "none",
    d: "M0 0h24v24H0V0z"
  }));
};

exports.SwipeLeftIcon = SwipeLeftIcon;

var SwipeRightIcon = function SwipeRightIcon(props) {
  return _react.default.createElement(_SvgIcon.default, _extends({
    xmlns: "http://www.w3.org/2000/svg",
    width: "24",
    height: "24",
    viewBox: "0 0 24 24"
  }, props), _react.default.createElement("path", {
    d: "M8.59 16.59L13.17 12 8.59 7.41 10 6l6 6-6 6-1.41-1.41z"
  }), _react.default.createElement("path", {
    fill: "none",
    d: "M0 0h24v24H0V0z"
  }));
};

exports.SwipeRightIcon = SwipeRightIcon;

var PlayVidIcon = function PlayVidIcon(props) {
  return _react.default.createElement(_SvgIcon.default, _extends({
    xmlns: "http://www.w3.org/2000/svg",
    width: "24",
    height: "24",
    viewBox: "0 0 35 37"
  }, props), _react.default.createElement("path", {
    d: "M17.1064718,31.5640397 L27.6064718,23.2390397 L17.1064718,14.9140397 L17.1064718,31.5640397 Z M20.6064718,4.73903967 C10.9464718,4.73903967 3.10647182,13.0270397 3.10647182,23.2390397 C3.10647182,33.4510397 10.9464718,41.7390397 20.6064718,41.7390397 C30.2664718,41.7390397 38.1064718,33.4510397 38.1064718,23.2390397 C38.1064718,13.0270397 30.2664718,4.73903967 20.6064718,4.73903967 Z M20.6064718,38.0390397 C12.8889718,38.0390397 6.60647182,31.3975397 6.60647182,23.2390397 C6.60647182,15.0805397 12.8889718,8.43903967 20.6064718,8.43903967 C28.3239718,8.43903967 34.6064718,15.0805397 34.6064718,23.2390397 C34.6064718,31.3975397 28.3239718,38.0390397 20.6064718,38.0390397 Z"
  }), _react.default.createElement("path", {
    fill: "none",
    d: "M0 0h24v24H0V0z"
  }));
};

exports.PlayVidIcon = PlayVidIcon;
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

var _default = function _default(props) {
  var _props$image = props.image,
      image = _props$image === void 0 ? {} : _props$image,
      _props$name = props.name,
      name = _props$name === void 0 ? '' : _props$name;
  var secure_url = image.secure_url,
      url = image.url;
  return secure_url ? _react.default.createElement("img", {
    src: secure_url,
    alt: name
  }) : null;
};

exports.default = _default;
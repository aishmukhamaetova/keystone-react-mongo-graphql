"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _compose = _interopRequireDefault(require("recompose/compose"));

var _defaultProps = _interopRequireDefault(require("recompose/defaultProps"));

var _core = require("@material-ui/core");

var _polished = require("polished");

var _styledComponents = _interopRequireDefault(require("styled-components"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

var map = {
  sm: 600,
  md: 960,
  lg: 1280,
  xl: 1920
};

var mediaGrid = function mediaGrid(type) {
  return function () {
    return "@media (max-width: ".concat(map[type] - 1, "px)");
  };
};

var enhance = (0, _compose.default)((0, _defaultProps.default)({
  A: {
    Poster: (0, _styledComponents.default)(_core.Grid).withConfig({
      displayName: "Poster",
      componentId: "sc-6t4bm7-0"
    })(["&&{background-position:center center;background-repeat:no-repeat background-color:#efefee;background-size:150% auto;text-align:center;height:", " ", "{background-size:150% auto;height:", "}", "{background-size:110% auto;height:", "}", "{background-size:100% auto;height:auto;}", "{background-size:auto 100%;height:auto;}}"], (0, _polished.rem)(600), mediaGrid("xl"), (0, _polished.rem)(600), mediaGrid("lg"), (0, _polished.rem)(600), mediaGrid("md"), mediaGrid("sm"))
  }
}));
var Poster = enhance(function (_ref) {
  var _ref$image = _ref.image,
      image = _ref$image === void 0 ? {} : _ref$image,
      _ref$name = _ref.name,
      name = _ref$name === void 0 ? '' : _ref$name,
      children = _ref.children,
      _ref$bgcolor = _ref.bgcolor,
      bgcolor = _ref$bgcolor === void 0 ? '#efefee' : _ref$bgcolor,
      A = _ref.A,
      height = _ref.height;
  var secure_url = image.secure_url,
      url = image.url;
  var style = {
    backgroundImage: secure_url ? 'url(' + secure_url + ')' : 'none',
    backgroundColor: bgcolor
  };
  return _react.default.createElement(A.Poster, {
    height: height,
    container: true,
    direction: 'row',
    justify: 'center',
    alignItems: 'center',
    spacing: 0,
    style: style
  }, children);
});
var _default = Poster;
exports.default = _default;
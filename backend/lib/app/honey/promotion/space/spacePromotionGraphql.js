"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _config = _interopRequireDefault(require("../../config/api/config"));

var _queryBannerArea = _interopRequireDefault(require("../api/graphql/queryBannerArea"));

var _queryPromotionalBar = _interopRequireDefault(require("../api/graphql/queryPromotionalBar"));

var _querySlider = _interopRequireDefault(require("../api/graphql/querySlider"));

var _spacePromotion = _interopRequireDefault(require("./spacePromotion"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _default = [_config.default.namespace, "promotionGraphql", [_spacePromotion.default], function () {
  return {
    isLoading: false
  };
}, {
  queryBannerArea: function queryBannerArea(getState, setState, methods, _ref) {
    var fromPromotion = _ref.fromPromotion;
    return (
      /*#__PURE__*/
      _regenerator.default.mark(function _callee(client) {
        var _ref2, data, _iteratorNormalCompletion, _didIteratorError, _iteratorError, _iterator, _step, doc;

        return _regenerator.default.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.prev = 0;

                if (!client) {
                  _context.next = 38;
                  break;
                }

                _context.next = 4;
                return setState({
                  isLoading: true
                });

              case 4:
                _context.next = 6;
                return client.query({
                  query: _queryBannerArea.default
                });

              case 6:
                _ref2 = _context.sent;
                data = _ref2.data;
                _context.next = 10;
                return fromPromotion.orderClear("bannerArea");

              case 10:
                _iteratorNormalCompletion = true;
                _didIteratorError = false;
                _iteratorError = undefined;
                _context.prev = 13;
                _iterator = data.promotion[Symbol.iterator]();

              case 15:
                if (_iteratorNormalCompletion = (_step = _iterator.next()).done) {
                  _context.next = 22;
                  break;
                }

                doc = _step.value;
                _context.next = 19;
                return fromPromotion.create(doc, {
                  orderName: "bannerArea"
                });

              case 19:
                _iteratorNormalCompletion = true;
                _context.next = 15;
                break;

              case 22:
                _context.next = 28;
                break;

              case 24:
                _context.prev = 24;
                _context.t0 = _context["catch"](13);
                _didIteratorError = true;
                _iteratorError = _context.t0;

              case 28:
                _context.prev = 28;
                _context.prev = 29;

                if (!_iteratorNormalCompletion && _iterator.return != null) {
                  _iterator.return();
                }

              case 31:
                _context.prev = 31;

                if (!_didIteratorError) {
                  _context.next = 34;
                  break;
                }

                throw _iteratorError;

              case 34:
                return _context.finish(31);

              case 35:
                return _context.finish(28);

              case 36:
                _context.next = 38;
                return setState({
                  isLoading: false
                });

              case 38:
                _context.next = 45;
                break;

              case 40:
                _context.prev = 40;
                _context.t1 = _context["catch"](0);
                _context.next = 44;
                return setState({
                  isLoading: false
                });

              case 44:
                console.error(_context.t1);

              case 45:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this, [[0, 40], [13, 24, 28, 36], [29,, 31, 35]]);
      })
    );
  },
  queryPromotionalBar: function queryPromotionalBar(getState, setState, methods, _ref3) {
    var fromPromotion = _ref3.fromPromotion;
    return (
      /*#__PURE__*/
      _regenerator.default.mark(function _callee2(client) {
        var _ref4, data, _iteratorNormalCompletion2, _didIteratorError2, _iteratorError2, _iterator2, _step2, doc;

        return _regenerator.default.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _context2.prev = 0;

                if (!client) {
                  _context2.next = 38;
                  break;
                }

                _context2.next = 4;
                return setState({
                  isLoading: true
                });

              case 4:
                _context2.next = 6;
                return client.query({
                  query: _queryPromotionalBar.default
                });

              case 6:
                _ref4 = _context2.sent;
                data = _ref4.data;
                _context2.next = 10;
                return fromPromotion.orderClear("bar");

              case 10:
                _iteratorNormalCompletion2 = true;
                _didIteratorError2 = false;
                _iteratorError2 = undefined;
                _context2.prev = 13;
                _iterator2 = data.promotion[Symbol.iterator]();

              case 15:
                if (_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done) {
                  _context2.next = 22;
                  break;
                }

                doc = _step2.value;
                _context2.next = 19;
                return fromPromotion.create(doc, {
                  orderName: "bar"
                });

              case 19:
                _iteratorNormalCompletion2 = true;
                _context2.next = 15;
                break;

              case 22:
                _context2.next = 28;
                break;

              case 24:
                _context2.prev = 24;
                _context2.t0 = _context2["catch"](13);
                _didIteratorError2 = true;
                _iteratorError2 = _context2.t0;

              case 28:
                _context2.prev = 28;
                _context2.prev = 29;

                if (!_iteratorNormalCompletion2 && _iterator2.return != null) {
                  _iterator2.return();
                }

              case 31:
                _context2.prev = 31;

                if (!_didIteratorError2) {
                  _context2.next = 34;
                  break;
                }

                throw _iteratorError2;

              case 34:
                return _context2.finish(31);

              case 35:
                return _context2.finish(28);

              case 36:
                _context2.next = 38;
                return setState({
                  isLoading: false
                });

              case 38:
                _context2.next = 45;
                break;

              case 40:
                _context2.prev = 40;
                _context2.t1 = _context2["catch"](0);
                _context2.next = 44;
                return setState({
                  isLoading: false
                });

              case 44:
                console.error(_context2.t1);

              case 45:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, this, [[0, 40], [13, 24, 28, 36], [29,, 31, 35]]);
      })
    );
  },
  querySlider: function querySlider(getState, setState, methods, _ref5) {
    var fromPromotion = _ref5.fromPromotion;
    return (
      /*#__PURE__*/
      _regenerator.default.mark(function _callee3(client) {
        var _ref6, data, _iteratorNormalCompletion3, _didIteratorError3, _iteratorError3, _iterator3, _step3, doc;

        return _regenerator.default.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                _context3.prev = 0;

                if (!client) {
                  _context3.next = 38;
                  break;
                }

                _context3.next = 4;
                return setState({
                  isLoading: true
                });

              case 4:
                _context3.next = 6;
                return client.query({
                  query: _querySlider.default
                });

              case 6:
                _ref6 = _context3.sent;
                data = _ref6.data;
                _context3.next = 10;
                return fromPromotion.orderClear("slider");

              case 10:
                _iteratorNormalCompletion3 = true;
                _didIteratorError3 = false;
                _iteratorError3 = undefined;
                _context3.prev = 13;
                _iterator3 = data.promotion[Symbol.iterator]();

              case 15:
                if (_iteratorNormalCompletion3 = (_step3 = _iterator3.next()).done) {
                  _context3.next = 22;
                  break;
                }

                doc = _step3.value;
                _context3.next = 19;
                return fromPromotion.create(doc, {
                  orderName: "slider"
                });

              case 19:
                _iteratorNormalCompletion3 = true;
                _context3.next = 15;
                break;

              case 22:
                _context3.next = 28;
                break;

              case 24:
                _context3.prev = 24;
                _context3.t0 = _context3["catch"](13);
                _didIteratorError3 = true;
                _iteratorError3 = _context3.t0;

              case 28:
                _context3.prev = 28;
                _context3.prev = 29;

                if (!_iteratorNormalCompletion3 && _iterator3.return != null) {
                  _iterator3.return();
                }

              case 31:
                _context3.prev = 31;

                if (!_didIteratorError3) {
                  _context3.next = 34;
                  break;
                }

                throw _iteratorError3;

              case 34:
                return _context3.finish(31);

              case 35:
                return _context3.finish(28);

              case 36:
                _context3.next = 38;
                return setState({
                  isLoading: false
                });

              case 38:
                _context3.next = 45;
                break;

              case 40:
                _context3.prev = 40;
                _context3.t1 = _context3["catch"](0);
                _context3.next = 44;
                return setState({
                  isLoading: false
                });

              case 44:
                console.error(_context3.t1);

              case 45:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3, this, [[0, 40], [13, 24, 28, 36], [29,, 31, 35]]);
      })
    );
  }
}];
exports.default = _default;
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _reactApollo = require("react-apollo");

var _recompose = require("recompose");

var _semanticUiReact = require("semantic-ui-react");

var _withSpace = _interopRequireDefault(require("../../../../../evoke-me/space/all/withSpace"));

var _spacePromotion = _interopRequireDefault(require("../../space/spacePromotion"));

var _spacePromotionGraphql = _interopRequireDefault(require("../../space/spacePromotionGraphql"));

var _PromotionList = _interopRequireDefault(require("../PromotionList"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

var enhance = (0, _recompose.compose)(_reactApollo.withApollo, (0, _withSpace.default)(_spacePromotion.default), (0, _withSpace.default)(_spacePromotionGraphql.default), (0, _recompose.lifecycle)({
  componentDidMount: function componentDidMount() {
    var _this$props = this.props,
        client = _this$props.client,
        fromPromotionGraphql = _this$props.fromPromotionGraphql;
    var fromPromotion = this.props.fromPromotion;
    var orderMap = fromPromotion.orderMap;

    if (!orderMap.bannerArea) {
      fromPromotionGraphql.queryBannerArea(client);
    }
  }
}));
var BlockBannerArea = enhance(function (_ref) {
  var fromPromotion = _ref.fromPromotion,
      fromPromotionGraphql = _ref.fromPromotionGraphql;
  var data = fromPromotion.data,
      orderMap = fromPromotion.orderMap;
  var isLoading = fromPromotionGraphql.isLoading;

  if (!orderMap.bannerArea || !orderMap.bannerArea.length) {
    return _react.default.createElement(_semanticUiReact.Segment, {
      placeholder: true,
      loading: isLoading
    }, _react.default.createElement(_semanticUiReact.Header, {
      icon: true
    }, _react.default.createElement(_semanticUiReact.Icon, {
      name: "audio description"
    }), "Banner area is empty"));
  }

  return _react.default.createElement(_PromotionList.default, {
    data: data,
    order: orderMap.bannerArea
  });
});
var _default = BlockBannerArea;
exports.default = _default;
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _recompose = require("recompose");

var _PromotionBlockBannerAreaList = _interopRequireDefault(require("../../promotionBlock/web/PromotionBlockBannerAreaList"));

var _PromotionBlockBarList = _interopRequireDefault(require("../../promotionBlock/web/PromotionBlockBarList"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

var enhance = (0, _recompose.compose)();
var Promotion = enhance(function (_ref) {
  var _ref$blocks = _ref.blocks,
      blocks = _ref$blocks === void 0 ? [] : _ref$blocks,
      type = _ref.type;
  var data = blocks.reduce(function (result, doc) {
    result[doc.id] = doc;
    return result;
  }, {});
  var order = Object.keys(data);
  return type === 'bar' ? _react.default.createElement(_PromotionBlockBarList.default, {
    data: data,
    order: order
  }) : _react.default.createElement(_PromotionBlockBannerAreaList.default, {
    data: data,
    order: order
  });
});
var _default = Promotion;
exports.default = _default;
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _reactPose = _interopRequireDefault(require("react-pose"));

var _popmotion = require("popmotion");

var _debounce = _interopRequireDefault(require("lodash/debounce"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var VELOCITY_THRESHOLD = 600;
var DISTANCE_PERCENTILE_THRESHOLD = 0.3;

var Draggable = _reactPose.default.div({
  draggable: 'x',
  rest: {
    x: function x(_ref) {
      var offset = _ref.offset;
      return -offset;
    },
    transition: {
      ease: 'easeOut',
      duration: 500
    }
  },
  dragEnd: {
    transition: function transition(_ref2) {
      var from = _ref2.from,
          to = _ref2.to,
          velocity = _ref2.velocity,
          offset = _ref2.offset;
      return (0, _popmotion.spring)({
        from: from,
        to: -offset,
        velocity: velocity
      });
    }
  }
});

var draggableStyle = {
  display: 'flex',
  height: '100%',
  flexWrap: 'nowrap'
};

function childrenBox(root, index) {
  var rootWidth = root.clientWidth;
  var el = root.children[index];
  return {
    offset: el.offsetLeft - (rootWidth - el.offsetWidth) / 2,
    width: el.offsetWidth
  };
}

var PopSlideCarousel =
/*#__PURE__*/
function (_PureComponent) {
  _inherits(PopSlideCarousel, _PureComponent);

  _createClass(PopSlideCarousel, null, [{
    key: "getDerivedStateFromProps",
    value: function getDerivedStateFromProps(_ref3, _ref4) {
      var slideIndex = _ref3.slideIndex;
      var root = _ref4.root;
      return root ? childrenBox(root, slideIndex) : null;
    }
  }]);

  function PopSlideCarousel(props) {
    var _this;

    _classCallCheck(this, PopSlideCarousel);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(PopSlideCarousel).call(this, props));

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "state", {
      root: null,
      offset: 0,
      width: 0
    });

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "x", (0, _popmotion.value)(0));

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "preventClick", false);

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "adjustCurrentBox", function () {
      var slideIndex = _this.props.slideIndex;
      var root = _this.state.root;

      _this.setState(childrenBox(root, slideIndex));
    });

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "goToNextSlide", function () {
      _this.goToSlide(_this.props.slideIndex + 1);
    });

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "goToPreviousSlide", function () {
      _this.goToSlide(_this.props.slideIndex - 1);
    });

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "goToSlide", function (newSlideIndex) {
      var children = _this.props.children;

      if (newSlideIndex >= 0 && newSlideIndex < children.length) {
        _this.props.onSlideChange(newSlideIndex);
      }
    });

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "onDragStart", function (e) {
      _this.preventClick = false;

      _this.props.onDragStart();
    });

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "onDragEnd", function (e) {
      var _this$state = _this.state,
          offset = _this$state.offset,
          width = _this$state.width;
      var start = -offset;
      var distance = _this.x.get() - start;

      var velocity = _this.x.getVelocity();

      if (distance !== 0) {
        // prevents click from firing in onClickCapture
        _this.preventClick = true;
        var threshold = DISTANCE_PERCENTILE_THRESHOLD * width;
        if (distance < -threshold || velocity < -VELOCITY_THRESHOLD) _this.goToNextSlide();else if (distance > threshold || velocity > VELOCITY_THRESHOLD) _this.goToPreviousSlide();
      }

      _this.props.onDragEnd();
    });

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "onClickCapture", function (e) {
      if (_this.preventClick) {
        e.stopPropagation();
      }
    });

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "registerRootElement", function (root) {
      if (root && !_this.state.root) {
        var slideIndex = _this.props.slideIndex;

        _this.setState(_objectSpread({
          root: root
        }, childrenBox(root, slideIndex)));
      }
    });

    _this.adjustCurrentBox = (0, _debounce.default)(_this.adjustCurrentBox, 250);
    return _this;
  }

  _createClass(PopSlideCarousel, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      window.addEventListener('resize', this.adjustCurrentBox);
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      window.removeEventListener('resize', this.adjustCurrentBox);
    }
  }, {
    key: "render",
    value: function render() {
      var _this$props = this.props,
          children = _this$props.children,
          className = _this$props.className,
          style = _this$props.style;
      var offset = this.state.offset;
      var valuesMap = {
        x: this.x
      };
      return _react.default.createElement("div", {
        className: className,
        style: style
      }, _react.default.createElement(Draggable, {
        ref: this.registerRootElement,
        values: valuesMap,
        offset: offset,
        style: draggableStyle,
        onClickCapture: this.onClickCapture,
        onDragStart: this.onDragStart,
        onDragEnd: this.onDragEnd,
        onPoseComplete: this.props.onTransitionEnd,
        poseKey: offset,
        pose: 'rest'
      }, children));
    }
  }]);

  return PopSlideCarousel;
}(_react.PureComponent);

exports.default = PopSlideCarousel;

_defineProperty(PopSlideCarousel, "defaultProps", {
  duration: 3000,
  onDragStart: function onDragStart() {},
  onDragEnd: function onDragEnd() {},
  onTransitionEnd: function onTransitionEnd() {}
});
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _compose = _interopRequireDefault(require("recompose/compose"));

var _withStateHandlers = _interopRequireDefault(require("recompose/withStateHandlers"));

var _defaultProps = _interopRequireDefault(require("recompose/defaultProps"));

var _PromotionBlockBar = _interopRequireDefault(require("./PromotionBlockBar"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _polished = require("polished");

var _core = require("@material-ui/core");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

var map = {
  sm: 600,
  md: 960,
  lg: 1280,
  xl: 1920
};

var mediaGrid = function mediaGrid(type) {
  return function () {
    return "@media (max-width: ".concat(map[type] - 1, "px)");
  };
};

var enhance = (0, _compose.default)((0, _withStateHandlers.default)(function () {
  return {};
}, {}), (0, _defaultProps.default)({
  data: {},
  order: [],
  A: {
    Title: (0, _styledComponents.default)(function (_ref) {
      var color = _ref.color,
          transform = _ref.transform,
          border = _ref.border,
          margin = _ref.margin,
          fweight = _ref.fweight,
          other = _objectWithoutProperties(_ref, ["color", "transform", "border", "margin", "fweight"]);

      return _react.default.createElement(_core.Typography, other);
    }).withConfig({
      displayName: "PromotionBlockBarList__Title",
      componentId: "sc-3q16j7-0"
    })(["&&{color:", ";border-bottom:", ";text-transform:", ";border-color:", ";margin:", ";font-weight:", ";min-width:auto;width:100%;font-size:", ";text-align:center;&:hover{background:none;},}"], function (props) {
      return props.color;
    }, function (props) {
      return props.border;
    }, function (props) {
      return props.transform || 'inherit';
    }, function (props) {
      return props.color;
    }, function (props) {
      return props.margin;
    }, function (props) {
      return props.fweight || '700';
    }, function (props) {
      return props.size;
    }),
    TagLine: (0, _styledComponents.default)(function (_ref2) {
      var color = _ref2.color,
          transform = _ref2.transform,
          border = _ref2.border,
          margin = _ref2.margin,
          fweight = _ref2.fweight,
          other = _objectWithoutProperties(_ref2, ["color", "transform", "border", "margin", "fweight"]);

      return _react.default.createElement(_core.Typography, other);
    }).withConfig({
      displayName: "PromotionBlockBarList__TagLine",
      componentId: "sc-3q16j7-1"
    })(["&&{font-size:", ";font-weight:", ";color:", ";margin:", ";text-align:center;font-style:normal;line-height:normal;}"], function (props) {
      return props.size || '1rem';
    }, function (props) {
      return props.fweight || 400;
    }, function (props) {
      return props.color;
    }, function (props) {
      return props.margin;
    })
  }
}));
var PromotionBlockBarList = enhance(function (_ref3) {
  var data = _ref3.data,
      order = _ref3.order,
      A = _ref3.A;
  return _react.default.createElement(_core.Grid, {
    container: true,
    spacing: 0,
    justify: "center",
    alignItems: "stretch",
    direction: "row"
  }, _react.default.createElement(_core.Grid, {
    item: true,
    md: 7,
    xs: 12
  }, _react.default.createElement(A.Title, {
    size: "1.875rem",
    color: "#000",
    transform: "uppercase",
    justify: "center",
    margin: "0 0 3.875rem 0"
  }, "SPECIAL OCCASIONS"), _react.default.createElement(A.TagLine, {
    color: "#000",
    size: "2rem",
    fweight: "300",
    margin: "0 0 2.688rem 0"
  }, "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas fringilla massa vitae nisl scelerisque lobortis.")), _react.default.createElement(_core.Grid, {
    item: true,
    xs: 11,
    sm: 11,
    md: 10
  }, _react.default.createElement(_core.Grid, {
    container: true,
    alignItems: "stretch",
    direction: "row",
    justify: "center",
    spacing: 24
  }, order.map(function (id, index) {
    return _react.default.createElement(_PromotionBlockBar.default, _extends({
      key: index
    }, data[id]));
  }))));
});
var _default = PromotionBlockBarList;
exports.default = _default;
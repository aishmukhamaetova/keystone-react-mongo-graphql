"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _recompose = require("recompose");

var _ProductPromotionalBannerArea = _interopRequireDefault(require("../../product/web/ProductPromotionalBannerArea"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

var enhance = (0, _recompose.compose)();

var Root = _styledComponents.default.div.withConfig({
  displayName: "PromotionBlockBannerArea__Root",
  componentId: "qadmxl-0"
})(["text-align:center;display:block;"]);

var PromotionBlock = enhance(function (_ref) {
  var content = _ref.content,
      product = _ref.product;
  return _react.default.createElement(Root, null, product ? _react.default.createElement(_ProductPromotionalBannerArea.default, product) : null, _react.default.createElement("div", {
    dangerouslySetInnerHTML: {
      __html: content
    }
  }));
});
var _default = PromotionBlock;
exports.default = _default;
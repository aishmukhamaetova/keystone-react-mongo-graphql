"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _compose = _interopRequireDefault(require("recompose/compose"));

var _withStateHandlers = _interopRequireDefault(require("recompose/withStateHandlers"));

var _defaultProps = _interopRequireDefault(require("recompose/defaultProps"));

var _PromotionBlock = _interopRequireDefault(require("./PromotionBlock"));

var _Carousel = _interopRequireDefault(require("./Carousel"));

var _core = require("@material-ui/core");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _polished = require("polished");

var _icons = require("../../icons");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

var map = {
  sm: 600,
  md: 960,
  lg: 1280,
  xl: 1920
};

var mediaGrid = function mediaGrid(type) {
  return function () {
    return "@media (max-width: ".concat(map[type] - 1, "px)");
  };
};

var enhance = (0, _compose.default)((0, _withStateHandlers.default)(function () {
  return {
    slideIndex: 0
  };
}, {
  setSlideIndex: function setSlideIndex() {
    return function (slideIndex) {
      return {
        slideIndex: slideIndex
      };
    };
  }
}), (0, _defaultProps.default)({
  data: {},
  order: [],
  A: {
    SwipeLeftIcon: (0, _styledComponents.default)(_icons.SwipeLeftIcon).withConfig({
      displayName: "PromotionBlockBannerAreaList1__SwipeLeftIcon",
      componentId: "sc-1j2xqqr-0"
    })(["&&{cursor:pointer;&.disabled{opacity:0.3}}"]),
    SwipeRightIcon: (0, _styledComponents.default)(_icons.SwipeRightIcon).withConfig({
      displayName: "PromotionBlockBannerAreaList1__SwipeRightIcon",
      componentId: "sc-1j2xqqr-1"
    })(["&&{cursor:pointer;&.disabled{opacity:0.3}}"]),
    Container: _styledComponents.default.div.withConfig({
      displayName: "PromotionBlockBannerAreaList1__Container",
      componentId: "sc-1j2xqqr-2"
    })(["display:inline-flex;margin:0 auto;"]),
    Right: (0, _styledComponents.default)(_core.Grid).withConfig({
      displayName: "PromotionBlockBannerAreaList1__Right",
      componentId: "sc-1j2xqqr-3"
    })(["text-align:right;"]),
    Left: (0, _styledComponents.default)(_core.Grid).withConfig({
      displayName: "PromotionBlockBannerAreaList1__Left",
      componentId: "sc-1j2xqqr-4"
    })([""]),
    Slide: _styledComponents.default.div.withConfig({
      displayName: "PromotionBlockBannerAreaList1__Slide",
      componentId: "sc-1j2xqqr-5"
    })(["min-width:100%;margin:0 ", ";cursor:pointer;text-align:center;display:flex;flex-direction:row;align-items:center;div{margin:auto;}"], (0, _polished.rem)(20)),
    Carousel: (0, _styledComponents.default)(_Carousel.default).withConfig({
      displayName: "PromotionBlockBannerAreaList1__Carousel",
      componentId: "sc-1j2xqqr-6"
    })(["&&{box-sizing:content-box;margin:0 auto;position:relative;overflow:hidden;width:100%;height:auto;},"])
  }
}));
var PromotionBlockList = enhance(function (_ref) {
  var data = _ref.data,
      order = _ref.order,
      A = _ref.A,
      slideIndex = _ref.slideIndex,
      setSlideIndex = _ref.setSlideIndex;
  return _react.default.createElement(_core.Grid, {
    container: true,
    direction: "row",
    justify: "space-around",
    alignItems: "center"
  }, _react.default.createElement(_core.Hidden, {
    smDown: true
  }, _react.default.createElement(A.Right, {
    item: true,
    xs: 1
  }, _react.default.createElement(A.SwipeLeftIcon, {
    className: slideIndex !== 0 ? '' : 'disabled',
    style: {
      backgroundColor: "#80808033"
    },
    onClick: function onClick() {
      return slideIndex !== 0 && setSlideIndex(slideIndex - 1);
    }
  }))), _react.default.createElement(_core.Grid, {
    item: true,
    xs: 10,
    sm: 8
  }, _react.default.createElement(A.Carousel, {
    slideIndex: slideIndex,
    onSlideChange: setSlideIndex
  }, order.map(function (id, index) {
    return _react.default.createElement(A.Slide, {
      key: index,
      onClick: function onClick() {
        return setSlideIndex(index);
      }
    }, _react.default.createElement(_PromotionBlock.default, _extends({
      key: id
    }, data[id])));
  }))), _react.default.createElement(_core.Hidden, {
    smDown: true
  }, _react.default.createElement(A.Left, {
    item: true,
    xs: 1
  }, _react.default.createElement(A.SwipeRightIcon, {
    className: slideIndex < order.length - 1 ? '' : 'disabled',
    style: {
      backgroundColor: "#80808033"
    },
    onClick: function onClick() {
      return slideIndex < order.length - 1 && setSlideIndex(slideIndex + 1);
    }
  }))));
});
var _default = PromotionBlockList;
exports.default = _default;
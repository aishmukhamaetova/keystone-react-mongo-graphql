"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _keystone = _interopRequireDefault(require("keystone"));

var _apolloServerExpress = require("apollo-server-express");

var _graphql = require("graphql");

var _language = require("graphql/language");

var _typeDefs = _interopRequireDefault(require("./typeDefs"));

var _list2 = _interopRequireDefault(require("../../../../box/auth/classic/api/keystone/graphql/server/list"));

var _list3 = _interopRequireDefault(require("../../../../box/auth/facebook/api/keystone/graphql/server/list"));

var _list4 = _interopRequireDefault(require("../../../../box/eshop/order/api/keystone/graphql/server/list"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

var list = _toConsumableArray(_list2.default).concat(_toConsumableArray(_list3.default), _toConsumableArray(_list4.default));

var populateRelated = function populateRelated(model, fieldList) {
  return new Promise(function (resolve) {
    _keystone.default.populateRelated(model, fieldList, function () {
      resolve(model);
    });
  });
};

var processWhere = function processWhere(where) {
  var map = {
    in: "$in",
    eq: "$eq",
    gt: "$gt",
    lt: "$lt",
    lte: "$lte",
    ne: "$ne",
    nin: "$nin"
  };

  var rename = function rename() {
    var doc = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
    return Object.keys(map).reduce(function (result, name) {
      if (doc[name]) {
        result[map[name]] = doc[name];
      }

      return result;
    }, {});
  };

  var fields = Object.keys(where);
  return fields.reduce(function (result, name) {
    result[name] = _objectSpread({}, result.name || {}, rename(where[name].int), rename(where[name].string), rename(where[name].boolean));
    return result;
  }, {});
};

var processFilter = function processFilter() {
  var filter = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
  var map = {
    in: "$in",
    eq: "$eq",
    gt: "$gt",
    lt: "$lt",
    lte: "$lte",
    ne: "$ne",
    nin: "$nin"
  };

  var rename = function rename() {
    var doc = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
    return Object.keys(map).reduce(function (result, name) {
      if (doc[name]) {
        result[map[name]] = doc[name];
      }

      return result;
    }, {});
  };

  var entityMap = {
    default: {}
  };
  var byMap = {};

  try {
    var _iteratorNormalCompletion = true;
    var _didIteratorError = false;
    var _iteratorError = undefined;

    try {
      for (var _iterator = filter[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
        var _ref2 = _step.value;
        var by = _ref2.by,
            name = _ref2.name,
            where = _ref2.where;

        var _name$split = name.split("."),
            _name$split2 = _slicedToArray(_name$split, 2),
            entity = _name$split2[0],
            fieldName = _name$split2[1];

        if (fieldName) {
          entityMap[entity] = entityMap[entity] || {};

          var whereAcc = _objectSpread({}, rename(where.boolean), rename(where.int), rename(where.float), rename(where.string));

          if (by) byMap[entity] = by;

          if (Object.keys(whereAcc).length) {
            entityMap[entity][fieldName] = whereAcc;
          }
        } else {
          var _whereAcc = _objectSpread({}, rename(where.boolean), rename(where.int), rename(where.float), rename(where.string));

          if (Object.keys(_whereAcc).length) {
            entityMap.default[entity] = _whereAcc;
          }
        }
      }
    } catch (err) {
      _didIteratorError = true;
      _iteratorError = err;
    } finally {
      try {
        if (!_iteratorNormalCompletion && _iterator.return != null) {
          _iterator.return();
        }
      } finally {
        if (_didIteratorError) {
          throw _iteratorError;
        }
      }
    }
  } catch (err) {
    console.log("Error", err);
  }

  return [entityMap, byMap];
};

var resolvers = {
  DateTime: new _graphql.GraphQLScalarType({
    name: 'DateTime',
    description: 'Date custom scalar type',
    parseValue: function parseValue(value) {
      return new Date(value);
    },
    serialize: function serialize(value) {
      return value.getTime();
    },
    parseLiteral: function parseLiteral(ast) {
      if (ast.kind === _language.Kind.INT) {
        return parseInt(ast.value, 10);
      }

      return null;
    }
  }),
  Mutation: _objectSpread({}, list.reduce(function (acc, _ref3) {
    var mutationMap = _ref3.mutationMap;
    return _objectSpread({}, acc, mutationMap);
  }, {}), require("../../../../box/auth/classic/api/keystone/graphql/server/mutationMap").default, {
    regionUpdate: function () {
      var _regionUpdate = _asyncToGenerator(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee(obj, args, _ref4, info) {
        var keystone, id, lat, lon, Region, region;
        return _regenerator.default.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                keystone = _ref4.keystone;
                _context.prev = 1;
                id = args.id, lat = args.lat, lon = args.lon;
                Region = keystone.list("ProductRegion");
                _context.next = 6;
                return Region.model.findOne({
                  _id: id
                });

              case 6:
                region = _context.sent;
                region.map = [lat, lon];
                _context.next = 10;
                return region.save();

              case 10:
                console.log(region);
                return _context.abrupt("return", true);

              case 14:
                _context.prev = 14;
                _context.t0 = _context["catch"](1);
                console.log("regionUpdate", "ERROR", _context.t0);

              case 17:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this, [[1, 14]]);
      }));

      function regionUpdate(_x, _x2, _x3, _x4) {
        return _regionUpdate.apply(this, arguments);
      }

      return regionUpdate;
    }()
  }),
  Query: _objectSpread({}, list.reduce(function (acc, _ref5) {
    var queryMap = _ref5.queryMap;
    return _objectSpread({}, acc, queryMap);
  }, {}), {
    content: function () {
      var _content = _asyncToGenerator(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee2(obj, args, _ref6, info) {
        var keystone, Content, Language, id, languageName, _args$where, where, whereFinal, language;

        return _regenerator.default.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                keystone = _ref6.keystone;
                Content = keystone.list('Content');
                Language = keystone.list('Language');
                id = args.id, languageName = args.languageName, _args$where = args.where, where = _args$where === void 0 ? {} : _args$where;
                whereFinal = _objectSpread({}, processWhere(where));

                if (!languageName) {
                  _context2.next = 10;
                  break;
                }

                _context2.next = 8;
                return Language.model.find({
                  name: languageName
                });

              case 8:
                language = _context2.sent;

                if (language.length && language[0]._id) {
                  whereFinal.language = language[0]._id;
                }

              case 10:
                if (id) whereFinal._id = id;
                _context2.next = 13;
                return Content.model.find(whereFinal).populate('language');

              case 13:
                return _context2.abrupt("return", _context2.sent);

              case 14:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, this);
      }));

      function content(_x5, _x6, _x7, _x8) {
        return _content.apply(this, arguments);
      }

      return content;
    }(),
    image: function () {
      var _image = _asyncToGenerator(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee3(obj, args, _ref7, info) {
        var keystone, Image;
        return _regenerator.default.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                keystone = _ref7.keystone;
                Image = keystone.list('Image');
                _context3.next = 4;
                return Image.model.find();

              case 4:
                return _context3.abrupt("return", _context3.sent);

              case 5:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3, this);
      }));

      function image(_x9, _x10, _x11, _x12) {
        return _image.apply(this, arguments);
      }

      return image;
    }(),
    location: function () {
      var _location = _asyncToGenerator(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee4(obj, args, _ref8, info) {
        var keystone, Location;
        return _regenerator.default.wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                keystone = _ref8.keystone;
                Location = keystone.list('Location');
                _context4.next = 4;
                return Location.model.find().populate('currency');

              case 4:
                return _context4.abrupt("return", _context4.sent);

              case 5:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4, this);
      }));

      function location(_x13, _x14, _x15, _x16) {
        return _location.apply(this, arguments);
      }

      return location;
    }(),
    locationCurrency: function () {
      var _locationCurrency = _asyncToGenerator(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee5(obj, args, _ref9, info) {
        var keystone, LocationCurrency;
        return _regenerator.default.wrap(function _callee5$(_context5) {
          while (1) {
            switch (_context5.prev = _context5.next) {
              case 0:
                keystone = _ref9.keystone;
                LocationCurrency = keystone.list('LocationCurrency');
                _context5.next = 4;
                return LocationCurrency.model.find();

              case 4:
                return _context5.abrupt("return", _context5.sent);

              case 5:
              case "end":
                return _context5.stop();
            }
          }
        }, _callee5, this);
      }));

      function locationCurrency(_x17, _x18, _x19, _x20) {
        return _locationCurrency.apply(this, arguments);
      }

      return locationCurrency;
    }(),
    me: function () {
      var _me = _asyncToGenerator(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee6(obj, args, _ref10, info) {
        var user;
        return _regenerator.default.wrap(function _callee6$(_context6) {
          while (1) {
            switch (_context6.prev = _context6.next) {
              case 0:
                user = _ref10.user;

                if (!user) {
                  _context6.next = 3;
                  break;
                }

                return _context6.abrupt("return", user);

              case 3:
                return _context6.abrupt("return", null);

              case 4:
              case "end":
                return _context6.stop();
            }
          }
        }, _callee6, this);
      }));

      function me(_x21, _x22, _x23, _x24) {
        return _me.apply(this, arguments);
      }

      return me;
    }(),
    post: function () {
      var _post = _asyncToGenerator(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee7(obj, args, _ref11, info) {
        var keystone, Post, id, _args$where2, where, whereFinal, populateRelatedList;

        return _regenerator.default.wrap(function _callee7$(_context7) {
          while (1) {
            switch (_context7.prev = _context7.next) {
              case 0:
                keystone = _ref11.keystone;
                Post = keystone.list('Post');
                id = args.id, _args$where2 = args.where, where = _args$where2 === void 0 ? {} : _args$where2;
                whereFinal = _objectSpread({}, processWhere(where));
                if (id) whereFinal._id = id;
                populateRelatedList = [{
                  path: "testimonials"
                }];
                _context7.t0 = populateRelated;
                _context7.next = 9;
                return Post.model.find(whereFinal).populate("categories images videos author");

              case 9:
                _context7.t1 = _context7.sent;
                _context7.t2 = populateRelatedList;
                _context7.next = 13;
                return (0, _context7.t0)(_context7.t1, _context7.t2);

              case 13:
                return _context7.abrupt("return", _context7.sent);

              case 14:
              case "end":
                return _context7.stop();
            }
          }
        }, _callee7, this);
      }));

      function post(_x25, _x26, _x27, _x28) {
        return _post.apply(this, arguments);
      }

      return post;
    }(),
    pressQuote: function () {
      var _pressQuote = _asyncToGenerator(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee8(obj, args, _ref12, info) {
        var keystone, PressQuote, id, _args$where3, where, whereFinal;

        return _regenerator.default.wrap(function _callee8$(_context8) {
          while (1) {
            switch (_context8.prev = _context8.next) {
              case 0:
                keystone = _ref12.keystone;
                PressQuote = keystone.list('PressQuote');
                id = args.id, _args$where3 = args.where, where = _args$where3 === void 0 ? {} : _args$where3;
                whereFinal = _objectSpread({}, processWhere(where));
                if (id) whereFinal._id = id;
                _context8.next = 7;
                return PressQuote.model.find(whereFinal).populate("logo");

              case 7:
                return _context8.abrupt("return", _context8.sent);

              case 8:
              case "end":
                return _context8.stop();
            }
          }
        }, _callee8, this);
      }));

      function pressQuote(_x29, _x30, _x31, _x32) {
        return _pressQuote.apply(this, arguments);
      }

      return pressQuote;
    }(),
    product: function () {
      var _product = _asyncToGenerator(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee9(obj, args, _ref13, info) {
        var keystone, referenceMap, populate, populateRelatedList, Entity, id, filter, _args$pagination, pagination, index, size, _processFilter, _processFilter2, filterFinal, byMap, whereFinal, filterItem, _list;

        return _regenerator.default.wrap(function _callee9$(_context9) {
          while (1) {
            switch (_context9.prev = _context9.next) {
              case 0:
                keystone = _ref13.keystone;
                referenceMap = {
                  'badge': keystone.list("ProductBadge"),
                  'pairs': keystone.list("ProductPair"),
                  'stores': keystone.list("ProductStore")
                };
                populate = "badge harvest location pairs regions categories images videos";
                populateRelatedList = [{
                  path: "stores",
                  populate: [{
                    path: "location"
                  }]
                }, {
                  path: "testimonials"
                }];
                Entity = keystone.list('Product');
                id = args.id, filter = args.filter, _args$pagination = args.pagination, pagination = _args$pagination === void 0 ? {} : _args$pagination;
                index = pagination.index, size = pagination.size;
                _processFilter = processFilter(filter), _processFilter2 = _slicedToArray(_processFilter, 2), filterFinal = _processFilter2[0], byMap = _processFilter2[1];
                whereFinal = filterFinal.default;
                if (id) whereFinal._id = id;
                console.log("filterFinal", filterFinal);
                _context9.t0 = _regenerator.default.keys(filterFinal);

              case 12:
                if ((_context9.t1 = _context9.t0()).done) {
                  _context9.next = 23;
                  break;
                }

                filterItem = _context9.t1.value;

                if (!(filterItem === "default")) {
                  _context9.next = 16;
                  break;
                }

                return _context9.abrupt("continue", 12);

              case 16:
                if (!(referenceMap[filterItem] && Object.keys(filterFinal[filterItem]).length)) {
                  _context9.next = 21;
                  break;
                }

                _context9.next = 19;
                return referenceMap[filterItem].model.find(filterFinal[filterItem]);

              case 19:
                _list = _context9.sent;

                // console.log(`LIST`, list)
                // console.log(`BY`, byMap)
                if (byMap[filterItem]) {
                  (function () {
                    var by = byMap[filterItem];
                    whereFinal._id = {
                      '$in': _list.map(function (doc) {
                        return doc[by];
                      })
                    };
                  })();
                } else {
                  whereFinal[filterItem] = {
                    '$in': _list
                  };
                }

              case 21:
                _context9.next = 12;
                break;

              case 23:
                console.log("whereFinal", whereFinal);
                _context9.t2 = populateRelated;
                _context9.next = 27;
                return Entity.model.find(whereFinal).skip(index * size).limit(size).populate(populate);

              case 27:
                _context9.t3 = _context9.sent;
                _context9.t4 = populateRelatedList;
                _context9.next = 31;
                return (0, _context9.t2)(_context9.t3, _context9.t4);

              case 31:
                return _context9.abrupt("return", _context9.sent);

              case 32:
              case "end":
                return _context9.stop();
            }
          }
        }, _callee9, this);
      }));

      function product(_x33, _x34, _x35, _x36) {
        return _product.apply(this, arguments);
      }

      return product;
    }(),
    productCategory: function () {
      var _productCategory = _asyncToGenerator(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee10(obj, args, _ref14, info) {
        var keystone, ProductCategory;
        return _regenerator.default.wrap(function _callee10$(_context10) {
          while (1) {
            switch (_context10.prev = _context10.next) {
              case 0:
                keystone = _ref14.keystone;
                ProductCategory = keystone.list('ProductCategory');
                _context10.next = 4;
                return ProductCategory.model.find();

              case 4:
                return _context10.abrupt("return", _context10.sent);

              case 5:
              case "end":
                return _context10.stop();
            }
          }
        }, _callee10, this);
      }));

      function productCategory(_x37, _x38, _x39, _x40) {
        return _productCategory.apply(this, arguments);
      }

      return productCategory;
    }(),
    productRegion: function () {
      var _productRegion = _asyncToGenerator(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee11(obj, args, _ref15, info) {
        var keystone, ProductRegion, id, _args$where4, where, whereFinal, _ref16, _ref17, doc, _id, name, _ref18, _ref19, lat, lon;

        return _regenerator.default.wrap(function _callee11$(_context11) {
          while (1) {
            switch (_context11.prev = _context11.next) {
              case 0:
                keystone = _ref15.keystone;
                _context11.prev = 1;
                ProductRegion = keystone.list('ProductRegion');
                id = args.id, _args$where4 = args.where, where = _args$where4 === void 0 ? {} : _args$where4;
                whereFinal = _objectSpread({}, processWhere(where));
                if (id) whereFinal._id = id;
                _context11.next = 8;
                return ProductRegion.model.find(whereFinal);

              case 8:
                _ref16 = _context11.sent;
                _ref17 = _slicedToArray(_ref16, 1);
                doc = _ref17[0];
                console.log("DOC", doc);
                _id = doc._id, name = doc.name;
                _ref18 = doc.map || [], _ref19 = _slicedToArray(_ref18, 2), lat = _ref19[0], lon = _ref19[1];
                return _context11.abrupt("return", [{
                  id: String(_id),
                  name: name,
                  lat: lat,
                  lon: lon
                }]);

              case 17:
                _context11.prev = 17;
                _context11.t0 = _context11["catch"](1);
                console.log("ERR", _context11.t0);

              case 20:
              case "end":
                return _context11.stop();
            }
          }
        }, _callee11, this, [[1, 17]]);
      }));

      function productRegion(_x41, _x42, _x43, _x44) {
        return _productRegion.apply(this, arguments);
      }

      return productRegion;
    }(),
    promotion: function () {
      var _promotion = _asyncToGenerator(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee12(obj, args, _ref20, info) {
        var keystone, Promotion, id, _args$where5, where, populateRelatedList, whereFinal;

        return _regenerator.default.wrap(function _callee12$(_context12) {
          while (1) {
            switch (_context12.prev = _context12.next) {
              case 0:
                keystone = _ref20.keystone;
                Promotion = keystone.list('Promotion');
                id = args.id, _args$where5 = args.where, where = _args$where5 === void 0 ? {} : _args$where5;
                populateRelatedList = [{
                  path: "blocks",
                  populate: [{
                    path: "product",
                    populate: [{
                      path: "images"
                    }, {
                      path: "videos"
                    }]
                  }]
                }];
                whereFinal = _objectSpread({}, processWhere(where));
                if (id) whereFinal._id = id;
                _context12.t0 = populateRelated;
                _context12.next = 9;
                return Promotion.model.find(whereFinal).populate('location');

              case 9:
                _context12.t1 = _context12.sent;
                _context12.t2 = populateRelatedList;
                _context12.next = 13;
                return (0, _context12.t0)(_context12.t1, _context12.t2);

              case 13:
                return _context12.abrupt("return", _context12.sent);

              case 14:
              case "end":
                return _context12.stop();
            }
          }
        }, _callee12, this);
      }));

      function promotion(_x45, _x46, _x47, _x48) {
        return _promotion.apply(this, arguments);
      }

      return promotion;
    }(),
    socialBuzz: function () {
      var _socialBuzz = _asyncToGenerator(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee13(obj, args, _ref21, info) {
        var keystone, SocialBuzz, id, _args$where6, where, whereFinal;

        return _regenerator.default.wrap(function _callee13$(_context13) {
          while (1) {
            switch (_context13.prev = _context13.next) {
              case 0:
                keystone = _ref21.keystone;
                SocialBuzz = keystone.list('SocialBuzz');
                id = args.id, _args$where6 = args.where, where = _args$where6 === void 0 ? {} : _args$where6;
                whereFinal = _objectSpread({}, processWhere(where));
                if (id) whereFinal._id = id;
                _context13.next = 7;
                return SocialBuzz.model.find(whereFinal);

              case 7:
                return _context13.abrupt("return", _context13.sent);

              case 8:
              case "end":
                return _context13.stop();
            }
          }
        }, _callee13, this);
      }));

      function socialBuzz(_x49, _x50, _x51, _x52) {
        return _socialBuzz.apply(this, arguments);
      }

      return socialBuzz;
    }()
  })
};

var _default = function _default(_ref22) {
  var app = _ref22.app,
      middleware = _ref22.middleware;
  var server = new _apolloServerExpress.ApolloServer({
    typeDefs: _typeDefs.default,
    resolvers: resolvers,
    context: function context(_ref23) {
      var req = _ref23.req,
          res = _ref23.res;
      return {
        keystone: _keystone.default,
        user: res.locals.user,
        req: req,
        res: res
      };
    }
  });
  server.applyMiddleware({
    app: app,
    path: "/api/graphql"
  });
};

exports.default = _default;
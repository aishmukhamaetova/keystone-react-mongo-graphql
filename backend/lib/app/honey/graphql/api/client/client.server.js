"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _config = _interopRequireDefault(require("next/config"));

var _apolloClient = _interopRequireDefault(require("apollo-client"));

var _apolloLinkHttp = require("apollo-link-http");

var _apolloCacheInmemory = require("apollo-cache-inmemory");

var _nodeFetch = _interopRequireDefault(require("node-fetch"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _getConfig = (0, _config.default)(),
    publicRuntimeConfig = _getConfig.publicRuntimeConfig;

var CLIENT_HOST = publicRuntimeConfig.CLIENT_HOST;

var _default = function _default(state) {
  return new _apolloClient.default({
    ssrMode: true,
    link: (0, _apolloLinkHttp.createHttpLink)({
      uri: "".concat(CLIENT_HOST, "/api/graphql"),
      credentials: 'same-origin',
      // headers: {
      //   cookie: req.header('Cookie'),
      // },
      fetch: _nodeFetch.default
    }),
    cache: new _apolloCacheInmemory.InMemoryCache().restore(state || {})
  });
};

exports.default = _default;
"use strict";

var _list = _interopRequireDefault(require("../../../../box/auth/classic/api/keystone/graphql/server/list"));

var _list2 = _interopRequireDefault(require("../../../../box/auth/facebook/api/keystone/graphql/server/list"));

var _list3 = _interopRequireDefault(require("../../../../box/eshop/order/api/keystone/graphql/server/list"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  scalar DateTime\n\n  type ErrorProp {\n    name: String\n    value: String\n  }\n\n  type Error {\n    type: String\n    code: Int\n    message: String\n    props: [ErrorProp]\n  }  \n\n  input WhereInt {\n    eq: Int\n    gt: Int\n    lt: Int\n    in: [Int]\n    lte: Int\n    ne: Int\n    nin: Int\n  }\n  \n  input WhereFloat {\n    eq: Float\n    gt: Float\n    lt: Float\n    in: [Float]\n    lte: Float\n    ne: Float\n    nin: Float\n  } \n\n  input WhereString {\n    eq: String\n    gt: String\n    lt: String\n    in: [String]\n    lte: String\n    ne: String\n    nin: String\n  }\n  \n  input WhereBoolean {\n    eq: Boolean\n    gt: Boolean\n    lt: Boolean\n    in: [Boolean]\n    lte: Boolean\n    ne: Boolean\n    nin: Boolean\n  }\n  \n  input Where {\n    int: WhereInt\n    float: WhereFloat\n    string: WhereString\n    boolean: WhereBoolean\n  }\n  \n  input Filter {\n    name: String\n    by: String\n    where: Where\n  }  \n  \n  input Pagination {\n    index: Int\n    size: Int\n  }\n  \n  type Content {\n    id: ID\n    name: String\n    text: String\n    content: String\n    language: Language\n  }\n  \n  input ContentInput {\n    language: Where\n  }\n  \n  type CloudinaryImage {\n    secure_url: String\n    url: String\n    resource_type: String\n    format: String\n    height: Float\n    width: Float\n    signature: String\n    version: Float    \n  }\n  \n  type Image {\n    id: ID\n    name: String\n    image: CloudinaryImage\n    count: Float\n  }\n  \n  type Language {\n    id: ID\n    name: String\n  }\n\n  type Location {\n    id: ID\n    name: String\n    currency: LocationCurrency \n  }\n\n  type LocationCurrency {\n    id: ID\n    name: String\n  }\n  \n  type Post {\n    id: ID\n    title: String\n    state: String\n    author: User\n    publishedDate: DateTime\n    content: PostContent\n    \n    categories: [PostCategory]\n    testimonials: [Testimonial]\n        \n    images: [Image]\n    videos: [Video]\n    \n    isPinnedToIndex: Boolean\n  }\n\n  input PostInput {\n    type: Where\n    isPinnedToIndex: Where\n  }\n  \n  type PostCategory {\n    id: ID\n    name: String\n  }\n  \n  type PostContent {\n    brief: String\n    extended: String\n  }\n  \n  type Product {\n    id: ID\n    sku: String\n    name: String\n    description: String\n    \n    badge: [ProductBadge]\n    categories: [ProductCategory]\n    harvest: [ProductHarvest]\n    pairs: [ProductPair]\n    regions: [ProductRegion]\n    stores: [ProductStore]\n    sweetness: Int\n    testimonials: [Testimonial]\n    weight: Int\n    \n    images: [Image]\n    videos: [Video]   \n   \n    isUniqueProposition: Boolean\n  }\n  \n  input ProductInput {\n    id: ID\n    isUniqueProposition: Where\n  }\n\n  type ProductBadge {\n    id: ID\n    name: String\n  }\n    \n  type ProductCategory {\n    id: ID\n    name: String\n  }\n  \n  type ProductHarvest {\n    id: ID\n    name: String\n  }\n  \n  type ProductPair {\n    id: ID\n    name: String\n  }\n  \n  type ProductRegion {\n    id: ID\n    name: String\n    lat: Float\n    lon: Float\n  }\n  \n  type ProductStore {\n    id: ID\n    product: Product\n    quantity: Int\n    price: Float\n    \n    location: Location\n  }\n  \n  type Promotion {\n    id: ID\n    timeBegin: DateTime\n    timeEnd: DateTime\n    type: String\n    location: Location\n    blocks: [PromotionBlock]\n  }\n  \n  input PromotionInput {\n    type: Where\n  }\n  \n  type PromotionBlock {\n    id: ID\n    content: String\n    promotion: Promotion\n    product: Product\n  }\n  \n  type PressQuote {\n    id: ID\n    name: String\n    logo: Image\n    description: String\n    link: String\n  }\n  \n  type SocialBuzz {\n    id: ID\n    name: String\n    linkInstagram: String\n  }\n  \n  type Testimonial {\n    id: ID\n    text: String\n    rating: Int\n  }\n  \n  type Name {\n    first: String\n    last: String\n  }\n  \n  type User {\n    id: ID\n    name: Name\n    email: String\n  }\n  \n  type Video {\n    id: ID\n    name: String\n    videoLink: String \n  }\n\n  ", "\n\n  type Query {\n    ", "\n  \n    image: [Image]\n    location: [Location]\n    locationCurrency: [LocationCurrency]\n    \n    content(\n      languageName: String\n      where: ContentInput\n    ): [Content]\n    \n    post(\n      id: ID    \n      where: PostInput\n    ): [Post]\n    \n    productRegion(\n      id: ID\n    ): [ProductRegion]\n    \n    product(\n      id: ID    \n      where: ProductInput\n      pagination: Pagination\n      \n      filter: [Filter]\n    ): [Product]\n    \n    pressQuote: [PressQuote]\n    \n    productCategory: [ProductCategory]\n    \n    promotion(\n      id: ID\n      where: PromotionInput\n    ): [Promotion]\n    \n    socialBuzz: [SocialBuzz]\n    \n    me: User\n  }\n  \n  ", "\n    \n  type Mutation {\n    ", "\n    \n    regionUpdate(\n      id: ID\n      lat: Float\n      lon: Float\n    ): Boolean  \n  \n    ", "\n  }\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

var _require = require('apollo-server-express'),
    gql = _require.gql;

var list = _toConsumableArray(_list.default).concat(_toConsumableArray(_list2.default), _toConsumableArray(_list3.default));

exports = module.exports = gql(_templateObject(), list.map(function (_ref) {
  var type = _ref.type;
  return type;
}).join('\n'), list.map(function (_ref2) {
  var query = _ref2.query;
  return query;
}).join('\n'), require("../../../../box/auth/classic/api/keystone/graphql/server/typeDefs").default, list.map(function (_ref3) {
  var mutation = _ref3.mutation;
  return mutation;
}).join('\n'), require("../../../../box/auth/classic/api/keystone/graphql/server/mutationDefs").default);
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _boxAuthFacebookLink = _interopRequireDefault(require("../../../../box/auth/facebook/api/keystone/rest/server/boxAuthFacebookLink"));

var _boxAuthFacebookLinkCallback = _interopRequireDefault(require("../../../../box/auth/facebook/api/keystone/rest/server/boxAuthFacebookLinkCallback"));

var _boxAuthFacebookLogin = _interopRequireDefault(require("../../../../box/auth/facebook/api/keystone/rest/server/boxAuthFacebookLogin"));

var _boxAuthFacebookLoginCallback = _interopRequireDefault(require("../../../../box/auth/facebook/api/keystone/rest/server/boxAuthFacebookLoginCallback"));

var _boxMastercardWebhook = _interopRequireDefault(require("../../../../box/mastercard/manager/api/keystone/rest/server/boxMastercardWebhook"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var middleware = function middleware(app) {
  app.get('/api/box.auth/facebook/link', _boxAuthFacebookLink.default);
  app.get('/api/box.auth/facebook/linkCallback', _boxAuthFacebookLinkCallback.default);
  app.get('/api/box.auth/facebook/login', _boxAuthFacebookLogin.default);
  app.get('/api/box.auth/facebook/loginCallback', _boxAuthFacebookLoginCallback.default);
  app.post('/api/box.mastercard/webhook', _boxMastercardWebhook.default);
};

var _default = middleware;
exports.default = _default;
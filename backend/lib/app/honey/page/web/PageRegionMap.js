"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _graphqlTag = _interopRequireDefault(require("graphql-tag"));

var _router = require("next/router");

var _Grid = _interopRequireDefault(require("@material-ui/core/Grid"));

var _Button = _interopRequireDefault(require("@material-ui/core/Button"));

var _mdiMaterialUi = require("mdi-material-ui");

var _pigeonMaps = _interopRequireDefault(require("pigeon-maps"));

var _react = _interopRequireDefault(require("react"));

var _reactApollo = require("react-apollo");

var _compose = _interopRequireDefault(require("recompose/compose"));

var _lifecycle = _interopRequireDefault(require("recompose/lifecycle"));

var _withHandlers = _interopRequireDefault(require("recompose/withHandlers"));

var _withStateHandlers = _interopRequireDefault(require("recompose/withStateHandlers"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _Page = _interopRequireDefault(require("../../../../evoke-me/page/web/Page"));

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : {
    default: obj
  };
}

function _slicedToArray(arr, i) {
  return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest();
}

function _nonIterableRest() {
  throw new TypeError("Invalid attempt to destructure non-iterable instance");
}

function _iterableToArrayLimit(arr, i) {
  var _arr = [];
  var _n = true;
  var _d = false;
  var _e = undefined;

  try {
    for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) {
      _arr.push(_s.value);

      if (i && _arr.length === i) break;
    }
  } catch (err) {
    _d = true;
    _e = err;
  } finally {
    try {
      if (!_n && _i["return"] != null) _i["return"]();
    } finally {
      if (_d) throw _e;
    }
  }

  return _arr;
}

function _arrayWithHoles(arr) {
  if (Array.isArray(arr)) return arr;
}

function _templateObject2() {
  var data = _taggedTemplateLiteral(["\nmutation regionUpdate($id: ID, $lat: Float, $lon: Float){\n  regionUpdate(\n    id: $id,\n    lat: $lat,\n    lon: $lon,\n  )\n}\n"]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = _taggedTemplateLiteral(["\nquery productRegion($id: ID) {\n  productRegion(\n    id: $id\n  ) {\n    id\n    name\n    lat\n    lon\n  }\n}"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) {
  if (!raw) {
    raw = strings.slice(0);
  }

  return Object.freeze(Object.defineProperties(strings, {
    raw: {
      value: Object.freeze(raw)
    }
  }));
}

var positionMap = {
  Leuven: [50.879, 4.6997],
  Lebanon: [33.888630, 35.495480]
};
var QUERY = (0, _graphqlTag.default)(_templateObject());
var MUTATION = (0, _graphqlTag.default)(_templateObject2());

var Marker = function Marker(_ref) {
  var anchor = _ref.anchor,
      left = _ref.left,
      top = _ref.top;
  return _react.default.createElement("div", {
    style: {
      position: 'absolute',
      left: left,
      top: top
    }
  }, _react.default.createElement("div", {
    style: {
      transform: 'translate(-50%,-50%)',
      left: '50%',
      top: '0%',
      position: 'absolute'
    }
  }, _react.default.createElement(_mdiMaterialUi.MapMarker, {
    style: {
      width: '48px',
      height: '48px',
      color: '#145869'
    }
  })));
};

var enhance = (0, _compose.default)(_reactApollo.withApollo, _router.withRouter, (0, _withStateHandlers.default)(function () {
  return {
    center: positionMap.Leuven,
    marker: positionMap.Lebanon,
    zoom: 12
  };
}, {
  centerSet: function centerSet() {
    return function (center) {
      return {
        center: center
      };
    };
  },
  markerSet: function markerSet() {
    return function (marker) {
      return {
        marker: marker
      };
    };
  },
  zoomSet: function zoomSet() {
    return function (zoom) {
      return {
        zoom: zoom
      };
    };
  }
}), (0, _withHandlers.default)({
  moveToLebanon: function moveToLebanon(_ref2) {
    var centerSet = _ref2.centerSet,
        zoomSet = _ref2.zoomSet;
    return function () {
      centerSet(positionMap.Lebanon);
      zoomSet(14);
    };
  },
  onMapClick: function onMapClick(_ref3) {
    var markerSet = _ref3.markerSet;
    return function (_ref4) {
      var latLng = _ref4.latLng;
      markerSet(latLng);
    };
  },
  onSave: function onSave(_ref5) {
    var client = _ref5.client,
        marker = _ref5.marker,
        router = _ref5.router;
    return function () {
      var id = router.query.id;

      var _marker = _slicedToArray(marker, 2),
          lat = _marker[0],
          lon = _marker[1];

      console.log(marker);
      client.mutate({
        mutation: MUTATION,
        variables: {
          id: id,
          lat: lat,
          lon: lon
        }
      });
    };
  }
}), (0, _withHandlers.default)(function () {
  var timerList = [];
  return {
    mount: function mount(_ref6) {
      var centerSet = _ref6.centerSet,
          markerSet = _ref6.markerSet,
          zoomSet = _ref6.zoomSet,
          lat = _ref6.lat,
          lon = _ref6.lon;
      return function () {
        //timerList.push(setTimeout(moveToLebanon, 1000))
        if (lat && lon) {
          centerSet([lat, lon]);
          markerSet([lat, lon]);
          zoomSet(14);
        }
      };
    },
    unmount: function unmount() {
      return function () {
        for (var _i2 = 0; _i2 < timerList.length; _i2++) {
          var timer = timerList[_i2];
          clearTimeout(timer);
        }
      };
    }
  };
}), (0, _lifecycle.default)({
  componentDidMount: function componentDidMount() {
    this.props.mount();
  },
  componentWillUnmount: function componentWillUnmount() {
    this.props.unmount();
  }
}));
var Title = (0, _styledComponents.default)(_Grid.default).withConfig({
  displayName: "PageRegionMap__Title",
  componentId: "sc-1si01at-0"
})(["background-color:rgba(0,0,0,0.6);color:white;padding:10px;"]);
var BlockMap = enhance(function (_ref7) {
  var center = _ref7.center,
      marker = _ref7.marker,
      router = _ref7.router,
      zoom = _ref7.zoom,
      onMapClick = _ref7.onMapClick,
      onSave = _ref7.onSave;
  return _react.default.createElement(_Grid.default, {
    container: true,
    justify: "center"
  }, _react.default.createElement(Title, {
    item: true,
    xs: 12
  }, "REGION LOCATION"), _react.default.createElement(_Grid.default, {
    item: true,
    xs: 12,
    style: {
      height: 600
    }
  }, _react.default.createElement(_pigeonMaps.default, {
    animate: true,
    attribution: false,
    center: center,
    zoom: zoom,
    onClick: onMapClick
  }, marker ? _react.default.createElement(Marker, {
    anchor: marker,
    payload: 1,
    onClick: function onClick() {}
  }) : null)), _react.default.createElement(_Grid.default, {
    item: true,
    xs: 12
  }, _react.default.createElement(_Button.default, {
    variant: "contained",
    color: "primary",
    style: {
      margin: 5
    },
    onClick: onSave
  }, "SAVE")));
});
var PageRegionMap = (0, _compose.default)(_router.withRouter)(function (_ref8) {
  var router = _ref8.router;
  var id = router.query.id;
  return _react.default.createElement(_Page.default, null, _react.default.createElement(_reactApollo.Query, {
    query: QUERY,
    variables: {
      id: id
    }
  }, function (_ref9) {
    var loading = _ref9.loading,
        error = _ref9.error,
        data = _ref9.data;

    if (loading || !data) {
      return "Loading";
    }

    var _data$productRegion = data.productRegion,
        productRegion = _data$productRegion === void 0 ? [] : _data$productRegion;

    if (productRegion && productRegion.length) {
      var _productRegion = _slicedToArray(productRegion, 1),
          _productRegion$ = _productRegion[0],
          lat = _productRegion$.lat,
          lon = _productRegion$.lon;

      console.log("DATA", lat, lon);
      return _react.default.createElement(BlockMap, {
        lat: lat,
        lon: lon
      });
    }

    return _react.default.createElement(BlockMap, null);
  }));
});
var _default = PageRegionMap;
exports.default = _default;
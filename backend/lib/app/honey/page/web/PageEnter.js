"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _SectionPageFooter = _interopRequireDefault(require("../../../default/pageFooter/web/section/SectionPageFooter"));

var _SectionPageHeader = _interopRequireDefault(require("../../../default/pageHeader/web/section/SectionPageHeader"));

var _SectionAuth = _interopRequireDefault(require("../../../../box/auth/manager/web/section/SectionAuth"));

var _SectionNotify = _interopRequireDefault(require("../../../../box/notify/manager/web/section/SectionNotify"));

var _Page = _interopRequireDefault(require("../../../../evoke-me/page/web/Page"));

var _themeWeb = _interopRequireDefault(require("../../theme/api/themeWeb"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

var removeHashFromBuggyFacebook = function removeHashFromBuggyFacebook() {
  try {
    if (location.hash = '#_=_') {
      window.history.replaceState("", document.title, window.location.pathname + window.location.search);
    }
  } catch (err) {}
};

var PageEnter = function PageEnter() {
  removeHashFromBuggyFacebook();
  return _react.default.createElement(_Page.default, {
    hasFooter: true
  }, _react.default.createElement(_SectionNotify.default, {
    theme: _themeWeb.default.default
  }), _react.default.createElement(_SectionPageHeader.default, {
    theme: _themeWeb.default.default
  }), _react.default.createElement(_SectionAuth.default, null), _react.default.createElement(_SectionPageFooter.default, {
    theme: _themeWeb.default.default
  }));
};

var _default = PageEnter;
exports.default = _default;
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _SectionNotify = _interopRequireDefault(require("../../../../box/notify/manager/web/section/SectionNotify"));

var _Page = _interopRequireDefault(require("../../../../evoke-me/page/web/Page"));

var _themeWeb = _interopRequireDefault(require("../../../default/theme/api/themeWeb"));

var _SectionPageFooter = _interopRequireDefault(require("../../../default/pageFooter/web/section/SectionPageFooter"));

var _SectionPageSubHeader = _interopRequireDefault(require("../../../default/pageHeader/web/section/SectionPageSubHeader"));

var _SectionPost = _interopRequireDefault(require("../../post/web/section/SectionPost"));

var _reactSticky = require("react-sticky");

var _compose = _interopRequireDefault(require("recompose/compose"));

var _defaultProps = _interopRequireDefault(require("recompose/defaultProps"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

var enhance = (0, _compose.default)((0, _defaultProps.default)({
  A: {
    SubHeaderWrapper: _styledComponents.default.div.withConfig({
      displayName: "PagePost__SubHeaderWrapper",
      componentId: "sc-11ybgvp-0"
    })(["&&{z-index:10;display:block;}"])
  }
}));
var PagePost = enhance(function (_ref) {
  var A = _ref.A;
  return _react.default.createElement(_Page.default, null, _react.default.createElement(_SectionNotify.default, {
    theme: _themeWeb.default.default
  }), _react.default.createElement(_reactSticky.StickyContainer, null, _react.default.createElement(_reactSticky.Sticky, null, function (_ref2) {
    var style = _ref2.style;
    return _react.default.createElement(A.SubHeaderWrapper, {
      style: style
    }, _react.default.createElement(_SectionPageSubHeader.default, {
      theme: _themeWeb.default.default
    }));
  }), _react.default.createElement(_SectionPost.default, {
    theme: _themeWeb.default.default
  })), _react.default.createElement(_SectionPageFooter.default, null));
});
var _default = PagePost;
exports.default = _default;
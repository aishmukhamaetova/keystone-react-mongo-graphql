"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _reactApollo = require("react-apollo");

var _compose = _interopRequireDefault(require("recompose/compose"));

var _lifecycle = _interopRequireDefault(require("recompose/lifecycle"));

var _spaceMenu = _interopRequireDefault(require("../../../../default/menu/space/spaceMenu"));

var _spaceAuth = _interopRequireDefault(require("../../../../../box/auth/manager/space/spaceAuth"));

var _spaceAuthClassic = _interopRequireDefault(require("../../../../../box/auth/classic/space/spaceAuthClassic"));

var _spaceBoxEshopCart = _interopRequireDefault(require("../../../../../box/eshop/cart/space/spaceBoxEshopCart"));

var _withSpace = _interopRequireDefault(require("../../../../../evoke-me/space/all/withSpace"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

// import spaceNav             from 'app/default/nav/space/spaceNav'
var enhance = (0, _compose.default)(_reactApollo.withApollo, (0, _withSpace.default)(_spaceAuth.default), (0, _withSpace.default)(_spaceAuthClassic.default), (0, _withSpace.default)(_spaceBoxEshopCart.default), (0, _withSpace.default)(_spaceMenu.default), (0, _lifecycle.default)({
  componentDidMount: function componentDidMount() {
    var _this$props = this.props,
        client = _this$props.client,
        fromAuth = _this$props.fromAuth;
    fromAuth.restore();
    var fromAuthClassic = this.props.fromAuthClassic;
    fromAuthClassic.check({}, {
      client: client
    });
    fromAuthClassic.restore();
    var fromBoxEshopCart = this.props.fromBoxEshopCart;
    fromBoxEshopCart.restore();
    var fromMenu = this.props.fromMenu;
    fromMenu.create({
      name: "Home",
      caption: "Home",
      path: "/"
    }, {
      orderName: "headerLeft"
    });
    fromMenu.create({
      name: "Products",
      caption: "Products",
      path: "/products"
    }, {
      orderName: "headerLeft"
    });
    fromMenu.create({
      name: "Posts",
      caption: "Posts",
      path: "/posts"
    }, {
      orderName: "headerLeft"
    });
    fromMenu.create({
      name: "Profile",
      caption: "Profile",
      path: "/profile"
    }, {
      orderName: "headerRight"
    });
    fromMenu.create({
      name: "Cart",
      caption: "Cart",
      path: "/cart"
    }, {
      orderName: "headerRight"
    });
    fromMenu.create({
      name: "Login",
      caption: "Enter",
      path: "/enter"
    }, {
      orderName: "headerRight"
    }); // const { fromAuthGraphql } = this.props
    //
    // fromAuthGraphql.query(client)
  }
}));

var _default = enhance(function () {
  return null;
});

exports.default = _default;
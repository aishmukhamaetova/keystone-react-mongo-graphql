"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _SectionNotify = _interopRequireDefault(require("../../../../box/notify/manager/web/section/SectionNotify"));

var _Page = _interopRequireDefault(require("../../../../evoke-me/page/web/Page"));

var _SectionPageFooter = _interopRequireDefault(require("../../../default/pageFooter/web/section/SectionPageFooter"));

var _SectionPageSubHeader = _interopRequireDefault(require("../../../default/pageHeader/web/section/SectionPageSubHeader"));

var _themeWeb = _interopRequireDefault(require("../../theme/api/themeWeb"));

var _compose = _interopRequireDefault(require("recompose/compose"));

var _defaultProps = _interopRequireDefault(require("recompose/defaultProps"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _reactSticky = require("react-sticky");

var _SectionProfile = _interopRequireDefault(require("../../../../box/profile/manager/web/section/SectionProfile"));

var _polished = require("polished");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

var enhance = (0, _compose.default)((0, _defaultProps.default)({
  A: {
    SubHeaderWrapper: _styledComponents.default.div.withConfig({
      displayName: "PageProfile__SubHeaderWrapper",
      componentId: "bvyxcn-0"
    })(["&&{z-index:10;display:block;}"]),
    Container: _styledComponents.default.div.withConfig({
      displayName: "PageProfile__Container",
      componentId: "bvyxcn-1"
    })(["&&{margin:", " auto;}"], (0, _polished.rem)(50))
  }
}));
var PageProfile = enhance(function (_ref) {
  var A = _ref.A;
  return _react.default.createElement(_Page.default, null, _react.default.createElement(_SectionNotify.default, {
    theme: _themeWeb.default.default
  }), _react.default.createElement(_reactSticky.StickyContainer, null, _react.default.createElement(_reactSticky.Sticky, null, function (_ref2) {
    var style = _ref2.style;
    return _react.default.createElement(A.SubHeaderWrapper, {
      style: style
    }, _react.default.createElement(_SectionPageSubHeader.default, {
      theme: _themeWeb.default.default
    }));
  }), _react.default.createElement(A.Container, null, _react.default.createElement(_SectionProfile.default, null))), _react.default.createElement(_SectionPageFooter.default, null));
});
var _default = PageProfile;
exports.default = _default;
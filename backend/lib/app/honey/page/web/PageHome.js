"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _reactSticky = require("react-sticky");

var _compose = _interopRequireDefault(require("recompose/compose"));

var _defaultProps = _interopRequireDefault(require("recompose/defaultProps"));

var _SectionPageFooter = _interopRequireDefault(require("../../../default/pageFooter/web/section/SectionPageFooter"));

var _SectionPageHeader = _interopRequireDefault(require("../../../default/pageHeader/web/section/SectionPageHeader"));

var _SectionPageSubHeader = _interopRequireDefault(require("../../../default/pageHeader/web/section/SectionPageSubHeader"));

var _SectionNotify = _interopRequireDefault(require("../../../../box/notify/manager/web/section/SectionNotify"));

var _Page = _interopRequireDefault(require("../../../../evoke-me/page/web/Page"));

var _themeWeb = _interopRequireDefault(require("../../theme/api/themeWeb"));

var _SectionProductUnique = _interopRequireDefault(require("../../product/web/section/SectionProductUnique"));

var _SectionBlogTeaser = _interopRequireDefault(require("../../post/web/section/SectionBlogTeaser"));

var _SectionBannerArea = _interopRequireDefault(require("../../promotion/web/section/SectionBannerArea"));

var _SectionBar = _interopRequireDefault(require("../../promotion/web/section/SectionBar"));

var _SectionSlider = _interopRequireDefault(require("../../promotion/web/section/SectionSlider"));

var _SectionPressQuotes = _interopRequireDefault(require("../../pressQuote/web/section/SectionPressQuotes"));

var _SectionSocialBuzz = _interopRequireDefault(require("../../socialBuzz/web/section/SectionSocialBuzz"));

var _SectionTestimonial = _interopRequireDefault(require("../../testimonial/web/section/SectionTestimonial"));

var _SectionStripe = _interopRequireDefault(require("../../../default/pageHeader/web/section/SectionStripe.js"));

var _core = require("@material-ui/core");

var _styledComponents = _interopRequireDefault(require("styled-components"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

var mui = (0, _core.createMuiTheme)({
  typography: {
    useNextVariants: true,
    fontFamily: ['Open Sans', 'sans-serif'].join(','),
    fontSize: 16,
    //dont change it
    htmlFontSize: 10 //font-size: 62.5%; /* 62.5% of 16px = 10px */

  },
  contrastThreshold: 3,
  tonalOffset: 0.2,
  palette: {
    primary: {
      main: '#080808',
      contrastText: '#fff'
    },
    secondary: {
      main: '#f5a623',
      contrastText: '#fff'
    },
    error: {
      main: '#f44336',
      contrastText: '#fff'
    },
    action: {
      hover: '#fff',
      hoverOpacity: 0.08
    }
  },
  fontWeightMedium: 500,
  button: {
    borderRadius: 0
  },
  overrides: {
    MuiButton: {
      // Name of the component ⚛️ / style sheet
      root: {
        // Name of the rule
        borderRadius: 0
      }
    }
  }
});
var enhance = (0, _compose.default)((0, _defaultProps.default)({
  A: {
    SubHeaderWrapper: _styledComponents.default.div.withConfig({
      displayName: "PageHome__SubHeaderWrapper",
      componentId: "sc-15gup78-0"
    })(["&&{z-index:10;display:block;}"])
  }
}));
var PageHome = enhance(function (_ref) {
  var A = _ref.A;
  return _react.default.createElement(_core.MuiThemeProvider, {
    theme: mui
  }, _react.default.createElement(_core.CssBaseline, null), _react.default.createElement(_Page.default, {
    hasFooter: true
  }, _react.default.createElement(_SectionNotify.default, null), _react.default.createElement(_SectionPageHeader.default, {
    theme: _themeWeb.default.default
  }), _react.default.createElement(_reactSticky.StickyContainer, null, _react.default.createElement(_reactSticky.Sticky, null, function (_ref2) {
    var style = _ref2.style;
    return _react.default.createElement(A.SubHeaderWrapper, {
      style: style
    }, _react.default.createElement(_SectionPageSubHeader.default, {
      theme: _themeWeb.default.default
    }));
  }), _react.default.createElement(_SectionStripe.default, {
    type: 3
  }), _react.default.createElement(_SectionSocialBuzz.default, null), _react.default.createElement(_SectionStripe.default, {
    type: 1
  }), _react.default.createElement(_SectionTestimonial.default, null), _react.default.createElement(_SectionStripe.default, {
    type: 3
  }), _react.default.createElement(_SectionPressQuotes.default, null), _react.default.createElement(_SectionStripe.default, {
    type: 3
  }), _react.default.createElement(_SectionProductUnique.default, null), _react.default.createElement(_SectionStripe.default, {
    type: 2
  }), _react.default.createElement(_SectionBlogTeaser.default, null), _react.default.createElement(_SectionStripe.default, {
    type: 2
  }), _react.default.createElement(_SectionBannerArea.default, null), _react.default.createElement(_SectionStripe.default, {
    type: 2
  }), _react.default.createElement(_SectionBar.default, null), _react.default.createElement(_SectionStripe.default, {
    type: 2
  })), _react.default.createElement(_SectionPageFooter.default, null)));
});
var _default = PageHome;
exports.default = _default;
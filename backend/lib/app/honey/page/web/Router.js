"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _reactRouterDom = require("react-router-dom");

var _compose = _interopRequireDefault(require("recompose/compose"));

var _lifecycle = _interopRequireDefault(require("recompose/lifecycle"));

var _spaceMenu = _interopRequireDefault(require("../../../default/menu/space/spaceMenu"));

var _spaceNav = _interopRequireDefault(require("../../../default/nav/space/spaceNav"));

var _withSpace = _interopRequireDefault(require("../../../../evoke-me/space/all/withSpace"));

var _PageEnter = _interopRequireDefault(require("./PageEnter"));

var _PageEditor = _interopRequireDefault(require("./PageEditor"));

var _PageHome = _interopRequireDefault(require("./PageHome"));

var _PageMyWork = _interopRequireDefault(require("./PageMyWork"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

var enhance = (0, _compose.default)((0, _withSpace.default)(_spaceNav.default), (0, _withSpace.default)(_spaceMenu.default), (0, _lifecycle.default)({
  componentDidMount: function componentDidMount() {
    var _this$props = this.props,
        match = _this$props.match,
        fromNav = _this$props.fromNav,
        fromMenu = _this$props.fromMenu;
    var path = match.path.replace("*", "");
    fromNav.pathSet(path);
    fromMenu.create({
      name: "Home",
      caption: "\u0414\u043E\u043C\u043E\u0439",
      path: "/"
    }, {
      orderName: "headerLeft"
    });
    fromMenu.create({
      name: "MyWork",
      caption: "\u041C\u043E\u0438 \u043A\u043E\u043B\u043B\u0430\u0436\u0438",
      path: "/my"
    }, {
      orderName: "headerLeft"
    });
    fromMenu.create({
      name: "Login",
      caption: "\u0412\u043E\u0439\u0442\u0438",
      path: "/enter"
    }, {
      orderName: "headerRight"
    });
  }
}));
var Router = enhance(function (_ref) {
  var match = _ref.match;
  var path = match.path.replace("*", "");
  var pathMap = {
    PATH_ENTER: "".concat(path, "/enter").replace(/\/\/+/g, '/'),
    PATH_EDITOR: "".concat(path, "/editor").replace(/\/\/+/g, '/'),
    PATH_MY_WORK: "".concat(path, "/my").replace(/\/\/+/g, '/')
  };
  return _react.default.createElement(_reactRouterDom.Switch, null, _react.default.createElement(_reactRouterDom.Route, {
    exact: true,
    path: pathMap.PATH_ENTER,
    component: _PageEnter.default
  }), _react.default.createElement(_reactRouterDom.Route, {
    exact: true,
    path: pathMap.PATH_EDITOR,
    component: _PageEditor.default
  }), _react.default.createElement(_reactRouterDom.Route, {
    exact: true,
    path: pathMap.PATH_MY_WORK,
    component: _PageMyWork.default
  }), _react.default.createElement(_reactRouterDom.Route, {
    path: path,
    component: _PageHome.default
  }));
});
var _default = Router;
exports.default = _default;
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _config = _interopRequireDefault(require("../../config/api/config"));

var _query = _interopRequireDefault(require("../api/graphql/query"));

var _spacePressQuote = _interopRequireDefault(require("./spacePressQuote"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _default = [_config.default.namespace, "pressQuoteGraphql", [_spacePressQuote.default], function () {
  return {
    isLoading: false
  };
}, {
  query: function query(getState, setState, methods, _ref) {
    var fromPressQuote = _ref.fromPressQuote;
    return (
      /*#__PURE__*/
      _regenerator.default.mark(function _callee(client) {
        var _ref2, data, _iteratorNormalCompletion, _didIteratorError, _iteratorError, _iterator, _step, doc;

        return _regenerator.default.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.prev = 0;

                if (!client) {
                  _context.next = 36;
                  break;
                }

                _context.next = 4;
                return setState({
                  isLoading: true
                });

              case 4:
                _context.next = 6;
                return client.query({
                  query: _query.default
                });

              case 6:
                _ref2 = _context.sent;
                data = _ref2.data;
                _iteratorNormalCompletion = true;
                _didIteratorError = false;
                _iteratorError = undefined;
                _context.prev = 11;
                _iterator = data.pressQuote[Symbol.iterator]();

              case 13:
                if (_iteratorNormalCompletion = (_step = _iterator.next()).done) {
                  _context.next = 20;
                  break;
                }

                doc = _step.value;
                _context.next = 17;
                return fromPressQuote.create(doc);

              case 17:
                _iteratorNormalCompletion = true;
                _context.next = 13;
                break;

              case 20:
                _context.next = 26;
                break;

              case 22:
                _context.prev = 22;
                _context.t0 = _context["catch"](11);
                _didIteratorError = true;
                _iteratorError = _context.t0;

              case 26:
                _context.prev = 26;
                _context.prev = 27;

                if (!_iteratorNormalCompletion && _iterator.return != null) {
                  _iterator.return();
                }

              case 29:
                _context.prev = 29;

                if (!_didIteratorError) {
                  _context.next = 32;
                  break;
                }

                throw _iteratorError;

              case 32:
                return _context.finish(29);

              case 33:
                return _context.finish(26);

              case 34:
                _context.next = 36;
                return setState({
                  isLoading: false
                });

              case 36:
                _context.next = 43;
                break;

              case 38:
                _context.prev = 38;
                _context.t1 = _context["catch"](0);
                _context.next = 42;
                return setState({
                  isLoading: false
                });

              case 42:
                console.error(_context.t1);

              case 43:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this, [[0, 38], [11, 22, 26, 34], [27,, 29, 33]]);
      })
    );
  }
}];
exports.default = _default;
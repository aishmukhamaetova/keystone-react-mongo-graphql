"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _reactApollo = require("react-apollo");

var _recompose = require("recompose");

var _semanticUiReact = require("semantic-ui-react");

var _withSpace = _interopRequireDefault(require("../../../../../evoke-me/space/all/withSpace"));

var _spacePressQuote = _interopRequireDefault(require("../../space/spacePressQuote"));

var _spacePressQuoteGraphql = _interopRequireDefault(require("../../space/spacePressQuoteGraphql"));

var _PressQuoteList = _interopRequireDefault(require("../PressQuoteList"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

var enhance = (0, _recompose.compose)(_reactApollo.withApollo, (0, _withSpace.default)(_spacePressQuote.default), (0, _withSpace.default)(_spacePressQuoteGraphql.default), (0, _recompose.lifecycle)({
  componentDidMount: function componentDidMount() {
    var client = this.props.client;
    var fromPressQuoteGraphql = this.props.fromPressQuoteGraphql;
    var fromPressQuote = this.props.fromPressQuote;
    var orderMap = fromPressQuote.orderMap;

    if (!orderMap.default.length) {
      fromPressQuoteGraphql.query(client);
    }
  }
}));
var BlockPressQuotes = enhance(function (_ref) {
  var fromPressQuote = _ref.fromPressQuote,
      fromPressQuoteGraphql = _ref.fromPressQuoteGraphql;
  var data = fromPressQuote.data,
      orderMap = fromPressQuote.orderMap;
  var isLoading = fromPressQuoteGraphql.isLoading;

  if (!orderMap.default || !orderMap.default.length) {
    return _react.default.createElement(_semanticUiReact.Segment, {
      placeholder: true,
      loading: isLoading
    }, _react.default.createElement(_semanticUiReact.Header, {
      icon: true
    }, _react.default.createElement(_semanticUiReact.Icon, {
      name: "hand peace"
    }), "BlockPressQuotes is empty"));
  }

  return _react.default.createElement(_PressQuoteList.default, {
    data: data,
    order: orderMap.default.slice(0, 3)
  });
});
var _default = BlockPressQuotes;
exports.default = _default;
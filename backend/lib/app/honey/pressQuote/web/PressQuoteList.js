"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _recompose = require("recompose");

var _PressQuote = _interopRequireDefault(require("./PressQuote"));

var _core = require("@material-ui/core");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

var enhance = (0, _recompose.compose)((0, _recompose.defaultProps)({
  data: {},
  order: []
}));
var PressQuoteList = enhance(function (_ref) {
  var data = _ref.data,
      order = _ref.order;
  return order.map(function (id) {
    return _react.default.createElement(_core.Grid, {
      key: id,
      item: true,
      sm: 4
    }, _react.default.createElement(_PressQuote.default, _extends({
      key: id
    }, data[id])));
  });
});
var _default = PressQuoteList;
exports.default = _default;
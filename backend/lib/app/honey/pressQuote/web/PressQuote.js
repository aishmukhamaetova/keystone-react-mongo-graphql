"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _recompose = require("recompose");

var _defaultProps = _interopRequireDefault(require("recompose/defaultProps"));

var _Image = _interopRequireDefault(require("../../image/web/Image"));

var _core = require("@material-ui/core");

var _styledComponents = _interopRequireDefault(require("styled-components"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

var enhance = (0, _recompose.compose)((0, _defaultProps.default)({
  A: {
    Image: (0, _styledComponents.default)(_core.ButtonBase).withConfig({
      displayName: "PressQuote__Image",
      componentId: "zayfxc-0"
    })(["&&{width:100%;height:130px;img{width:auto;height:130px;}}"]),
    Typography: (0, _styledComponents.default)(function (_ref) {
      var size = _ref.size,
          align = _ref.align,
          margin = _ref.margin,
          bold = _ref.bold,
          color = _ref.color,
          other = _objectWithoutProperties(_ref, ["size", "align", "margin", "bold", "color"]);

      return _react.default.createElement(_core.Typography, other);
    }).withConfig({
      displayName: "PressQuote__Typography",
      componentId: "zayfxc-1"
    })(["&&{color:", ";font-size:", ";text-align:", ";line-height:1.4;font-weight:", ";font-style:italic;margin:", ";padding:", ";a{text-decoration:underline;color:", ";font-weight:", ";&:hover{text-decoration:none;}}}"], function (props) {
      return props.color || '#fff';
    }, function (props) {
      return props.size;
    }, function (props) {
      return props.align || 'left';
    }, function (props) {
      return props.bold || 'normal';
    }, function (props) {
      return props.margin || '0';
    }, function (props) {
      return props.padding || '0';
    }, function (props) {
      return props.color || '#fff';
    }, function (props) {
      return props.bold || 'normal';
    })
  }
}));
var PressQuote = enhance(function (_ref2) {
  var description = _ref2.description,
      logo = _ref2.logo,
      link = _ref2.link,
      A = _ref2.A;
  return _react.default.createElement(_core.Grid, {
    container: true,
    direction: 'column',
    justify: "center",
    alignItems: "center",
    spacing: 40
  }, _react.default.createElement(_core.Grid, {
    item: true,
    xs: true
  }, _react.default.createElement(A.Image, null, _react.default.createElement("a", {
    href: link
  }, _react.default.createElement(_Image.default, logo)))), _react.default.createElement(_core.Grid, {
    item: true,
    xs: true
  }, _react.default.createElement(A.Typography, {
    color: "#000",
    bold: "700",
    align: "center",
    size: "1.125rem",
    variant: "subtitle1",
    gutterBottom: true,
    dangerouslySetInnerHTML: {
      __html: description
    }
  })));
});
var _default = PressQuote;
exports.default = _default;
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _compose = _interopRequireDefault(require("recompose/compose"));

var _defaultProps = _interopRequireDefault(require("recompose/defaultProps"));

var _withPropsOnChange = _interopRequireDefault(require("recompose/withPropsOnChange"));

var _Block = _interopRequireDefault(require("../../../../../evoke-me/layout/web/Block"));

var _BlockPressQuotes = _interopRequireDefault(require("../block/BlockPressQuotes"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _core = require("@material-ui/core");

var _polished = require("polished");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

var enhance = (0, _compose.default)((0, _defaultProps.default)({
  A: {
    Container: _styledComponents.default.div.withConfig({
      displayName: "SectionPressQuotes__Container",
      componentId: "scmhkg-0"
    })(["&&{text-align:center;"]),
    Title: (0, _styledComponents.default)(function (_ref) {
      var color = _ref.color,
          transform = _ref.transform,
          border = _ref.border,
          margin = _ref.margin,
          fweight = _ref.fweight,
          other = _objectWithoutProperties(_ref, ["color", "transform", "border", "margin", "fweight"]);

      return _react.default.createElement(_core.Typography, other);
    }).withConfig({
      displayName: "SectionPressQuotes__Title",
      componentId: "scmhkg-1"
    })(["&&{color:", ";border-bottom:", ";text-transform:", ";border-color:", ";margin:", ";font-weight:", ";min-width:auto;width:100%;text-align:center;font-size:", ";}"], function (props) {
      return props.color;
    }, function (props) {
      return props.border;
    }, function (props) {
      return props.transform || 'inherit';
    }, function (props) {
      return props.color;
    }, function (props) {
      return props.margin;
    }, function (props) {
      return props.fweight || '700';
    }, (0, _polished.rem)(35)),
    Wrap: _styledComponents.default.div.withConfig({
      displayName: "SectionPressQuotes__Wrap",
      componentId: "scmhkg-2"
    })(["&&{background:rgba(0,0,0,0.05);padding:", " 1.875rem;"], (0, _polished.rem)(69))
  }
}));
var SectionPressQuoteList = enhance(function (_ref2) {
  var A = _ref2.A;
  return _react.default.createElement(A.Container, null, _react.default.createElement(A.Title, {
    color: "black",
    transform: "uppercase",
    margin: "0 0 2.25rem 0"
  }, "Press Quotes"), _react.default.createElement(A.Wrap, null, _react.default.createElement(_core.Grid, {
    item: true,
    xs: 12,
    sm: 10,
    style: {
      'margin': 'auto'
    }
  }, _react.default.createElement(_core.Grid, {
    container: true,
    direction: 'row',
    justify: 'space-between',
    alignItems: 'flex-start',
    spacing: 40
  }, _react.default.createElement(_BlockPressQuotes.default, null)))));
});
var _default = SectionPressQuoteList;
exports.default = _default;
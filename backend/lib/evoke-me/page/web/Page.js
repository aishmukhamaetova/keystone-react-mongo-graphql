"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

var Content = _styledComponents.default.div.withConfig({
  displayName: "Page__Content",
  componentId: "srabpv-0"
})(["flex:1;"]);

var Page = function Page(_ref) {
  var children = _ref.children,
      hasFooter = _ref.hasFooter;

  var list = _react.default.Children.toArray(children);

  if (hasFooter) {
    return _react.default.createElement(_react.default.Fragment, null, _react.default.createElement(Content, null, list.slice(0, list.length - 1)), _react.default.createElement("div", null, list.slice(-1)));
  }

  return _react.default.createElement(_react.default.Fragment, null, _react.default.createElement(Content, null, children));
};

var _default = Page;
exports.default = _default;
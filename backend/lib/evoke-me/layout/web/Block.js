"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _propTypes = _interopRequireDefault(require("prop-types"));

var _react = _interopRequireWildcard(require("react"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var alignHContainer = function alignHContainer(props) {
  var left = props.left,
      center = props.center,
      right = props.right;
  return "text-align: ".concat(center ? 'center' : right ? 'right' : 'left', ";");
};

var alignVContainer = function alignVContainer(props) {
  var top = props.top,
      middle = props.middle,
      bottom = props.bottom;
  return (top || middle || bottom) && "\n    display: table;\n    height: 100%;\n    width: 100%;\n  ";
};

var alignVItem = function alignVItem(props) {
  var top = props.top,
      middle = props.middle,
      bottom = props.bottom;
  return (top || middle || bottom) && "\n    display: table-cell;\n    vertical-align: ".concat(top ? "top" : middle ? 'middle' : 'bottom', ";\n  ");
};

var maxWidth = function maxWidth(_ref) {
  var maxWidth = _ref.maxWidth;

  if (maxWidth) {
    return "width: 100%";
  }
};

var Vertical = _styledComponents.default.div.withConfig({
  displayName: "Block__Vertical",
  componentId: "yu0cbp-0"
})(["", ""], alignVContainer);

var Horizontal = _styledComponents.default.div.withConfig({
  displayName: "Block__Horizontal",
  componentId: "yu0cbp-1"
})(["", " ", ""], alignHContainer, alignVItem);

var Content = _styledComponents.default.div.withConfig({
  displayName: "Block__Content",
  componentId: "yu0cbp-2"
})(["", " display:inline-block;text-align:left;"], maxWidth);

var Block =
/*#__PURE__*/
function (_Component) {
  _inherits(Block, _Component);

  function Block() {
    _classCallCheck(this, Block);

    return _possibleConstructorReturn(this, _getPrototypeOf(Block).apply(this, arguments));
  }

  _createClass(Block, [{
    key: "render",
    value: function render() {
      var children = this.props.children;

      var _this$props = this.props,
          style = _this$props.style,
          rest = _objectWithoutProperties(_this$props, ["style"]);

      return _react.default.createElement(Vertical, this.props, _react.default.createElement(Horizontal, rest, _react.default.createElement(Content, rest, children)));
    }
  }]);

  return Block;
}(_react.Component);

_defineProperty(Block, "propTypes", {
  left: _propTypes.default.bool,
  center: _propTypes.default.bool,
  right: _propTypes.default.bool,
  top: _propTypes.default.bool,
  middle: _propTypes.default.bool,
  bottom: _propTypes.default.bool
});

_defineProperty(Block, "defaultProps", {
  left: false,
  center: false,
  right: false,
  top: false,
  middle: false,
  bottom: false
});

var _default = Block;
exports.default = _default;
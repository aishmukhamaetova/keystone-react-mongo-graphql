"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _default = {
  BOOLEAN: "BOOLEAN",
  INT: "INT",
  FLOAT: "FLOAT",
  RELATION: "RELATION",
  STRING: "STRING"
};
exports.default = _default;
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.spaceSaga = spaceSaga;
exports.createSpaceStore = exports.spaceReducer = exports.createSpace = exports.optionSet = exports.types = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _lodash = _interopRequireDefault(require("lodash.upperfirst"));

var _redux = require("redux");

var _reduxDevtoolsExtension = require("redux-devtools-extension");

var _reduxSaga = _interopRequireDefault(require("redux-saga"));

var _effects = require("redux-saga/effects");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _marked2 =
/*#__PURE__*/
_regenerator.default.mark(call),
    _marked3 =
/*#__PURE__*/
_regenerator.default.mark(spaceSaga),
    _marked4 =
/*#__PURE__*/
_regenerator.default.mark(rootSaga);

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var spaceMap = {};
var options = {
  HMR: false
};
var NAME = "evoke-me/space";
var VIEW_DEFAULT = "default";
var serviceUpdaterMap = {
  BEFORE_CALL: "__before",
  AFTER_CALL: "__after"
};
var types = {
  ASSIGN: "@@redux-space/ASSIGN",
  CALL: "@@redux-space/CALL"
};
exports.types = types;

var optionSet = function optionSet(value) {
  options = _objectSpread({}, options, value);
};

exports.optionSet = optionSet;
var afterMap = {};
var beforeMap = {};

var createSpace = function createSpace(_ref, store) {
  var _ref2 = _slicedToArray(_ref, 6),
      namespace = _ref2[0],
      entity = _ref2[1],
      dependencies = _ref2[2],
      stateInitial = _ref2[3],
      updaters = _ref2[4],
      mapPropsToView = _ref2[5];

  if (!spaceMap[namespace]) spaceMap[namespace] = {};

  if (!spaceMap[namespace][entity] || options.HMR) {
    var methodsFrom = dependencies.reduce(function (result, spaceConfig) {
      var _spaceConfig = _slicedToArray(spaceConfig, 2),
          namespace = _spaceConfig[0],
          entity = _spaceConfig[1];

      var _createSpace = createSpace(spaceConfig, store),
          methods = _createSpace.methods;

      var name = "from".concat((0, _lodash.default)(entity));
      result[name] = methods;
      return result;
    }, {});

    if (updaters[serviceUpdaterMap.AFTER_CALL]) {
      afterMap[namespace] = updaters[serviceUpdaterMap.AFTER_CALL];
    }

    if (updaters[serviceUpdaterMap.BEFORE_CALL]) {
      beforeMap[namespace] = updaters[serviceUpdaterMap.BEFORE_CALL];
    }

    var nameList = Object.keys(updaters).filter(function (name) {
      return !Object.values(serviceUpdaterMap).includes(name);
    });
    var methods = nameList.reduce(function (result, updaterName) {
      result[updaterName] =
      /*#__PURE__*/
      _regenerator.default.mark(function _callee() {
        var _marked,
            view,
            _len,
            updaterArgs,
            _key,
            setState,
            getState,
            _arr2,
            _i2,
            beforeMethod,
            method,
            _result,
            _arr3,
            _i3,
            afterMethod,
            _args2 = arguments;

        return _regenerator.default.wrap(function _callee$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                getState = function _ref4() {
                  var state, _state;

                  return _regenerator.default.wrap(function getState$(_context) {
                    while (1) {
                      switch (_context.prev = _context.next) {
                        case 0:
                          _context.prev = 0;
                          _context.next = 3;
                          return (0, _effects.select)();

                        case 3:
                          _context.t0 = namespace;
                          _context.t1 = entity;
                          _context.t2 = view;
                          state = _context.sent.space[_context.t0][_context.t1][_context.t2];

                          if (!state) {
                            _context.next = 9;
                            break;
                          }

                          return _context.abrupt("return", state);

                        case 9:
                          throw new Error();

                        case 12:
                          _context.prev = 12;
                          _context.t3 = _context["catch"](0);
                          _context.next = 16;
                          return setState({});

                        case 16:
                          _context.next = 18;
                          return (0, _effects.select)();

                        case 18:
                          _state = _context.sent;
                          return _context.abrupt("return", _state.space[namespace][entity][view]);

                        case 20:
                        case "end":
                          return _context.stop();
                      }
                    }
                  }, _marked, this, [[0, 12]]);
                };

                setState = function _ref3(value) {
                  return (0, _effects.put)({
                    type: [types.ASSIGN, namespace, entity, view],
                    payload: {
                      value: value
                    }
                  });
                };

                _marked =
                /*#__PURE__*/
                _regenerator.default.mark(getState);
                view = VIEW_DEFAULT;

                for (_len = _args2.length, updaterArgs = new Array(_len), _key = 0; _key < _len; _key++) {
                  updaterArgs[_key] = _args2[_key];
                }

                try {
                  view = mapPropsToView ? mapPropsToView(updaterArgs) : VIEW_DEFAULT;
                } catch (err) {
                  console.warn(NAME, namespace, entity, "mapPropsToView", "error", err);
                }

                _context2.prev = 6;
                _arr2 = Object.values(beforeMap);
                _i2 = 0;

              case 9:
                if (!(_i2 < _arr2.length)) {
                  _context2.next = 16;
                  break;
                }

                beforeMethod = _arr2[_i2];
                _context2.next = 13;
                return beforeMethod(namespace, entity, view, updaterName);

              case 13:
                _i2++;
                _context2.next = 9;
                break;

              case 16:
                _context2.next = 18;
                return updaters[updaterName](getState, setState, methods, methodsFrom);

              case 18:
                method = _context2.sent;
                _context2.next = 21;
                return method.apply(void 0, updaterArgs);

              case 21:
                _result = _context2.sent;
                _arr3 = Object.values(afterMap);
                _i3 = 0;

              case 24:
                if (!(_i3 < _arr3.length)) {
                  _context2.next = 31;
                  break;
                }

                afterMethod = _arr3[_i3];
                _context2.next = 28;
                return afterMethod(namespace, entity, view, updaterName);

              case 28:
                _i3++;
                _context2.next = 24;
                break;

              case 31:
                return _context2.abrupt("return", _result);

              case 34:
                _context2.prev = 34;
                _context2.t0 = _context2["catch"](6);
                console.warn(NAME, namespace, entity, view, updaterName, "error", _context2.t0);

              case 37:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee, this, [[6, 34]]);
      });
      return result;
    }, {});
    var actions = nameList.reduce(function (result, updaterName) {
      result[updaterName] = function () {
        for (var _len2 = arguments.length, updaterArgs = new Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
          updaterArgs[_key2] = arguments[_key2];
        }

        return {
          type: types.CALL,
          payload: {
            updaterName: updaterName,
            updaterArgs: updaterArgs,
            meta: {
              namespace: namespace,
              entity: entity
            }
          }
        };
      };

      return result;
    }, {});
    var handlers = nameList.reduce(function (result, updaterName) {
      result[updaterName] = function () {
        return store.dispatch(actions[updaterName].apply(actions, arguments));
      };

      return result;
    }, {
      initState: function initState() {
        var view = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : "default";
        store.dispatch({
          type: [types.ASSIGN, namespace, entity, view],
          payload: {
            value: {}
          }
        });
      }
    });
    return spaceMap[namespace][entity] = {
      actions: actions,
      entity: entity,
      handlers: handlers,
      mapPropsToView: mapPropsToView,
      methods: methods,
      methodsFrom: methodsFrom,
      namespace: namespace,
      stateInitial: stateInitial,
      updaters: updaters //updatersFrom,

    };
  }

  return spaceMap[namespace][entity];
};

exports.createSpace = createSpace;

var merge = function merge() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
  var type = arguments.length > 1 ? arguments[1] : undefined;
  var payload = arguments.length > 2 ? arguments[2] : undefined;
  var value = payload.value;

  var _type = _slicedToArray(type, 4),
      namespace = _type[1],
      entity = _type[2],
      view = _type[3];

  var mergeView = function mergeView() {
    var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
    return _objectSpread({}, state, _defineProperty({}, view, _objectSpread({}, state[view] || spaceMap[namespace][entity].stateInitial(), value)));
  };

  var mergeNamespace = function mergeNamespace() {
    var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
    return _objectSpread({}, state, _defineProperty({}, entity, mergeView(state[entity])));
  };

  return _objectSpread({}, state, _defineProperty({}, namespace, mergeNamespace(state[namespace])));
};

var spaceReducer = function spaceReducer() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

  var _ref5 = arguments.length > 1 ? arguments[1] : undefined,
      type = _ref5.type,
      payload = _ref5.payload;

  if (Array.isArray(type) && type[0] === types.ASSIGN) {
    return merge(state, type, payload);
  }

  return state;
};

exports.spaceReducer = spaceReducer;

function call(_ref6) {
  var payload, updaterName, updaterArgs, meta, namespace, entity, spaceMapEntity, methods, method;
  return _regenerator.default.wrap(function call$(_context3) {
    while (1) {
      switch (_context3.prev = _context3.next) {
        case 0:
          payload = _ref6.payload;
          _context3.prev = 1;
          updaterName = payload.updaterName, updaterArgs = payload.updaterArgs, meta = payload.meta;
          namespace = meta.namespace, entity = meta.entity;
          spaceMapEntity = spaceMap[namespace][entity];
          methods = spaceMap[namespace][entity].methods;
          method = methods[updaterName];
          _context3.next = 9;
          return method.apply(void 0, _toConsumableArray(updaterArgs));

        case 9:
          return _context3.abrupt("return", _context3.sent);

        case 12:
          _context3.prev = 12;
          _context3.t0 = _context3["catch"](1);
          console.warn(NAME, "* call", "error", _context3.t0);

        case 15:
        case "end":
          return _context3.stop();
      }
    }
  }, _marked2, this, [[1, 12]]);
}

function spaceSaga() {
  return _regenerator.default.wrap(function spaceSaga$(_context4) {
    while (1) {
      switch (_context4.prev = _context4.next) {
        case 0:
          _context4.next = 2;
          return (0, _effects.takeEvery)(types.CALL, call);

        case 2:
        case "end":
          return _context4.stop();
      }
    }
  }, _marked3, this);
}

function rootSaga() {
  return _regenerator.default.wrap(function rootSaga$(_context5) {
    while (1) {
      switch (_context5.prev = _context5.next) {
        case 0:
          _context5.next = 2;
          return (0, _effects.all)([spaceSaga()]);

        case 2:
        case "end":
          return _context5.stop();
      }
    }
  }, _marked4, this);
}

var createSpaceStore = function createSpaceStore() {
  var sagaMiddleware = (0, _reduxSaga.default)();
  var store = (0, _redux.createStore)((0, _redux.combineReducers)({
    space: spaceReducer
  }), (0, _reduxDevtoolsExtension.composeWithDevTools)((0, _redux.applyMiddleware)(sagaMiddleware)));
  sagaMiddleware.run(rootSaga);
  return store;
};

exports.createSpaceStore = createSpaceStore;
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _redux = require("redux");

var _reduxDevtoolsExtension = require("redux-devtools-extension");

var _reduxSaga = _interopRequireDefault(require("redux-saga"));

var _effects = require("redux-saga/effects");

var _manager = require("./manager");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _marked =
/*#__PURE__*/
_regenerator.default.mark(rootSaga);

function rootSaga() {
  return _regenerator.default.wrap(function rootSaga$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          _context.next = 2;
          return (0, _effects.all)([(0, _manager.spaceSaga)()]);

        case 2:
        case "end":
          return _context.stop();
      }
    }
  }, _marked, this);
}

var _default = function _default() {
  var sagaMiddleware = (0, _reduxSaga.default)();
  var store = (0, _redux.createStore)((0, _redux.combineReducers)({
    space: _manager.spaceReducer
  }), (0, _reduxDevtoolsExtension.composeWithDevTools)((0, _redux.applyMiddleware)(sagaMiddleware)));
  sagaMiddleware.run(rootSaga);
  return store;
};

exports.default = _default;
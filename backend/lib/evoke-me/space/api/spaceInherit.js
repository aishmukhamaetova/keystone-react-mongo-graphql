"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _default = function _default(spaceConfig, _ref) {
  var dependencies = _ref.dependencies,
      entity = _ref.entity,
      mapPropsToView = _ref.mapPropsToView,
      namespace = _ref.namespace,
      stateInit = _ref.stateInit,
      updaters = _ref.updaters;
  return [namespace || spaceConfig[0], entity || spaceConfig[1], dependencies || spaceConfig[2], stateInit || spaceConfig[3], updaters || spaceConfig[4], mapPropsToView || spaceConfig[5]];
};

exports.default = _default;
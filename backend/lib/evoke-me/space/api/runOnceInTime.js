"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _default = function _default(run, timeout) {
  var time;
  return function () {
    var next = Date.now();

    if (!time || next - time >= timeout) {
      time = next;
      return run.apply(void 0, arguments);
    }
  };
};

exports.default = _default;
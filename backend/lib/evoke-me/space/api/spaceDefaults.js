"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _v = _interopRequireDefault(require("uuid/v4"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

function _toPropertyKey(arg) { var key = _toPrimitive(arg, "string"); return _typeof(key) === "symbol" ? key : String(key); }

function _toPrimitive(input, hint) { if (_typeof(input) !== "object" || input === null) return input; var prim = input[Symbol.toPrimitive]; if (prim !== undefined) { var res = prim.call(input, hint || "default"); if (_typeof(res) !== "object") return res; throw new TypeError("@@toPrimitive must return a primitive value."); } return (hint === "string" ? String : Number)(input); }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

var _default = {
  state: {
    data: {},
    orderMap: {
      default: []
    },
    persist: false,
    persistName: "default"
  },
  updaters: {
    restore: function restore(getState, setState, methods, _ref) {
      var fromPersist = _ref.fromPersist;
      return (
        /*#__PURE__*/
        _regenerator.default.mark(function _callee() {
          var _ref2, persist, persistName, statePersist, data, orderMap;

          return _regenerator.default.wrap(function _callee$(_context) {
            while (1) {
              switch (_context.prev = _context.next) {
                case 0:
                  _context.next = 2;
                  return getState();

                case 2:
                  _ref2 = _context.sent;
                  persist = _ref2.persist;
                  persistName = _ref2.persistName;

                  if (!persist) {
                    _context.next = 13;
                    break;
                  }

                  _context.next = 8;
                  return fromPersist.get(persistName);

                case 8:
                  statePersist = _context.sent;

                  if (!statePersist) {
                    _context.next = 13;
                    break;
                  }

                  data = statePersist.data, orderMap = statePersist.orderMap;
                  _context.next = 13;
                  return setState({
                    data: data,
                    orderMap: orderMap
                  });

                case 13:
                case "end":
                  return _context.stop();
              }
            }
          }, _callee, this);
        })
      );
    },
    create: function create(getState, setState, methods, _ref3) {
      var fromPersist = _ref3.fromPersist;
      return (
        /*#__PURE__*/
        _regenerator.default.mark(function _callee2(doc) {
          var options,
              orderName,
              _ref4,
              data,
              orderMap,
              persist,
              persistName,
              id,
              orderMapNext,
              _ref5,
              _data,
              _orderMap,
              _args2 = arguments;

          return _regenerator.default.wrap(function _callee2$(_context2) {
            while (1) {
              switch (_context2.prev = _context2.next) {
                case 0:
                  options = _args2.length > 1 && _args2[1] !== undefined ? _args2[1] : {};
                  orderName = options.orderName;
                  _context2.next = 4;
                  return getState();

                case 4:
                  _ref4 = _context2.sent;
                  data = _ref4.data;
                  orderMap = _ref4.orderMap;
                  persist = _ref4.persist;
                  persistName = _ref4.persistName;
                  id = doc.id || (0, _v.default)();
                  orderMapNext = data[id] ? {
                    default: orderMap.default
                  } : {
                    default: _toConsumableArray(orderMap.default).concat([id])
                  };

                  if (orderName) {
                    orderMapNext[orderName] = _toConsumableArray(orderMap[orderName] || []).concat([id]);
                  }

                  _context2.next = 14;
                  return setState({
                    data: _objectSpread({}, data, _defineProperty({}, id, doc)),
                    orderMap: _objectSpread({}, orderMap, orderMapNext)
                  });

                case 14:
                  if (!persist) {
                    _context2.next = 22;
                    break;
                  }

                  _context2.next = 17;
                  return getState();

                case 17:
                  _ref5 = _context2.sent;
                  _data = _ref5.data;
                  _orderMap = _ref5.orderMap;
                  _context2.next = 22;
                  return fromPersist.set(persistName, {
                    data: _data,
                    orderMap: _orderMap
                  });

                case 22:
                case "end":
                  return _context2.stop();
              }
            }
          }, _callee2, this);
        })
      );
    },
    update: function update(getState, setState, methods, _ref6) {
      var fromPersist = _ref6.fromPersist;
      return (
        /*#__PURE__*/
        _regenerator.default.mark(function _callee3(doc) {
          var _ref7, data, persist, persistName, _ref8, _data2, orderMap;

          return _regenerator.default.wrap(function _callee3$(_context3) {
            while (1) {
              switch (_context3.prev = _context3.next) {
                case 0:
                  _context3.next = 2;
                  return getState();

                case 2:
                  _ref7 = _context3.sent;
                  data = _ref7.data;
                  persist = _ref7.persist;
                  persistName = _ref7.persistName;

                  if (data[doc.id]) {
                    _context3.next = 8;
                    break;
                  }

                  return _context3.abrupt("return", null);

                case 8:
                  _context3.next = 10;
                  return setState({
                    data: _objectSpread({}, data, _defineProperty({}, doc.id, _objectSpread({}, data[doc.id], doc)))
                  });

                case 10:
                  if (!persist) {
                    _context3.next = 18;
                    break;
                  }

                  _context3.next = 13;
                  return getState();

                case 13:
                  _ref8 = _context3.sent;
                  _data2 = _ref8.data;
                  orderMap = _ref8.orderMap;
                  _context3.next = 18;
                  return fromPersist.set(persistName, {
                    data: _data2,
                    orderMap: orderMap
                  });

                case 18:
                case "end":
                  return _context3.stop();
              }
            }
          }, _callee3, this);
        })
      );
    },
    removeAll: function removeAll(getState, setState, methods, _ref9) {
      var fromPersist = _ref9.fromPersist;
      return (
        /*#__PURE__*/
        _regenerator.default.mark(function _callee4() {
          var _ref10, persist, persistName, data, orderMap;

          return _regenerator.default.wrap(function _callee4$(_context4) {
            while (1) {
              switch (_context4.prev = _context4.next) {
                case 0:
                  _context4.next = 2;
                  return getState();

                case 2:
                  _ref10 = _context4.sent;
                  persist = _ref10.persist;
                  persistName = _ref10.persistName;
                  data = {};
                  orderMap = {
                    default: []
                  };
                  _context4.next = 9;
                  return setState({
                    data: data,
                    orderMap: orderMap
                  });

                case 9:
                  if (!persist) {
                    _context4.next = 12;
                    break;
                  }

                  _context4.next = 12;
                  return fromPersist.remove(persistName);

                case 12:
                case "end":
                  return _context4.stop();
              }
            }
          }, _callee4, this);
        })
      );
    },
    remove: function remove(getState, setState, methods, _ref11) {
      var fromPersist = _ref11.fromPersist;
      return (
        /*#__PURE__*/
        _regenerator.default.mark(function _callee5(_ref12) {
          var id,
              options,
              _options$orderName,
              orderName,
              _ref13,
              prevData,
              orderMap,
              persist,
              persistName,
              docRemoved,
              data,
              _ref14,
              _data3,
              _orderMap2,
              _args5 = arguments;

          return _regenerator.default.wrap(function _callee5$(_context5) {
            while (1) {
              switch (_context5.prev = _context5.next) {
                case 0:
                  id = _ref12.id;
                  options = _args5.length > 1 && _args5[1] !== undefined ? _args5[1] : {};
                  _options$orderName = options.orderName, orderName = _options$orderName === void 0 ? "default" : _options$orderName;
                  _context5.next = 5;
                  return getState();

                case 5:
                  _ref13 = _context5.sent;
                  prevData = _ref13.data;
                  orderMap = _ref13.orderMap;
                  persist = _ref13.persist;
                  persistName = _ref13.persistName;
                  docRemoved = prevData[id], data = _objectWithoutProperties(prevData, [id].map(_toPropertyKey));
                  _context5.next = 13;
                  return setState({
                    data: data,
                    orderMap: _objectSpread({}, orderMap, _defineProperty({}, orderName, orderMap[orderName].filter(function (_id) {
                      return _id !== id;
                    })))
                  });

                case 13:
                  if (!persist) {
                    _context5.next = 21;
                    break;
                  }

                  _context5.next = 16;
                  return getState();

                case 16:
                  _ref14 = _context5.sent;
                  _data3 = _ref14.data;
                  _orderMap2 = _ref14.orderMap;
                  _context5.next = 21;
                  return fromPersist.set(persistName, {
                    data: _data3,
                    orderMap: _orderMap2
                  });

                case 21:
                case "end":
                  return _context5.stop();
              }
            }
          }, _callee5, this);
        })
      );
    },
    orderClear: function orderClear(getState, setState) {
      return (
        /*#__PURE__*/
        _regenerator.default.mark(function _callee6(name) {
          var _ref15, orderMapPrev, orderSelected, orderMap;

          return _regenerator.default.wrap(function _callee6$(_context6) {
            while (1) {
              switch (_context6.prev = _context6.next) {
                case 0:
                  _context6.next = 2;
                  return getState();

                case 2:
                  _ref15 = _context6.sent;
                  orderMapPrev = _ref15.orderMap;
                  orderSelected = orderMapPrev[name], orderMap = _objectWithoutProperties(orderMapPrev, [name].map(_toPropertyKey));
                  _context6.next = 7;
                  return setState({
                    orderMap: orderMap
                  });

                case 7:
                case "end":
                  return _context6.stop();
              }
            }
          }, _callee6, this);
        })
      );
    },
    orderMove: function orderMove(getState, setState) {
      return (
        /*#__PURE__*/
        _regenerator.default.mark(function _callee7(id, from, to) {
          var _objectSpread5;

          var _ref16, orderMap, fromOrder, toOrder;

          return _regenerator.default.wrap(function _callee7$(_context7) {
            while (1) {
              switch (_context7.prev = _context7.next) {
                case 0:
                  _context7.next = 2;
                  return getState();

                case 2:
                  _ref16 = _context7.sent;
                  orderMap = _ref16.orderMap;
                  fromOrder = orderMap[from].filter(function (_id) {
                    return _id != id;
                  });
                  toOrder = _toConsumableArray(orderMap[to] || []).concat([id]);
                  _context7.next = 8;
                  return setState({
                    orderMap: _objectSpread({}, orderMap, (_objectSpread5 = {}, _defineProperty(_objectSpread5, from, fromOrder), _defineProperty(_objectSpread5, to, toOrder), _objectSpread5))
                  });

                case 8:
                case "end":
                  return _context7.stop();
              }
            }
          }, _callee7, this);
        })
      );
    }
  }
};
exports.default = _default;
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _compose = _interopRequireDefault(require("recompose/compose"));

var _lifecycle = _interopRequireDefault(require("recompose/lifecycle"));

var _withHandlers = _interopRequireDefault(require("recompose/withHandlers"));

var _withStateHandlers = _interopRequireDefault(require("recompose/withStateHandlers"));

var _reactRedux = require("react-redux");

var _createStore = _interopRequireDefault(require("../api/createStore"));

var _Context = _interopRequireDefault(require("./Context"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

var enhance = (0, _compose.default)((0, _withStateHandlers.default)(function () {
  return {
    store: null
  };
}, {
  storeSet: function storeSet() {
    return function (store) {
      return {
        store: store
      };
    };
  }
}), (0, _withHandlers.default)(function () {
  return {
    mount: function mount(_ref) {
      var storeSet = _ref.storeSet;
      return function () {
        storeSet((0, _createStore.default)());
      };
    }
  };
}), (0, _lifecycle.default)({
  componentDidMount: function componentDidMount() {
    this.props.mount();
  }
}));
var ProviderSpace = enhance(function (_ref2) {
  var children = _ref2.children,
      store = _ref2.store;
  return store ? _react.default.createElement(_Context.default.Provider, {
    value: {
      store: store
    }
  }, _react.default.createElement(_reactRedux.Provider, {
    store: store
  }, _react.default.createElement(_react.default.Fragment, null, children))) : null;
});
var _default = ProviderSpace;
exports.default = _default;
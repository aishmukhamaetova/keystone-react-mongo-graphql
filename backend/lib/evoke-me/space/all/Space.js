"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _lodash = _interopRequireDefault(require("lodash.upperfirst"));

var _react = _interopRequireWildcard(require("react"));

var _reactRedux = require("react-redux");

var _compose = _interopRequireDefault(require("recompose/compose"));

var _defaultProps = _interopRequireDefault(require("recompose/defaultProps"));

var _lifecycle = _interopRequireDefault(require("recompose/lifecycle"));

var _withHandlers = _interopRequireDefault(require("recompose/withHandlers"));

var _withStateHandlers = _interopRequireDefault(require("recompose/withStateHandlers"));

var _manager = require("../api/manager");

var _Context = _interopRequireDefault(require("./Context"));

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

var withSpaceContext = function withSpaceContext(WrappedComponent) {
  return function (props) {
    return _react.default.createElement(_Context.default.Consumer, null, function (context) {
      return _react.default.createElement(WrappedComponent, _extends({}, props, context));
    });
  };
};

var enhanceSpace = (0, _compose.default)((0, _defaultProps.default)({
  view: "default"
}), (0, _reactRedux.connect)(function (state, _ref) {
  var spaceConfig = _ref.spaceConfig,
      view = _ref.view;

  var _spaceConfig = _slicedToArray(spaceConfig, 2),
      namespace = _spaceConfig[0],
      entity = _spaceConfig[1];

  try {
    return {
      viewState: state.space[namespace][entity][view]
    };
  } catch (err) {
    return {
      viewState: null
    };
  }
}), withSpaceContext, (0, _withStateHandlers.default)(function () {
  return {
    handlers: null
  };
}, {
  handlersSet: function handlersSet() {
    return function (handlers) {
      return {
        handlers: handlers
      };
    };
  }
}), (0, _withHandlers.default)(function () {
  return {
    mount: function mount(_ref2) {
      var handlersSet = _ref2.handlersSet,
          store = _ref2.store,
          spaceConfig = _ref2.spaceConfig,
          viewState = _ref2.viewState,
          view = _ref2.view;
      return function () {
        var _createSpace = (0, _manager.createSpace)(spaceConfig, store),
            handlers = _createSpace.handlers;

        handlersSet(handlers);
      };
    }
  };
}), (0, _lifecycle.default)({
  componentDidMount: function componentDidMount() {
    this.props.mount();
  },
  componentDidUpdate: function componentDidUpdate() {
    var _this$props = this.props,
        handlers = _this$props.handlers,
        view = _this$props.view,
        viewState = _this$props.viewState;

    if (handlers && !viewState) {
      handlers.initState(view);
    }
  }
}));
var Space = enhanceSpace(function (_ref3) {
  var children = _ref3.children,
      handlers = _ref3.handlers,
      viewState = _ref3.viewState,
      spaceConfig = _ref3.spaceConfig;

  var _spaceConfig2 = _slicedToArray(spaceConfig, 2),
      entity = _spaceConfig2[1];

  var name = "from".concat((0, _lodash.default)(entity));
  return handlers && viewState ? children(_defineProperty({}, name, _objectSpread({}, viewState, handlers))) : null;
});
var _default = Space;
exports.default = _default;
const keystone = require('keystone')
const Types = keystone.Field.Types


const Testimonial = new keystone.List('Testimonial')

Testimonial.add({
  text: { type: Types.Textarea, },
  rating: { type: Types.Number, },

  author: { type: Types.Relationship, ref: 'User', index: true, },
  product: { type: Types.Relationship, ref: 'Product', },
  post: { type: Types.Relationship, ref: 'Post', },
})

Testimonial.defaultColumns = 'author, product, post'
Testimonial.register()

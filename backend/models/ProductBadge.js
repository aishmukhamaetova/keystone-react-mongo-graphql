var keystone = require('keystone')
var Types = keystone.Field.Types


var ProductBadge = new keystone.List('ProductBadge')

ProductBadge.add({
  name: { type: String, required: true },
})

ProductBadge.relationship({ ref: 'Product', path: 'products', refPath: 'badge' })

ProductBadge.defaultColumns = 'name'
ProductBadge.register()

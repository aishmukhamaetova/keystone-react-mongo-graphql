var keystone = require('keystone')
var Types = keystone.Field.Types


var ProductStore = new keystone.List('ProductStore')

ProductStore.add({
  product: { type: Types.Relationship, ref: 'Product' },

  quantity: { type: Types.Number, },
  price: { type: Types.Money, },

  location: { type: Types.Relationship, ref: 'Location', },
})

ProductStore.defaultColumns = 'location, product, quantity, price'
ProductStore.register()

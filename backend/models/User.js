const keystone = require('keystone')


const Types = keystone.Field.Types
const User = new keystone.List('User')

User.add({
  email: { type: Types.Email, unique: true, },
  emailCode: { type: String, },

  password: { type: Types.Password, initial: true, },

  name: { type: Types.Name, required: true, index: true, },

  isConfirmed: { type: Types.Boolean, },
}, 'Permissions', {
  isAdmin: { type: Boolean, label: 'Can access Keystone', index: true },
})

User.schema.virtual('canAccessKeystone').get(function () {
  return this.isAdmin
})

User.relationship({ ref: 'Post', path: 'posts', refPath: 'author' })

User.defaultColumns = 'name, email, isAdmin'
User.register()

var keystone = require('keystone')
var Types = keystone.Field.Types


var Content = new keystone.List('Content')

Content.add({
	name: { type: String, },

	language: { type: Types.Relationship, ref: 'Language', },

  text: { type: String, },
  content: { type: Types.Html, wysiwyg: true, height: 150 },
})

Content.defaultColumns = 'name, language, text, content'
Content.register()

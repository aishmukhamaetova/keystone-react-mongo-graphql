var keystone = require('keystone')
var Types = keystone.Field.Types


var ProductHarvest = new keystone.List('ProductHarvest')

ProductHarvest.add({
  name: { type: String, required: true },
})

ProductHarvest.relationship({ ref: 'Product', path: 'products', refPath: 'harvest' })

ProductHarvest.defaultColumns = 'name'
ProductHarvest.register()

var keystone = require('keystone')
var Types = keystone.Field.Types


var LocationCurrency = new keystone.List('LocationCurrency')

LocationCurrency.add({
  name: { type: String, required: true },
})

LocationCurrency.relationship({ ref: 'Location', path: 'locations', refPath: 'currency' });

LocationCurrency.defaultColumns = 'name'
LocationCurrency.register()

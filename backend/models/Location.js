var keystone = require('keystone')
var Types = keystone.Field.Types


var Location = new keystone.List('Location')

Location.add({
  name: { type: String, required: true },
  currency: { type: Types.Relationship, ref: 'LocationCurrency', index: true },
})

Location.relationship({ ref: 'ProductStore', path: 'productsStore', refPath: 'location' })

Location.defaultColumns = 'name'
Location.register()

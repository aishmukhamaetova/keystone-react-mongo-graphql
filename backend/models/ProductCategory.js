var keystone = require('keystone')
var Types = keystone.Field.Types


var ProductType = new keystone.List('ProductCategory')

ProductType.add({
  name: { type: String, required: true },
})

ProductType.relationship({ ref: 'Product', path: 'products', refPath: 'types' })

ProductType.defaultColumns = 'name'
ProductType.register()

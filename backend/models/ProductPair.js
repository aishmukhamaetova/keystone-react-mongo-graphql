var keystone = require('keystone')
var Types = keystone.Field.Types


var ProductPair = new keystone.List('ProductPair')

ProductPair.add({
  name: { type: String, required: true },
})

ProductPair.relationship({ ref: 'Product', path: 'products', refPath: 'pairs' })

ProductPair.defaultColumns = 'name'
ProductPair.register()

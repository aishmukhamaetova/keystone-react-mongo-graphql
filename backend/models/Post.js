var keystone = require('keystone')
var Types = keystone.Field.Types


var Post = new keystone.List('Post', {
	map: { name: 'title' },
	autokey: { path: 'slug', from: 'title', unique: true },
})

Post.add({
  title: { type: String, required: true},

  state: { type: Types.Select, options: 'draft, published, archived', default: 'draft', index: true, },

  author: { type: Types.Relationship, ref: 'User', index: true, },
  publishedDate: { type: Types.Date, index: true, dependsOn: {state: 'published'}, },

  content: {
    brief: { type: Types.Html, wysiwyg: true, height: 150, },
    extended: { type: Types.Html, wysiwyg: true, height: 400, },
  },

  categories: { type: Types.Relationship, ref: 'PostCategory', many: true, },

  images: { type: Types.Relationship, ref: 'Image', many: true, },
  videos: { type: Types.Relationship, ref: 'Video', many: true, },

  isPinnedToIndex: { type: Types.Boolean, },

  index: { type: Types.Number, },

  preview: { type: Types.Url, noedit: true },
})

Post.relationship({ ref: 'Testimonial', path: 'testimonials', refPath: 'post' })

Post.schema.virtual('previewURI').get(function(payload) {
  return `${ process.env.CLIENT_HOST }/post/` + this._id
})

Post.schema.post('init', function() {
  this.preview = this.previewURI
})

Post.schema.pre('save', async function (next) {
  const Image = keystone.list('Image')

  const doc = await Post.model.findById(this.id)

  if (!doc) return next()

  const imageIdListSource = doc.images.map((value) => value.toString())
  const imageIdListDestination = this.images.map((value) => value.toString())

  const imageIdListAdded = imageIdListDestination.filter(
    (id) => !imageIdListSource.includes(id)
  )
  const imageIdListRemoved = imageIdListSource.filter(
    (id) => !imageIdListDestination.includes(id)
  )

  for (let imageId of imageIdListAdded) {
    const imageDoc = await Image.model.findById(imageId)
    imageDoc.count += 1
    imageDoc.save()
  }

  for (let imageId of imageIdListRemoved) {
    const imageDoc = await Image.model.findById(imageId)
    imageDoc.count -= 1
    imageDoc.save()
  }

  return next()
})

Post.schema.virtual('content.full').get(function () {
	return this.content.extended || this.content.brief
})

Post.defaultColumns = 'title, state|20%, author|20%, publishedDate|20%'
Post.register()

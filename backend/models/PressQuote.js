const keystone = require('keystone')
const Types = keystone.Field.Types


const PressQuote = new keystone.List('PressQuote')

PressQuote.add({
  name: { type: String, },
  logo: { type: Types.Relationship, ref: 'Image', },
  description: { type: Types.Html, wysiwyg: true, height: 150, },
  link:  { type: Types.Url, },
})

PressQuote.defaultColumns = 'name, link'
PressQuote.register()

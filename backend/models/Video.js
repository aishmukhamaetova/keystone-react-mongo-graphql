var keystone = require('keystone')
var Types = keystone.Field.Types


var Video = new keystone.List('Video')

Video.add({
  name: { type: String, required: true },
  videoLink: { type: String },

  tags: { type: Types.Relationship, ref: 'MediaTag', many: true, },

  count: { type: Types.Number, default: 0, },
})

Video.defaultColumns = 'name, videoLink'
Video.register()

var keystone = require('keystone')
var Types = keystone.Field.Types

var PromotionBlock = new keystone.List('PromotionBlock')

PromotionBlock.add({
  name: { type: String, required: true },
  content: { type: Types.Html, wysiwyg: true, height: 150 },

  promotion: { type: Types.Relationship, ref: 'Promotion', },

  product: { type: Types.Relationship, ref: 'Product', },
})

PromotionBlock.defaultColumns = 'name, promotion'
PromotionBlock.register()

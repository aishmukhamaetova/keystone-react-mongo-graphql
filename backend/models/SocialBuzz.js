const keystone = require('keystone')
const Types = keystone.Field.Types


const SocialBuzz = new keystone.List('SocialBuzz')

SocialBuzz.add({
  name: { type: String, },
  linkInstagram: { type: Types.Url, },
})

SocialBuzz.defaultColumns = 'name, linkInstagram'
SocialBuzz.register()

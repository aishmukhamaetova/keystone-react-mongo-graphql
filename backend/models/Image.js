var keystone = require('keystone')
var Types = keystone.Field.Types


var Image = new keystone.List('Image')

Image.add({
  name: { type: String, required: true },
  image: { type: Types.CloudinaryImage },

  tags: { type: Types.Relationship, ref: 'MediaTag', many: true, },

  count: { type: Types.Number, default: 0, noedit: true, },
})

Image.schema.pre('remove', async function (next) {
  const doc = await Image.model.findById(this.id)

  if (doc.count > 0) {
    return
  }

  next()
})

Image.defaultColumns = 'name'
Image.register()

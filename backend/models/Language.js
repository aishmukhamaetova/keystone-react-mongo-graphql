const keystone = require('keystone')
const Types = keystone.Field.Types


const Language = new keystone.List('Language')

Language.add({
  name: { type: String, },
})

Language.defaultColumns = 'name'
Language.register()

var keystone = require('keystone')
var Types = keystone.Field.Types


var ProductRegion = new keystone.List('ProductRegion')

ProductRegion.add({
  name: { type: String, required: true },

  map: { type: Types.GeoPoint, enableMapsAPI: true, },

  preview: { type: Types.Url, noedit: true },
})

ProductRegion.relationship({ ref: 'Product', path: 'products', refPath: 'regions' })

ProductRegion.schema.virtual('previewURI').get(function(payload) {
  return `${ process.env.CLIENT_HOST }/regionMap/` + this._id
})

ProductRegion.schema.post('init', function() {
  this.preview = this.previewURI
})

ProductRegion.defaultColumns = 'name'
ProductRegion.register()




var keystone = require('keystone')


var Types = keystone.Field.Types
var Product = new keystone.List('Product')

Product.add({
  sku: { type: String, index: { unique: true }, required: true, initial: true, },
  name: { type: String, required: true, initial: true, },
  description: { type: Types.Textarea, },

  sweetness: { type: Types.Number, },

  badge: { type: Types.Relationship, ref: 'ProductBadge', many: true, },
  harvest: { type: Types.Relationship, ref: 'ProductHarvest', many: true },
  pairs: { type: Types.Relationship, ref: 'ProductPair', many: true, },
  regions: { type: Types.Relationship, ref: `ProductRegion`, many: true, label: `Made in Region`, },
  categories: { type: Types.Relationship, ref: 'ProductCategory', many: true, },

  images: { type: Types.Relationship, ref: 'Image', many: true },
  videos: { type: Types.Relationship, ref: 'Video', many: true },

  weight: { type: Types.Number, },

  isUniqueProposition: { type: Types.Boolean, },

  preview: { type: Types.Url, noedit: true },
})

Product.relationship({ ref: 'ProductStore', path: 'stores', refPath: 'product' })
Product.relationship({ ref: 'Testimonial', path: 'testimonials', refPath: 'product' })

Product.schema.virtual('previewURI').get(function(payload) {
  return `${ process.env.CLIENT_HOST }/product/` + this._id
})

Product.schema.post('init', function() {
  this.preview = this.previewURI
})

Product.schema.pre('save', async function (next) {
  const Image = keystone.list('Image')

  const doc = await Product.model.findById(this.id)

  if (!doc) return next()

  const imageIdListSource = doc.images.map((value) => value.toString())
  const imageIdListDestination = this.images.map((value) => value.toString())

  const imageIdListAdded = imageIdListDestination.filter(
    (id) => !imageIdListSource.includes(id)
  )
  const imageIdListRemoved = imageIdListSource.filter(
    (id) => !imageIdListDestination.includes(id)
  )

  for (let imageId of imageIdListAdded) {
    const imageDoc = await Image.model.findById(imageId)
    imageDoc.count += 1
    imageDoc.save()
  }

  for (let imageId of imageIdListRemoved) {
    const imageDoc = await Image.model.findById(imageId)
    imageDoc.count -= 1
    imageDoc.save()
  }

  return next()
})

Product.defaultColumns = 'sku, name, weight, badge, sweetness, harvest'
Product.register()

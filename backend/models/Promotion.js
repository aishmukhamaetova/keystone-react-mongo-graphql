var keystone = require('keystone')


var Types = keystone.Field.Types
var Promotion = new keystone.List('Promotion')

Promotion.add({
  name: { type: String, },

  timeBegin: { type: Types.Datetime, },
  timeEnd: { type: Types.Datetime, },

  type: { type: Types.Select, options: 'bar, slider, bannerArea, pressQuotes', },

  location: { type: Types.Relationship, ref: 'Location', },
})

Promotion.relationship({ path: 'blocks', ref: 'PromotionBlock', refPath: 'promotion' })

Promotion.defaultColumns = 'name, timeBegin, timeEnd'
Promotion.register()

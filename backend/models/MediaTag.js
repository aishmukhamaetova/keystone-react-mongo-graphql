var keystone = require('keystone')
var Types = keystone.Field.Types


var MediaTag = new keystone.List('MediaTag')

MediaTag.add({
  name: { type: String, required: true },
})

MediaTag.relationship({ ref: 'Video', path: 'videos', refPath: 'tags' })
MediaTag.relationship({ ref: 'Image', path: 'images', refPath: 'tags' })

MediaTag.defaultColumns = 'name'
MediaTag.register()

var keystone = require('keystone')
var uuid = require('uuid/v4')


const who = [`updates`, `0.0.2-init`, `create`]

const items = {
  Language: [
    {
      name: `English`,
      __ref: `__en`,
    }
  ],
  LocationCurrency: [
    {
      name: `USD`,
      __ref: '__usd'
    },
    { name: `EUR`, },
  ],
  Location: [
    {
      name: `Lebanon`,
      currency: `__usd`,

      __ref: `__lebanon`,
    }
  ],
  ProductBadge: [
    { name: `CNN`, },
    {
      name: `BESTSELLER`,
      __ref: `bestseller`,
    },
    { name: `ETC`, },
    { name: `LIMITED EDITION`, },
  ],
  ProductHarvest: [
    {
      name: `1`,
      __ref: `__january`,
    },
    { name: `2`, },
    { name: `3`, },
    { name: `4`, },
    { name: `5`, },
    { name: `6`, },
    { name: `7`, },
    { name: `8`, },
    {
      name: `9`,
      __ref: `__september`,
    },
    { name: `10`, },
    { name: `11`, },
    { name: `12`, },
  ],
  ProductStore: [
    {
      name: `Product 1 [250]`,
      quantity: 20,
      price: 33,

      location: `__lebanon`,
      product: `__product1`,
    },
  ],
  ProductPair: [
    { name: `Meat`, },
    { name: `Red wine`, },
    { name: `Cookies`, },
    { name: `Cheese`, },
  ],
  ProductRegion: [
    { name: `Kobbet Chamra`, },
    { name: `Akkar`, },
    { name: `Lebanon`, },
  ],
  ProductCategory: [
    { name: `Honey` },
    { name: `Honey-Candles` },
    { name: `Hyves` },
  ],
  Product: [
    {
      sku: uuid(),
      name: `Product 1`,
      description: `Product 1 description`,
      sweetness: 40,

      badge: `bestseller`,
      harvest: [`__january`, `__september`],
      location: `__lebanon`,

      __ref: `__product1`,
    },
  ],
}

const randomInteger = (min, max) => Math.round(min - 0.5 + Math.random() * (max - min + 1))

for (let index = 2; index <= 50; index++) {
  items.Product.push({
    sku: uuid(),
    name: `Product ${ index }`,
    description: `Product ${ index } description`,
    sweetness: randomInteger(0, 100),

    badge: `bestseller`,
    harvest: [`__january`, `__september`],
    location: `__lebanon`,
  })
}

const create = async (done) => {
  keystone.createItems(items, { verbose: true }, (err, stats) => {
    stats && console.log(...who, stats.message)
    done(err)
  })
}

exports = module.exports = function (done) {
  create(done)
}

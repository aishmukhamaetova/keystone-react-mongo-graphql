const keystone = require('keystone')
const cookieParser = require('cookie-parser')
const apolloServer = require('../lib/app/honey/graphql/api/server').default

// const facebookCallback = require('../lib/app/default/auth/api/server/callback')
// const facebookConfirm = require('../lib/app/default/auth/api/server/confirm')
// const facebookLogin = require('../lib/app/default/auth/api/server/login')
// const facebookLogout = require('../lib/app/default/auth/api/server/logout')
const honeyMiddleware = require('../lib/app/honey/keystone/api/middleware').default

keystone.pre('routes', function(req, res, next) {
  res.locals.user = req.user

  next()
})

exports = module.exports = function (app) {
  app.use(cookieParser())

  honeyMiddleware(app)

  // app.get('/api/auth/facebook/callback', keystone.middleware.api, facebookCallback)
  // app.get('/api/auth/facebook/confirm', keystone.middleware.api, facebookConfirm)
  // app.get('/api/auth/facebook/login', keystone.middleware.api, facebookLogin)
  // app.get('/api/auth/facebook/logout', keystone.middleware.api, facebookLogout)

  app.get('/api/session', keystone.middleware.api, (req, res, next) => {
    return res.apiResponse({
      sessionId: req.signedCookies['keystone.uid']
    })
  })

  apolloServer({ app })
}

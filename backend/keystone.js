require(`dotenv`).config({ path: `../.env` })

var keystone = require('keystone')


keystone.init({
  'name': `ADM Client`,
  'port': 3010,
  'brand': `L'Atelier du Miel SARL`,
  'auto update': true,
  'session': true,
  'auth': true,
  'user model': `User`,
})

keystone.import(`models`)

keystone.set(`nav`, {
  posts: [`posts`, `post-categories`],
  products: [
    `products`,
    `product-badges`,
    `product-harvests`,
    `product-pairs`,
    `product-regions`,
    `product-stores`,
    `product-categories`,
  ],
  promotions: [
    `promotions`,
    `promotion-blocks`,
  ],
  location: [`locations`, `location-currencies`],
  media: [`images`, `videos`, 'media-tags'],
  localise: [`languages`, `contents`, ],
  users: `users`,
})

keystone.set('google api key', process.env.GOOGLE_BROWSER_KEY)
keystone.set('google server api key', process.env.GOOGLE_SERVER_KEY)

keystone.set('routes', require('./routes'))

keystone.start()

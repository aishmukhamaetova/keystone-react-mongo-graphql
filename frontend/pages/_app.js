import React, { Component } from 'react'
import { ApolloProvider }   from 'react-apollo'
import { getDataFromTree }  from 'react-apollo'

import App, { Container }   from 'next/app'
import Head                 from 'next/head'


import ProviderSpace        from 'evoke-me/space/all/Provider'

// import LanguageProvider     from 'app/language/all/Provider'
import createClient         from 'app/honey/graphql/api/client'
import BlockStart           from 'app/honey/page/web/block/BlockStart'


class MyApp extends App {
  static async getInitialProps({ Component, router, ctx }) {
    let pageProps = {}

    if (Component.getInitialProps) {
      pageProps = await Component.getInitialProps(ctx)
    }

    let serverState = {
      apollo: {
        data: {}
      }
    }

    if (!process.browser) {
      const client = createClient()

      try {
        await getDataFromTree(
          <ApolloProvider client={client}>
            <Component {...pageProps} />
          </ApolloProvider>,
          {
            router: {
              asPath: ctx.asPath,
              pathname: ctx.pathname,
              query: ctx.query
            }
          }
        )
      } catch (error) {
      }

      Head.rewind()

      serverState = {
        apollo: {
          data: client.cache.extract()
        }
      }
    }

    return {
      pageProps,
      serverState,
    }
  }

  constructor (props) {
    super(props)

    this.client = createClient(this.props.serverState.apollo.data)
  }

  render () {
    const {
      Component,
      pageProps,
    } = this.props

    return (
      <Container>
        <ApolloProvider client={this.client}>
          <ProviderSpace>
            <BlockStart />
            {/*<LanguageProvider>*/}
              <Component {...pageProps} />
            {/*</LanguageProvider>*/}
          </ProviderSpace>
        </ApolloProvider>
      </Container>
    )
  }
}

export default MyApp

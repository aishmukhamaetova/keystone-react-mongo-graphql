import React                from 'react'
import Document             from 'next/document'
import { Head }             from 'next/document'
import { Main }             from 'next/document'
import { NextScript }       from 'next/document'

import { ServerStyleSheet } from 'styled-components'
import { createGlobalStyle } from 'styled-components'


const GlobalStyle =  createGlobalStyle`
  html, body {
    font-family: 'Open Sans', sans-serif;
    margin: 0;
  }
  
  html {
    height: 100%;  
  }
  
  body {
    min-height: 100%;
    height: auto;
  }

  #__next {
    display: flex;
    min-height: 100vh;
    flex-direction: column;    
  }
`

class MyDocument extends Document {
  static getInitialProps ({ renderPage }) {
    const sheet = new ServerStyleSheet()
    const page = renderPage(App => props => sheet.collectStyles((
        <React.Fragment>
          <GlobalStyle />
          <App {...props} />
        </React.Fragment>
      ))
    )
    const styleTags = sheet.getStyleElement()
    return { ...page, styleTags }
  }

  render () {
    return (
      <html>
      <Head>
        <meta name='viewport' content='width=device-width, initial-scale=1.0' />
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,700&amp;subset=cyrillic' rel='stylesheet' />
        <link href='https://fonts.googleapis.com/css?family=PT+Serif:400,700&amp;subset=cyrillic' rel='stylesheet' />
        <link rel='stylesheet' href='//cdn.jsdelivr.net/npm/semantic-ui@2.4.0/dist/semantic.min.css' />
        <script src='https://test-bobsal.gateway.mastercard.com/checkout/version/49/checkout.js' />
        { this.props.styleTags }
      </Head>
      <body>
        <GlobalStyle />
        <Main />
        <NextScript />
      </body>
      </html>
    )
  }
}

export default MyDocument

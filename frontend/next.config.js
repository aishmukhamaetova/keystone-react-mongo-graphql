const path = require('path')
const webpack = require('webpack')
const withOptimizedImages = require('next-optimized-images')


module.exports = withOptimizedImages({
  webpack(config, options) {
    config.plugins.push(new webpack.DefinePlugin({
      CLIENT_HOST: `'${ process.env.CLIENT_HOST }'`,
    }))

    // config.resolve.modules.push(path.resolve('./src'))

    return config
  },
  publicRuntimeConfig: {
    CLIENT_HOST: process.env.CLIENT_HOST
  }
})

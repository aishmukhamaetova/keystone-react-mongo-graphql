import spaceDefaults        from 'evoke-me/space/api/spaceDefaults'
import config               from '../../config/api/config'


export default [
  config.namespace,
  `product`,
  [],
  () => ({
    ...spaceDefaults.state,
  }),
  {
    ...spaceDefaults.updaters,
  }
]

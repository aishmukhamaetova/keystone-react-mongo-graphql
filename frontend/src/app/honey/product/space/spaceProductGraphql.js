import config               from '../../config/api/config'
import QUERY                from '../api/graphql/query'
import QUERY_BY_ID          from '../api/graphql/queryById'
import QUERY_UNIQUE         from '../api/graphql/queryUniqueProposition'
import spaceProduct         from './spaceProduct'


export default [
  config.namespace,
  `productGraphql`,
  [
    spaceProduct
  ],
  () => ({
    index: 0,
    size: 10,

    isLoading: false,

    countPrev: null,

    badgeName: null,

    pairList: null,

    price: null,
    sweetness: null,
  }),
  {
    badgeNameSet: (getState, setState) => function* (badgeName) { yield setState({ badgeName })},

    paginationSet: (getState, setState) => function* (index, size) { yield setState({ index, size }) },

    applyFilter: (
      getState,
      setState,
      {
        badgeNameSet,
        query,
      },
      { fromProduct }
    ) => function* (payload, options) {
      try {
        const {
          badgeName,
          pairList,
          price,
          sweetness,
        } = payload
        const { client } = options

        if (badgeName !== undefined) {
          yield badgeNameSet(badgeName)
        }

        if (pairList !== undefined) {
          yield setState({ pairList })
        }

        if (price !== undefined) {
          yield setState({ price })
        }

        if (sweetness !== undefined) {
          yield setState({ sweetness })
        }

        yield setState({ index: 0, size: 10 })
        yield fromProduct.removeAll()
        yield query({}, { client })
      } catch (err) {
        console.error(err)
      }
    },

    queryNext: (getState, setState, { query }) => function* (payload, options) {
      const { index } = yield getState()
      yield setState({ index: index + 1 })
      yield query(payload, options)
    },

    query: (getState, setState, methods, { fromProduct }) => function* (payload, options) {
      try {
        const {
          client,
        } = options

        if (client) {
          const state = yield getState()

          const {
            index,
            size,
            badgeName,
            pairList,
            price,
            sweetness,
          } = state

          yield setState({ isLoading: true })

          const { data } = yield client.query({
            query: QUERY,
            variables: {
              badgeName,
              pairList,
              pagination: {
                index: index,
                size: size,
              },
              price,
              sweetness,
            },
          })

          // console.log(`DATA`, data)

          for (let doc of data.product) {
            yield fromProduct.create(doc)
          }

          yield setState({
            isLoading: false,
            countPrev: data.product.length,
          })
        }
      } catch (err) {
        yield setState({ isLoading: false })

        console.error(err)
      }
    },
    queryById: (getState, setState, methods, { fromProduct }) => function* ({ id }, { client }) {
      try {
        if (client) {
          yield setState({ isLoading: true })

          const { data } = yield client.query({
            query: QUERY_BY_ID,
            variables: { id },
          })

          for (let doc of data.product) {
            yield fromProduct.create(doc)
          }

          yield setState({ isLoading: false })
        }
      } catch (err) {
        yield setState({ isLoading: false })

        console.error(err)
      }
    },
    queryUnique: (getState, setState, methods, { fromProduct }) => function* (client) {
      try {
        if (client) {
          yield setState({ isLoading: true })

          const { data } = yield client.query({ query: QUERY_UNIQUE })

          //console.log(`productGraphql`, `queryUnique`, `data`, data)

          yield fromProduct.orderClear('unique')
          for (let doc of data.product) {
            yield fromProduct.create(doc, { orderName: `unique` })
          }

          yield setState({ isLoading: false })
        }
      } catch (err) {
        yield setState({ isLoading: false })

        console.error(err)
      }
    }
  }
]

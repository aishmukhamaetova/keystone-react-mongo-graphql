import React, { Component } from 'react'
import { compose }          from 'recompose'
import { defaultProps }     from 'recompose'
import { withProps }        from 'recompose'
import {rem} from 'polished'

// import { Button }           from 'semantic-ui-react'
import {Grid, Button, Typography} from '@material-ui/core';
import styled from 'styled-components'
import { Link } from '../../page/api/routes'

const map = {
  sm: 600,
  md: 960,
  lg: 1280,
  xl: 1920,
}

const mediaGrid = (type) => () => `@media (max-width: ${map[type] - 1}px)`

const enhance = compose(
  defaultProps({
    A: {
      Occasion: styled(({ bgurl, image={}, ...other }) => (
        <Grid {...other} />
      ))`
      && {
        color: ${props => props.color};
        text-align: center;
        background-image: url(${props => props.image.secure_url});
        background-position: center center;
        background-repeat: no-repeat;
        background-size: cover;
      
        button {
          margin-bottom: ${rem(40)};
        }
        
        .Top {
          height: 48%;
        }
        .Bottom {
          height: 52%;
        }
        
        height: ${rem(720)};
        margin: 0 auto;
       
        ${mediaGrid(`xl`)}{//lg d
          width: 100%;
          height: ${rem(720)};
        }
        ${mediaGrid(`lg`)}{//sm d
          width: 100%;
          height: ${rem(720)};
        }
        ${mediaGrid(`md`)}{//tablet
          width: 80%;
          height: ${rem(720)};
        }
        ${mediaGrid(`sm`)}{//phone
          width: 100%;
          height: ${rem(720)};
          .Title {
            font-size: 1.2rem;
            white-space:nowrap;
          }
          .TagLine {
            font-size: 1.0rem;
          }
      
        }
      }
      `,
      Title: styled(({ color, transform, border, margin, fweight, ...other }) => (
        <Typography {...other} />
      ))`&& {
        color: ${props => props.color || 'inherit'};
        border-bottom: ${props => props.border};
        text-transform: ${props => props.transform || 'inherit'};
        border-color: ${props => props.color};
        margin: ${props => props.margin};
        font-weight: ${props => props.fweight || '700'};
        min-width: auto;
        width: 100%;
        font-size: ${props => props.size};
        text-align: center;
        a {
          color: inherit;
        }
        &:hover {
          background: none;
        },
      }
      `,
      
      TagLine: styled(({ color, transform, border, margin, fweight, ...other }) => (
        <Typography {...other} />
      ))`
      && {
        font-size: ${props => props.size || 16};
        font-weight: ${props => props.fweight || 400};
        color: ${props => props.color};
        margin: ${props => props.margin};
        text-align: center;
        font-style: normal;
        line-height: normal; 
      }
      `,
      Button: styled(Button)`
      && {
        background-color: rgba(255, 255, 255, 0.05);
        color: #fff;
        box-shadow: 1px 1px 0px  white inset, -1px -1px 0px  white inset;
        font-size: ${rem(20)};
        font-weight: 600;
        text-transform: uppercase;
        min-width: ${rem(150)};
        margin: 1.312em;
        line-height: 1.321;
        &:hover {
          color: black;
        }
      }
    `,

    }
  }),
  withProps(({ description='' }) => {
    return {
      descriptionSplitted: description.split(' ', 10).join(' ')+'...'
    }
  }),

)

const ProductPromotionalBar = enhance(({
  id,
  sku,
  name,
  description,
  images = [],
  videos = [],
  A,
  key,
  descriptionSplitted,
  content,
  children

}) => {
  return (

    <Grid item xs={9} sm={7} md={4}>
    {images ? images.map((doc, index) => index === 0 ?
    
        <A.Occasion key={doc.id} {...doc} alt={doc.name} container direction="column" justify="space-between" alignItems="center" spacing={0} color="#fff">
          <Grid item>
            &nbsp;
          </Grid>
          <Grid item>
            <A.Title className={'Title'} size="1.875rem" fweight="700" color="#fff" transform="uppercase" margin="0 auto">
              <Link route='product' params={{ id }}><a>{name}</a></Link>
            </A.Title>
          </Grid>
          <Grid item>
            <A.TagLine className={'TagLine'} margin="0 auto" color="#fff" fweight="300" size="1.25rem">
              <div dangerouslySetInnerHTML={{ __html: content }} />
            </A.TagLine>
          </Grid>
          <Grid item justify="center" container>
            <A.Button variant="contained">
              {children}
            </A.Button>
          </Grid>
      </A.Occasion> : null) : null}

    </Grid>
    
  )
})

export default ProductPromotionalBar

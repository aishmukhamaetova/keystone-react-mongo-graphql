import { FormGroup }        from '@material-ui/core'
import { FormControlLabel } from '@material-ui/core'
import { Switch  }          from '@material-ui/core'
import { Tab }              from '@material-ui/core'
import { Tabs }             from '@material-ui/core'
import { Grid }             from '@material-ui/core'
import { InputBase }        from '@material-ui/core'
import { withStyles }       from '@material-ui/core/styles'
import { rem }              from 'polished'
import React                from 'react'
import { withApollo }       from 'react-apollo'
import compose              from 'recompose/compose'
import defaultProps         from 'recompose/defaultProps'
import withHandlers         from 'recompose/withHandlers'
import withStateHandlers    from 'recompose/withStateHandlers'
import styled               from 'styled-components'


import { IconSearch }       from 'app/honey/icons'
import spaceProductGraphql  from 'app/honey/product/space/spaceProductGraphql'
import withSpace            from 'evoke-me/space/all/withSpace'
import SectionExpandedFilter from './SectionExpandedFilter'


const map = {
  sm: 600,
  md: 960,
  lg: 1280,
  xl: 1920,
}

const mediaGrid = (type) => () => `@media (max-width: ${map[type] - 1}px)`

export default compose(
  withApollo,
  withSpace(spaceProductGraphql),
  withStateHandlers(
    () => ({
      isOpen: false,
      tabValue: 1,
      error: null,
    }),
    {
      onClickFilter: ({ isOpen }) => () => ({ isOpen: !isOpen }),
      tabValueSet: () => (tabValue) => ({ tabValue }),
    }
  ),
  withHandlers({
    onTabChange: ({
      client,
      tabValueSet,
      fromProductGraphql,
    }) => (e, tabValue) => {
      tabValueSet(tabValue)
      if (tabValue === 0) {
        fromProductGraphql.applyFilter(
          { badgeName: `MUST HAVE` },
          { client },
        )
      } else {
        fromProductGraphql.applyFilter(
          { badgeName: null },
          { client },
        )
      }
    }
  }),
  defaultProps({
    A: {
      Container: styled.div`
        position: relative;
      `,
      FormControlLabel: styled(FormControlLabel)`
        && {
          margin: auto;
          .label {
            font-size: ${rem(14)};
            display: flex;
          }
        },
      `,
      Tab: styled(Tab)`
        && {
          font-weight:normal;
          font-size: ${rem(14)};
          text-transform: uppercase;
          min-width: inherit;
          width: 50%;
          height: ${rem(48)};
      
          &:first-child {
            border-right: 1px solid rgba(0,0,0,0.2);
          }
        }
      `,

      Tabs: styled(Tabs)`
        && {
          width:100%;
          margin-top:${rem(10)};
          margin-bottom:${rem(10)};
          background-color: #eaeaea;
        }
      `,
      Search: styled(FormGroup)`
          && {
            margin: auto ${rem(30)};  
    
            ${mediaGrid(`xl`)}{//lg d
              margin: auto ${rem(20)};  
            }
            ${mediaGrid(`lg`)}{//sm d
              margin: auto ${rem(30)};  
            }
            ${mediaGrid(`md`)}{//tablet
              margin: auto ${rem(80)};  
            }
            ${mediaGrid(`sm`)}{//phone
              margin: ${rem(20)} 30%;  
            }
          }
      `,
      InputBase: styled(({ fontSize, margin, ...other }) => (
        <InputBase {...other} />
      ))`
        && {
          font-size: ${rem(15)};
          margin: auto;
          background: none;
    
          ${mediaGrid(`sm`)}{//phone
            background: #d8d8d8;
            height:${rem(48)};
            padding: ${rem(5)};
          }
          button {
            margin-right: ${rem(5)};
            border-radius: 100px;
            overflow: hidden;
            padding: ${rem(5)};
          }
        }
      `,

      IconSearch: styled(({ ...other }) => (
        <IconSearch {...other} />
      ))`
        font-size: inherit;
      `,
    }
  }),
  withStyles((theme) => ({
    indicator: {
      backgroundColor: 'rgba(0,0,0,0)'
    },
    tabSelected: {},
    tabRoot: {
      textTransform: 'initial',
      fontWeight: 300,
        '&:hover': {
          color: '#000',
          opacity: 1,
          fontWeight: 600,
        },
        '&$tabSelected': {
          color: '#000',
          opacity: 1,
          fontWeight: 600,
        },
        '&:focus': {
          opacity: 1,
          color: '#000',
          fontWeight: 600,
        },
      }
    })
  )
)(({
  isOpen, A, onClickFilter, 
  tabValue,
  onTabChange,
  classes
}) => {
  return (
    <A.Container>

      <Grid container direction={'row'} justify={'center'} alignItems={'center'} spacing={0}>
        <Grid xs={10} item>

          <Grid container direction="row" justify="center" alignItems="center" spacing={0} >

            <Grid xs={12} sm={4} md={4} item >
              <FormGroup row >
                <A.FormControlLabel control={<Switch value={isOpen} checked={isOpen} onChange={onClickFilter} color="default" />} label={<span className={'label'}>Filter</span>} />
              </FormGroup>
            </Grid>

            <Grid xs={12} sm={4} md={4} item>
              <A.Tabs
                fullWidth
                value={tabValue}
                onChange={onTabChange}
                classes={{ indicator: classes.indicator }}
              >
                <A.Tab fullWidth classes={{ selected: classes.tabSelected, root: classes.tabRoot }} label="must have" />
                <A.Tab fullWidth classes={{ selected: classes.tabSelected, root: classes.tabRoot }} label="list all" />
              </A.Tabs>
            </Grid>

            <Grid xs={12} sm={4} md={4} item >
              <A.Search row >
                <A.InputBase autoFocus={true} startAdornment={<A.IconSearch disableripple="true" disabletouchripple="true" style={{fontSize: 15}}  />} placeholder="Search" />
              </A.Search>
            </Grid>

          </Grid>
        </Grid>
      </Grid>

      <SectionExpandedFilter isOpen={isOpen} />

    </A.Container>
  )
    
})

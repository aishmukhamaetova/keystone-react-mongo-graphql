import { Grid }             from '@material-ui/core'
import { rem }              from 'polished'
import React                from 'react'
import compose              from 'recompose/compose'
import defaultProps         from 'recompose/defaultProps'
import styled               from 'styled-components'


import BlockProductList     from '../block/BlockProductList'


export default compose(
  defaultProps({
    A: {
      SectionProductList: styled.div`
        margin-top: ${rem(40)};
      `,
    }
  }),
)((props) => {
  const { A } = props

  return <A.SectionProductList>
    <Grid
      container
      direction='row'
      justify='center'
      alignItems='center'
      spacing={0}
    >
      <Grid xs={10} item>
        <Grid
          container
          direction="row"
          justify="center"
          alignItems="flex-start"
          spacing={40}
        >
          <BlockProductList />
        </Grid>
      </Grid>
    </Grid>
  </A.SectionProductList>
})

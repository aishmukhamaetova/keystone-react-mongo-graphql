import React, { Component } from 'react'
import { Container }        from 'react-grid-system'
import { Col }              from 'react-grid-system'
import { Row }              from 'react-grid-system'

import compose              from 'recompose/compose'
import defaultProps         from 'recompose/defaultProps'
import withPropsOnChange    from 'recompose/withPropsOnChange'

import styled from 'styled-components'
import {Grid, Typography} from '@material-ui/core'
import  {rem} from 'polished'

import BlockProductUnique   from '../block/BlockProductUnique'
const enhance = compose(
  defaultProps({
    A: {
     Container: styled.div`
      display: inline-flex;
      text-align:center;
      margin: 0 auto;
    `      

    }
  }),
    
)


const SectionProductList = enhance(({A}) => {
  return (
    <Grid container direction={'row'} justify={'center'} alignItems={'center'} spacing={0}>
      <Grid item xs={8} sm={5} >
        <A.Container>
          <BlockProductUnique />
        </A.Container>
      </Grid>
    </Grid>
  )
})

export default SectionProductList

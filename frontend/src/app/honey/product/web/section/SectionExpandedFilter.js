import React                from 'react'
import { withApollo }       from 'react-apollo'
import compose              from 'recompose/compose'
import defaultProps         from 'recompose/defaultProps'
import withHandlers         from 'recompose/withHandlers'
// import withPropsOnChange    from 'recompose/withPropsOnChange'
import withStateHandlers    from 'recompose/withStateHandlers'
import {
  FormGroup,
  FormControlLabel,
  Grid,
  Typography,
  FormControl,
  Radio,
  Checkbox

} from '@material-ui/core';
import Slider from '@material-ui/lab/Slider'
import {rem} from 'polished'
import styled from 'styled-components'
import posed                from 'react-pose'
import { withStyles } from '@material-ui/core/styles'

import spaceProductGraphql  from 'app/honey/product/space/spaceProductGraphql'
import withSpace            from 'evoke-me/space/all/withSpace'


const poseMap = {
  OPEN: `open`,
  CLOSE: `close`,
}

const cntrMap = {
  Lebanon:  `🇱🇧`, 
  France:  `🇫🇷`, 
  Italy:  `🇮🇹`, 
  Kuwait:  `🇱🇧`, 
  Spain:  `🇪🇸`, 
  Scotland: `🏴`
}

const Content = styled(posed.div({
  [poseMap.CLOSE]: { height: 0, paddingBottom: 0 },
  [poseMap.OPEN]: { height: 'auto', paddingBottom : 30 }
}))`
  height: 0;
  overflow: hidden;
  padding-bottom: 0;
`;

const enhance = compose(
  withApollo,
  withSpace(spaceProductGraphql),
  withStateHandlers(
    () => ({ 
      slider: {},
      pairing: {},
      country: {
        'Lebanon': true
      }
    
    }),
    {
      sliderSetItem: ({ slider }) => (name, value) => ({
        slider: {
          ...slider,
          [name]: value,
        }
      }),

      pairingSetItem: ({ pairing }) => (name, value) => (console.log(`pairingSetItem`, name, value), {
        pairing: {
          ...pairing,
          [name]: value
        }
      }),
    },
  ),
  withHandlers({
    onSliderChange: ({
      client,
      sliderSetItem,
      fromProductGraphql,
    }) => (value, label) => {
      if (label === `sweetness`) {
        fromProductGraphql.applyFilter(
          { sweetness: value },
          { client }
        )
      }

      if (label === `price`) {
        fromProductGraphql.applyFilter(
          { price: value * 10 },
          { client }
        )
      }

      sliderSetItem(label, value)
    },

    onPairChange: ({
      client,
      fromProductGraphql,
      pairing, pairingSetItem,
    }) => (event, name) => {
      const pairingNext = {
        ...pairing,
        [name]: event.target && event.target.checked
      }

      const pairList = Object.keys(pairingNext)
        .reduce((acc, name) =>
          (pairingNext[name] && acc.push(name), acc),
          []
        )

      fromProductGraphql.applyFilter(
        { pairList: pairList.length ? pairList : null },
        { client }
      )

      pairingSetItem(name, event.target && event.target.checked)
    }
  }),
  defaultProps({
    A: {
      FieldSetContainer: styled.div`
        && {
          align-items: center;
          width: 100%;
          font-size: ${rem(13)};
          lable{
            font-size: inherit;
          }
          span {
            font-size: inherit;
          }
        }

    `,

    SliderFooter: styled.div`
    && {
      display:flex;
      flex-direction:row;
      justify-content:space-between;
      align-items: center;
      width: 100%;
      font-size: ${rem(13)};
      p {
        font-size: inherit;
        &.left{
          font-style: italic;
          font-weight: normal;
          font-stretch: normal;
          line-height: normal;
          letter-spacing: normal;
          padding-top: 0.40rem;
          font-size:  ${rem(10)};
          } 
        &.right {
          text-align: right;
          font-style: italic;
          font-weight: normal;
          font-stretch: normal;
          line-height: normal;
          letter-spacing: normal;
          padding-top: 0.40rem;
          font-size:  ${rem(10)};
        }
      }
    }
    `,

    FormControlLabel: styled(FormControlLabel)`
    && {
      margin: auto;
      .label {
        font-size: ${rem(14)};
        display: flex;
      }
    },
  `,

  Radio: styled(({ ...other }) => (
    <Radio {...other} />
  ))`
    &&{
      padding: 0px ${rem(5)} ${rem(5)} ${rem(5)};
      &.primary{
        color: black;
      }
    }
  `,

  Checkbox: styled(({ ...other }) => (
    <Checkbox {...other} />
  ))`
    &&{
      padding: 0px ${rem(5)} ${rem(5)} ${rem(5)};
      &.primary{
        color: black;
      }
    }
  `,

  Typography: styled.div`
  && {
    text-transform: uppercase;
    &.title {
      font-size: ${rem(10)};
      font-weight: bold;
      padding-bottom: 0.5rem;
      padding-top: 0.5rem;
      font-family: Open Sans,sans-serif;
    }


  }
`,

    }
  }),
)

const ExpandedFilter = enhance(({
  isOpen, 
  A, 
  slider,
  handleChangePairing, 
  pairing,
  error,
  country,
  classes,

  onSliderChange,
  onPairChange,
}) => {
  
return (

  <Content pose={isOpen ? 'open' : 'close'}>

  <Grid container direction={'row'} justify={'center'} alignItems={'center'} spacing={0}>
    <Grid xs={8} item >
      <Grid container direction='row' justify='center' alignItems='flex-start' spacing={16} >
        
        <Grid xs={12} sm={10} md={4} item >
        <div style={{width: '90%'}}>

          {['sweetness', 'texture', 'price'].map((item, index) =>
            <FormGroup key={index} row>
              <A.Typography className='title' id={item}>{item}</A.Typography>
                <Slider
                  classes={{track: classes.track, thumb: classes.thumb}}
                  value={slider[item] || 0}
                  aria-labelledby={item}
                  onChange={(event, value) => onSliderChange(value, item)}
                />
                <A.SliderFooter>
                  <Typography className={'left'} id={item}>Least</Typography>
                  <Typography className={'right'} id={item}>Most</Typography>
                </A.SliderFooter>
            </FormGroup>
            )}


        </div>

        </Grid>

        <Grid xs={12} sm={10} md={4} item >

          <A.Typography className='title' id='price'>pairing</A.Typography>
          
          <A.FieldSetContainer>
            <Grid container alignItems='stretch' justify='flex-start'>
              {['Cheese', 'Cookies', 'Meat', 'Red wine'].map((label, index) =>
                <Grid key={index} item xs={3} sx={6} md={6}>
                <FormControl component='fieldset' required error={error}>
                <FormControlLabel
                    control={
                      <A.Checkbox
                        className='primary'
                        checked={pairing[label] || false}
                        onChange={(event) => onPairChange(event, label)}
                        value={label}
                      />
                    }
                    label={label}
                  />
                  </FormControl>
                  </Grid> 
              )}
            </Grid>
          </A.FieldSetContainer>
        </Grid>

        <Grid xs={12} sm={10} md={4} item >
          <A.Typography className='title' id='country'>country</A.Typography>
          <A.FieldSetContainer>
            <Grid container alignItems='stretch' justify='flex-start' >
                {['Lebanon', 'France', 'Italy', 'Kuwait', 'Spain', 'Scotland'].map((cntr, index)=> 
                  <Grid key={index} item xs={3} sx={6} md={6}>
                    <FormControl component='fieldset' required error={error}  >
                      <FormControlLabel
                        disabled={index !== 0}
                        value={country[cntr]}
                        control={<A.Radio checked={index === 0} className='primary' color='primary' />}
                        label={<nobr>{`${cntr} ${cntrMap[cntr]}`}</nobr>}
                        labelPlacement='end'
                      />
                    </FormControl>
                  </Grid>
                )}
              </Grid>
          </A.FieldSetContainer>

        </Grid>
      </Grid>



      </Grid>
  </Grid>
  

</Content>

  )
})

export default withStyles(theme=>({
  thumb: {
    backgroundColor: '#000'
  },
  track: {
    backgroundColor: '#000'
  }


}))(ExpandedFilter)

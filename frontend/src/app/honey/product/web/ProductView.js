import React, { Component } from 'react'
import { compose }          from 'recompose'
import { defaultProps }     from 'recompose'
import { withProps }        from 'recompose'
import {rem} from 'polished'

// import { Button }           from 'semantic-ui-react'
import {Grid, Button, ButtonBase,  Typography, FormControl, Hidden, FormGroup} from '@material-ui/core';
import styled from 'styled-components'

import ThumbImage                from '../../image/web/ThumbImage'
import Image                from '../../image/web/Image'
import Emojis                from '../../image/web/Emojis'
import { Link }             from '../../page/api/routes'


const map = {
  sm: 600,
  md: 960,
  lg: 1280,
  xl: 1920,
}

const mediaGrid = (type) => () => `@media (max-width: ${map[type] - 1}px)`


const enhance = compose(
  defaultProps({
    A: {
      FormGroup: styled(FormGroup)`
        margin-top: ${rem(10)};
      `,

      ProductDesc: styled.div`
      && {
        font-size:  ${rem(12)};
        padding: ${rem(4)};
        margin-top: ${rem(5)}; 
        font-style: italic;
        line-height: normal;
        box-shadow: 0 0 ${rem(1)} rgba(34, 25, 25, 0.4);
        box-sizing: border-box;
        margin-bottom:${rem(19)};
        margin-top: ${rem(19)}; 
      }
    `,

      Detail: styled(Typography)`
        && {
          font-size:  ${rem(16)};
          font-style: normal;
          font-stretch: normal;
          line-height: normal;
          letter-spacing: normal; 
          margin-bottom: ${rem(5)};
        }
      `,

      SubTitle: styled(Typography)`
        && {
          font-size: ${rem(16)};
          margin-bottom: ${rem(18)};
          text-align: left;
        }
      `,
      Description: styled.div`
        text-align:left;
      `,

      Title: styled(Typography)`
        && {
          font-weight: 700;
          font-size: ${rem(30)};
          line-height: normal;
          border-bottom: ${rem(1)} solid;
          padding-bottom: ${rem(7)};
          margin-bottom: ${rem(13)};
          color: rgba(0, 0, 0, 0.87);
          border-color: rgba(0, 0, 0, 0.87);
          text-transform: uppercase;
          ${mediaGrid(`sm`)}{//phone
            font-size: ${rem(20)};
          }

        }
      `,

      Container: styled.div`
        display: inline-flex;
        margin: 0;
      `,

      Img: styled(ButtonBase)`
        && {
          text-align: left;
          margin-right: ${rem(58)};
          img {
            width: ${rem(200)};
            margin-bottom: ${rem(20)};
            height: auto;
          }
          &.fullImg {
            img {
              width: ${rem(350)};
              height: auto;
            }

          } 
          
          ${mediaGrid(`sm`)}{//phone
            img {
              width: ${rem(180)};
              height: auto;
            }
            &.fullImg {
              img {
                width: ${rem(350)};
                height: auto;
              }

          }
        }
      `,
      ReadMore: styled(Typography)`
        && {
          text-align: center;
          color: #000;
          font-size: ${rem(10)};
          line-height: 1.4;
          font-weight: 600;
          margin-bottom: ${rem(18)};
          margin: ${rem(15)} auto;
          padding: inherit;
        
          a {
            text-decoration: underline;
            color: #000;
            font-weight: 600;
            &:hover {
              text-decoration: none;
            }
          }
        
        }
      `,
    
      Title: styled(Typography)`
        && {
          font-weight: 700;
          font-size: ${rem(17)};
          line-height: normal;
          border-bottom: 1px solid;
          padding-bottom: ${rem(7)};
          margin-bottom: ${rem(13)};
          color: rgba(0, 0, 0, 0.87);
          border-color: rgba(0, 0, 0, 0.87);
          text-transform: uppercase;
          a {
            color: #000;
          }
        }
    `    
    }
  }),
  withProps(({ description='', images=[] }) => {
    return {
      descriptionSplitted: description.split(' ', 10).join(' ')+'...',
      imgMain: images[0],
    }
  }),

)

const ProductView = enhance((props) => {
  const {
    id,
    children,
    sku,
    name,
    description,
    images = [],
    videos = [],
    A,
    key,
    descriptionSplitted,
    setActiveMedia,
    imgMain,
    pairs,
    regions,
    sweetness,
    stores,
  
  } = props;
  return (
    <Grid container justify="center" alignItems="center" >
       <Grid item xs={10} sm={8}>
       
            <Grid container direction="row" justify="flex-start" alignItems="flex-start" >
              <Hidden smDown>
                <Grid item sm={2}>
                  
                      <FormControl>
                        {images.map((image, index) =>  <ButtonBase  disableRipple={true} disabletouchripple={true} onClick={()=> {setActiveMedia(image)}}  key={index}><ThumbImage {...image} /></ButtonBase>)} 
            
                        {/* video thumbs // <A.ButtonThumbImg style={{ backgroundImage: 'url(' + '/img/vid.jpg' + ')' }}>
                        // <PlayVidspan><PlayVidIcon style={{fontSize: 35,fill:'#ffffffa6' }}></PlayVidIcon></PlayVidspan>
                        // </A.ButtonThumbImg> */}

                      </FormControl>
                </Grid>
              </Hidden>


              <Grid item xs={12} sm={3}>
                  {/* className={isImage? 'fullImg' : ''}  */}
                  <A.Img>
                    <Image name={name} {...imgMain}/>
                  </A.Img>
              </Grid>

              <Grid item xs={12} sm={7}>
                  <A.Description>

                      <A.Title>{name}</A.Title>

                      <A.SubTitle variant="caption">
                        {description}
                      </A.SubTitle>

                      <div style={{margin: '20px auto'}}>
                        <Emojis />
                      </div>

                      <A.Detail paragraph={true}>
                        <b>Sweetness:</b> {sweetness}
                      </A.Detail>
                      <A.Detail paragraph={true}>
                        <b>Regions:</b> {regions.map((region,i)=>(region.name + ( (i<regions.length && regions.length !==1 ) ?', '   : '') ))}
                      </A.Detail>
                      <A.Detail paragraph={true}>
                        <b>Food Pairing:</b> {pairs.map((pair,i)=>(pair.name + ( (i<pairs.length && pairs.length !==1 ) ?', '   : '') ))}
                      </A.Detail>
                      <A.Detail paragraph={true}>
                        <b>Stores:</b> {stores.map((store,i)=>(store.location && store.location.name + ( (i<stores.length && stores.length !==1 ) ?', ' : '') ))}
                      </A.Detail>

                      <A.ProductDesc>
                        <p> ✔︎ This honey contains potassium, vitamin A and magnesium, and has been </p>
                        <p> Shown to have excellent diuretic properties.</p>
                      </A.ProductDesc>



                      <A.FormGroup row>

                        {children}

                      </A.FormGroup>

                  </A.Description>
              </Grid>

            </Grid>
             
       </Grid>
    </Grid>

  )
})

export default ProductView

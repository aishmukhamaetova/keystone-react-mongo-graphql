import React                from 'react'
import { withApollo }       from 'react-apollo'
import compose              from 'recompose/compose'
import lifecycle            from 'recompose/lifecycle'
import withHandlers         from 'recompose/withHandlers'
import withStateHandlers    from 'recompose/withStateHandlers'


import spaceBoxEshopCart    from 'box/eshop/cart/space/spaceBoxEshopCart'
import withSpace            from 'evoke-me/space/all/withSpace'

import spaceProduct         from '../../space/spaceProduct'
import spaceProductGraphql  from '../../space/spaceProductGraphql'
import ProductList          from '../ProductList'


export default compose(
  withApollo,
  withSpace(spaceBoxEshopCart),
  withSpace(spaceProduct),
  withSpace(spaceProductGraphql),
  withHandlers({
    fetchMore: ({
      client,
      fromProduct,
      fromProductGraphql,
      paginationSet,
    }) => () => {
      const { orderMap } = fromProduct
      const { index, size } = fromProductGraphql

      if (orderMap.default.length === (index + 1) * size) {
        fromProductGraphql.queryNext({}, { client })
      }
    },
  }),
  withHandlers({
    onScroll: ({ fetchMore }) => (e) => {
      if ((window.innerHeight + window.pageYOffset) >= document.body.offsetHeight - 2) {
        fetchMore()
      }
    },
  }),
  withHandlers({
    mount: ({
      index,
      client,
      size,
      fromProduct,
      fromProductGraphql,
      onScroll,
    }) => () => {
      const { orderMap } = fromProduct

      if (orderMap.default.length < 3) {
        fromProductGraphql.query({
          pagination: {
            index,
            size,
          },
        }, { client })
      }

      if (window) {
        window.addEventListener(`scroll`, onScroll)
      }
    },
    unmount: ({
      onScroll,
    }) => () => {
      if (window) {
        window.removeEventListener(`scroll`, onScroll)
      }
    },
  }),
  lifecycle({
    componentDidMount() {
      this.props.mount()
    },
    componentWillUnmount() {
      this.props.unmount()
    },
  })
)(({
  fromBoxEshopCart,
  fromProduct,
  fromProductGraphql,
}) => {
  const { data, orderMap } = fromProduct
  const { isLoading } = fromProductGraphql

  return (
    <ProductList
      data={data}
      order={orderMap.default}
      dataCart={fromBoxEshopCart.data}

      onCartAdd={(payload) => {
        const { id, name, price, sku } = payload
        fromBoxEshopCart.create({ id, name, price, sku, quantity: 1 })
      }}

      onCartMinus={({ id }) => {
        const { quantity } = fromBoxEshopCart.data[id]

        if (quantity === 1) {
          fromBoxEshopCart.remove({ id })
        } else {
          fromBoxEshopCart.update({ id, quantity: quantity > 0 ? quantity - 1 : quantity })
        }
      }}

      onCartPlus={({ id }) => {
        const { quantity } = fromBoxEshopCart.data[id]

        fromBoxEshopCart.update({ id, quantity: quantity + 1})
      }}
    />
  )
})

import { withRouter }       from 'next/router'
import React, { Component } from 'react'
import { withApollo }       from 'react-apollo'
import { compose }          from 'recompose'
import { lifecycle }        from 'recompose'

import spaceCart            from 'box/eshop/cart/space/spaceBoxEshopCart'
import withSpace            from 'evoke-me/space/all/withSpace'

import spaceProduct         from '../../space/spaceProduct'
import spaceProductGraphql  from '../../space/spaceProductGraphql'
import ProductListView          from '../ProductListView'


const enhance = compose(
  withRouter,
  withApollo,
  withSpace(spaceCart),
  withSpace(spaceProduct),
  withSpace(spaceProductGraphql),
  lifecycle({
    componentDidMount() {
      const { client } = this.props
      const { fromProductGraphql } = this.props
      const { router } = this.props
      const { query: { id }} = router

      const { fromProduct } = this.props
      const { data, orderMap } = fromProduct

      if (!data[id]) {
        fromProductGraphql.queryById({ id }, { client })
      }
    }
  })
)

const BlockProduct = enhance(({
  fromBoxEshopCart,
  fromProduct,
  fromProductGraphql,
  router,
}) => {
  const { query: { id }} = router
  const { data } = fromProduct
  const { isLoading } = fromProductGraphql

  if (isLoading || !data[id]) return null

  return (
    <ProductListView
      data={data}
      order={[id]}
      dataCart={fromBoxEshopCart.data}

      onCartAdd={(payload) => {
        const { id, name, price, sku } = payload
        fromBoxEshopCart.create({ id, name, price, sku, quantity: 1 })
      }}

      onCartMinus={({ id }) => {
        const { quantity } = fromBoxEshopCart.data[id]

        if (quantity === 1) {
          fromBoxEshopCart.remove({ id })
        } else {
          fromBoxEshopCart.update({ id, quantity: quantity > 0 ? quantity - 1 : quantity })
        }
      }}

      onCartPlus={({ id }) => {
        const { quantity } = fromBoxEshopCart.data[id]

        fromBoxEshopCart.update({ id, quantity: quantity + 1 })
      }}
    />
  )
})

export default BlockProduct

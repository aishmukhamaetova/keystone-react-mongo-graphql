import React, { Component } from 'react'
import { withApollo }       from 'react-apollo'
import { compose }          from 'recompose'
import { lifecycle }        from 'recompose'

import { Segment }          from 'semantic-ui-react'
import { Header }           from 'semantic-ui-react'
import { Icon }             from 'semantic-ui-react'


import spaceCart            from 'box/eshop/cart/space/spaceBoxEshopCart'
import withSpace            from 'evoke-me/space/all/withSpace'

import spaceProduct         from '../../space/spaceProduct'
import spaceProductGraphql  from '../../space/spaceProductGraphql'
import ProductListUnique          from '../ProductListUnique'


const enhance = compose(
  withApollo,
  withSpace(spaceCart),
  withSpace(spaceProduct),
  withSpace(spaceProductGraphql),
  lifecycle({
    componentDidMount() {
      const { client } = this.props
      const { fromProductGraphql } = this.props

      const { fromProduct } = this.props
      const { data, orderMap } = fromProduct

      if (!orderMap.unique) {
        fromProductGraphql.queryUnique(client)
      }
    }
  })
)

const BlockProductUnique = enhance(({
  fromBoxEshopCart,
  fromProduct,
  fromProductGraphql,
}) => {
  const { data, orderMap } = fromProduct
  const { isLoading } = fromProductGraphql

  if (!orderMap.unique || !orderMap.unique.length) {
    return (
      <Segment placeholder loading={isLoading}>
        <Header icon>
          <Icon name='hand peace' />
          No unique proposition
        </Header>
      </Segment>
    )
  }

  return (
    <ProductListUnique
      data={data}
      order={orderMap.unique.slice(0, 1)}
      dataCart={fromBoxEshopCart.data}

      onCartAdd={(payload) => {
        const { id, name, price, sku } = payload
        fromBoxEshopCart.create({ id, name, price, sku, quantity: 1 })
      }}

      onCartMinus={({ id }) => {
        const { quantity } = fromBoxEshopCart.data[id]

        if (quantity === 1) {
          fromBoxEshopCart.remove({ id })
        } else {
          fromBoxEshopCart.update({ id, quantity: quantity > 0 ? quantity - 1 : quantity })
        }
      }}

      onCartPlus={({ id }) => {
        const { quantity } = fromBoxEshopCart.data[id]

        fromBoxEshopCart.update({ id, quantity: quantity + 1})
      }}
    />
  )
})

export default BlockProductUnique


import React, { Component } from 'react'
import compose              from 'recompose/compose'
import defaultProps         from 'recompose/defaultProps'

import { Button }           from 'semantic-ui-react'
import { Link }             from '../../page/api/routes'

import Image                from '../../image/web/Image'
import Video                from '../../video/web/Video'
import Emojis                from '../../image/web/Emojis'

import styled from 'styled-components'
import {rem} from 'polished'
import {Grid, Typography, FormControl} from '@material-ui/core';

const map = {
  sm: 600,
  md: 960,
  lg: 1280,
  xl: 1920,
}

const mediaGrid = (type) => () => `@media (max-width: ${map[type] - 1}px)`


const enhance = compose(
  defaultProps({
    A: {
      Image: styled.div`
        && {
          text-align: center;

          img {
            width: ${rem(200)};
            margin-right: ${rem(40)};
            height: auto;
          }

          ${mediaGrid(`md`)}{//tablet
            text-align: left;
              img {
                width: ${rem(200)};
                height: auto;
              }
            }
          ${mediaGrid(`sm`)}{//phone
            text-align: center;
            img {
              width: ${rem(200)};
              height: auto;
            }
          }
        }
      `,
      Description: styled(Typography)`
        && {
            font-size: ${rem(16)};
          margin-bottom: ${rem(15)};
        }
      `,
      Details: styled(FormControl)`
        text-align: left;
        width: 80%;
      `,
      Detail: styled(Typography)`
        && {
          font-size:  ${rem(16)};
          line-height:  ${rem(23)};
          font-style: normal;
          font-stretch: normal;
          line-height: normal;
          letter-spacing: normal; 
          margin-bottom: ${rem(5)};
        }
      `,
      Title: styled(Typography)`
        && {
          font-weight: 700;
          font-size: ${rem(30)};
          line-height: normal;
          border-bottom: 1px solid;
          padding-bottom: ${rem(7)};
          margin-bottom: ${rem(13)};
          color: #000;
          border-color: #000;
          text-transform: uppercase;
          white-space: nowrap;
          a {
            color: #000;
          }
        }
      `
    
    }
}))

const ProductUnique = enhance(({
  children,
  sku,
  name,
  description,
  images = [],
  videos = [],
  id,
  pairs,
  regions,
  sweetness,
  stores,
  A
}) => {
  return (
    <React.Fragment>
      <A.Image>
        { images.map((doc, index) =>  index===0 ?<Image key={ doc.id } {...doc} />:null) }
      </A.Image>
      {/* { videos.map((doc) => <Video key={ doc.id } {...doc} />) } */}

      <A.Details>
        <Grid container direction={'column'} alignItems={'stretch'} justify={'space-around'} spacing={0}>
            <Grid item xs>
              <A.Title>
                <Link route={'product'} params={{ id }}>{ `${ name }` }</Link>
              </A.Title>

              <A.Description variant="caption">
                { description }
              </A.Description>         

              <div style={{margin: '20px auto'}}>
                <Emojis />
              </div>

              <A.Detail paragraph={true}>
                <b>Sweetness:</b> {sweetness}
              </A.Detail>
              <A.Detail paragraph={true}>
                <b>Regions:</b> {regions.map((region,i)=>(region.name + ( (i<regions.length && regions.length !==1 ) ?', '   : '') ))}
              </A.Detail>
              <A.Detail paragraph={true}>
                <b>Food pairing:</b> {pairs.map((pair,i)=>(pair.name + ( (i<pairs.length && pairs.length !==1 ) ?', '   : '') ))}
              </A.Detail>
              <A.Detail paragraph={true}>
                <b>Stores:</b> {stores.map((store,i)=>(store.location && store.location.name + ( (i<stores.length && stores.length !==1 ) ?', ' : '') ))}
              </A.Detail>

            </Grid>
            
            <Grid item xs>
              { children }
            </Grid>
        </Grid>

      </A.Details>

    </React.Fragment>
  
  )
})

export default ProductUnique

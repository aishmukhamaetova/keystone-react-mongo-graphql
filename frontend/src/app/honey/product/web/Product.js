import React, { Component } from 'react'
import { compose }          from 'recompose'
import { defaultProps }     from 'recompose'
import { withProps }        from 'recompose'
import {rem} from 'polished'

// import { Button }           from 'semantic-ui-react'
import {Grid, Button, Typography, FormControl} from '@material-ui/core';
import styled from 'styled-components'

import Image                from '../../image/web/Image'
import Video                from '../../video/web/Video'
import { Link }             from '../../page/api/routes'


const map = {
  sm: 600,
  md: 960,
  lg: 1280,
  xl: 1920,
}

const mediaGrid = (type) => () => `@media (max-width: ${map[type] - 1}px)`


const enhance = compose(
  defaultProps({
    A: {
      Container: styled.div`
        text-align: center;
      `,
      ButtonBaseImg: styled(FormControl)`
        && {
          text-align: center;
          margin: ${rem(20)} auto;
          width: ${rem(100)};
          justify: center;

          img {
            width: ${rem(100)};
            height: auto;
            margin: auto;
          }

          ${mediaGrid(`md`)}{//tablet
            text-align: left;
              img {
                width: ${rem(100)};
                height: auto;
              }
            }
          ${mediaGrid(`sm`)}{//phone
            text-align: center;
            img {
              width: ${rem(100)};
              height: auto;
            }
          }
        }
      `,

      ReadMore: styled(Typography)`
        && {
          text-align: center;
          color: #000;
          font-size: ${rem(10)};
          line-height: 1.4;
          font-weight: 600;
          margin-bottom: ${rem(18)};
          margin: ${rem(15)} auto;
          padding: inherit;
        
          a {
            text-decoration: underline;
            color: #000;
            font-weight: 600;
            &:hover {
              text-decoration: none;
            }
          }
        
        }
      `,
      SubTitle: styled(Typography)`
        && {
          font-size: ${rem(14)};
          text-align: center;
          width: ${rem(180)};
          
        }
    `,
    
      Title: styled(Typography)`
        && {
          font-weight: 700;
          font-size: ${rem(17)};
          line-height: normal;
          border-bottom: 1px solid;
          padding-bottom: ${rem(7)};
          margin-bottom: ${rem(13)};
          color: rgba(0, 0, 0, 0.87);
          border-color: rgba(0, 0, 0, 0.87);
          text-transform: uppercase;
          a {
            color: #000;
          }
        }
    `    
    }
  }),
  withProps(({ description='' }) => {
    return {
      descriptionSplitted: description.split(' ', 10).join(' ')+'...'
    }
  }),

)

const Product = enhance(({
  id,
  children,
  sku,
  name,
  description,
  images = [],
  videos = [],
  A,
  key,
  descriptionSplitted
}) => {
  return (
    <A.Container>
      <A.Title><Link route='product' params={{ id }}><a>{name}</a></Link></A.Title>
      
      <FormControl>
        
        <A.SubTitle variant='caption'>
          {descriptionSplitted}
        </A.SubTitle>
        <A.ReadMore variant='button' gutterBottom>
          <Link route='product' params={{ id }}><a>Read more</a></Link>
        </A.ReadMore>

        <A.ButtonBaseImg>
          {images.map((doc, index) => index === 0? <Image key={doc.id} {...doc} /> : null)}
        </A.ButtonBaseImg>
        {/* <A.ButtonBaseImg>
          { videos.map((doc) => <Video key={ doc.id } {...doc} />) }
        </A.ButtonBaseImg> */}

        { children }

      </FormControl>          

    </A.Container>
  )
})

export default Product

import React, { Component } from 'react'
import { compose }          from 'recompose'
import { defaultProps }     from 'recompose'
import Product              from './Product'
import CartProductQuantity  from 'box/eshop/cart/web/CartProductQuantity'
import {Grid, Button} from '@material-ui/core';
import styled from 'styled-components'
import {rem} from 'polished'
import { Link }             from 'app/honey/page/api/routes'

const enhance = compose(
  defaultProps({
    data: {},
    order: [],
    A: {
      ButtonBuy: styled(Button)`
        && {
          border-radius:0;
          background-color: rgba(255, 255, 255, 0.05);
          color: #000;
          box-shadow: 1px 1px 0px  black inset, -1px -1px 0px  black inset;
          font-size: ${rem(18)};
          &.cart {
            margin: ${rem(10)} auto;
          }
          font-weight: 600;
          text-transform: uppercase;
          line-height: 2.07;
          a {
            color: #000;
            &:hover {
              color: white;
            }
          }
          &:hover {
            a {
              color: white;
            }
            color: white;
            background: #000;
          }
        }
      `,
    }
  })
)

const ProductList = enhance(({
  data,
  order,
  dataCart = {},
  onCartAdd,
  onCartMinus,
  onCartPlus,
  A
}) => {
  return order.map((id, index) => {
    const { stores } = data[id]
    const price = stores.length ? stores[0].price : null
    
    return (
      <Grid xs={12} sm={6} md={4} item key={id} >
        <Product
          {...data[id]}
        >
          {
          price && dataCart[id] ? (
            <React.Fragment>
              <CartProductQuantity
                {...dataCart[id]}
                onMinus={() => onCartMinus({ id })}
                onPlus={() => onCartPlus({ id })}
              />
              <A.ButtonBuy className={'cart'} fullWidth variant="contained"  align="center" color="primary">
                <Link route='cart' >
                  <a>go to cart</a>
                </Link>
              </A.ButtonBuy>
            </React.Fragment>

          ) : 
          <A.ButtonBuy disabled={!price} onClick={() => onCartAdd({ ...data[id], price })} fullWidth variant="contained"  align='center' color="primary">Buy Now</A.ButtonBuy>
          }

          

        </Product>                        
      </Grid>


    )
  })
})

export default ProductList

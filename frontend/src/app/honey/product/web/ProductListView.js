import React, { Component } from 'react'
import { compose }          from 'recompose'
import { defaultProps }     from 'recompose'
import { withProps }        from 'recompose'
import { withStateHandlers }        from 'recompose'
import ProductView              from './ProductView'
import CartProductQuantity  from 'box/eshop/cart/web/CartProductQuantity'
import {Grid, Button, Typography, ButtonBase} from '@material-ui/core';
import styled from 'styled-components'
import {rem} from 'polished'
import { Link }             from 'app/honey/page/api/routes'
import MediaView                from '../../image/web/MediaView'
import secure_url from 'app/honey/media/sample3.jpg'
import { IconStar }       from 'app/honey/icons'


const map = {
  sm: 600,
  md: 960,
  lg: 1280,
  xl: 1920,
}

const mediaGrid = (type) => () => `@media (max-width: ${map[type] - 1}px)`


const enhance = compose(
  withStateHandlers(
    () => ({
      activeMedia: null,
      indexMedia: 0,
      staticBlock: {
        image: {secure_url}
      }
    }),
    {
      setActiveMedia: ({}) => (activeMedia) => ({ activeMedia })
    }
  ),

  defaultProps({
    data: {},
    order: [],
    A: {
      MediaContainer: styled.div`
        margin-bottom: ${rem(30)};
      `,
      StarSpan: styled(({ width, ...other }) => (
        <p {...other} />
      ))`
        margin-right: ${rem(10)};
        margin-bottom: -${rem(6)};
      }
      `,      
      Rating: styled(Typography)`
        && {
          font-size:  ${rem(12)};
          line-height:  ${rem(23)};
          font-style: italic;
          line-height: normal;
          }
      `,
      ButtonBlock: styled(ButtonBase)`
        && {
          margin-left: ${rem(20)};
        `,
      TagLineContainer: styled.div`
        && {
          text-align: center;
          &.dark {
            padding: ${rem(70)} ${rem(45)};
            background-color: rgba(0, 0, 0, 0.2);
          }
      }`,
      TagLine: styled(({ ...other }) => (
        <Typography {...other} />
      ))`
      && {
        font-size: ${rem(35)};
        font-weight: 300;
        color: #fff;
        text-align: center;
        font-style: normal;
        line-height: normal; 
        &.dark {
          padding: ${rem(70)} ${rem(45)};
          background-color: rgba(0, 0, 0, 0.2);
      
        }
        ${mediaGrid(`xl`)}{//lg d
          font-size: ${rem(33.6)};
        }
        ${mediaGrid(`lg`)}{//sm d
          font-size: ${rem(30)};
        }
        ${mediaGrid(`md`)}{//md tablet
          font-size: ${rem(24)};
        }
        ${mediaGrid(`sm`)}{//sm phone
          font-size: ${rem(20.8)};
        }
      }
      `,
      
      
      ButtonBuy: styled(Button)`
        && {  
          border-radius:0;
          background-color: rgba(255, 255, 255, 0.05);
          color: #000;
          box-shadow: 1px 1px 0px  black inset, -1px -1px 0px  black inset;
          font-size: ${rem(18)};
          font-weight: 600;
          text-transform: uppercase;
          line-height: 2.07;
          a {
            color: #000;
            &:hover {
              color: white;
            }
          }
          &:hover {
            color: white;
            a {
              color: white;
            }
            background: #000;
          }
        }
      `,
      MediaTitle: styled.div`
        text-transform: uppercase;
        background-color: rgba(0, 0, 0, 0.2);
        font-size: ${rem(30)};
        font-weight: 600;
        padding: ${rem(30)} ${rem(150)};
        color: #fff;
        margin: ${rem(30)} auto;
      `
    }
  }),
  withProps(({ order, data }) => {
    
    return {
    }
  }),

)

const ProductList = enhance(({
  data,
  order,
  dataCart = {},
  onCartAdd,
  onCartMinus,
  onCartPlus,
  A,
  activeMedia,
  indexMedia,
  staticBlock, 
  setActiveMedia
}) => {
  return order.map((id, index) => {
    const { stores } = data[id]
    const price = stores.length ? stores[0].price : null
    const {images} = data[id]
    return (
      <React.Fragment key={id} >

        <A.MediaContainer>

          {activeMedia ? 
            <MediaView {...activeMedia}>
              <Grid item xs={10} sm={8}>
                <A.MediaTitle>{activeMedia.name}</A.MediaTitle>
              </Grid>
            </MediaView>
            : staticBlock ? 
            <MediaView {...staticBlock}>
            <Grid item xs={10} sm={8}>
                <A.TagLineContainer className={'dark'}>
                  <A.TagLine  >
                    In winter, on the southern coast of Lebanon, the Alman Plateau outside Saida provides local floral diversity including loquat, inula and wild flowers.
                  </A.TagLine>
                </A.TagLineContainer>
            </Grid>
          </MediaView> 
          : Array.isArray(images) && images.length  ? 
            <MediaView {...images[indexMedia]}>
              <Grid item xs={10} sm={8}>
                <A.MediaTitle>{images[indexMedia].name}</A.MediaTitle>
              </Grid>
          </MediaView> : null}
          
        </A.MediaContainer>


          <ProductView setActiveMedia={setActiveMedia}
            {...data[id]}
          >
            {
            price && dataCart[id] ? (
              <React.Fragment>
                <CartProductQuantity
                  {...dataCart[id]}
                  onMinus={() => onCartMinus({ id })}
                  onPlus={() => onCartPlus({ id })}
                />
                <A.ButtonBlock>
                  <A.ButtonBuy variant="contained"  align="center" color="primary">
                  <Link route='cart' >
                    <a>go to cart</a>
                  </Link>
                </A.ButtonBuy>
                </A.ButtonBlock>
              </React.Fragment>

            ) : 

              (
                <React.Fragment>

                  <A.ButtonBuy disabled={!price} onClick={() => onCartAdd({ ...data[id], price })} variant="contained"  align='center' color="primary">Buy Now</A.ButtonBuy>
                  <A.ButtonBlock>
                    {Array(5).fill(null).map((i, idx) => <A.StarSpan key={idx}><IconStar></IconStar></A.StarSpan>)}
                    <A.Rating>Rating 5/5</A.Rating>
                  </A.ButtonBlock>

                </React.Fragment>
              )
            }

            

          </ProductView>      
        
      </React.Fragment>
    )
  })
})

export default ProductList

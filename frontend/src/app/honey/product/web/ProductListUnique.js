import React, { Component } from 'react'
import { compose }          from 'recompose'
import { defaultProps }     from 'recompose'


import { Link }             from 'app/honey/page/api/routes'

import ProductUnique              from './ProductUnique'
import CartProductQuantity  from 'box/eshop/cart/web/CartProductQuantity'

import { Button } from '@material-ui/core';
import styled from 'styled-components';
import {rem} from 'polished'


const enhance = compose(
  defaultProps({
    data: {},
    order: [],
    A: {
      Button: styled(({ width, ...other }) => (
        <Button {...other} />
      ))`
        && {
          background-color: #000;
          color: white;
          font-weight: 600;
          font-size: ${rem(20)};
          text-transform: uppercase;
          width: ${props => props.width} || inherit;
          padding-top: ${rem(10)};
          padding-bottom: ${rem(10)};
          margin-top: ${rem(15)};
          a {
            color: white;
          }
          &:hover {
            a{
              color: white;
            }
          }
         }
      `,
    }
  })
)

const ProductListUnique = enhance(({
  data,
  order,
  dataCart = {},
  onCartAdd,
  onCartMinus,
  onCartPlus,
  A
}) => {
  return order.map((id) => {
    const { stores } = data[id]
    const price = stores.length ? stores[0].price : null
    return (
      <ProductUnique
        id={id}
        key={id}
        {...data[id]}
      >
        {price && dataCart[id] ? (
          <div style={{marginTop: 20}}>
            <CartProductQuantity
              {...dataCart[id]}
              onMinus={() => onCartMinus({ id })}
              onPlus={() => onCartPlus({ id })}
            />
            <A.Button fullWidth variant="contained"  align="center" color="primary">
              <Link route='cart' >
                <a>go to cart</a>
              </Link>
            </A.Button>            
          </div>
        ) : null}
        {price && !dataCart[id] ? (
          <div>
            <A.Button fullWidth variant="contained" color="primary" onClick={() => onCartAdd({ ...data[id], price })}>
              Add to cart
            </A.Button>
          </div>
        ) : null}
      </ProductUnique>
    )
  })
})

export default ProductListUnique

import gql                  from 'graphql-tag'


export default gql`
query {
	product(
	  filter: [
      {
        name: "isUniqueProposition"
        where: {
          boolean: {
            eq: true
          }
        }        
      }
	  ]
	)
	{
    id
    sku
    name
    description
    
    sweetness
    
    badge {
      id
      name
    }
    
    harvest {
      id
      name
    }
    
    pairs {
      id
      name
    }
    
    regions {
      id
      name
    }
    
    categories {
      id
      name
    }
    
    images {
      id
      name
      image {
        secure_url
        url
      }
    }
    
    stores {
      quantity
      price
      location {
        name
      }
    }    
    
    videos {
      id
      name
      videoLink
    }
    
    isUniqueProposition
  }
}
`

import gql                  from 'graphql-tag'


export default gql`
query product(
  $badgeName: String
  $pagination: Pagination
  $pairList: [String]
  $price: Float
  $sweetness: Float
) {
	product(
    filter: [
      {
        name: "badge.name"
        where: {
          string: {
            eq: $badgeName
          }
        }
      }
      {
        name: "sweetness"
        where: {
          float: {
            lt: $sweetness
          }
        }
      }
      {
        name: "stores.price"
        by: "product"
        where: {
          float: {
            lt: $price
          }
        }
      }
      {
        name: "pairs.name"
        where: {
          string: {
            in: $pairList
          }
        }
      }      
    ]	
	  pagination: $pagination
  ) {
    id
    sku
    name
    description
    
    sweetness
    
    badge {
      id
      name
    }
    
    harvest {
      id
      name
    }
    
    pairs {
      id
      name
    }
    
    regions {
      id
      name
    }
    
    categories {
      id
      name
    }
    
    images {
      id
      name
      image {
        secure_url
        url
      }
    }
    
    stores {
      quantity
      price
      location {
        name
      }
    }    
    
    videos {
      id
      name
      videoLink
    }
    
    isUniqueProposition
  }
}
`

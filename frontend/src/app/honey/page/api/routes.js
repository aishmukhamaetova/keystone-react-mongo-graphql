const routes = require('next-routes')

module.exports = routes()

.add({ name: 'b2b',  pattern: '/b2b',    page: 'b2b', })
.add({ name: 'subscriptions',  pattern: '/subscriptions',    page: 'subscriptions', })
.add({ name: 'about',  pattern: '/about',    page: 'about', })
.add({ name: 'cart',      pattern: '/cart',        page: 'cart', })


.add({ name: `profile`,   pattern: `/profile`,     page: `profile` })

.add({ name: `index`,   pattern: `/`,     page: `index` })

.add({ name: 'posts',     pattern: '/posts',       page: 'posts', })
.add({ name: 'post',      pattern: '/post/:id',    page: 'post', })

.add({ name: 'products',  pattern: '/products',    page: 'products', })
.add({ name: 'product',   pattern: '/product/:id', page: 'product', })

.add({ name: `regionMap`, pattern: `/regionMap/:id`, page: `regionMap`, })

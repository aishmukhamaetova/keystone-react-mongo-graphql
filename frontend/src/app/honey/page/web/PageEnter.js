import React, { Component } from 'react'


import SectionPageFooter    from 'app/default/pageFooter/web/section/SectionPageFooter'
import SectionPageHeader    from 'app/default/pageHeader/web/section/SectionPageHeader'
import SectionAuth          from 'box/auth/manager/web/section/SectionAuth'
import SectionNotify        from 'box/notify/manager/web/section/SectionNotify'
import Page                 from 'evoke-me/page/web/Page'

import theme                from '../../theme/api/themeWeb'


const removeHashFromBuggyFacebook = () => {
  try {
    if (location.hash = '#_=_') {
      window.history.replaceState("", document.title, window.location.pathname + window.location.search)
    }
  } catch (err) {}
}

const PageEnter = () => {
  removeHashFromBuggyFacebook()

  return (
    <Page hasFooter>
      <SectionNotify theme={theme.default} />
      <SectionPageHeader theme={theme.default} />
      <SectionAuth />
      <SectionPageFooter theme={theme.default} />
    </Page>
  )
}

export default PageEnter

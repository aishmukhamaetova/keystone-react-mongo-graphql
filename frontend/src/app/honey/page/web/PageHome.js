import React, { Component } from 'react'
import { StickyContainer, Sticky } from 'react-sticky';
import compose              from 'recompose/compose'
import defaultProps         from 'recompose/defaultProps'


import SectionPageFooter    from 'app/default/pageFooter/web/section/SectionPageFooter'
import SectionPageHeader    from 'app/default/pageHeader/web/section/SectionPageHeader'
import SectionPageSubHeader    from 'app/default/pageHeader/web/section/SectionPageSubHeader'
import SectionNotify        from 'box/notify/manager/web/section/SectionNotify'
import Page                 from 'evoke-me/page/web/Page'

import theme                from '../../theme/api/themeWeb'
import SectionProductUnique from '../../product/web/section/SectionProductUnique'
import SectionBlogTeaser    from '../../post/web/section/SectionBlogTeaser'
import SectionBanerArea     from '../../promotion/web/section/SectionBannerArea'
import SectionBar           from '../../promotion/web/section/SectionBar'
import SectionSlider        from '../../promotion/web/section/SectionSlider'
import SectionPressQuotes   from '../../pressQuote/web/section/SectionPressQuotes'
import SectionSocialBuzz    from '../../socialBuzz/web/section/SectionSocialBuzz'
import SectionTestimonial    from '../../testimonial/web/section/SectionTestimonial'
import SectionStripe        from 'app/default/pageHeader/web/section/SectionStripe.js'

import { CssBaseline, MuiThemeProvider, createMuiTheme} from '@material-ui/core';
import styled from 'styled-components'


const mui = createMuiTheme({
  typography: {
    useNextVariants: true,
    fontFamily: [
      'Open Sans',
      'sans-serif',
    ].join(','),
    fontSize: 16,//dont change it
    htmlFontSize: 10,//font-size: 62.5%; /* 62.5% of 16px = 10px */
  },
  contrastThreshold: 3,
  tonalOffset: 0.2,
  palette: {
    primary: {
      main: '#080808',
      contrastText: '#fff'
    },
    secondary: {
      main: '#f5a623',
      contrastText: '#fff'
    },
    error: {
      main: '#f44336',
      contrastText: '#fff',   
    },
    action: {
      hover: '#fff',
      hoverOpacity: 0.08
    }
  },
  fontWeightMedium: 500,
  button: {
    borderRadius: 0,
  },
  overrides: {
    MuiButton: { // Name of the component ⚛️ / style sheet
      root: { // Name of the rule
        borderRadius: 0
      },
    },

    
  },

});

const enhance = compose(
  defaultProps({
    A: {
      SubHeaderWrapper: styled.div`
        && {
          z-index: 10;
          display: block;
        
        }
      `,    
    }
  }),
)


const PageHome = enhance(({A}) => {
  return (
    <MuiThemeProvider theme={mui}>
      <CssBaseline />
      <Page hasFooter={true}>
        <SectionNotify />
        <SectionPageHeader theme={theme.default} />
        <StickyContainer>   

          <Sticky>

            {({style}) => (
              <A.SubHeaderWrapper  style={style} ><SectionPageSubHeader theme={theme.default} /></A.SubHeaderWrapper>
              )}

          </Sticky>

          <SectionStripe type={3}/>
          <SectionSocialBuzz />
          <SectionStripe type={1}/>

          <SectionTestimonial/>
          <SectionStripe type={3}/>

          <SectionPressQuotes />
          <SectionStripe type={3}/>


          <SectionProductUnique />
          <SectionStripe type={2}/>

          <SectionBlogTeaser />
          <SectionStripe type={2}/>

          <SectionBanerArea />
          <SectionStripe type={2}/>

          <SectionBar />
          <SectionStripe type={2}/>

          {/* 
          <SectionSlider /> */}

        </StickyContainer>

        <SectionPageFooter/>

    </Page>
    </MuiThemeProvider>

  )
})

export default PageHome

import React, { Component } from 'react'


import SectionNotify        from 'box/notify/manager/web/section/SectionNotify'
import Page                 from 'evoke-me/page/web/Page'

import theme                from 'app/default/theme/api/themeWeb'
import SectionPageFooter    from 'app/default/pageFooter/web/section/SectionPageFooter'
import SectionPageSubHeader    from 'app/default/pageHeader/web/section/SectionPageSubHeader'

import SectionPost          from 'app/honey/post/web/section/SectionPost'

import { StickyContainer, Sticky } from 'react-sticky';

import compose              from 'recompose/compose'
import defaultProps         from 'recompose/defaultProps'

import styled from 'styled-components'


const enhance = compose(
  defaultProps({
    A: {
      SubHeaderWrapper: styled.div`
        && {
          z-index: 10;
          display: block;
        
        }
      `,    
    }
  }),
)

const PagePost = enhance(({A}) => {
  return (
    <Page>
      <SectionNotify theme={theme.default} />

      <StickyContainer>   

        <Sticky>

          {({style}) => (
            <A.SubHeaderWrapper  style={style} ><SectionPageSubHeader theme={theme.default} /></A.SubHeaderWrapper>
            )}

        </Sticky>
        <SectionPost theme={theme.default} />
      </StickyContainer>
 
      <SectionPageFooter/>

    </Page>
  )
})

export default PagePost


import React, { Component } from 'react'

import SectionNotify        from 'box/notify/manager/web/section/SectionNotify'
import Page                 from 'evoke-me/page/web/Page'

import SectionPageFooter    from 'app/default/pageFooter/web/section/SectionPageFooter'
import SectionPageSubHeader    from 'app/default/pageHeader/web/section/SectionPageSubHeader'

import theme                from '../../theme/api/themeWeb'

import compose              from 'recompose/compose'
import defaultProps         from 'recompose/defaultProps'

import styled from 'styled-components'

import SectionPostList      from '../../post/web/section/SectionPostList'


import { StickyContainer, Sticky } from 'react-sticky';


const enhance = compose(
  defaultProps({
    A: {
      SubHeaderWrapper: styled.div`
        && {
          z-index: 10;
          display: block;
        
        }
      `,    
    }
  }),
)

const PagePostList = enhance(({A}) => {
  return (
    <Page>
      <SectionNotify theme={theme.default} />

      <StickyContainer>   

        <Sticky>

          {({style}) => (
            <A.SubHeaderWrapper  style={style} ><SectionPageSubHeader theme={theme.default} /></A.SubHeaderWrapper>
            )}

        </Sticky>
  
        <SectionPostList theme={theme.default} />
  
      </StickyContainer>

      <SectionPageFooter/>


    </Page>
  )
})

export default PagePostList

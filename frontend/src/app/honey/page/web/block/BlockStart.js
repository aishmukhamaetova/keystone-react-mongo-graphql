import React, { Component } from 'react'
import { withApollo }       from 'react-apollo'
import compose              from 'recompose/compose'
import lifecycle            from 'recompose/lifecycle'


import spaceMenu            from 'app/default/menu/space/spaceMenu'
// import spaceNav             from 'app/default/nav/space/spaceNav'

import spaceAuth            from 'box/auth/manager/space/spaceAuth'
import spaceAuthClassic     from 'box/auth/classic/space/spaceAuthClassic'
import spaceBoxEshopCart    from 'box/eshop/cart/space/spaceBoxEshopCart'
import withSpace            from 'evoke-me/space/all/withSpace'


const enhance = compose(
  withApollo,
  withSpace(spaceAuth),
  withSpace(spaceAuthClassic),
  withSpace(spaceBoxEshopCart),
  withSpace(spaceMenu),
  lifecycle({
    componentDidMount() {
      const { client, fromAuth } = this.props

      fromAuth.restore()

      const { fromAuthClassic } = this.props

      fromAuthClassic.check({}, { client })
      fromAuthClassic.restore()

      const { fromBoxEshopCart } = this.props

      fromBoxEshopCart.restore()

      const { fromMenu } = this.props

      fromMenu.create(
        { name: `Home`, caption: `Home`, path: `/` },
        { orderName: `headerLeft` }
      )

      fromMenu.create(
        { name: `Products`, caption: `Products`, path: `/products` },
        { orderName: `headerLeft` }
      )

      fromMenu.create(
        { name: `Posts`, caption: `Posts`, path: `/posts` },
        { orderName: `headerLeft` }
      )

      fromMenu.create(
        { name: `Profile`, caption: `Profile`, path: `/profile` },
        { orderName: `headerRight` }
      )

      fromMenu.create(
        { name: `Cart`, caption: `Cart`, path: `/cart` },
        { orderName: `headerRight` }
      )

      fromMenu.create(
        { name: `Login`, caption: `Enter`, path: `/enter` },
        { orderName: `headerRight` }
      )

      // const { fromAuthGraphql } = this.props
      //
      // fromAuthGraphql.query(client)
    }
  })
)

export default enhance(() => null)


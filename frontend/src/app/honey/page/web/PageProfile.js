import React, { Component } from 'react'

import SectionNotify        from 'box/notify/manager/web/section/SectionNotify'
import Page                 from 'evoke-me/page/web/Page'

import SectionPageFooter    from 'app/default/pageFooter/web/section/SectionPageFooter'
import SectionPageSubHeader    from 'app/default/pageHeader/web/section/SectionPageSubHeader'

import theme                from '../../theme/api/themeWeb'

import compose              from 'recompose/compose'
import defaultProps         from 'recompose/defaultProps'

import styled from 'styled-components'

import { StickyContainer, Sticky } from 'react-sticky';

import SectionProfile       from 'box/profile/manager/web/section/SectionProfile'

import {rem} from 'polished'

const enhance = compose(
  defaultProps({
    A: {
      SubHeaderWrapper: styled.div`
        && {
          z-index: 10;
          display: block;
        
        }
      `,    
      Container: styled.div`
        && {
          margin: ${rem(50)} auto;
        }
      `,    
    }
  }),
)

const PageProfile = enhance(({A}) => {
  return (
    <Page>
      <SectionNotify theme={theme.default} />

      <StickyContainer>   

        <Sticky>

          {({style}) => (
            <A.SubHeaderWrapper  style={style} ><SectionPageSubHeader theme={theme.default} /></A.SubHeaderWrapper>
            )}

        </Sticky>

        <A.Container>
          <SectionProfile />
        </A.Container>

      </StickyContainer>

      <SectionPageFooter/>


    </Page>
  )
})

export default PageProfile

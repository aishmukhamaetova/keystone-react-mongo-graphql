import boxAuthFacebookLink  from 'box/auth/facebook/api/keystone/rest/server/boxAuthFacebookLink'
import boxAuthFacebookLinkCallback  from 'box/auth/facebook/api/keystone/rest/server/boxAuthFacebookLinkCallback'
import boxAuthFacebookLogin from 'box/auth/facebook/api/keystone/rest/server/boxAuthFacebookLogin'
import boxAuthFacebookLoginCallback from 'box/auth/facebook/api/keystone/rest/server/boxAuthFacebookLoginCallback'

import boxMastercardWebhook from 'box/mastercard/manager/api/keystone/rest/server/boxMastercardWebhook'


const middleware = (app) => {
  app.get('/api/box.auth/facebook/link', boxAuthFacebookLink)
  app.get('/api/box.auth/facebook/linkCallback', boxAuthFacebookLinkCallback)
  app.get('/api/box.auth/facebook/login', boxAuthFacebookLogin)
  app.get('/api/box.auth/facebook/loginCallback', boxAuthFacebookLoginCallback)

  app.post('/api/box.mastercard/webhook', boxMastercardWebhook)
}

export default middleware

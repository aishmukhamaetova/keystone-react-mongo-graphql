import themeWeb             from 'app/default/theme/api/themeWeb'


export default {
  default: {
    ...themeWeb.default,
  }
}

import React, {Component} from 'react'
import {withStyles, Grid, Paper, ButtonBase, Typography, Avatar} from '@material-ui/core';
import {IconStar} from '../../../icons'
import compose from 'recompose/compose'
import defaultProps from 'recompose/defaultProps'
import {rem} from 'polished'
import styled from 'styled-components'

const TypographyItalic = styled(({ size, align, margin, bold, color, ...other }) => (
  <Typography {...other} />
))`
&& {
  color: ${props => props.color || '#fff'};
  font-size: ${props => props.size};
  text-align: ${props => props.align || 'left'};
  line-height: 1.4;
  font-weight: ${props => props.bold || 'normal'};
  font-style: italic;
  margin: ${props => props.margin || '0'};
  padding: ${props => props.padding || '0'};

  a {
    text-decoration: underline;
    color: ${props => props.color || '#fff'};
    font-weight: ${props => props.bold || 'normal'};
    &:hover {
      text-decoration: none;
    }
  }

}
`;

const AvatarUI = styled(({ width, height, margin, ...other }) => (
  <Avatar {...other} />
))`&& {
  width: ${props => props.width || 'auto'};
  height: ${props => props.height || 'auto'};
  margin: ${props => props.margin || 'auto'};
}
`;


const Title = styled(({ color, transform, border, margin, fweight, ...other }) => (
  <Typography {...other} />
))`&& {
  color: ${props => props.color};
  border-bottom: ${props => props.border};
  text-transform: ${props => props.transform || 'inherit'};
  border-color: ${props => props.color};
  margin: ${props => props.margin};
  font-weight: ${props => props.fweight || '700'};
  min-width: auto;
  width: 100%;
  text-align: center;
  font-size: ${rem(35)};

}
`;


const styles = theme => ({
});

const GridCustom = styled(({ background, margin, ...other }) => (
  <Grid {...other} />
))`
&& {
  background: ${props => props.background};
  margin: ${props => props.margin};
  padding: ${props => props.padding};
}
`;

const enhance = compose(
  defaultProps({
    A: {
      PaperCustom: styled(Paper)`
      && {
        box-shadow: none;
        background: none;
        padding: ${rem(40)} 0 ${rem(20)};

      }
    `,

    }
  })
);

const Testimonial = enhance(({
  A
}) => {
  return (
    <Grid container alignItems="stretch" direction="row" justify="center" spacing={0}>

      <Grid container spacing={0} justify="center">
        <Grid item>
          <Title size="1.875em" color="#000" transform="uppercase" justify="center" margin="0 0 1.5rem 0">
            Testimonial
          </Title>
        </Grid>
      </Grid>

      <GridCustom background="#ececec" container spacing={0} justify="center" alignItems="stretch" direction="row">
        <Grid item xs={10} sm={8}>

          <A.PaperCustom square={true}>
            <Grid container spacing={40} justify="center" alignItems="center">
              <Grid item>
                <AvatarUI width="150px" height="150px" src="https://via.placeholder.com/150" alt="avatar"/>
              </Grid>
              <Grid item xs={12} sm container justify="center">
                <TypographyItalic color="#000" bold="600" align="center" size="1.125rem" variant="body1" gutterBottom>
                  Honey, just allow me one more chance to get along with you Honey, just allow me one more chance
                  Ah'll do anything with you. Well, I'm a-walkin' down the road with my head in my hand I'm lookin'
                  for a woman needs a worried man Just-a one kind favor I ask you Allow me just-a one more chance.
                </TypographyItalic>
                <TypographyItalic color="#000" margin="1rem 0 0;" bold="300" align="center" size="1.125rem"
                                  variant="body1" gutterBottom>
                  &mdash;&nbsp;Bob Dylan &nbsp;
                  <ButtonBase>
                    {Array(4).fill(null).map((i, idx) => <span key={idx}><IconStar></IconStar></span>)}
                  </ButtonBase>
                </TypographyItalic>
              </Grid>
            </Grid>

          </A.PaperCustom>
        </Grid>
      </GridCustom>

    </Grid>
  )
})

export default withStyles(styles)(Testimonial)
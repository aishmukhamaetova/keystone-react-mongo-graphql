const { gql } = require('apollo-server-express')


import boxAuthClassicList   from 'box/auth/classic/api/keystone/graphql/server/list'
import boxAuthFacebookList  from 'box/auth/facebook/api/keystone/graphql/server/list'
import boxEshopOrderList    from 'box/eshop/order/api/keystone/graphql/server/list'


const list = [
  ...boxAuthClassicList,
  ...boxAuthFacebookList,
  ...boxEshopOrderList,
]

exports = module.exports = gql`
  scalar DateTime

  type ErrorProp {
    name: String
    value: String
  }

  type Error {
    type: String
    code: Int
    message: String
    props: [ErrorProp]
  }  

  input WhereInt {
    eq: Int
    gt: Int
    lt: Int
    in: [Int]
    lte: Int
    ne: Int
    nin: Int
  }
  
  input WhereFloat {
    eq: Float
    gt: Float
    lt: Float
    in: [Float]
    lte: Float
    ne: Float
    nin: Float
  } 

  input WhereString {
    eq: String
    gt: String
    lt: String
    in: [String]
    lte: String
    ne: String
    nin: String
  }
  
  input WhereBoolean {
    eq: Boolean
    gt: Boolean
    lt: Boolean
    in: [Boolean]
    lte: Boolean
    ne: Boolean
    nin: Boolean
  }
  
  input Where {
    int: WhereInt
    float: WhereFloat
    string: WhereString
    boolean: WhereBoolean
  }
  
  input Filter {
    name: String
    by: String
    where: Where
  }  
  
  input Pagination {
    index: Int
    size: Int
  }
  
  type Content {
    id: ID
    name: String
    text: String
    content: String
    language: Language
  }
  
  input ContentInput {
    language: Where
  }
  
  type CloudinaryImage {
    secure_url: String
    url: String
    resource_type: String
    format: String
    height: Float
    width: Float
    signature: String
    version: Float    
  }
  
  type Image {
    id: ID
    name: String
    image: CloudinaryImage
    count: Float
  }
  
  type Language {
    id: ID
    name: String
  }

  type Location {
    id: ID
    name: String
    currency: LocationCurrency 
  }

  type LocationCurrency {
    id: ID
    name: String
  }
  
  type Post {
    id: ID
    title: String
    state: String
    author: User
    publishedDate: DateTime
    content: PostContent
    
    categories: [PostCategory]
    testimonials: [Testimonial]
        
    images: [Image]
    videos: [Video]
    
    isPinnedToIndex: Boolean
  }

  input PostInput {
    type: Where
    isPinnedToIndex: Where
  }
  
  type PostCategory {
    id: ID
    name: String
  }
  
  type PostContent {
    brief: String
    extended: String
  }
  
  type Product {
    id: ID
    sku: String
    name: String
    description: String
    
    badge: [ProductBadge]
    categories: [ProductCategory]
    harvest: [ProductHarvest]
    pairs: [ProductPair]
    regions: [ProductRegion]
    stores: [ProductStore]
    sweetness: Int
    testimonials: [Testimonial]
    weight: Int
    
    images: [Image]
    videos: [Video]   
   
    isUniqueProposition: Boolean
  }
  
  input ProductInput {
    id: ID
    isUniqueProposition: Where
  }

  type ProductBadge {
    id: ID
    name: String
  }
    
  type ProductCategory {
    id: ID
    name: String
  }
  
  type ProductHarvest {
    id: ID
    name: String
  }
  
  type ProductPair {
    id: ID
    name: String
  }
  
  type ProductRegion {
    id: ID
    name: String
    lat: Float
    lon: Float
  }
  
  type ProductStore {
    id: ID
    product: Product
    quantity: Int
    price: Float
    
    location: Location
  }
  
  type Promotion {
    id: ID
    timeBegin: DateTime
    timeEnd: DateTime
    type: String
    location: Location
    blocks: [PromotionBlock]
  }
  
  input PromotionInput {
    type: Where
  }
  
  type PromotionBlock {
    id: ID
    content: String
    promotion: Promotion
    product: Product
  }
  
  type PressQuote {
    id: ID
    name: String
    logo: Image
    description: String
    link: String
  }
  
  type SocialBuzz {
    id: ID
    name: String
    linkInstagram: String
  }
  
  type Testimonial {
    id: ID
    text: String
    rating: Int
  }
  
  type Name {
    first: String
    last: String
  }
  
  type User {
    id: ID
    name: Name
    email: String
  }
  
  type Video {
    id: ID
    name: String
    videoLink: String 
  }

  ${list.map(({ type }) => type).join('\n')}

  type Query {
    ${list.map(({ query }) => query).join('\n')}
  
    image: [Image]
    location: [Location]
    locationCurrency: [LocationCurrency]
    
    content(
      languageName: String
      where: ContentInput
    ): [Content]
    
    post(
      id: ID    
      where: PostInput
    ): [Post]
    
    productRegion(
      id: ID
    ): [ProductRegion]
    
    product(
      id: ID    
      where: ProductInput
      pagination: Pagination
      
      filter: [Filter]
    ): [Product]
    
    pressQuote: [PressQuote]
    
    productCategory: [ProductCategory]
    
    promotion(
      id: ID
      where: PromotionInput
    ): [Promotion]
    
    socialBuzz: [SocialBuzz]
    
    me: User
  }
  
  ${require('box/auth/classic/api/keystone/graphql/server/typeDefs').default}
    
  type Mutation {
    ${list.map(({ mutation }) => mutation).join('\n')}
    
    regionUpdate(
      id: ID
      lat: Float
      lon: Float
    ): Boolean  
  
    ${require('box/auth/classic/api/keystone/graphql/server/mutationDefs').default}
  }
`

import getConfig            from 'next/config'

import ApolloClient         from 'apollo-client'
import { createHttpLink }   from 'apollo-link-http'
import { InMemoryCache }    from 'apollo-cache-inmemory'

import fetch                from 'node-fetch'


const { publicRuntimeConfig } = getConfig()
const { CLIENT_HOST } = publicRuntimeConfig

export default (state) => {
  return new ApolloClient({
    ssrMode: true,
    link: createHttpLink({
      uri: `${ CLIENT_HOST }/api/graphql`,
      credentials: 'same-origin',
      // headers: {
      //   cookie: req.header('Cookie'),
      // },
      fetch,
    }),
    cache: new InMemoryCache().restore(state || {})
  })
}

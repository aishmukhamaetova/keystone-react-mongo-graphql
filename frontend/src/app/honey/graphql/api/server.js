import keystone             from 'keystone'
import { ApolloServer }     from 'apollo-server-express'
import { GraphQLScalarType } from 'graphql'
import { Kind }             from 'graphql/language'


import typeDefs             from './typeDefs'

import boxAuthClassicList   from 'box/auth/classic/api/keystone/graphql/server/list'
import boxAuthFacebookList  from 'box/auth/facebook/api/keystone/graphql/server/list'
import boxEshopOrderList    from 'box/eshop/order/api/keystone/graphql/server/list'


const list = [
  ...boxAuthClassicList,
  ...boxAuthFacebookList,
  ...boxEshopOrderList,
]

const populateRelated = (model, fieldList) => new Promise((resolve) => {
  keystone.populateRelated(model, fieldList, () => {
    resolve(model)
  })
})

const processWhere = (where) => {
  const map = {
    in: `$in`,
    eq: `$eq`,
    gt: `$gt`,
    lt: `$lt`,
    lte: `$lte`,
    ne: `$ne`,
    nin: `$nin`,
  }

  const rename = (doc = {}) => Object.keys(map).reduce((result, name) => {
    if (doc[name]) {
      result[map[name]] = doc[name]
    }
    return result
  }, {})

  const fields = Object.keys(where)

  return fields.reduce((result, name) => {
    result[name] = {
      ...(result.name || {}),
      ...rename(where[name].int),
      ...rename(where[name].string),
      ...rename(where[name].boolean),
    }
    return result
  }, {})
}

const processFilter = (filter = []) => {
  const map = {
    in: `$in`,
    eq: `$eq`,
    gt: `$gt`,
    lt: `$lt`,
    lte: `$lte`,
    ne: `$ne`,
    nin: `$nin`,
  }

  const rename = (doc = {}) => Object.keys(map).reduce((result, name) => {
    if (doc[name]) {
      result[map[name]] = doc[name]
    }
    return result
  }, {})

  const entityMap = {
    default: {},
  }

  const byMap = {}

  try {
    for (let { by, name, where } of filter) {
      const [entity, fieldName] = name.split(`.`)

      if (fieldName) {
        entityMap[entity] = entityMap[entity] || {}

        const whereAcc = {
          ...rename(where.boolean),
          ...rename(where.int),
          ...rename(where.float),
          ...rename(where.string),
        }

        if (by) byMap[entity] = by

        if (Object.keys(whereAcc).length) {
          entityMap[entity][fieldName] = whereAcc
        }
      } else {
        const whereAcc = {
          ...rename(where.boolean),
          ...rename(where.int),
          ...rename(where.float),
          ...rename(where.string),
        }

        if (Object.keys(whereAcc).length) {
          entityMap.default[entity] = whereAcc
        }
      }
    }
  } catch (err) {
    console.log(`Error`, err)
  }

  return [entityMap, byMap]
}

const resolvers = {
  DateTime: new GraphQLScalarType({
    name: 'DateTime',
    description: 'Date custom scalar type',
    parseValue(value) {
      return new Date(value)
    },
    serialize(value) {
      return value.getTime()
    },
    parseLiteral(ast) {
      if (ast.kind === Kind.INT) {
        return parseInt(ast.value, 10)
      }
      return null
    },
  }),

  Mutation: {
    ...list.reduce((acc, { mutationMap }) => ({ ...acc, ...mutationMap }), {}),

    ...require('box/auth/classic/api/keystone/graphql/server/mutationMap').default,

    regionUpdate: async (obj, args, { keystone }, info) => {
      try {
        const { id, lat, lon } = args
        const Region = keystone.list(`ProductRegion`)

        const region = await Region.model.findOne({ _id: id })

        region.map = [lat, lon]
        await region.save()

        console.log(region)

        return true
      } catch (err) {
        console.log(`regionUpdate`, `ERROR`, err)
      }
    },
  },

  Query: {
    ...list.reduce((acc, { queryMap }) => ({ ...acc, ...queryMap }), {}),

    content: async (obj, args, { keystone }, info) => {
      const Content = keystone.list('Content')
      const Language = keystone.list('Language')

      const { id, languageName, where = {} } = args

      const whereFinal = {
        ...processWhere(where)
      }

      if (languageName) {
        const language = await Language.model.find({ name: languageName })

        if (language.length && language[0]._id) {
          whereFinal.language = language[0]._id
        }
      }

      if (id) whereFinal._id = id

      return await Content.model
        .find(whereFinal)
        .populate('language')
    },

    image: async (obj, args, { keystone }, info) => {
      const Image = keystone.list('Image')

      return await Image.model.find()
    },

    location: async (obj, args, { keystone }, info) => {
      const Location = keystone.list('Location')

      return await Location.model
        .find()
        .populate('currency')
    },

    locationCurrency: async (obj, args, { keystone }, info) => {
      const LocationCurrency = keystone.list('LocationCurrency')

      return await LocationCurrency.model.find()
    },

    me: async (obj, args, { user }, info) => {
      if (user) {
        return user
      }

      return null
    },

    post: async (obj, args, { keystone }, info) => {
      const Post = keystone.list('Post')

      const { id, where = {} } = args

      const whereFinal = {
        ...processWhere(where)
      }

      if (id) whereFinal._id = id

      const populateRelatedList = [
        {
          path: `testimonials`,
        }
      ]

      return await populateRelated(
        await Post.model
          .find(whereFinal)
          .populate(`categories images videos author`),
        populateRelatedList,
      )
    },

    pressQuote: async (obj, args, { keystone }, info) => {
      const PressQuote = keystone.list('PressQuote')

      const { id, where = {} } = args

      const whereFinal = {
        ...processWhere(where)
      }

      if (id) whereFinal._id = id

      return await await PressQuote.model
        .find(whereFinal)
        .populate(`logo`)
    },

    product: async (obj, args, { keystone }, info) => {
      const referenceMap = {
        'badge': keystone.list(`ProductBadge`),
        'pairs': keystone.list(`ProductPair`),
        'stores': keystone.list(`ProductStore`),
      }

      const populate = `badge harvest location pairs regions categories images videos`
      const populateRelatedList = [
        {
          path: `stores`,
          populate: [{
            path: `location`,
          }]
        },
        {
          path: `testimonials`,
        }
      ]

      const Entity = keystone.list('Product')

      const {
        id,
        filter,
        pagination = {},
      } = args

      const {
        index,
        size,
      } = pagination

      const [filterFinal, byMap] = processFilter(filter)

      const whereFinal = filterFinal.default

      if (id) whereFinal._id = id

      console.log(`filterFinal`, filterFinal)

      for (let filterItem in filterFinal) {
        if (filterItem === `default`) continue

        if (referenceMap[filterItem] && Object.keys(filterFinal[filterItem]).length) {
          const list = await referenceMap[filterItem].model.find(filterFinal[filterItem])

          // console.log(`LIST`, list)
          // console.log(`BY`, byMap)

          if (byMap[filterItem]) {
            const by = byMap[filterItem]
            whereFinal._id = {
              '$in': list.map((doc) => doc[by])
            }
          } else {
            whereFinal[filterItem] = {
              '$in': list,
            }
          }
        }
      }

      console.log(`whereFinal`, whereFinal)

      return await populateRelated(
        await Entity.model
          .find(whereFinal)
          .skip(index * size)
          .limit(size)
          .populate(populate),
        populateRelatedList,
      )
    },

    productCategory: async (obj, args, { keystone }, info) => {
      const ProductCategory = keystone.list('ProductCategory')

      return await ProductCategory.model.find()
    },

    productRegion: async (obj, args, { keystone }, info) => {
      try {
        const ProductRegion = keystone.list('ProductRegion')

        const { id, where = {} } = args

        const whereFinal = {
          ...processWhere(where)
        }

        if (id) whereFinal._id = id

        const [doc] = await ProductRegion.model.find(whereFinal)

        console.log(`DOC`, doc)

        const { _id, name } = doc
        const [lat, lon] = doc.map || []

        return [{
          id: String(_id),
          name,
          lat,
          lon,
        }]
      } catch (err) {
        console.log(`ERR`, err)
      }
    },

    promotion: async (obj, args, { keystone }, info) => {
      const Promotion = keystone.list('Promotion')

      const { id, where = {} } = args

      const populateRelatedList = [
        {
          path: `blocks`,
          populate: [{
            path: `product`,
            populate: [
              { path: `images`, },
              { path: `videos`, },
            ]
          }]
        }
      ]

      const whereFinal = {
        ...processWhere(where)
      }

      if (id) whereFinal._id = id

      return await populateRelated(
        await Promotion.model
          .find(whereFinal)
          .populate('location'),
        populateRelatedList,
      )
    },

    socialBuzz: async (obj, args, { keystone }, info) => {
      const SocialBuzz = keystone.list('SocialBuzz')

      const { id, where = {} } = args

      const whereFinal = {
        ...processWhere(where)
      }

      if (id) whereFinal._id = id

      return await await SocialBuzz.model
        .find(whereFinal)
    },
  }
}

export default ({ app, middleware }) => {
  const server = new ApolloServer({
    typeDefs,
    resolvers,
    context: ({ req, res }) => {
      return {
        keystone,
        user: res.locals.user,
        req,
        res,
      }
    }
  })

  server.applyMiddleware({ app, path: `/api/graphql`, })
}

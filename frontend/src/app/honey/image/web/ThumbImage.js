import React, { Component } from 'react'

import compose from 'recompose/compose'
import defaultProps         from 'recompose/defaultProps'

import {Grid, ButtonBase} from '@material-ui/core';
import {rem} from 'polished'
import styled from 'styled-components'

const map = {
  sm: 600,
  md: 960,
  lg: 1280,
  xl: 1920,
}

const mediaGrid = (type) => () => `@media (max-width: ${map[type] - 1}px)`

const enhance = compose(
  defaultProps({
    A: {
      Thumb: styled.div`
      && {
        background-position: center center;
        background-repeat: no-repeat;
        background-color: none;
        background-size: 100% auto;
        width: ${rem(90)};
        height: ${rem(90)};
        margin: ${rem(10)};
      }
    `,
    }

  }),
)


const ThumbImage = enhance(({ setActiveMedia, image = {}, name = '', children, bgcolor = '#efefee', A, height}) => {
  const { secure_url, url } = image
  const style = {
    backgroundImage: secure_url ? 'url(' + secure_url + ')' : 'none',
    backgroundColor: bgcolor
  };

  return (
    <A.Thumb style={style} />
  )
})


export default ThumbImage

import React from 'react';
import Icon from '@material-ui/core/Icon';
import styled from 'styled-components'
import {rem} from 'polished'

const IconEmojiStyled = styled(({ fontSize,margin, ...other }) => (
   <Icon {...other} />
 ))`
   && {
     font-size: inherit;
     margin: ${rem(7)};
     margin-top: ${rem(18)};
   }
 `;



class Emojis extends React.Component {
   render() {
      return (
      <IconEmojiStyled  color="secondary">
       🍷 🍗 🥩 🍓
      </IconEmojiStyled>
      );
   }
}

export default Emojis
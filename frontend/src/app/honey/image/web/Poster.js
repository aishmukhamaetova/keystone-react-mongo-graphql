import React, { Component } from 'react'

import compose from 'recompose/compose'
import defaultProps         from 'recompose/defaultProps'

import {Grid} from '@material-ui/core';
import {rem} from 'polished'
import styled from 'styled-components'

const map = {
  sm: 600,
  md: 960,
  lg: 1280,
  xl: 1920,
}

const mediaGrid = (type) => () => `@media (max-width: ${map[type] - 1}px)`

const enhance = compose(
  defaultProps({
    A: {
      Poster: styled(Grid)`
      && {
        background-position: center center;
        background-repeat: no-repeat
        background-color: #efefee;
        background-size: 150% auto;
        text-align: center;
        
        height: ${rem(600)}

        ${mediaGrid(`xl`)}{//lg d
          background-size: 150% auto;
          height: ${rem(600)}
        }
        ${mediaGrid(`lg`)}{//sm d
          background-size: 110% auto;
          height: ${rem(600)}
        }
        ${mediaGrid(`md`)}{//tablet
          background-size: 100% auto;
          height: auto;
        }
        ${mediaGrid(`sm`)}{//phone
          background-size: auto 100%;
          height: auto;
        }

      }
    `,
    }

  }),
)


const Poster = enhance(({ image = {}, name = '', children, bgcolor = '#efefee', A, height}) => {
  const { secure_url, url } = image
  const style = {
    backgroundImage: secure_url ? 'url(' + secure_url + ')' : 'none',
    backgroundColor: bgcolor
  };

  return (
    <A.Poster height={height} container direction={'row'} justify={'center'} alignItems={'center'} spacing={0} style={style}  >
      {children}
    </A.Poster>
  )
})


export default Poster

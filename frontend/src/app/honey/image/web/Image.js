import React, { Component } from 'react'


export default (props) => {
  const { image = {}, name=''} = props
  const { secure_url, url } = image
  return (
    secure_url ? <img src={secure_url} alt={name} /> : null
  )
}

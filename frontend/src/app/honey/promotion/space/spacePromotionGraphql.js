import config               from '../../config/api/config'
import QUERY_BANNER_AREA    from '../api/graphql/queryBannerArea'
import QUERY_BAR            from '../api/graphql/queryPromotionalBar'
import QUERY_SLIDER         from '../api/graphql/querySlider'
import spacePromotion       from './spacePromotion'


export default [
  config.namespace,
  `promotionGraphql`,
  [
    spacePromotion,
  ],
  () => ({
    isLoading: false,
  }),
  {
    queryBannerArea: (getState, setState, methods, { fromPromotion }) => function* (client) {
      try {
        if (client) {
          yield setState({ isLoading: true })

          const { data } = yield client.query({ query: QUERY_BANNER_AREA })

          yield fromPromotion.orderClear(`bannerArea`)
          for (let doc of data.promotion) {
            yield fromPromotion.create(doc, { orderName: `bannerArea` })
          }

          yield setState({ isLoading: false })
        }
      } catch (err) {
        yield setState({ isLoading: false })

        console.error(err)
      }
    },
    queryPromotionalBar: (getState, setState, methods, { fromPromotion }) => function* (client) {
      try {
        if (client) {
          yield setState({ isLoading: true })

          const { data } = yield client.query({ query: QUERY_BAR })

          yield fromPromotion.orderClear(`bar`)
          for (let doc of data.promotion) {
            yield fromPromotion.create(doc, { orderName: `bar` })
          }

          yield setState({ isLoading: false })
        }
      } catch (err) {
        yield setState({ isLoading: false })

        console.error(err)
      }
    },
    querySlider: (getState, setState, methods, { fromPromotion }) => function* (client) {
      try {
        if (client) {
          yield setState({ isLoading: true })

          const { data } = yield client.query({ query: QUERY_SLIDER })

          yield fromPromotion.orderClear(`slider`)
          for (let doc of data.promotion) {
            yield fromPromotion.create(doc, { orderName: `slider` })
          }

          yield setState({ isLoading: false })
        }
      } catch (err) {
        yield setState({ isLoading: false })

        console.error(err)
      }
    }
  }
]

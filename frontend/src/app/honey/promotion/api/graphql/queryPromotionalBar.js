import gql                  from 'graphql-tag'


export default gql`
query {
  promotion (
    where: {
      type: {
        string: {
          eq: "bar"
        }
      }
    }
  ) {
    id
    timeBegin
    timeEnd
    type
    location {
      id
      name
    }
    blocks {
      id
      content
      product {
        id
        name
        
        images {
          id
          image {
            secure_url
          }
        }
      }
    }
  }
}
`

import React, { Component } from 'react'
import { withApollo }       from 'react-apollo'
import { compose }          from 'recompose'
import { lifecycle }        from 'recompose'

import { Segment }          from 'semantic-ui-react'
import { Header }           from 'semantic-ui-react'
import { Icon }             from 'semantic-ui-react'


import withSpace            from 'evoke-me/space/all/withSpace'

import spacePromotion       from '../../space/spacePromotion'
import spacePromotionGraphql from '../../space/spacePromotionGraphql'
import PromotionList        from '../PromotionList'


const enhance = compose(
  withApollo,
  withSpace(spacePromotion),
  withSpace(spacePromotionGraphql),
  lifecycle({
    componentDidMount() {
      const { client, fromPromotionGraphql } = this.props
      const { fromPromotion } = this.props
      const { orderMap } = fromPromotion

      if (!orderMap.slider) {
        fromPromotionGraphql.querySlider(client)
      }
    }
  })
)

const BlockSlider = enhance(({
  fromPromotion, fromPromotionGraphql,
}) => {
  const { data, orderMap } = fromPromotion
  const { isLoading } = fromPromotionGraphql

  if (!orderMap.slider || !orderMap.slider.length) {
    return (
      <Segment placeholder loading={ isLoading }>
        <Header icon>
          <Icon name='th' />
          Slider is empty
        </Header>
      </Segment>
    )
  }

  return (
    <PromotionList
      data={data}
      order={orderMap.slider}
    />
  )
})

export default BlockSlider

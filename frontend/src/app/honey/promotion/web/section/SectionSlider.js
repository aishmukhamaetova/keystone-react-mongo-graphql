import React, { Component } from 'react'
import { Container }        from 'react-grid-system'
import { Col }              from 'react-grid-system'
import { Row }              from 'react-grid-system'


import BlockSlider          from '../block/BlockSlider'


const SectionSlider = () => {
  return (
    <BlockSlider />
    )
}

export default SectionSlider

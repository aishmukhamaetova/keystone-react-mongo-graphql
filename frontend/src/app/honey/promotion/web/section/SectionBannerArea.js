import React, { Component } from 'react'
import { Container }        from 'react-grid-system'
import { Col }              from 'react-grid-system'
import { Row }              from 'react-grid-system'


import BlockBannerArea      from '../block/BlockBannerArea'


const SectionBannerArea = () => {
  return (
    <BlockBannerArea />
    )
}

export default SectionBannerArea

import React, { Component } from 'react'
import { Container }        from 'react-grid-system'
import { Col }              from 'react-grid-system'
import { Row }              from 'react-grid-system'


import BlockBar  from '../block/BlockBar'


const SectionBar = () => {
  return (
    <BlockBar />
  )
}

export default SectionBar

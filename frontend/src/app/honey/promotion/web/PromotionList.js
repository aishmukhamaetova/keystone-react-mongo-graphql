import React, { Component } from 'react'
import { compose }          from 'recompose'
import { defaultProps }     from 'recompose'


import Promotion              from './Promotion'


const enhance = compose(
  defaultProps({
    data: {},
    order: [],
  })
)

const PromotionList = ({ data, order, type }) => {
  return order.map((id) => (
    <Promotion
      type={type}
      key={id}
      {...data[id]}
    />
  ))
}

export default PromotionList

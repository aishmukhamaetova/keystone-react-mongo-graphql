import React, { Component } from 'react'
import { compose }          from 'recompose'


import PromotionBlockBannerAreaList   from '../../promotionBlock/web/PromotionBlockBannerAreaList'
import PromotionBlockBarList   from '../../promotionBlock/web/PromotionBlockBarList'


const enhance = compose()

const Promotion = enhance(({
  blocks = [], type
}) => {
  const data = blocks.reduce((result, doc) => {
    result[doc.id] = doc
    return result
  }, {})

  const order = Object.keys(data)
  return (
    type === 'bar' ? <PromotionBlockBarList 
      data={data}
      order={order}
    />  :  
    <PromotionBlockBannerAreaList
      data={data}
      order={order}
    />
  )
})

export default Promotion

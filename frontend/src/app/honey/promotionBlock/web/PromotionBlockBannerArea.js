import React, { Component } from 'react'
import { compose }          from 'recompose'
import ProductPromotionalBannerArea   from '../../product/web/ProductPromotionalBannerArea'
import styled from 'styled-components'


const enhance = compose()

const Root = styled.div`
  text-align: center;
  display:block;
`

const PromotionBlock = enhance(({
  content, product
}) => {
  return (
    <Root>
      {product ? <ProductPromotionalBannerArea {...product} /> : null} 
      <div dangerouslySetInnerHTML={{ __html: content }} />
    </Root>
  )
})

export default PromotionBlock

import React, { Component } from 'react'
import { compose }          from 'recompose'
import ProductPromotionalBar   from '../../product/web/ProductPromotionalBar'
import styled from 'styled-components'


const enhance = compose()


const PromotionBlock = enhance(({
  content, product={}
}) => {
  return (
    <ProductPromotionalBar {...product} content={content}>Explore</ProductPromotionalBar>
    )
})

export default PromotionBlock

import React, { Component } from 'react'

import compose from 'recompose/compose'
import withStateHandlers    from 'recompose/withStateHandlers'
import defaultProps         from 'recompose/defaultProps'
import PromotionBlockBar       from './PromotionBlockBar'
import styled from 'styled-components'
import {rem} from 'polished'


var map = {
  sm: 600,
  md: 960,
  lg: 1280,
  xl: 1920
};

var mediaGrid = function mediaGrid(type) {
  return function () {
    return "@media (max-width: ".concat(map[type] - 1, "px)");
  };
};

import {withStyles, Grid, Paper, ButtonBase, Typography} from '@material-ui/core';


const enhance = compose(
  withStateHandlers(
    () => ({
    }),
      {
      },
    ),

  defaultProps({
    data: {},
    order: [],

    A: {
      Title: styled(({ color, transform, border, margin, fweight, ...other }) => (
        <Typography {...other} />
      ))`&& {
        color: ${props => props.color};
        border-bottom: ${props => props.border};
        text-transform: ${props => props.transform || 'inherit'};
        border-color: ${props => props.color};
        margin: ${props => props.margin};
        font-weight: ${props => props.fweight || '700'};
        min-width: auto;
        width: 100%;
        font-size: ${props => props.size};
        text-align: center;
        &:hover {
          background: none;
        },
      
      }
      `,
      TagLine: styled(({ color, transform, border, margin, fweight, ...other }) => (
        <Typography {...other} />
      ))`
      && {
        font-size: ${props => props.size || '1rem'};
        font-weight: ${props => props.fweight || 400};
        color: ${props => props.color};
        margin: ${props => props.margin};
        text-align: center;
        font-style: normal;
        line-height: normal; 
      }
      `,
      
    }
  }),

)

const PromotionBlockBarList = enhance(({ data, order, A }) => {
  return (
    <Grid container spacing={0} justify="center" alignItems="stretch" direction="row">

      <Grid item md={7} xs={12}>
        <A.Title size="1.875rem" color="#000" transform="uppercase" justify="center" margin="0 0 3.875rem 0">
          SPECIAL OCCASIONS
        </A.Title>
        <A.TagLine color="#000" size="2rem" fweight="300" margin="0 0 2.688rem 0">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas fringilla massa vitae nisl scelerisque
          lobortis.
        </A.TagLine>
      </Grid>

      <Grid item xs={11} sm={11} md={10}>
        <Grid container alignItems="stretch" direction="row" justify="center" spacing={24}>

          {order.map((id, index) => (
            <PromotionBlockBar
            key={index}
            {...data[id]}
            />
          ))}           

        </Grid>
      </Grid>

    
    </Grid>



  )
}
)
export default PromotionBlockBarList

import React, { Component } from 'react'

import compose from 'recompose/compose'
import withStateHandlers    from 'recompose/withStateHandlers'
import defaultProps         from 'recompose/defaultProps'

import PromotionBlockBannerArea       from './PromotionBlockBannerArea'
import Carousel from './Carousel'

import {Grid, Hidden} from '@material-ui/core';

import styled from 'styled-components'
import {rem} from 'polished'


var map = {
  sm: 600,
  md: 960,
  lg: 1280,
  xl: 1920
};

var mediaGrid = function mediaGrid(type) {
  return function () {
    return "@media (max-width: ".concat(map[type] - 1, "px)");
  };
};

import {SwipeLeftIcon, SwipeRightIcon} from 'app/honey/icons'


const enhance = compose(
  withStateHandlers(
    () => ({
      slideIndex: 0,
    }),
      {
        setSlideIndex: () => (slideIndex) => ({slideIndex}),
      },
    ),

  defaultProps({
    data: {},
    order: [],

    A: {
      SwipeLeftIcon: styled(SwipeLeftIcon)`
      && {
        cursor: pointer;
        &.disabled {
          opacity: 0.3
        }

      }
      `,
      SwipeRightIcon: styled(SwipeRightIcon)`
      && {
        cursor: pointer;
        &.disabled {
          opacity: 0.3
        }

      }
      `,
      Container: styled.div`
        display: inline-flex;
        margin: 0 auto;
      `,
     
      Right: styled(Grid)`
        text-align:right;
      `,
      Left: styled(Grid)`
      `,

      Slide: styled.div`
        min-width: 100%;
        margin: 0 ${rem(20)};
        cursor: pointer;
        text-align: center;
        display: flex;
        flex-direction: row;
        align-items: center;
        div {
          margin: auto;
        }
      `,
      Carousel: styled(Carousel)`
        && {
          box-sizing: content-box;
          margin: 0 auto;
          position: relative;
          overflow: hidden;
          width: 100%;
          height: auto;
           },
      `
    }
  }),

)

const PromotionBlockList = enhance(({ data, order, A, slideIndex, setSlideIndex }) => {
  return (
    <Grid container direction="row" justify="space-around" alignItems="center" >

    <Hidden smDown>
      <A.Right item xs={1}>
        <A.SwipeLeftIcon className={slideIndex!==0 ? '' : 'disabled'} style={{ backgroundColor: "#80808033"}} onClick={() => slideIndex!==0 && setSlideIndex(slideIndex-1)} />
      </A.Right>
    </Hidden>
    
    <Grid item xs={10} sm={8}>

        <A.Carousel slideIndex={slideIndex} onSlideChange={setSlideIndex} >
           
              {order.map((id, index) => (

                <A.Slide

                key={index}
                onClick={() => setSlideIndex(index)}
                >

                    <PromotionBlockBannerArea
                      key={id}
                      {...data[id]}
                    />


                </A.Slide>
                )
              )}           

          </A.Carousel>
    </Grid>

    <Hidden smDown>
      <A.Left item xs={1}>
        <A.SwipeRightIcon className={slideIndex<order.length-1 ? '' : 'disabled'} style={{ backgroundColor: "#80808033"}} onClick={() => slideIndex<order.length-1 && setSlideIndex(slideIndex+1)} />
      </A.Left>
    </Hidden>



  </Grid>
  )
}
)
export default PromotionBlockList

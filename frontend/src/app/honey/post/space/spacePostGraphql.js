import config               from '../../config/api/config'
import QUERY                from '../api/graphql/query'
import QUERY_BY_ID          from '../api/graphql/queryById'
import QUERY_BLOG_TEASER    from '../api/graphql/queryBlogTeaser'
import spacePost            from './spacePost'


export default [
  config.namespace,
  `postGraphql`,
  [
    spacePost
  ],
  () => ({
    isLoading: false,
  }),
  {
    query: (getState, setState, methods, { fromPost }) => function* (payload, { client }) {
      try {
        if (client) {
          yield setState({ isLoading: true })

          const { data } = yield client.query({ query: QUERY })

          for (let doc of data.post) {
            yield fromPost.create(doc)
          }

          yield setState({ isLoading: false })
        }
      } catch (err) {
        yield setState({ isLoading: false })

        console.error(err)
      }
    },
    queryById: (getState, setState, methods, { fromPost }) => function* ({ id }, { client }) {
      try {
        if (client) {
          yield setState({ isLoading: true })

          const { data } = yield client.query({
            query: QUERY_BY_ID,
            variables: { id },
          })

          for (let doc of data.post) {
            yield fromPost.create(doc)
          }

          yield setState({ isLoading: false })
        }
      } catch (err) {
        yield setState({ isLoading: false })

        console.error(err)
      }
    },
    queryBlogTeaser: (getState, setState, methods, { fromPost }) => function* (payload, { client }) {
      try {
        if (client) {
          yield setState({ isLoading: true })

          const { data } = yield client.query({ query: QUERY_BLOG_TEASER })

          yield fromPost.orderClear('blogTeaser')
          for (let doc of data.post) {
            yield fromPost.create(doc, { orderName: `blogTeaser` })
          }

          yield setState({ isLoading: false })
        }
      } catch (err) {
        yield setState({ isLoading: false })

        console.error(err)
      }
    }
  }
]

import gql                  from 'graphql-tag'


export default gql`
query post($id: ID){
  post(
    id: $id
  )
  {
    id
    title
    state
    
    author {
      id
      name {
        first
        last
      }
    }
    
    publishedDate
    
    content {
    	brief
      extended
  	}
    
    categories {
      id
      name
    }
    
    images {
      id
      image {
        secure_url
        url
      }
    }
    
    videos {
    	id
      videoLink
  	}
    
    isPinnedToIndex
  }
}
`

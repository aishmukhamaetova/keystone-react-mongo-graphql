import gql                  from 'graphql-tag'


export default gql`
query {
  post (
    where: {
      isPinnedToIndex: {
        boolean: {
          eq: true
        }
      }
    }
  )  {
    id
    title
    state
    
    author {
      id
      name {
        first
        last
      }
    }
    
    publishedDate
    
    content {
    	brief
      extended
  	}
    
    categories {
      id
      name
    }
    
    images {
      id
      image {
        secure_url
        url
      }
    }
    
    videos {
    	id
      videoLink
  	}

    
    isPinnedToIndex
  }
}
`

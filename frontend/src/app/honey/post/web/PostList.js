import React, { Component } from 'react'
import { compose }          from 'recompose'
import { defaultProps }     from 'recompose'

import Post                 from './Post'


const enhance = compose(
  defaultProps({
    data: {},
    order: [],
  })
)

const PostList = ({ data, order }) => {
  return order.map((id) => (
    <Post
      key={id}
      {...data[id]}
      id={id}
    />
  ))
}

export default PostList

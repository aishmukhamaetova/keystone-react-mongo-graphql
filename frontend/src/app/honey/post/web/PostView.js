import React, { Component } from 'react'
import compose from 'recompose/compose'
import defaultProps from 'recompose/defaultProps'

import styled from 'styled-components';
import {rem} from 'polished'
import { Grid, Typography } from '@material-ui/core';

import Image                from 'app/honey/image/web/Image'
// import Video                from 'app/honey/video/web/Video'

const enhance = compose(
  
  defaultProps({
    A: {
      Container: styled(Grid)`
        && {
          text-align: center;
          color: #000;
          img {
            width: 50%;
            height: auto;  
            margin: auto;
  
          }
        }
      `,

      P: styled(({ color, transform, border, fweight, ...other }) => (
        <Typography {...other} />
      ))`
        && {
          font-size: ${rem(20)};
          text-align: left;
        }
      `,  
      Title: styled(Typography)`
        && {
          color: #000;
          margin: ${props => props.margin};
          font-weight: ${props => props.fweight || '700'};
          min-width: auto;
          width: 100%;
          font-size: ${rem(35)};
          text-align: center;
          font-size: ${rem(38)};
          text-transform: uppercase;
        
        }
      `,

    }

  }),
)
const PostView = enhance(({
  content,
  images,
  link,
  title,
  videos,
  A
}) => {
  return images.map((doc, index) => (index ===0 && 

    // {videos.map((doc) => (<Video key={doc.id} {...doc} />))}

    <A.Container container direction={'row'} justify={'center'} alignItems={'center'} spacing={0} >
      <Grid item xs={10} sm={8} >
        <Image key={doc.id} {...doc} />
      </Grid>
  
      <Grid item xs={10} sm={8} >
        <A.Title fweight="700" border="0" margin="0 0 1.25rem 0"  transform="uppercase">
          {title}
        </A.Title>
        <A.P fweight="300" dangerouslySetInnerHTML={{ __html: content.brief }} />
        <A.P fweight="300" dangerouslySetInnerHTML={{ __html: content.extended }} />

      </Grid>
  </A.Container>
  ))


})

export default PostView
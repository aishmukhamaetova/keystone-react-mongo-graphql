import { withRouter }       from 'next/router'
import React                from 'react'
import { withApollo }       from 'react-apollo'
import { compose }          from 'recompose'
import { lifecycle }        from 'recompose'



import withSpace            from 'evoke-me/space/all/withSpace'

import spacePost            from '../../space/spacePost'
import spacePostGraphql     from '../../space/spacePostGraphql'
import PostView             from '../PostView'


const enhance = compose(
  withRouter,
  withApollo,
  withSpace(spacePost),
  withSpace(spacePostGraphql),
  lifecycle({
    componentDidMount() {
      const { client } = this.props
      const { fromPostGraphql } = this.props
      const { router } = this.props
      const { query: { id }} = router

      const { fromPost } = this.props
      const { data  } = fromPost

      if (!data[id]) {
        fromPostGraphql.queryById({ id }, { client })
      }
    }
  })
)

const BlockPost = enhance(({
  fromPost, fromPostGraphql, router,
}) => {
  const { query: { id }} = router
  const { data } = fromPost
  const { isLoading } = fromPostGraphql

  if (isLoading || !data[id]) return null

  return (
    <PostView {...data[id]} />
  )
})

export default BlockPost

import React, { Component } from 'react'
import { withApollo }       from 'react-apollo'
import { compose }          from 'recompose'
import { lifecycle }        from 'recompose'

import { Segment }          from 'semantic-ui-react'
import { Header }           from 'semantic-ui-react'
import { Icon }             from 'semantic-ui-react'


import withSpace            from 'evoke-me/space/all/withSpace'

import spacePost         from '../../space/spacePost'
import spacePostGraphql  from '../../space/spacePostGraphql'
import PostList          from '../PostList'


const enhance = compose(
  withApollo,
  withSpace(spacePost),
  withSpace(spacePostGraphql),
  lifecycle({
    componentDidMount() {
      const { client } = this.props
      const { fromPostGraphql } = this.props

      const { fromPost } = this.props
      const { orderMap } = fromPost

      if (orderMap.default.length < 3) {
        fromPostGraphql.queryBlogTeaser({}, { client })
      }
    }
  })
)

const BlockPostList = enhance(({
  fromPost,
  fromPostGraphql,
}) => {
  const { data, orderMap } = fromPost
  const { isLoading } = fromPostGraphql

  if (!orderMap.blogTeaser || !orderMap.blogTeaser.length) {
    return (
      <Segment placeholder loading={isLoading}>
        <Header icon>
          <Icon name='pdf file outline' />
          No posts is pinned to index
        </Header>
      </Segment>
    )
  }

  return (
    <PostList
      data={data}
      order={orderMap.default.slice(0, 1)}
    />
  )
})

export default BlockPostList

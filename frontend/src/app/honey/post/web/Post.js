import React, { Component } from 'react'
import compose from 'recompose/compose'
import defaultProps from 'recompose/defaultProps'
import { Link }             from 'app/honey/page/api/routes'

import styled from 'styled-components';
import {rem} from 'polished'
import { Grid, Typography } from '@material-ui/core';

import Poster                from 'app/honey/image/web/Poster'
// import Video                from 'app/honey/video/web/Video'

const enhance = compose(
  
  defaultProps({
    A: {
      Container: styled.div`
        && {
          text-align: center;
          &.dark {
            padding: ${rem(70)} ${rem(45)};
            background-color: rgba(0, 0, 0, 0.2);
          }
        }
      `,
      TagLine: styled(({ color, transform, border, fweight, ...other }) => (
        <Typography {...other} />
      ))`
        && {
          font-size: ${rem(35)};
          font-weight: ${props => props.fweight || 400};
          color: ${props => props.color};
          text-align: center;
          font-style: normal;
          line-height: normal; 
          &.dark {
            padding: ${rem(70)} ${rem(45)};
            background-color: rgba(0, 0, 0, 0.2);
        
          }
        }
      `,  
      Title: styled(Typography)`
        && {
          color: ${props => props.textColor || '#fff'};
          margin: ${props => props.margin};
          font-weight: ${props => props.fweight || '700'};
          min-width: auto;
          width: 100%;
          font-size: ${rem(35)};
          text-align: center;
          font-size: ${rem(38)};
          text-transform: uppercase;
        
        }
      `,
      Link: styled.div`
        && {
          text-align: center;
          color: ${props => props.color || '#fff'};
          font-size: ${props => props.size};
          line-height: 1.4;
          font-weight: ${props => props.bold || 'normal'};
          font-style: italic;
          margin: ${props => props.margin || '0'};
          padding: ${props => props.padding || '0'};
        
          a {
            text-decoration: underline;
            color: ${props => props.color || '#fff'};
            font-weight: ${props => props.bold || 'normal'};
            &:hover {
              text-decoration: none;
            }
          }
        
        }
      `,
    }

  }),
)
const Post = enhance(({
  content,
  images,
  link,
  title,
  videos,
  A,
  id
}) => {

  return images.map((doc, index) => 
    (index ===0 && 
      <Poster key={doc.id} {...doc} >
        <Grid item xs={10} sm={8} >
          <A.Container className={'dark'}>
            <A.Title fweight="700" border="0" margin="0 0 1.25rem 0"  transform="uppercase">
              {title}
            </A.Title>
            <A.TagLine color="#fff" fweight="300" dangerouslySetInnerHTML={{ __html: content.brief }} />
            <A.Link bold="600" size="0.875rem" margin="1.875rem auto 0.9375rem" variant="button" gutterBottom>
            <Link route='post' params={{ id }}>
              <a>read more</a>
            </Link>
            </A.Link>     
          </A.Container>
        </Grid>
      </Poster>)
    )

    // {videos.map((doc) => (<Video key={doc.id} {...doc} />))}

})

export default Post
import React, { Component } from 'react'
import { Container }        from 'react-grid-system'
import { Col }              from 'react-grid-system'
import { Row }              from 'react-grid-system'


import Block                from 'evoke-me/layout/web/Block'

import BlockBlogTeaser      from '../block/BlockBlogTeaser'


const SectionBlogTeaser = () => {
  return (
    <BlockBlogTeaser />
  )
}

export default SectionBlogTeaser

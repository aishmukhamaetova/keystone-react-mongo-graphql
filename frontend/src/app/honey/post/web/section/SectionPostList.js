import React, { Component } from 'react'
import { Container }        from 'react-grid-system'
import { Col }              from 'react-grid-system'
import { Row }              from 'react-grid-system'


import Block                from 'evoke-me/layout/web/Block'

import BlockPostList     from '../block/BlockPostList'


const SectionPostList = () => {
  return (
    <Container fluid style={{ padding: 10 }}>
      <Row justify='center'>
        <Col sm={10} lg={6}>
          <Block center middle style={{ padding: 5 }}>
            <BlockPostList />
          </Block>
        </Col>
      </Row>
    </Container>
  )
}

export default SectionPostList

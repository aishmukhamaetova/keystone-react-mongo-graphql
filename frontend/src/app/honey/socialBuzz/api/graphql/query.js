import gql                  from 'graphql-tag'


export default gql`
query {
  socialBuzz {
    id
    linkInstagram
    name
  }
}
`


import BlockSocialBuzz     from '../block/BlockSocialBuzz'
import React, {Component} from 'react'
import {Grid, Typography} from '@material-ui/core'
import compose from 'recompose/compose'
import styled from 'styled-components'
import {rem} from 'polished'

const enhance = compose()

const Title = styled(({color, transform, border, margin, fweight, ...other}) => (
  <Typography {...other} />
))`&& {
  color: ${props => props.color};
  border-bottom: ${props => props.border};
  text-transform: ${props => props.transform || 'inherit'};
  border-color: #000;
  margin: ${props => props.margin};
  font-weight: ${props => props.fweight || '700'};
  min-width: auto;
  width: 100%;
  text-align: center;
  font-size: ${rem(35)};
}
`;

const SectionSocialBuzz = enhance(() => {
  return (
    <React.Fragment>
      <Title transform="uppercase" margin="0 0 2.25rem 0">
        Sozial Buzz
      </Title>
      <BlockSocialBuzz/>
    </React.Fragment>
  )
})

export default SectionSocialBuzz
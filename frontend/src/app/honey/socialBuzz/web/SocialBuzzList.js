import React, { Component } from 'react'
import { compose }          from 'recompose'
import { defaultProps }     from 'recompose'

import styled               from 'styled-components'
import SocialBuzz           from './SocialBuzz'


const enhance = compose(
  defaultProps({
    data: {},
    order: [],
  })
)

const SocialBuzzList = ({ data, order, A }) => {
  return order.length && order.map((id) =>     <SocialBuzz
  key={id}
  {...data[id]}
  />
  )
}

export default SocialBuzzList

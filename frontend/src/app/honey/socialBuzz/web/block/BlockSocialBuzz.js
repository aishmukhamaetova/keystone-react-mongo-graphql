import React, { Component } from 'react'
import { withApollo }       from 'react-apollo'
import { compose }          from 'recompose'
import { lifecycle }        from 'recompose'

import { Segment }          from 'semantic-ui-react'
import { Header }           from 'semantic-ui-react'
import { Icon }             from 'semantic-ui-react'


import withSpace            from 'evoke-me/space/all/withSpace'

import spaceSocialBuzz      from '../../space/spaceSocialBuzz'
import spaceSocialBuzzGraphql from '../../space/spaceSocialBuzzGraphql'
import SocialBuzzList       from '../SocialBuzzList'

import {Grid} from '@material-ui/core'

const enhance = compose(
  withApollo,
  withSpace(spaceSocialBuzz),
  withSpace(spaceSocialBuzzGraphql),
  lifecycle({
    componentDidMount() {
      const { client } = this.props
      const { fromSocialBuzzGraphql } = this.props

      const { fromSocialBuzz } = this.props
      const { orderMap } = fromSocialBuzz

      if (!orderMap.default.length) {
        fromSocialBuzzGraphql.query(client)
      }
    }
  })
)

const BlockSocialBuzz = enhance(({
  fromSocialBuzz,
  fromSocialBuzzGraphql,
}) => {
  const { data, orderMap } = fromSocialBuzz
  const { isLoading } = fromSocialBuzzGraphql

  if (!orderMap.default || !orderMap.default.length) {
    return (
      <Segment placeholder loading={isLoading}>
        <Header icon>
          <Icon name='hand peace' />
            BlockSocialBuzz is empty!
        </Header>
      </Segment>
    )
  }
  return (
    <Grid container direction={'row'} justify="space-evenly" alignItems={'center'} spacing={0}>
      <SocialBuzzList
        data={data}
        order={orderMap.default.slice(0, 5)}
      />
    </Grid>
  )
})

export default BlockSocialBuzz

import React, { Component } from 'react'
import compose              from 'recompose/compose'
import defaultProps         from 'recompose/defaultProps'

import styled               from 'styled-components'
import {Grid, Typography} from '@material-ui/core'
import {rem} from 'polished'
import {IconFacebook, IconInstagram, IconVimeo, IconYoutube} from 'app/honey/icons'


const enhance = compose(
  defaultProps({
    A: {

      SocialThumbnail: styled(Grid)`
        position: relative;
        background: rgba(0,0,0,0.2);
      `,
      
      SocialThumbnailLink: styled.a`
        position: relative;
        width: 100%;
        height: 100%;
        text-align: right;
        display: block;
        overflow: hidden;
        border-right: 3px solid white;
        
      `,
      
      SocialThumbnailImg: styled.img`
        width: 100%;
        height: 100%;
        text-align: right;
        display: block;
        overflow: hidden;
        border-right: 3px solid white;
      `,
      
      SocialIcon:styled.div`
        display: flex;
        position: absolute;
        bottom: 0;
        right: ${rem(20)};
        height: ${rem(45)};
        svg {
          height: 100%;
        }
      `,

    
    }
  }),


)

const SocialBuzz = enhance(({
  imageLink='',
  linkInstagram='',
  name='',
  A,
  ...props,
}) => {
  return (

    <A.SocialThumbnail md={3} sm={6} xs={12} item >
      <A.SocialThumbnailLink href={linkInstagram}
        target="_blank"
        rel="noopener noreferrer">
        <A.SocialThumbnailImg src={imageLink} alt={name} />
        <A.SocialIcon>
          <IconInstagram style={{fontSize: 22, fill: '#fff'}} />
        </A.SocialIcon>
      </A.SocialThumbnailLink>
    </A.SocialThumbnail>

  )
})

export default SocialBuzz

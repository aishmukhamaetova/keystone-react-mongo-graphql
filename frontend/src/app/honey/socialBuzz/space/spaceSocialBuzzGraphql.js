import config               from '../../config/api/config'
import QUERY                from '../api/graphql/query'
import spaceSocialBuzz      from './spaceSocialBuzz'


export default [
  config.namespace,
  `socialBuzzGraphql`,
  [
    spaceSocialBuzz
  ],
  () => ({
    isLoading: false,
  }),
  {
    query: (getState, setState, methods, { fromSocialBuzz }) => function* (client) {
      try {
        if (client) {
          yield setState({ isLoading: true })

          const { data } = yield client.query({ query: QUERY })

          for (let doc of data.socialBuzz) {
            const { linkInstagram } = doc
            const result = yield fetch(`${linkInstagram}media`)
            let imageLink = null
            if (result.status === 200) {
              imageLink = result.url
            }
            yield fromSocialBuzz.create({ ...doc, imageLink })
          }

          yield setState({ isLoading: false })
        }
      } catch (err) {
        yield setState({ isLoading: false })

        console.error(err)
      }
    },
  }
]

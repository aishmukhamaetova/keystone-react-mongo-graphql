import spaceDefaults        from 'evoke-me/space/api/spaceDefaults'
import config               from '../../config/api/config'


export default [
  config.namespace,
  `socialBuzz`,
  [],
  () => ({
    ...spaceDefaults.state,
  }),
  {
    ...spaceDefaults.updaters,
  }
]

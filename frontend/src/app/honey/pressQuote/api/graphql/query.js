import gql                  from 'graphql-tag'


export default gql`
query {
  pressQuote {
    id
    logo {
      id
      image {
        secure_url
      }
    }
    description
    link
  }
}
`

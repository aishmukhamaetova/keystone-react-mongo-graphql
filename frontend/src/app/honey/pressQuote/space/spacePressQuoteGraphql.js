import config               from '../../config/api/config'
import QUERY                from '../api/graphql/query'
import spacePressQuote      from './spacePressQuote'


export default [
  config.namespace,
  `pressQuoteGraphql`,
  [
    spacePressQuote
  ],
  () => ({
    isLoading: false,
  }),
  {
    query: (getState, setState, methods, { fromPressQuote }) => function* (client) {
      try {
        if (client) {
          yield setState({ isLoading: true })

          const { data } = yield client.query({ query: QUERY })

          for (let doc of data.pressQuote) {
            yield fromPressQuote.create(doc)
          }

          yield setState({ isLoading: false })
        }
      } catch (err) {
        yield setState({ isLoading: false })

        console.error(err)
      }
    },
  }
]

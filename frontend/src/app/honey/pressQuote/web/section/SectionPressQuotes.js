import React, { Component } from 'react'
import compose              from 'recompose/compose'
import defaultProps         from 'recompose/defaultProps'
import withPropsOnChange    from 'recompose/withPropsOnChange'

import Block                from 'evoke-me/layout/web/Block'

import BlockPressQuotes     from '../block/BlockPressQuotes'
import styled from 'styled-components'
import {Grid, Typography} from '@material-ui/core'
import  {rem} from 'polished'

const enhance = compose(
  defaultProps({
    A: {
      Container: styled.div`&& {
        text-align: center;
      `, 
      Title: styled(({ color, transform, border, margin, fweight, ...other }) => (
        <Typography {...other} />
      ))`&& {
        color: ${props => props.color};
        border-bottom: ${props => props.border};
        text-transform: ${props => props.transform || 'inherit'};
        border-color: ${props => props.color};
        margin: ${props => props.margin};
        font-weight: ${props => props.fweight || '700'};
        min-width: auto;
        width: 100%;
        text-align: center;
        font-size: ${rem(35)};
      
      }
      `,
      Wrap: styled.div`&& {
        background: rgba(0,0,0,0.05);
        padding: ${rem(69)} 1.875rem;
      `, 
            
    }
  }),
)

const SectionPressQuoteList = enhance(({A}) => {
  return (
  <A.Container>
    <A.Title color="black" transform="uppercase" margin="0 0 2.25rem 0">
      Press Quotes
    </A.Title>
    <A.Wrap>
      <Grid item xs={12} sm={10} style={{ 'margin': 'auto' }}>
        <Grid container direction={'row'} justify={'space-between'} alignItems={'flex-start'} spacing={40}>
          <BlockPressQuotes />
        </Grid>
      </Grid>
    </A.Wrap>
  </A.Container>

  )
})

export default SectionPressQuoteList

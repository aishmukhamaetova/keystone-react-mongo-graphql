import React, { Component } from 'react'
import { compose }          from 'recompose'
import defaultProps         from 'recompose/defaultProps'
import Image                from '../../image/web/Image'

import {Grid, Typography, ButtonBase} from '@material-ui/core'
import styled from 'styled-components'

const enhance = compose(
  defaultProps({
    A: {
      Image: styled(ButtonBase)`
      && {
        width: 100%;
        height: 130px;
        img {
          width: auto;
          height: 130px;
        }
      }  
      `,
      Typography: styled(({ size, align, margin, bold, color, ...other }) => (
        <Typography {...other} />
      ))`
      && {
        color: ${props => props.color || '#fff'};
        font-size: ${props => props.size};
        text-align: ${props => props.align || 'left'};
        line-height: 1.4;
        font-weight: ${props => props.bold || 'normal'};
        font-style: italic;
        margin: ${props => props.margin || '0'};
        padding: ${props => props.padding || '0'};
      
        a {
          text-decoration: underline;
          color: ${props => props.color || '#fff'};
          font-weight: ${props => props.bold || 'normal'};
          &:hover {
            text-decoration: none;
          }
        }
      
      }
      `,
      
    }
  }),
)

const PressQuote = enhance(({
  description,
  logo,
  link,
  A,
}) => {
  return (
    <Grid container direction={'column'} justify="center" alignItems="center" spacing={40}>
      <Grid item xs>
        <A.Image>
          <a href={link}><Image {...logo} /></a>
        </A.Image>
      </Grid>
      <Grid item xs>  
        <A.Typography color="#000" bold="700" align="center" size="1.125rem" variant="subtitle1" gutterBottom dangerouslySetInnerHTML={{ __html: description }} />
      </Grid>
    </Grid>

  )
})

export default PressQuote

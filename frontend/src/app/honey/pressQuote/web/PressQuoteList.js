import React, { Component } from 'react'
import { compose }          from 'recompose'
import { defaultProps }     from 'recompose'
import PressQuote           from './PressQuote'
import {Grid, Typography} from '@material-ui/core'


const enhance = compose(
  defaultProps({
    data: {},
    order: [],
  })
)

const PressQuoteList = enhance(({ data, order }) => {
  return order.map((id) => (
    <Grid key={ id } item sm={4}>
      <PressQuote
        key={ id }
        {...data[id]}
      />
    </Grid>
  ))
})

export default PressQuoteList

import React, { Component } from 'react'
import { withApollo }       from 'react-apollo'
import { compose }          from 'recompose'
import { lifecycle }        from 'recompose'

import { Segment }          from 'semantic-ui-react'
import { Header }           from 'semantic-ui-react'
import { Icon }             from 'semantic-ui-react'


import withSpace            from 'evoke-me/space/all/withSpace'

import spacePressQuote      from '../../space/spacePressQuote'
import spacePressQuoteGraphql from '../../space/spacePressQuoteGraphql'
import PressQuoteList       from '../PressQuoteList'


const enhance = compose(
  withApollo,
  withSpace(spacePressQuote),
  withSpace(spacePressQuoteGraphql),
  lifecycle({
    componentDidMount() {
      const { client } = this.props
      const { fromPressQuoteGraphql } = this.props

      const { fromPressQuote } = this.props
      const { orderMap } = fromPressQuote

      if (!orderMap.default.length) {
        fromPressQuoteGraphql.query(client)
      }
    }
  })
)

const BlockPressQuotes = enhance(({
  fromPressQuote,
  fromPressQuoteGraphql,
}) => {
  const { data, orderMap } = fromPressQuote
  const { isLoading } = fromPressQuoteGraphql

  if (!orderMap.default || !orderMap.default.length) {
    return (
      <Segment placeholder loading={isLoading}>
        <Header icon>
          <Icon name='hand peace' />
          BlockPressQuotes is empty
        </Header>
      </Segment>
    )
  }

  return (
    <PressQuoteList
      data={data}
      order={orderMap.default.slice(0, 3)}
    />
  )
})

export default BlockPressQuotes

import React, { Component } from 'react'


export default ({ videoLink }) => {
  return (
    <iframe
      src={ videoLink }

      allow='autoplay; encrypted-media'
      allowFullScreen
      frameBorder={ 0 }
      width={ 560 }
      height={ 315 }
    />
  )
}

import config               from '../../config/api/config'


export default [
  config.namespace,
  `user`,
  [],
  () => ({
    isLoading: false,
    data: {},
    orderMap: {
      default: [],
    }
  }),
  {
    create: (getState, setState) => function *(doc, options = {}) {
      const { orderName = `default` } = options
      const { data, orderMap } = yield getState()

      if (data[doc.id]) return

      yield setState({
        data: {
          ...data,
          [doc.id]: doc,
        },
        orderMap: {
          ...orderMap,
          [orderName]: [
            ...(orderMap[orderName] || []),
            doc.id,
          ]
        }
      })
    },
  },
  (ownArgs) => {
    const [, options = {}] = ownArgs

    if (options.view) {
      return options.view
    }

    return `default`
  }
]
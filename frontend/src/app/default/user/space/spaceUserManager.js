import spaceUserConfig      from './spaceUser'
import config               from '../../config/api/config'


export default [
  `app.default`,
  'userManager',
  [
    spaceUserConfig,
  ],
  () => {
    return {
      isSeeded: false,
      isSelectMultiple: false,
      select: {},
      animateDisappear: {},
    }
  },
  {
    seed: (getState, setState, methods, { fromUser }) => function* (payload, options) {
      const { isSeeded } = yield getState()
      
      if (!isSeeded) {
        yield fromUser.create({ id: `id0`, name: `user0`, }, options)
        yield fromUser.create({ id: `id1`, name: `user1`, }, options)
        yield fromUser.create({ id: `id2`, name: `user2`, }, options)
        yield fromUser.create({ id: `id3`, name: `user3`, }, options)
      }

      return yield setState({ isSeeded: true })
    },
  }
]
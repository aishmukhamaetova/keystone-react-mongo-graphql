import config               from '../../config/api/config'
// import spaceNotify          from '../../notify/space/spaceNotify'


export default [
  config.namespace,
  `nav`,
  [
    // spaceNotify,
  ],
  () => ({
    path: `/`,
  }),
  {
    pathSet: (getState, setState, methods, { fromNotify }) => function* (pathDirty) {
      const path = pathDirty.replace(`*`, ``)
      // yield fromNotify.create({ message: `Redirected to ${path}` }, { orderName: `show` })
      yield setState({ path })
    },
    go: (getState) => function* (destination, push) {
      try {
        const { path } = yield getState()
        const pathname = `${path}/${destination}`.replace(/\/\/+/g, '/')      
        push(pathname)
      } catch (err) {
        throw new Error(`DESTINATION ${destination}`)
      }
    }
  }
]

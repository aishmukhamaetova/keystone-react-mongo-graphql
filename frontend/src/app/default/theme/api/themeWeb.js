// import BlockAuth            from 'app/default/auth/web/block/BlockAuth'
// import BlockFbAuth          from 'app/default/auth/web/block/BlockFbAuth'

// import BlockNotify          from 'app/default/notify/web/block/BlockNotify'

import BlockPageFooterLinks from 'app/default/pageFooter/web/block/BlockPageFooterLinks'
import BlockPageFooterSocial from 'app/default/pageFooter/web/block/BlockPageFooterSocial'

import BlockPageHeaderLogo  from 'app/default/pageHeader/web/block/BlockPageHeaderLogo'
import BlockPageHeaderMenu  from 'app/default/pageHeader/web/block/BlockPageHeaderMenu'


export default {
  default: {
    // BlockAuth,
    // BlockFbAuth,
    //
    // BlockNotify,

    BlockPageFooterLinks,
    BlockPageFooterSocial,

    BlockPageHeaderLogo,
    BlockPageHeaderMenu,
  }
}

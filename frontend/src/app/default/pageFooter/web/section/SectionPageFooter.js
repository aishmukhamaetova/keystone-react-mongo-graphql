import React, { Component } from 'react'
import BlockPageFooterLinks     from '../block/BlockPageFooterLinks'
import BlockPageFooterSubscribe     from '../block/BlockPageFooterSubscribe'

import { compose }          from 'recompose'
import defaultProps         from 'recompose/defaultProps'

import {IconFacebook, IconInstagram, IconVimeo, IconYoutube} from 'app/honey/icons'

var map = {
  sm: 600,
  md: 960,
  lg: 1280,
  xl: 1920
};

var mediaGrid = function mediaGrid(type) {
  return function () {
    return "@media (max-width: ".concat(map[type] - 1, "px)");
  };
};

import {Grid, Typography, ButtonBase, Hidden} from '@material-ui/core'
import styled from 'styled-components'

import {rem} from 'polished'


const iconMap = {
  'instagram': IconInstagram,
  'fb': IconFacebook,
  'youtube': IconYoutube,
  'vimeo': IconVimeo
}


const enhance = compose(
  defaultProps({

    A: 
      {
        SocialIcon: styled.div`
          display: flex;
          bottom: 0;
          svg {
            height: 100%;
          }`,

        SocialThumbnail: styled.a`
          position: relative;
          width: 100%;
          height: 100%;
          text-align: right;
          display: block;
          overflow: hidden;
          border-right: 3px solid white;
      `,
        Link: styled(Typography)`
        && {
          font-size: ${rem(16)};
          margin-bottom: ${rem(5)}
        }
        `,
        Social: styled(Grid)`
          && {
          
            ${mediaGrid(`md`)}{//tablet
            margin: ${rem(20)} 0 0;
            }

            ${mediaGrid(`sm`)}{//phone
              margin: ${rem(20)} 0;
            }
          }`,

        Menu: styled(Grid)`
          && {
          
            ${mediaGrid(`md`)}{//tablet
            margin: ${rem(20)} 0 0;
            }

            ${mediaGrid(`sm`)}{//phone
              margin: ${rem(20)} 0 0;
            }
          }
          `,
        Copyright: styled(Typography)`
          && {
            font-size: ${rem(15)};
            text-align:left;

            ${mediaGrid(`md`)}{//tablet
              text-align:left;
              margin: ${rem(20)} 0 0;
            }

            ${mediaGrid(`sm`)}{//phone
              text-align:center;
              margin: ${rem(20)} 0 0;
            }
          }
          `,
        Footer: styled.div`
          && {
            font-size: 1.14rem;
            font-weight: 300;
            .link {
              padding-bottom: 0.3125rem;
              a {
                color: #000;
                &:hover {
                  text-decoration: underline;
                }
              }
              &.active {
                a {
                  font-weight: bold;
                }
              }
            }
            .mdUp {
              margin: 0 0 2rem 0;
            }
            .title {
              display: block;
              padding-bottom: 0.2rem;
              font-size: 1.3rem;
              font-weight: 600;
              margin: 0;
            }
            
            input {
              font-size: 1.125rem;
              font-style: italic;
            }
          
            ${mediaGrid(`sm`)} { //phone
              .copyright {
                text-align: center;
                font-size: 85%;
              }
              .copyright, .link {
                margin-bottom: 0.6em;
              }
              
              .title, .link {
                text-align: center;
              }
          
            
            }
          }
          `,
        Container: styled(({ background, margin, ...other }) => (
          <Grid {...other} />
        ))`
          && {
            background: ${props => props.background};
            margin: ${props => props.margin};
            padding: ${props => props.padding};
          }
          `,

        }
    }
  ),
)

const SectionPageFooter = enhance(({ theme = {}, A }) => {

  if (!BlockPageFooterLinks || !BlockPageFooterSubscribe) {
    return (
      <Segment placeholder>
        <Header icon>
          <Icon name='pdf file outline' />
          SectionPageHeader cannot find BlockPageFooterLinks or BlockPageFooterSubscribe
        </Header>
      </Segment>
    )
  }

  return (
    <A.Footer>
      
      <A.Container background="#f1f1f1" margin="2rem 0 0" padding="2.5rem 0" container direction={'row'} justify="center" alignItems={'center'} spacing={0}>
        <Grid xs={10} item>
          <Grid container direction={'row'} justify="center" spacing={0}>


            <Hidden mdUp>
              <BlockPageFooterSubscribe mdUp={'mdUp'}/>
            </Hidden>


            <Grid xs={12} sm={12} md item>

              <Grid container direction={'row'} spacing={16} alignItems={'center'}>
                <BlockPageFooterLinks />
              </Grid>

            </Grid>

            <Hidden smDown>
              <BlockPageFooterSubscribe />
            </Hidden>

      
          </Grid>
        </Grid>
      </A.Container>

      <A.Container background="#fff" margin="0" padding="1rem 0" container direction={'row'} justify="center"
        alignItems={'center'} spacing={0}>
      
          <Grid md={10} xs={12} item>

            <Grid container direction={'row'} justify="center" alignItems={'center'} spacing={0}>
              <Grid xs={12} md={5} item>
                <A.Copyright variant="body2" paragraph={true}>&copy; L’Atelier du&nbsp;Miel 2018, all rights reserved.</A.Copyright>
              </Grid>

                <Grid md={7} item>

                  <Grid container direction={'row'} justify="center" alignItems={'center'} spacing={0}>

                    <Grid md={8} xs={12} item>

                      <A.Menu container direction={'row'} alignItems={'center'} justify="center" spacing={0}>
                        <Grid xs item>
                          <A.Link variant="body1" paragraph={true}>English</A.Link>
                        </Grid>
                        <Grid xs item>
                          <A.Link variant="body1" paragraph={true}>فرس عربي</A.Link>
                        </Grid>
                        <Grid xs item>
                          <A.Link variant="body1" paragraph={true}>Français</A.Link>
                        </Grid>
                      </A.Menu>

                    </Grid>

                    <Grid md={4} xs={8} item>

                      <A.Social container direction={'row'} justify="space-between" alignItems={'center'} spacing={0}>
                        {Object.keys(iconMap).map((item, index) => {
                          let Icon = iconMap[item];
                          return (
                            <Grid xs key={index} item className={'socialThumbnail'}>
                              <A.SocialThumbnail href={`//www.instagram.com/p/Bp9tfPzDdbQ/`}
                                target="_blank"
                                rel="noopener noreferrer">
                                <A.SocialIcon>
                                  <Icon style={{
                                    fontSize: item === 'youtube' ? 24 : item === 'vimeo' ? 53 : 20, 
                                    fill: '#0000009e'}} />
                                </A.SocialIcon>
                              </A.SocialThumbnail>
                            </Grid>
                          )
                        })}
                      </A.Social>

                    </Grid>
                  </Grid>

              </Grid>

            </Grid>

          </Grid>

      </A.Container>

    </A.Footer>
  )
})

export default SectionPageFooter
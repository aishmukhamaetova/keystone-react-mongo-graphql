import React, { Component } from 'react'

import { Menu }             from 'semantic-ui-react'
import compose from 'recompose/compose'
import defaultProps         from 'recompose/defaultProps'

import {Grid, Typography, Button, InputBase} from '@material-ui/core'
import styled from 'styled-components'
import {rem} from 'polished'

var map = {
  sm: 600,
  md: 960,
  lg: 1280,
  xl: 1920
};

var mediaGrid = function mediaGrid(type) {
  return function () {
    return "@media (max-width: ".concat(map[type] - 1, "px)");
  };
};

const enhance = compose(  
  defaultProps({

    A: {
      Button: styled(({ width, ...other }) => (
        <Button {...other} />
      ))`
        && {
          background-color: #000;
          color: white;
          font-weight: 600;
          font-size: ${rem(20)};
          text-transform: uppercase;
          width: ${props => props.width} || inherit;
          padding:${rem(12.6)} 0;
          border-radius: 0;
        }
      `, 
      Follow: styled.div`
      && {
        p {
          margin-bottom: 1em;
        }
        
        ${mediaGrid(`sm`)}{ //phone
          text-align: center;
          margin-bottom: 1em;
        }
      }
      
    `,
    Input: styled(({ width, align, ...other }) => (
      <InputBase {...other} />
    ))`
      && {
        margin: none;
        border: none;
        box-shadow: 1px 1px 0px #b1b1b1 inset, -1px -1px 0px #b1b1b1 inset;
        font-size: ${rem(18)};
        width: ${props => props.width || 'auto'};
        input {
          color: #000;
          text-align: ${props => props.align || 'left'};
          padding: ${rem(16.8)};
        }
      }
    `,
      


      }
    })
)

const MenuSubscribe = enhance(({
  height, mdUp, children, A
}) => {
  return (
    <Grid xs={12} sm={12} md item className={mdUp}>

      <A.Follow>
        <Typography className={'title'} noWrap={true} variant="body2" paragraph={true}>
          Follow our honey trail
        </Typography>
          <p>
            Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia, excepteur sint occaecat cupidatat
            non proident, sunt in culpa qui officia
          </p>
      </A.Follow>


      <Grid container direction={'row'} justify="center" spacing={16}>
          <Grid sm={6} xs={12} item>
            <A.Input
              bg="none"
              placeholder="This is some email"
              width="100%"
              variant="standard"
              name="value"
              label=""
              id="standard-name"
              value={''}
              margin="none"
            />
          </Grid>
          <Grid sm={6} xs={12} item>
            <A.Button fullWidth variant="contained" color="primary">Subscribe</A.Button>
          </Grid>
        </Grid>

    </Grid>
    )
})

export default MenuSubscribe
import React, { Component } from 'react'
import compose              from 'recompose/compose'
import lifecycle            from 'recompose/lifecycle'


import MenuSubscribe           from '../MenuSubscribe'
import Block                from 'evoke-me/layout/web/Block'


const enhance = compose()

const BlockPageFooterSubscribe = enhance(() => {
  return (
    <MenuSubscribe />
    )
})

export default BlockPageFooterSubscribe
import React, { Component } from 'react'
import compose              from 'recompose/compose'
import lifecycle            from 'recompose/lifecycle'


import MenuSocial           from '../MenuSocial'
import Block                from 'evoke-me/layout/web/Block'


const enhance = compose()

const BlockPageFooterSocial = enhance(() => {
  return (
    <MenuSocial />
    )
})

export default BlockPageFooterSocial
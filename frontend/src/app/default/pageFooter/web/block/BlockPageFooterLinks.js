import React, { Component } from 'react'
import compose              from 'recompose/compose'
import lifecycle            from 'recompose/lifecycle'


import MenuLinks            from '../MenuLinks'
import Block                from 'evoke-me/layout/web/Block'


const enhance = compose()

const BlockPageFooterLinks = enhance(() => {
  return (
    <MenuLinks />
  )
})    

export default BlockPageFooterLinks
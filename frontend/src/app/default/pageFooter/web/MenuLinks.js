import React, { Component } from 'react'

import compose from 'recompose/compose'
import defaultProps         from 'recompose/defaultProps'

import {Grid, Typography, Button, InputBase} from '@material-ui/core'
import styled from 'styled-components'
import {rem} from 'polished'

var map = {
  sm: 600,
  md: 960,
  lg: 1280,
  xl: 1920
};

var mediaGrid = function mediaGrid(type) {
  return function () {
    return "@media (max-width: ".concat(map[type] - 1, "px)");
  };
};

const enhance = compose(  
  defaultProps({

    A: {
      LinkMenu: styled(Typography)`
      && {
        font-size: ${rem(14)};
        margin-bottom: ${rem(5)}
      }
      `,

      TitleMenu: styled(Typography)`
      && {
        font-size: ${rem(18)};
        margin-bottom: ${rem(10)};
        font-weight: 600;
      }
      `,

    }
  }),
)

const MenuLinks = enhance(({
  height, A
}) => {
  return (
    ['Information', 'Customer Service', 'Legal'].map((value, index) => (
      <Grid key={index} item xs={12} sm>
        <A.TitleMenu noWrap={true} variant="body1" paragraph={true}>{value}</A.TitleMenu>
        <A.LinkMenu variant="body1" paragraph={true}>Our Story</A.LinkMenu>
        <A.LinkMenu variant="body1" paragraph={true}>Retail Stores</A.LinkMenu>
        <A.LinkMenu variant="body1" paragraph={true}>Business</A.LinkMenu>
        <A.LinkMenu variant="body1" paragraph={true}>Press</A.LinkMenu>
        <A.LinkMenu variant="body1" paragraph={true}>Carreers</A.LinkMenu>
      </Grid>))
    
  )
})

export default MenuLinks
import config               from '../../config/api/config'
import QUERY                from '../api/graphql/query'
import spaceAuth            from './spaceAuth'


export default [
  config.namespace,
  `authGraphql`,
  [
    spaceAuth,
  ],
  () => ({
    isLoading: false,
  }),
  {
    query: (getState, setState, methods, { fromAuth }) => function* (client) {
      try {
        if (client) {
          yield setState({ isLoading: true, })

          const { data } = yield client.query({ query: QUERY })

          if (data.me) {
            const { id } = data.me

            yield fromAuth.isAuthSet(true)
            yield fromAuth.idSet(id)
          } else {
            yield fromAuth.isAuthSet(false)
          }

          yield setState({ isLoading: false, })
        }
      } catch (err) {
        yield setState({ isLoading: false, })

        console.error(err)
      }
    }
  }
]

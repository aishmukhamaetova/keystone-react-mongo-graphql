import config               from '../../config/api/config'
import spaceNotify          from '../../notify/space/spaceNotify'


export default [
  config.namespace,
  `auth`,
  [
    spaceNotify,
  ],
  () => ({
    id: null,
    username: ``,
    password: ``,
    sessionId: null,

    errors: [],

    isAuth: false,
    isLoading: false,
  }),
  {
    isAuthSet: (getState, setState) => function* (isAuth) { yield setState({ isAuth }) },
    idSet: (getState, setState) => function* (id) { yield setState({ id }) },
    usernameSet: (getState, setState) => function* (username) { yield setState({ username }) },
    passwordSet: (getState, setState) => function* (password) { yield setState({ password }) },

    login: (getState, setState, methods, { fromNotify }) => function* () {
      try {
        const { username, password } = yield getState()
        
        yield setState({ errors: [], isAuth: false, isLoading: true })

        const sessionId = yield new Promise((resolve, reject) => {
          setTimeout(() => {
            if (username === `root`) {
              return resolve('SESSION_ID')
            }
            reject(`CRASH_ME`)
          }, 200)
        })

        yield fromNotify.create({ message: `Welcome` }, { orderName: `show` })

        yield setState({ isAuth: true, isLoading: false, sessionId, errors: [], })
      } catch (err) {
        const error = { error: true, message: `Wrong username or password` }

        yield fromNotify.create(error, { orderName: `show` })

        yield setState({
          isAuth: false,
          isLoading: false,
          errors: [
            error,
          ]
        })
      }
    },
    logout: (getState, setState, methods, { fromNotify }) => function* () {
      yield fromNotify.create({ message: `Bye, bye!` }, { orderName: `show` })
      yield setState({ errors: [], isAuth: false, isLoading: false })
    },
  }
]

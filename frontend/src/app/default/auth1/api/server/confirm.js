const keystone = require('keystone')


const User = keystone.list('User')


const processKeystoneLogin = (req, res, next) => {
  console.log(`OUR USER IS`, req.session.user)

  var onSuccess = function(user) {
    console.log('[auth.confirm] - Successfully signed in.')
    console.log('------------------------------------------------------------')

    return res.redirect(`/enter`)
  }

  var onFail = function(err) {
    console.log('[auth.confirm] - Failed signing in.', err)
    console.log('------------------------------------------------------------')

    return res.redirect(`/enter`)
  }

  keystone.session.signin(String(req.session.user._id), req, res, onSuccess, onFail)
}

const processExisting = (req, res, next) => {
  const {
    avatar,
    name: { first, last, },
    profileId,
  } = req.session.authFb

  User.model.findOne({ facebookId: profileId })
    .exec(function (err, userResult) {
      if (userResult) {
        console.log(`USER EXIST`, userResult)

        req.session.user = userResult

        return processKeystoneLogin(req, res, next)
      } else {
        const doc = {
          name: { first, last, },
          facebookId: profileId,
          password: Math.random().toString(36).slice(-8),
        }

        const user = new User.model(doc)
        user.save(function (err) {
          console.log(`CREATE USER`, userResult)

          req.session.user = user
          return processKeystoneLogin(req, res, next)
        })
      }

    })
}

exports = module.exports = function (req, res, next) {
  if (req.session.authFb) {
    return processExisting(req, res, next)
  }
}

const keystone = require('keystone')
const passport = require('passport')
const passportFacebook = require('passport-facebook')


const User = keystone.list('User')



const credentials = {
  clientID: process.env.FACEBOOK_CLIENT_ID,
  clientSecret: process.env.FACEBOOK_CLIENT_SECRET,
  callbackURL: process.env.FACEBOOK_CALLBACK_URL,
}

exports = module.exports = function (req, res, next) {
  const facebookStrategy = new passportFacebook.Strategy(
    credentials,
    (accessToken, refreshToken, profile, done) => {
      done(null, {
        accessToken: accessToken,
        refreshToken: refreshToken,
        profile: profile,
        profileFields: ['id', 'displayName', 'photos', 'email'],
      })
    }
  )

  passport.use(facebookStrategy)
  passport.authenticate('facebook', { scope: ['email'] })(req, res, next)
}

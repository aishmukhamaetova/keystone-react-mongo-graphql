const keystone = require('keystone')
const passport = require('passport')
const passportFacebook = require('passport-facebook')


const User = keystone.list('User')


const credentials = {
  clientID: process.env.FACEBOOK_CLIENT_ID,
  clientSecret: process.env.FACEBOOK_CLIENT_SECRET,
  callbackURL: process.env.FACEBOOK_CALLBACK_URL,
}

exports = module.exports = function (req, res, next) {
  const facebookStrategy = new passportFacebook.Strategy(
    credentials,
    (accessToken, refreshToken, profile, done) => {
      done(null, {
        accessToken: accessToken,
        refreshToken: refreshToken,
        profile: profile,
        profileFields: ['id', 'displayName', 'photos', 'email'],
      })
    }
  )

  passport.use(facebookStrategy)

  passport.authenticate(
    'facebook',
    { session: false },
    (err, data, info) => {
      if (err || !data) {
        console.log('[services.facebook] - Error retrieving Facebook account data - ' + JSON.stringify(err))
        console.log('DATA - ' + JSON.stringify(data))
        return res.redirect('/enter')
      }

      const name = data.profile && data.profile.displayName ? data.profile.displayName.split(' ') : []

      const auth = {
        type: 'facebook',

        name: {
          first: name.length ? name[0] : '',
          last: name.length > 1 ? name[1] : ''
        },

        email: data.profile.emails && data.profile.emails.length ? _.first(data.profile.emails).value : null,

        website: data.profile._json.blog,

        profileId: data.profile.id,

        username: data.profile.username,
        avatar: 'https://graph.facebook.com/' + data.profile.id + '/picture?width=600&height=600',

        accessToken: data.accessToken,
        refreshToken: data.refreshToken
      }

      req.session.authFb = auth

      return res.redirect(`${ process.env.CLIENT_HOST }/api/auth/facebook/confirm`)
    }
  )(req, res, next)
}

import React, { Component } from 'react'
import compose              from 'recompose/compose'
import lifecycle            from 'recompose/lifecycle'


import FormLogin            from 'app/default/auth/web/FormLogin'
import FormLogout           from 'app/default/auth/web/FormLogout'
import withSpace            from 'evoke-me/space/all/withSpace'

import spaceAuth            from '../../space/spaceAuth'


const enhance = compose(
  withSpace(spaceAuth),
)

const BlockAuth = enhance(({
  fromAuth,
}) => {
  const { username, usernameSet } = fromAuth
  const { password, passwordSet } = fromAuth
  const { errors } = fromAuth
  const { isAuth, isLoading } = fromAuth
  const { login, logout } = fromAuth

  return isAuth ? (
    <FormLogout
      onLogout={logout}
    />
  ) : (
    <FormLogin
      username={username}
      password={password}
      errors={errors}

      isLoading={isLoading}

      onChangeUsername={usernameSet}
      onChangePassword={passwordSet}
      onLogin={login}
    />
  )
})

export default BlockAuth
import { withRouter }       from 'next/router'
import React, { Component } from 'react'
import { withApollo }       from 'react-apollo'
import compose              from 'recompose/compose'
import lifecycle            from 'recompose/lifecycle'


import withSpace            from 'evoke-me/space/all/withSpace'


import spaceNav             from '../../../nav/space/spaceNav'
import spaceAuth            from '../../space/spaceAuth'
import spaceAuthGraphql     from '../../space/spaceAuthGraphql'
import FormFbLogin          from '../FormFbLogin'
import FormFbLogout         from '../FormFbLogout'


// https://github.com/jaredhanson/passport-facebook/issues/12
const removeHashFromBuggyFacebook = () => {
  try {
    if (location.hash = '#_=_') {
      window.history.replaceState("", document.title, window.location.pathname + window.location.search)
    }
  } catch (err) {}
}

const enhance = compose(
  withRouter,
  withApollo,
  withSpace(spaceAuth),
  withSpace(spaceAuthGraphql),
  withSpace(spaceNav),
  lifecycle({
    componentDidMount() {
      removeHashFromBuggyFacebook()

      const { client } = this.props
      const { fromAuthGraphql } = this.props

      fromAuthGraphql.query(client)
    }
  })
)

const BlockAuth = enhance(({
  fromAuth,
  fromNav,
  router,
}) => {
  const { errors } = fromAuth
  const { isAuth, isLoading } = fromAuth
  const { go } = fromNav

  return isAuth ? (
    <FormFbLogout
      onLogout={() => go(`/api/auth/facebook/logout`, router.push)}
    />
  ) : (
    <FormFbLogin
      errors={ errors }

      isLoading={ isLoading }

      onLogin={() => go(`/api/auth/facebook/login`, router.push)}
    />
  )
})

export default BlockAuth

import React, { Component } from 'react'
import compose              from 'recompose/compose'
import defaultProps         from 'recompose/defaultProps'
import withPropsOnChange    from 'recompose/withPropsOnChange'

import { Button }           from 'semantic-ui-react'
import { Form }             from 'semantic-ui-react'
import { Message }          from 'semantic-ui-react'


import Block                from 'evoke-me/layout/web/Block'


const runOnceInTime = (run, timeout) => {
  let time

  return (...args) => {
    const next = Date.now()
    if (!time || next - time >= timeout) {
      time = next
      return run(...args)
    }
  }
}

const enhance = compose(
  defaultProps({
    errors: [],
    isLoading: false,
  }),
  withPropsOnChange(
    ['onLogout'],
    ({ onLogout }) => ({
      onLogout: runOnceInTime(onLogout, 1000),
    })
  ),  
)

const FormLogout = enhance(({
  errors,
  isLoading,
  onLogout,  
}) => {
  return (
    <Form loading={isLoading} error={errors.length > 0}>
      {errors.length ? (
        <Message error content={errors[0].message} />
      ) : (
        <Message info content='Welcome %username%' />
      )}
      <Block center middle>
        <Button
          type='submit'

          onClick={(e) => {
            e.preventDefault()
            onLogout && onLogout()
          }}
        >
          Logout
        </Button>
      </Block>
    </Form>
  )
})

export default FormLogout
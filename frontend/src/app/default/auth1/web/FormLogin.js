import React, { Component } from 'react'
import compose              from 'recompose/compose'
import defaultProps         from 'recompose/defaultProps'
import withPropsOnChange    from 'recompose/withPropsOnChange'

import { Button }           from 'semantic-ui-react'
import { Checkbox }         from 'semantic-ui-react'
import { Form }             from 'semantic-ui-react'
import { Message }          from 'semantic-ui-react'


import Block                from 'evoke-me/layout/web/Block'


const runOnceInTime = (run, timeout) => {
  let time

  return (...args) => {
    const next = Date.now()
    if (!time || next - time >= timeout) {
      time = next
      return run(...args)
    }
  }
}

const enhance = compose(
  defaultProps({
    username: ``,
    password: ``,
    errors: [],
    isLoading: false,
  }),
  withPropsOnChange(
    'onLogin',
    ({ onLogin }) => ({
      onLogin: runOnceInTime(onLogin, 1000)
    })
  ),
)

const FormLogin = enhance((props) => {
  const {
    username,
    password,

    errors,

    isLoading,

    onLogin,
    onChangeUsername,
    onChangePassword,
  } = props

  return (
    <Form loading={isLoading} error={errors.length > 0}>
      {errors.length ? (
        <Message error content={errors[0].message} />
      ) : (
        <Message info content='Write username and password' />
      )}
      <Block center middle>
        <Form.Field>
          <label>Email</label>
          <input
            placeholder='User name'
            value={username}

            onChange={({ target: { value } }) => onChangeUsername && onChangeUsername(value)}
          />
        </Form.Field>
        <Form.Field>
          <label>Password</label>
          <input
            placeholder='Password'
            type='password'
            value={password}

            onChange={({ target: { value } }) => onChangePassword && onChangePassword(value)}
          />
        </Form.Field>
        <Form.Field>
          <Checkbox label='I agree to the Terms and Conditions' />
        </Form.Field>
        <Button
          type='submit'

          onClick={(e) => {
            e.preventDefault()
            onLogin()
          }}
        >
          Login
        </Button>
      </Block>
    </Form>
  )
})

export default FormLogin

import React, { Component } from 'react'
import compose              from 'recompose/compose'
import defaultProps         from 'recompose/defaultProps'
import withPropsOnChange    from 'recompose/withPropsOnChange'

import { Button }           from 'semantic-ui-react'
import { Icon }             from 'semantic-ui-react'
import { Form }             from 'semantic-ui-react'
import { Message }          from 'semantic-ui-react'


import Block                from 'evoke-me/layout/web/Block'


const runOnceInTime = (run, timeout) => {
  let time

  return (...args) => {
    const next = Date.now()
    if (!time || next - time >= timeout) {
      time = next
      return run(...args)
    }
  }
}

const enhance = compose(
  defaultProps({
    errors: [],
    isLoading: false,
  }),
  withPropsOnChange(
    ['onLogin'],
    ({ onLogin }) => ({
      onLogin: onLogin ? runOnceInTime(onLogin, 1000) : () => {},
    })
  ),
)

const FormFbLogin = enhance((props) => {
  const {
    errors,

    isLoading,

    onLogin,
  } = props

  return (
    <Form
      loading={ isLoading }
      error={ errors.length > 0 }
    >
      {errors.length ? (
        <Message error content={errors[0].message} />
      ) : (
        <Message info content='To Facebook login click the button' />
      )}
      <Block center middle>
        <Button
          type='submit'

          onClick={(e) => {
            e.preventDefault()
            onLogin()
          }}

          icon
        >
          <Icon name='facebook' />
          Login
        </Button>
      </Block>
    </Form>
  )
})

export default FormFbLogin

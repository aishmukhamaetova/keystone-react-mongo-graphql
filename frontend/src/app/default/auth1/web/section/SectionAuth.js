import React, { Component } from 'react'
import { Container }        from 'react-grid-system'
import { Col }              from 'react-grid-system'
import { Row }              from 'react-grid-system'

import { Header }           from 'semantic-ui-react'
import { Icon }             from 'semantic-ui-react'
import { Segment }          from 'semantic-ui-react'


const SectionAuth = ({ theme = {} }) => {
  const { BlockAuth } = theme

  if (!BlockAuth) {
    return (
      <Segment placeholder>
        <Header icon>
          <Icon name='pdf file outline' />
          SectionAuth cannot find BlockAuth
        </Header>
      </Segment>
    )
  }

  return (
    <Container fluid style={{ padding: 20 }}>
      <Row justify='center'>
        <Col sm={10} lg={6}>
          <BlockAuth />
        </Col>
      </Row>
    </Container>
  )
}

export default SectionAuth
import React, { Component } from 'react'
import { Container }        from 'react-grid-system'
import { Col }              from 'react-grid-system'
import { Row }              from 'react-grid-system'

import { Header }           from 'semantic-ui-react'
import { Icon }             from 'semantic-ui-react'
import { Segment }          from 'semantic-ui-react'


const SectionFbAuth = ({ theme = {} }) => {
  const { BlockFbAuth } = theme

  if (!BlockFbAuth) {
    return (
      <Segment placeholder>
        <Header icon>
          <Icon name='pdf file outline' />
          SectionFbAuth cannot find BlockFbAuth
        </Header>
      </Segment>
    )
  }

  return (
    <Container fluid style={{ padding: 20 }}>
      <Row justify='center'>
        <Col sm={10} lg={6}>
          <BlockFbAuth />
        </Col>
      </Row>
    </Container>
  )
}

export default SectionFbAuth

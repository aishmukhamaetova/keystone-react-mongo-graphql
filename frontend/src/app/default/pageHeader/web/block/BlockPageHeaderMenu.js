import { withRouter }       from 'next/router'
import React, { Component } from 'react'
import compose              from 'recompose/compose'


import Menu                 from 'app/default/pageHeader/web/Menu'
import Block                from 'evoke-me/layout/web/Block'
import withSpace            from 'evoke-me/space/all/withSpace'


import spaceNav             from '../../../nav/space/spaceNav'
import spaceMenu            from '../../../menu/space/spaceMenu'
import spacePageHeader      from '../../space/spacePageHeader'


const enhance = compose(
  withRouter,
  withSpace(spacePageHeader),
  withSpace(spaceMenu),
  withSpace(spaceNav),
)

const BlockPageHeaderMenu = enhance(({
  fromMenu,
  fromNav,
  fromPageHeader,
  router,
}) => {
  const { data, orderMap } = fromMenu
  const { height } = fromPageHeader
  const { go } = fromNav

  const {
    headerLeft = [],
    headerRight = [],
  } = orderMap

  return (
    <Block
      center
      middle
      maxWidth
      style={{ height }}
    >
      <Menu
        data={data}
        orderLeft={headerLeft}
        orderRight={headerRight}
        onClick={({ id }) => go(data[id].path, router.push)}
      />
    </Block>
  )
})

export default BlockPageHeaderMenu

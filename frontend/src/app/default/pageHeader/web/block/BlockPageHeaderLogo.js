import React, { Component } from 'react'
import compose              from 'recompose/compose'


import Logo                 from 'app/default/pageHeader/web/Logo'
import withSpace            from 'evoke-me/space/all/withSpace'
import Block                from 'evoke-me/layout/web/Block'

import spacePageHeader      from '../../space/spacePageHeader'


const enhance = compose(
  withSpace(spacePageHeader),
)

const BlockPageHeaderLogo = enhance(({
  fromPageHeader,
}) => {
  const { logo } = fromPageHeader

  return (
    <Block right middle>
      <Logo {...logo} />
    </Block>
  )
})

export default BlockPageHeaderLogo

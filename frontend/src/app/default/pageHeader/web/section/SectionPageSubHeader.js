import React, {Component} from 'react';
import {Grid, Hidden, ButtonBase, Button} from '@material-ui/core';
import compose from 'recompose/compose'
import {withStateHandlers} from 'recompose'
import defaultProps from 'recompose/defaultProps'

import styled from 'styled-components';
import {rem} from 'polished'
import {IconMenu, IconLogo, IconCart} from 'app/honey/icons'
import {Clear} from '@material-ui/icons';

import { Link }             from 'app/honey/page/api/routes'

import SectionPageBurgerMenu          from './SectionPageBurgerMenu'

import posed from 'react-pose';

const map = {
  sm: 600,
  md: 960,
  lg: 1280,
  xl: 1920,
}

const mediaGrid = (type) => () => `@media (max-width: ${map[type] - 1}px)`

const enhance = compose(
  
  defaultProps({
    A: {
      IconCart: styled(IconCart)`
        && {
        }
      `,
      ButtonMenu: styled.div`
        && {
          display: block;
          font-size: ${rem(14)};
          padding-top: ${rem(4)};
          font-weight: 700;
          text-transform: uppercase;  
          color: #000000;
          cursor: pointer;
          a {
            text-transform: uppercase;  
            color: #000000;
            &:hover {
              color: rgba(0, 0, 0, 0.7);
              text-decoration: none;
            }
          }
          &:hover {
            color: rgba(0, 0, 0, 0.7);
            text-decoration: none;
          }
        }
      `,      
      IconLogoWrap: styled.div`
      && {
        margin-top: -98px; 
        text-align: left;
      
        ${mediaGrid(`sm`)}{//phone
          text-align: center;
        }
      }
      `,
            
      Root: styled.div`
      && {
        position: relative;
        overflow: hidden;
       }
      `,
            
      GridCustom: styled(({ background, margin, ...other }) => (
        <Grid {...other} />
      ))`
      && {
        position: relative;
        height: ${rem(72)};
        background: #ffffff;
        z-index: 1000;
        box-shadow: 0px 2px 4px -1px rgba(0, 0, 0, 0.2), 0px 4px 5px 0px rgba(0, 0, 0, 0.14), 0px 1px 10px 0px rgba(0, 0, 0, 0.12);
      }
      `,
      
      ContainerIcon: styled(
        posed.div({
          show: {
            opacity: 1,
          },
          hide: {
            opacity: 0,
          }
        })
      )`
      
      && {
        display: ${props => props.display || 'flex'};
      
      }
      `
      
    }
  }),

  withStateHandlers(
    () => ({
      showMenu: false,
      menu: [{title: 'subscriptions', name: 'subscriptions'}, {title: 'shop', name: 'products'}, {title: 'b2b', name: 'b2b'}, {title: 'our story', name: 'about'}],
    }),
    {
      toggleMenu: ({ showMenu }) => () => ({
        showMenu: !showMenu
      }),
    }
  ),
    
)


const SectionPageSubHeader = enhance(({
  showMenu, toggleMenu, A, menu
}) => {
  return (
    <A.Root>
      <A.GridCustom container direction={'row'} justify="center" alignItems={'center'} spacing={0}>

        <Grid item xs={11}>

          <Grid container alignItems="stretch" direction="row" justify="space-between" style={{ position: 'relative', height: '25px' }}>

            <Grid item xs={2} sm={4}>

              <Hidden smDown>
                <A.IconLogoWrap style={{ fontSize: 220 }}>
                 <Link route='index' >
                    <a><IconLogo coloricon="#4a4a4a"/></a>
                  </Link>
                </A.IconLogoWrap>
              </Hidden>

              <Hidden mdUp>
                <A.ContainerIcon pose={!showMenu ? 'show' : 'hide'} display={!showMenu ? 'flex' : 'none'} >
                    <ButtonBase disableripple="true" disabletouchripple="true"  onClick={toggleMenu}>
                      <IconMenu/>
                    </ButtonBase>
                </A.ContainerIcon>
                <A.ContainerIcon pose={showMenu ? 'show' : 'hide'} display={showMenu ? 'flex' : 'none'} >
                    <ButtonBase style={{color: '#4a4a4a'}} disableripple="true" disabletouchripple="true"  onClick={toggleMenu}>
                      <Clear color="inherit" style={{fontSize: 30}}/>
                    </ButtonBase>
                </A.ContainerIcon>
              </Hidden>

            </Grid>


            <Grid item xs={8} sm={6}>

              <Hidden mdUp>
                <A.IconLogoWrap style={{ fontSize: '221px' }}>
                  <IconLogo coloricon="#4a4a4a"/>
                </A.IconLogoWrap>
              </Hidden>

              <Hidden smDown>
                <Grid container direction={'row'} justify="space-between" alignItems={'center'} spacing={40}>
                  {menu.map((value, index) => (
                    <Grid key={index} item>
                      <A.ButtonMenu>
                        <Link route={value.name} ><a>{value.title}</a></Link>
                      </A.ButtonMenu>
                    </Grid>
                  ))}
                </Grid>
              </Hidden>

            </Grid>

            <Grid item xs={2} style={{ textAlign: 'right' }}>
            <Link route='cart' >
              <a><A.IconCart/></a>
            </Link>

            </Grid>


          </Grid>
        </Grid>
      </A.GridCustom>

      <Hidden mdUp>
        <SectionPageBurgerMenu showMenu={showMenu} />
      </Hidden>
          
    </A.Root>
  )
})

export default SectionPageSubHeader;
import React, { Component } from 'react'
import ContainerDimensions  from 'react-container-dimensions'
import styled               from 'styled-components'


const Root = styled.div`
  width: 100vw;
  height: 630px;
  overflow: hidden;
  position: absolute;
  z-index: -99;  
`
const Video = styled.video`
video{
  @media (min-aspect-ratio: 16/9) {
    width: 100%;
    height: auto;
  }
 
  @media (max-aspect-ratio: 16/9) {
    width: auto;
    height: 100%;
    left: 50%;
    transform: translateX(-50%);
  }
}

`

const VideoBackground = () => {
  return (
    <Root>
      <ContainerDimensions>
        {({ width: widthSource }) => {
          const width = widthSource < 1480 ? 1480 : widthSource
          const height = 630 * (width / 1480)
          const marginLeft = widthSource < 740 ? - 1480 / 2 + widthSource / 2 : 0

          return (

            <Video
              autoPlay
              controls={false}
              muted
              loop
              width={width}
              height={height}
              style={{ marginLeft }}
            >
              <source src='https://cdn.evokeme.io/static/latelierdumiel/launch.mp4' type='video/mp4' />
              <source src='https://cdn.evokeme.io/static/latelierdumiel/launch.ogv' type='video/ogg' />
            
            </Video>
            // <iframe
            //   allowFullScreen
            //   frameBorder={0}
            //   width={width}
            //   height={height}
            //   src='https://www.youtube.com/embed/2fkCsrI-Rz4?autoplay=1&showinfo=0&controls=0&&mute=1&loop=1&playlist=2fkCsrI-Rz4'
            //   style={{ marginLeft }}
            // />
          )
        }}
      </ContainerDimensions>
    </Root>
  )

  
}

export default VideoBackground

// <source src='https://my.mixtape.moe/wzpbpv.mp4' type='video/mp4' />
// https://streamable.com/s/vte31/hhrvdz

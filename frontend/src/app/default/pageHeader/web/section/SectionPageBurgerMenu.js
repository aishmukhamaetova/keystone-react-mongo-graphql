import React, {Component} from 'react';
import {Grid, Button} from '@material-ui/core';
import {ExpandMore, ExpandLess} from '@material-ui/icons';
import compose from 'recompose/compose'
import {withStateHandlers} from 'recompose'
import styled from 'styled-components';
import defaultProps from 'recompose/defaultProps'
import posed from 'react-pose';
import {rem} from 'polished'

const SubMenu = styled(
  posed.div({
    show: {
      opacity: 1,
      height: 'auto',
    },
    hide: {
      opacity: 0,
      height: 0,
    }
  })
)`

`;


const ContainerIcon = styled(
  posed.div({
    show: {
      opacity: 1,
      delayChildren: 100,
      staggerChildren: 50
    },
    hide: {
      opacity: 0,
      delayChildren: 100,
      staggerChildren: 50
      
    }
  })
)`

&& {
  position: absolute;
  right: ${rem(20)};
  top: auto;
  display: ${props => props.display || 'flex'};

}
`

const Root = styled(
  posed.div({
    show: {
      x: 0,
    },
    hide: {
      x: '+100%'
    }
  })
)`
  position: relative;
  z-index: 10000;
  top: 0;
  left: 0;
  height: 100%;
  width: 100%;
  background: #ffffff;
  z-index: 1000;
  box-shadow: 0px 2px 4px -1px rgba(0, 0, 0, 0.2), 0px 4px 5px 0px rgba(0, 0, 0, 0.14), 0px 1px 10px 0px rgba(0, 0, 0, 0.12);
`;


const TitleButton =  styled(Button)`
&& {
  font-size: ${rem(18)};
  position: relative;
}
`
const Container = styled.div`
&& {
  position: relative;
}
`

const enhance = compose(
  
  defaultProps({
  }),

  withStateHandlers(
    () => ({
      showSubMenu: {},
      menuIdx: 0
    }),
    {
      toggleSubMenu: ({ showSubMenu }) => (menuIdx) => {
        showSubMenu[menuIdx] = !showSubMenu[menuIdx];
        return ({
          showSubMenu,
          menuIdx
        })
      }
    }
  ),
    
)


const MenuHeader = ({title, showSubMenu, toggleSubMenu, idx, menuIdx}) => (
  <Container>
    <Grid container direction={'column'} justify="center" alignItems="stretch" spacing={0}>
      <Grid item >
      
        <TitleButton onClick={()=>toggleSubMenu(idx)} fullWidth disableripple="true" disabletouchripple="true" >
         
          {title}
         
          <ContainerIcon pose={!showSubMenu[idx] ? 'show' : 'hide'} display={!showSubMenu[idx] ? 'flex' : 'none'} >
            <ExpandMore/>
          </ContainerIcon>
         
          <ContainerIcon pose={showSubMenu[idx] ? 'show' : 'hide'} display={showSubMenu[idx] ? 'flex' : 'none'} >
            <ExpandLess/>
          </ContainerIcon>

        </TitleButton>

      </Grid>
    </Grid>

  </Container>

)

const SectionPageBurgerMenu = enhance(({
  showMenu, toggleSubMenu, showSubMenu, menuIdx
}) => {
  return (
    <Root pose={showMenu ? 'show' : 'hide'} style={{display: showMenu ? 'flex' : 'none', height: showMenu ? 'auto' : '0'}}>
          
      <Grid container direction={'column'} justify="center" alignItems="stretch" spacing={8}>

        {['title1' ,'title2', 'title3'].map((arg, idx) => (
          <Grid item key={idx}>

            <MenuHeader menuIdx={menuIdx} idx={idx} title={arg} toggleSubMenu={toggleSubMenu} showSubMenu={showSubMenu} />

            <SubMenu  pose={showSubMenu[idx] ? 'show' : 'hide'} style={{display: showSubMenu[idx] ? 'flex' : 'none', height: showSubMenu[idx] ? 'auto' : '0'}} >

              <Grid container direction={'column'} justify="center" alignItems="stretch" spacing={0}>
                  {['sub1', 'sub2', 'sub3'].map((sub, index) => (
                    <Grid item key={index}>
                      <TitleButton fullWidth disableripple="true" disabletouchripple="true" >{sub}</TitleButton>
                    </Grid>

                  ))}
                </Grid>
      
            </SubMenu>

          </Grid>
        ))}
      </Grid>


  </Root>
  )
})

export default SectionPageBurgerMenu;
import React, {Component} from 'react'
import compose from 'recompose/compose'
import styled from 'styled-components'
import {rem} from 'polished'
import stripe1 from 'app/honey/media/stripe1.svg'
import stripe2 from 'app/honey/media/stripe2.svg'
import stripe3 from 'app/honey/media/stripe3.svg'

const StripeBlock = styled.div`
   background-position: center left;
   background-repeat: repeat-x;
   background-image: url(${props => props.type === 1 ? stripe1 : props.type === 2 ? stripe2 : props.type === 3 ? stripe3 : stripe1});
   height: ${rem(40)};
   margin: ${rem(70)} 0;
`
const StripeEmpty = styled.div`
  margin: 0 0 ${rem(70)} 0;
`

const enhance = compose(
 )

const Stripe = enhance(({
  type = 1, empty = false
}) => {
  return (
      empty ? <StripeEmpty/> : <StripeBlock type={type} />
    )
  })


export default Stripe
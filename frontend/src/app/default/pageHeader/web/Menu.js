import React, { Component } from 'react'
import compose              from 'recompose/compose'
import defaultProps         from 'recompose/defaultProps'
import { Menu }             from 'semantic-ui-react'



const enhance = compose(
  defaultProps({
    data: {
      'home': { id: `home`, caption: `Home`, },
      'login': { id: `login`, caption: `Login`, },
    },
    orderLeft: [`home`],
    orderRight: [`login`],
  })
)

const OurMenu = enhance(({
  data,
  orderLeft,
  orderRight,
  onClick,
}) => {
  return (
    <Menu pointing>
      {orderLeft.map((id) => (
        <Menu.Item
          key={id}
          name={data[id].caption}
          onClick={() => onClick && onClick({ id })}
        />
      ))}
      <Menu.Menu position='right'>
        {orderRight.map((id) => (
          <Menu.Item
            key={id}
            name={data[id].caption}
            onClick={() => onClick && onClick({ id })}
          />
        ))}
      </Menu.Menu>
    </Menu>
  )
})

export default OurMenu
import React, { Component } from 'react'


const Logo = ({
  imageLink,
  height,
}) => {
  return (
    <img
      src={imageLink}
      style={{ width: `auto`, height: 64 }}
    />
  )
}

export default Logo
import config               from '../../config/api/config'

export default [
  config.namespace,
  `pageHeader`,
  [],
  () => ({
    logo: {
      imageLink: `https://i.imgur.com/fGM1JLc.jpg`,
    },

    height: 64,
  }),
  {

  }
]
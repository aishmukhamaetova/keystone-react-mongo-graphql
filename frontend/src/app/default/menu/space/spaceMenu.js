import spaceDefaults        from 'evoke-me/space/api/spaceDefaults'
import config               from '../../config/api/config'


export default [
  config.namespace,
  `menu`,
  [],
  () => ({
    ...spaceDefaults.state,
  }),
  {
    ...spaceDefaults.updaters,
  }
]
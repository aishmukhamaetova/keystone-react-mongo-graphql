import store                from 'store'


import config               from '../../config/api/config'


export default [
  config.namespace,
  'persist',
  [],
  () => ({
  }),
  {
    get: () => function* (persistName) {
      try {
        return JSON.parse(store.get(persistName))
      } catch (err) {
        return null
      }
    },
    remove: () => function* (name) {
      store.remove(name)
    },
    set: () => function* (persistName, state) {
      store.set(persistName, JSON.stringify(state))
    },
  }
]

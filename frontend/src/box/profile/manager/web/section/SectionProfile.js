import Grid                 from '@material-ui/core/Grid'
import React, { Component } from 'react'
import compose              from 'recompose/compose'
import styled               from 'styled-components'


import BlockAuthFacebookLink from 'box/auth/facebook/web/block/BlockAuthFacebookLink'
import BlockOrderMyList     from 'box/eshop/order/web/block/BlockOrderMyList'
import withSpace            from 'evoke-me/space/all/withSpace'
import spaceProfile         from '../../space/spaceProfile'
import theme                from '../theme'


const Section = styled.div``

const enhance = compose(
  withSpace(spaceProfile),
)

const SectionProfile = enhance(({
  fromProfile
}) => {
  return (
    <Section>
      <Grid
        container
        justify='center'
        spacing={8}
      >
        <Grid item xs={4}>
          <BlockOrderMyList />
        </Grid>
        <Grid item xs={4}>
          <BlockAuthFacebookLink />
        </Grid>
      </Grid>
    </Section>
  )
})

export default SectionProfile

import config               from '../api/config'


import QUERY_ME             from '../api/keystone/graphql/client/boxAuthFacebookMeQuery'


export default [
  config.namespace,
  `authFacebook`,
  [],
  () => ({
    avatar: null,

    isLoading: false,
  }),
  {
    avatarSet: (getState, setState, { validate }) => function* (avatar) { yield setState({ avatar }) },

    me: (getState, setState, { avatarSet }, { fromAuth, fromNotify }) => function* (payload, { client }) {
      try {
        if (client) {
          yield setState({ isLoading: true })

          const { data } = yield client.query({
            query: QUERY_ME,
          })

          const { boxAuthFacebookMe } = data

          if (boxAuthFacebookMe) {
            const { result, errors } = boxAuthFacebookMe

            if (result && result.avatar) {
              yield avatarSet(result.avatar)
              yield setState({ isLoading: false })
            }
          }

          yield setState({ isLoading: false })
        }
      } catch (err) {
        console.log(err)

        yield setState({ isLoading: false })
      }
    },
  }
]

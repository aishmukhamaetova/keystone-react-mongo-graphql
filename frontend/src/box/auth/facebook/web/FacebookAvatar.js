import React, { Component } from 'react'
import styled               from 'styled-components'


const background = ({ imageLink }) => `background-image: url(${imageLink});`

const Image = styled.div`
  ${background}
  
  width: 200px;
  height: 200px;
  
  background-size: cover;
  background-position: center;
  background-repeat: no-repeat;  
`

const FacebookAvatar = (props) => {
  return (
    <Image {...props} />
  )
}

export default FacebookAvatar

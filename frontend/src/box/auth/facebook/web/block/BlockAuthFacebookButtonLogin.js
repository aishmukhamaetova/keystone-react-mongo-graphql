import { withRouter }       from 'next/router'
import React, { Component } from 'react'
import compose              from 'recompose/compose'
import styled               from 'styled-components'


import withSpace            from 'evoke-me/space/all/withSpace'
import spaceAuthFacebook    from '../../space/spaceAuthFacebook'
import ButtonFacebookLogin  from '../ButtonFacebookLogin'


const enhance = compose(
  withRouter,
  withSpace(spaceAuthFacebook),
)

const BlockAuthFacebookButtonLogin = enhance(({
  fromAuthFacebook,
  router,
}) => {
  return (
    <ButtonFacebookLogin
      onClick={() => router.push('/api/box.auth/facebook/login')}
    />
  )
})

export default BlockAuthFacebookButtonLogin

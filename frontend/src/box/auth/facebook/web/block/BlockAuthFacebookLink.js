import { withRouter }       from 'next/router'
import { withApollo }       from 'react-apollo'
import React, { Component } from 'react'
import compose              from 'recompose/compose'
import lifecycle            from 'recompose/lifecycle'
import withHandlers         from 'recompose/withHandlers'
import styled               from 'styled-components'


import withSpace            from 'evoke-me/space/all/withSpace'
import spaceAuthFacebook    from '../../space/spaceAuthFacebook'
import ButtonFacebookLink   from '../ButtonFacebookLink'
import FacebookAvatar       from '../FacebookAvatar'


const AvatarStyled = styled(FacebookAvatar)`
  margin: 5px;
`

const enhance = compose(
  withApollo,
  withRouter,
  withSpace(spaceAuthFacebook),
  withHandlers({
    mount: ({ client, fromAuthFacebook }) => async () => {
      fromAuthFacebook.me({}, { client })
    }
  }),
  lifecycle({
    componentDidMount() {
      this.props.mount()
    }
  })
)

const BlockAuthFacebookLink = enhance(({
  fromAuthFacebook,
  router,
}) => {
  const { avatar, isLoading } = fromAuthFacebook

  return (
    <React.Fragment>
      {avatar ? (
      <AvatarStyled imageLink={avatar} />
      ) : null}
      <ButtonFacebookLink
        onClick={() => router.push('/api/box.auth/facebook/link')}
      />
    </React.Fragment>
  )
})

export default BlockAuthFacebookLink

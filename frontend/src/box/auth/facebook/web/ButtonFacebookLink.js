import React, { Component } from 'react'
import compose              from 'recompose/compose'
import withPropsOnChange    from 'recompose/withPropsOnChange'

import { Button }           from 'semantic-ui-react'
import { Icon }             from 'semantic-ui-react'


import runOnceInTime        from 'evoke-me/space/api/runOnceInTime'


const enhance = compose(
  withPropsOnChange(
    ['onClick'],
    ({ onClick }) => ({
      onClick: runOnceInTime(onClick, 1000)
    })
  ),
)

const ButtonFacebookLogin = enhance((props) => {
  const {
    onClick,
  } = props

  return (
    <Button
      type='submit'
      icon
      fluid
      secondary

      onClick={(e) => {
        e.preventDefault()
        onClick()
      }}
    >
      Link profile with Facebook&nbsp;
      <Icon name='facebook' />
    </Button>
  )
})

export default ButtonFacebookLogin



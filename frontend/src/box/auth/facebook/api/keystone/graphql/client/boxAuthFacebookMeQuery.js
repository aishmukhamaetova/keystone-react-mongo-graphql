import gql                  from 'graphql-tag'


export default gql`
query boxAuthFacebookMe {
	boxAuthFacebookMe {
    result {
      avatar
    }
    
    errors {
      type
      code
      message
      props {
        name
        value
      }
    }
  }
}`

import config               from 'box/auth/facebook/api/config'
import authErrorMap         from 'box/auth/manager/api/authErrorMap'


const NAME = `Me`

const { name } = config
const { nameType } = config

const queryName = `${name}${NAME}`
const typeQuery = `${nameType}${NAME}`
const typePayload = `${typeQuery}Payload`
const typeResult = `${typeQuery}Result`

export const type = `
  type ${typeQuery} {
    result: ${typeResult}
    errors: [Error]
  }
  
  type ${typeResult} {
    avatar: String
  }
  
  input ${typePayload} {
    empty: Boolean
  }
`

export const query = `
  ${queryName}(
    payload: ${typePayload}
  ): ${typeQuery}
`

const mutation = ``

const mutationMap = {}

const error = (code, message, props = []) => ({ result: null, errors: [{
  type: config.namespace,
  code,
  message,
  props
}]})

const queryMap = {
  [queryName]: async (obj, args, { keystone, req, res }, info) => {
    try {
      const { user } = res.locals

      if (user) {
        const UserFacebook = keystone.list('UserFacebook')
        const userFacebook = await UserFacebook.model.findOne({ user })

        return {
          result: userFacebook,
          errors: [],
        }
      }

      return error(authErrorMap.FACEBOOK_NOT_AUTHENTICATED, `You are not authenticated`)
    } catch (err) {
      console.log(`ERROR`, queryName, err)

      const message = process.env.NODE_ENV === `development` ?
        err : `${queryName} unknown error`

      return error(authErrorMap.UNKNOWN, message)
    }
  }
}

export default {
  type,

  mutation,
  mutationMap,

  query,
  queryMap,
}

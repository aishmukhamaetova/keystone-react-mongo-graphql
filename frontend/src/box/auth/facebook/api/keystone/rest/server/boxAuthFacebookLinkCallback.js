const keystone = require('keystone')
const passport = require('passport')
const passportFacebook = require('passport-facebook')


const User = keystone.list('User')
const UserFacebook = keystone.list('UserFacebook')

const credentials = {
  clientID: process.env.FACEBOOK_CLIENT_ID,
  clientSecret: process.env.FACEBOOK_CLIENT_SECRET,
  callbackURL: process.env.FACEBOOK_LINK_URL,
}

export default async (req, res, next) => {
  const facebookStrategy = new passportFacebook.Strategy(
    credentials,
    (accessToken, refreshToken, profile, done) => {
      done(null, {
        accessToken: accessToken,
        refreshToken: refreshToken,
        profile: profile,
        profileFields: ['id', 'displayName', 'photos', 'email'],
      })
    }
  )

  passport.use(facebookStrategy)

  passport.authenticate(
    'facebook',
    { session: false },
    async (err, data, info) => {
      if (err || !data) {
        console.log('[services.facebook] - Error retrieving Facebook account data - ' + JSON.stringify(err))
        console.log('DATA - ' + JSON.stringify(data))
        return res.redirect('/enter')
      }

      const avatar = 'https://graph.facebook.com/' + data.profile.id + '/picture?width=600&height=600'
      const facebookId = data.profile.id

      const { user } = res.locals

      if (user) {
        const userFacebook = await UserFacebook.model.findOne({ user: user._id })

        if (userFacebook) {
          userFacebook.avatar = avatar
          userFacebook.facebookId = facebookId

          await userFacebook.save()
        } else {
          const userFacebookNew = await new UserFacebook.model({
            avatar,
            facebookId,
            user,
          }).save()
        }
      }

      return res.redirect(`${process.env.FACEBOOK_LINK_REDIRECT}`)
    }
  )(req, res, next)
}

export default (keystone) => {
  const Types = keystone.Field.Types
  const UserFacebook = new keystone.List('UserFacebook')

  UserFacebook.add({
    facebookId: { type: String, },
    avatar: { type: String, },

    user: { type: Types.Relationship, ref: 'User' },
  })

  UserFacebook.defaultColumns = 'facebookId, avatar'
  UserFacebook.register()
}


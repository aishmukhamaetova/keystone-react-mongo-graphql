import Joi                  from 'joi'


import errorConfig          from 'box/error/manager/api/config'
import errorMap             from 'box/error/manager/api/errorMap'
import spacePersist         from 'box/persist/manager/space/spacePersist'

import authScreenMap        from 'box/auth/manager/api/authScreenMap'
import config               from 'box/auth/manager/api/config'
import spaceAuth            from 'box/auth/manager/space/spaceAuth'

import QUERY_CHECK          from '../api/keystone/graphql/client/boxAuthClassicCheckQuery'
import MUTATION_CONFIRM     from '../api/keystone/graphql/client/boxAuthClassicConfirm'
import MUTATION_LOGIN       from '../api/keystone/graphql/client/boxAuthClassicLogin'
import MUTATION_LOGOUT      from '../api/keystone/graphql/client/boxAuthClassicLogout'
import MUTATION_SIGNUP      from '../api/keystone/graphql/client/boxAuthClassicSignup'

import schemaJoi            from '../api/schemaJoi'


const ENTITY = `authClassic`

export default [
  config.namespace,
  ENTITY,
  [
    spaceAuth,
    spacePersist,
  ],
  () => ({
    nameFirst: ``,
    nameLast: ``,
    username: ``,
    password: ``,

    errors: [],
    validateErrors: [],

    isConfirmed: false,
    isLoading: false,

    redirectAfterConfirm: `/`,

    persist: true,
    persistName: `${config.namespace}.${ENTITY}`
  }),
  {
    restore: (getState, setState, methods, { fromPersist }) => function* () {
      const { persist, persistName } = yield getState()
      if (persist) {
        const statePersist = yield fromPersist.get(persistName)
        if (statePersist) {
          const { username } = statePersist
          yield setState({ username })
        }
      }
    },

    validate: (getState, setState) => function* (payload) {
      const { nameFirst, nameLast, username, password } = yield getState()
      const { error } = Joi.validate({
        nameFirst,
        nameLast,
        username,
        password,
      }, schemaJoi)

      if (error && error.details) {
        yield setState({
          validateErrors: error.details.map((current) => {
            const { path, message } = current
            return {
              type: errorConfig.namespace,
              code: errorMap.VALIDATE,
              message,
              props: {
                path,
              }
            }
          })
        })
      } else {
        yield setState({ validateErrors: [] })
      }
    },

    nameFirstSet: (getState, setState, { validate }) => function* (nameFirst) {
      yield setState({ nameFirst })
      yield validate({ nameFirst })
    },

    nameLastSet: (getState, setState, { validate }) => function* (nameLast) {
      yield setState({ nameLast })
      yield validate({ nameLast })
    },

    usernameSet: (getState, setState, { validate }, { fromPersist }) => function* (username) {
      const { persist, persistName } = yield getState()
      yield setState({ username })
      yield validate({ username })

      if (persist) {
        yield fromPersist.set(persistName, { username })
      }
    },

    passwordSet: (getState, setState, { validate }) => function* (password) {
      yield setState({ password })
      yield validate({ password })
    },

    check: (getState, setState, methods, { fromAuth, fromNotify }) => function* ({ emailCode }, { client }) {
      try {
        if (client) {
          const { data } = yield client.query({
            query: QUERY_CHECK,
          })

          const { boxAuthClassicCheck } = data

          if (boxAuthClassicCheck && boxAuthClassicCheck.result && boxAuthClassicCheck.result.status) {
            yield fromAuth.isAuthSet(true)
          } else {
            yield fromAuth.isAuthSet(false)
          }
        }
      } catch (err) {
        console.log(err)
      }
    },

    confirm: (getState, setState, methods, { fromAuth, fromNotify }) => function* ({ emailCode }, { client }) {
      try {
        if (client) {
          yield setState({ isLoading: true })

          const { data } = yield client.mutate({
            mutation: MUTATION_CONFIRM,
            variables: {
              emailCode,
            }
          })

          const { boxAuthClassicConfirm } = data

          if (boxAuthClassicConfirm) {
            const { result } = boxAuthClassicConfirm

            const errors = boxAuthClassicConfirm.errors.map(({ type, code, message, props}) => {
              return {
                type, code, message,
                props: props.reduce((acc, { name, value }) => acc[name] = value, {})
              }
            })

            if (errors.length) {
              yield setState({
                isLoading: false,
                errors,
              })
            } else {
              yield fromAuth.isAuthSet(true)
              yield setState({
                password: ``,
                isLoading: false,
                isConfirmed: true,
                errors: []
              })
            }
          }
        } else {
          console.log(`apolloClient miss`)
        }
      } catch (err) {
        console.log(err)

        yield setState({
          isLoading: false,
          errors: []
        })
      }
    },

    login: (getState, setState, methods, { fromAuth, fromNotify }) => function* (payload, { client }) {
      try {
        if (client) {
          yield setState({ isLoading: true })

          const { username, password } = yield getState()

          const { data } = yield client.mutate({
            mutation: MUTATION_LOGIN,
            variables: {
              username,
              password,
            }
          })

          const { boxAuthClassicLogin } = data

          if (boxAuthClassicLogin) {
            const { result } = boxAuthClassicLogin

            const errors = boxAuthClassicLogin.errors.map(({ type, code, message, props}) => {
              return {
                type, code, message,
                props: props.reduce((acc, { name, value }) => acc[name] = value, {})
              }
            })

            if (errors.length) {
              yield setState({
                isLoading: false,
                errors,
              })
            } else {
              yield fromAuth.isAuthSet(true)
              yield setState({
                password: ``,
                isLoading: false,
                errors: [],
              })
            }
          }
        } else {
          console.log(`apolloClient miss`)
        }
      } catch (err) {
        console.log(err)

        yield setState({
          isLoading: false,
          errors: [],
        })
      }
    },

    logout: (getState, setState, methods, { fromAuth, fromNotify }) => function* (payload, { client }) {
      try {
        if (client) {
          yield setState({ isLoading: true })

          const { data } = yield client.mutate({
            mutation: MUTATION_LOGOUT,
          })

          const { boxAuthClassicLogout } = data

          if (boxAuthClassicLogout) {
            const { result } = boxAuthClassicLogout

            const errors = boxAuthClassicLogout.errors.map(({ type, code, message, props}) => {
              return {
                type, code, message,
                props: props.reduce((acc, { name, value }) => acc[name] = value, {})
              }
            })

            if (errors.length) {
              yield setState({
                isLoading: false,
                errors,
              })
            } else {
              yield fromAuth.isAuthSet(false)
              yield setState({
                isLoading: false,
                errors: [],
              })
            }
          }
        } else {
          console.log(`apolloClient miss`)
        }
      } catch (err) {
        console.log(err)

        yield setState({
          isLoading: false,
          errors: [],
        })
      }
    },

    signup: (getState, setState, methods, { fromAuth, fromNotify }) => function* (payload, { client }) {
      try {
        if (client) {
          yield setState({ isLoading: true })

          const {
            username,
            password,
            nameFirst,
            nameLast,
          } = yield getState()

          const { data } = yield client.mutate({
            mutation: MUTATION_SIGNUP,
            variables: {
              username,
              password,
              nameFirst,
              nameLast,
            }
          })

          const { boxAuthClassicSignup } = data

          if (boxAuthClassicSignup) {
            const { result } = boxAuthClassicSignup

            const errors = boxAuthClassicSignup.errors.map(({ type, code, message, props}) => {
              return {
                type, code, message,
                props: props.reduce((acc, { name, value }) => acc[name] = value, {})
              }
            })

            if (errors.length) {
              yield setState({
                isLoading: false,
                errors,
              })
            } else {
              yield fromAuth.screenSet(authScreenMap.LOGIN)
              yield setState({
                password: ``,
                isLoading: false,
                errors: [],
              })
            }
          }
        } else {
          console.log(`apolloClient miss`)
        }
      } catch (err) {
        console.log(err)

        yield setState({
          isLoading: false,
          errors: [],
        })
      }
    },
  }
]

import Grid                 from '@material-ui/core/Grid'
import React, { Component } from 'react'
import styled               from 'styled-components'


import BlockConfirmAuto     from '../block/BlockAuthClassicConfirmAuto'
import theme                from '../theme'


const Section = styled.div``

const Item = styled(Grid)`
  max-width: 320px !important; 
`

const SectionAuthClassicConfirmAuto = () => {
  return (
    <Section>
      <Grid
        container
        justify='center'
        spacing={8}
      >
        <Item item xs={8}>
          <BlockConfirmAuto theme={theme} />
        </Item>
      </Grid>
    </Section>
  )
}

export default SectionAuthClassicConfirmAuto

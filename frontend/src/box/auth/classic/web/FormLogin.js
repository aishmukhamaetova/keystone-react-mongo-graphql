import React, { Component } from 'react'
import compose              from 'recompose/compose'
import defaultProps         from 'recompose/defaultProps'
import withPropsOnChange    from 'recompose/withPropsOnChange'

import { Button }           from 'semantic-ui-react'
import { Divider }          from 'semantic-ui-react'
import { Form }             from 'semantic-ui-react'
import { Header }           from 'semantic-ui-react'
import { Message }          from 'semantic-ui-react'


import runOnceInTime        from 'evoke-me/space/api/runOnceInTime'


const enhance = compose(
  defaultProps({
    username: ``,
    password: ``,
    errors: [],
    isLoading: false,
  }),
  withPropsOnChange(
    ['onSubmit'],
    ({ onSubmit }) => ({
      onSubmit: onSubmit ? runOnceInTime(onSubmit, 1000) : null
    })
  ),
)

const FormLogin = enhance((props) => {
  const {
    children,

    username, usernameSet,
    password, passwordSet,

    errors,

    isLoading,

    onSubmit,
  } = props

  const count = React.Children.count(children)

  return (
    <Form loading={isLoading} error={errors.length > 0}>
      {errors.length ? (
        <Message error content={errors[0].message} />
      ) : (
        <Header>Log in</Header>
      )}
      <Form.Field>
        <label>Email</label>
        <input
          placeholder='Email'
          value={username}

          onChange={({ target: { value } }) => usernameSet && usernameSet(value)}
        />
      </Form.Field>
      <Form.Field>
        <label>Password</label>
        <input
          placeholder='Password'
          type='password'
          value={password}

          onChange={({ target: { value } }) => passwordSet && passwordSet(value)}
        />
      </Form.Field>
      <Button
        type='submit'
        positive
        fluid

        onClick={(e) => {
          e.preventDefault()
          onSubmit && onSubmit()
        }}
      >
        Login
      </Button>
      {count ? (
        <React.Fragment>
          <Divider horizontal>Or</Divider>
          {children}
        </React.Fragment>
      ) : null}
    </Form>
  )
})

export default FormLogin

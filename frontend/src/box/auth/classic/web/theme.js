import BlockAuthClassicConfirmAuto from './block/BlockAuthClassicConfirmAuto'
import BlockAuthClassicFormLogin from './block/BlockAuthClassicFormLogin'
import BlockAuthClassicFormLogout from './block/BlockAuthClassicFormLogout'
import BlockAuthClassicFormSignup from './block/BlockAuthClassicFormSignup'


export default {
  BlockAuthClassicConfirmAuto,
  BlockAuthClassicFormLogin,
  BlockAuthClassicFormLogout,
  BlockAuthClassicFormSignup,
}

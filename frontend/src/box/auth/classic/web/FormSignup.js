import React, { Component } from 'react'
import compose              from 'recompose/compose'
import defaultProps         from 'recompose/defaultProps'
import withPropsOnChange    from 'recompose/withPropsOnChange'

import { Button }           from 'semantic-ui-react'
import { Form }             from 'semantic-ui-react'
import { Header }           from 'semantic-ui-react'
import { Message }          from 'semantic-ui-react'


import runOnceInTime        from 'evoke-me/space/api/runOnceInTime'


const enhance = compose(
  defaultProps({
    username: ``,
    password: ``,
    validateErrors: [],
    isLoading: false,
  }),
  withPropsOnChange(
    ['onSubmit'],
    ({ onSubmit }) => ({
      onSubmit: onSubmit ? runOnceInTime(onSubmit, 1000) : null
    })
  ),
)

const FormSignup = enhance((props) => {
  const {
    children,

    nameFirst, nameFirstSet,
    nameLast, nameLastSet,
    username, usernameSet,
    password, passwordSet,

    validateErrors,

    isLoading,

    onSubmit,
  } = props

  const errorFields = validateErrors.map(({ props: { path }}) => path[0])

  return (
    <Form loading={isLoading} error={validateErrors.length > 0}>
      {validateErrors.length ? (
        <Message error content={validateErrors[0].message} />
      ) : (
        <Header>Sign up</Header>
      )}
      <Form.Field error={errorFields.includes(`nameFirst`)}>
        <label>First name</label>
        <input
          placeholder='First name'
          value={nameFirst}

          onChange={({ target: { value } }) => nameFirstSet && nameFirstSet(value)}
        />
      </Form.Field>
      <Form.Field error={errorFields.includes(`nameLast`)}>
        <label>Second name</label>
        <input
          placeholder='Last name'
          value={nameLast}

          onChange={({ target: { value } }) => nameLastSet && nameLastSet(value)}
        />
      </Form.Field>
      <Form.Field error={errorFields.includes(`username`)}>
        <label>Email</label>
        <input
          placeholder='Email'
          value={username}

          onChange={({ target: { value } }) => usernameSet && usernameSet(value)}
        />
      </Form.Field>
      <Form.Field error={errorFields.includes(`password`)}>
        <label>Password</label>
        <input
          placeholder='Password'
          type='password'
          value={password}

          onChange={({ target: { value } }) => passwordSet && passwordSet(value)}
        />
      </Form.Field>
      <Button
        type='submit'
        fluid
        positive

        onClick={(e) => {
          e.preventDefault()
          onSubmit && onSubmit()
        }}
      >
        Register Now
      </Button>
      {children}
    </Form>
  )
})

export default FormSignup

import React, { Component } from 'react'
import compose              from 'recompose/compose'
import defaultProps         from 'recompose/defaultProps'
import withPropsOnChange    from 'recompose/withPropsOnChange'

import { Button }           from 'semantic-ui-react'
import { Form }             from 'semantic-ui-react'
import { Message }          from 'semantic-ui-react'


import runOnceInTime        from 'evoke-me/space/api/runOnceInTime'


const enhance = compose(
  defaultProps({
    errors: [],
    isLoading: false,
  }),
  withPropsOnChange(
    ['onSubmit'],
    ({ onSubmit }) => ({
      onSubmit: onSubmit ? runOnceInTime(onSubmit, 1000) : null
    })
  ),  
)

const FormLogout = enhance(({
  errors,
  isLoading,
  onSubmit,
}) => {
  return (
    <Form loading={isLoading} error={errors.length > 0}>
      {errors.length ? (
        <Message error content={errors[0].message} />
      ) : (
        <Message info content='Welcome %username%' />
      )}
      <Button
        type='submit'
        positive
        fluid

        onClick={(e) => {
          e.preventDefault()
          onSubmit && onSubmit()
        }}
      >
        Logout
      </Button>
    </Form>
  )
})

export default FormLogout

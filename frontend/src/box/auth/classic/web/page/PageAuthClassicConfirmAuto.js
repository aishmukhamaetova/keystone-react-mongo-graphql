import React, { Component } from 'react'


import Page                 from 'evoke-me/page/web/Page'

import SectionAuthClassicConfirmAuto from '../section/SectionAuthClassicConfirmAuto'


const PageAuthClassicConfirmAuto = () => {
  return (
    <Page>
      <SectionAuthClassicConfirmAuto />
    </Page>
  )
}

export default PageAuthClassicConfirmAuto

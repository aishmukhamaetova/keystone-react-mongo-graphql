import { withRouter }       from 'next/router'
import React, { Component } from 'react'
import { withApollo }       from 'react-apollo'
import compose              from 'recompose/compose'
import withHandlers         from 'recompose/withHandlers'
import lifecycle            from 'recompose/lifecycle'


import spaceAuthClassic     from 'box/auth/classic/space/spaceAuthClassic'

import withSpace            from 'evoke-me/space/all/withSpace'


const enhance = compose(
  withRouter,
  withApollo,
  withSpace(spaceAuthClassic),
  withHandlers({
    mount: ({
      client,
      fromAuthClassic,
      router,
    }) => async () => {
      const { query: { emailCode }} = router

      await fromAuthClassic.confirm({ emailCode }, { client })
    }
  }),
  lifecycle({
    async componentDidMount() {
      this.props.mount()
    },
    componentDidUpdate() {
      const { fromAuthClassic, router } = this.props
      const { isConfirmed, redirectAfterConfirm } = fromAuthClassic

      if (isConfirmed) {
        router.push(redirectAfterConfirm)
      }
    }
  })
)

const BlockAuthClassicConfirmAuto = enhance(({ fromAuthClassic }) => {
  const { errors } = fromAuthClassic
  return errors.map(({ message }, index) => (
    <div key={index}>{message}</div>
  ))
})

export default BlockAuthClassicConfirmAuto

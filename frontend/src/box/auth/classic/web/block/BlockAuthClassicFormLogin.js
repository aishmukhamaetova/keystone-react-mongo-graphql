import React, { Component } from 'react'
import { withApollo }       from 'react-apollo'
import compose              from 'recompose/compose'
import styled               from 'styled-components'


import withSpace            from 'evoke-me/space/all/withSpace'
import spaceAuthClassic     from '../../space/spaceAuthClassic'
import FormLogin            from '../FormLogin'


const enhance = compose(
  withApollo,
  withSpace(spaceAuthClassic),
)

const BlockAuthClassicFormLogin = enhance(({
  children,
  client,
  fromAuthClassic,
}) => {
  return (
    <FormLogin
      {...fromAuthClassic}
      onSubmit={() => fromAuthClassic.login({}, { client })}
    >
      {children}
    </FormLogin>
  )
})

export default BlockAuthClassicFormLogin

import React, { Component } from 'react'
import { withApollo }       from 'react-apollo'
import compose              from 'recompose/compose'
import styled               from 'styled-components'


import withSpace            from 'evoke-me/space/all/withSpace'
import spaceAuthClassic     from '../../space/spaceAuthClassic'
import FormLogout           from '../FormLogout'


const enhance = compose(
  withApollo,
  withSpace(spaceAuthClassic),
)

const BlockAuthClassicFormLogout = enhance(({
  children,
  client,
  fromAuthClassic,
}) => {
  return (
    <FormLogout
      {...fromAuthClassic}
      onSubmit={() => fromAuthClassic.logout({}, { client })}
    >
      {children}
    </FormLogout>
  )
})

export default BlockAuthClassicFormLogout

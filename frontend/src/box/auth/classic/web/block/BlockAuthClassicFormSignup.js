import React, { Component } from 'react'
import { withApollo }       from 'react-apollo'
import compose              from 'recompose/compose'
import styled               from 'styled-components'


import withSpace            from 'evoke-me/space/all/withSpace'
import spaceAuthClassic     from '../../space/spaceAuthClassic'
import FormSignup           from '../FormSignup'


const enhance = compose(
  withApollo,
  withSpace(spaceAuthClassic),
)

const BlockAuthClassicFormSignup = enhance(({
  children,
  client,
  fromAuthClassic,
}) => {
  return (
    <FormSignup
      {...fromAuthClassic}
      onSubmit={() => fromAuthClassic.signup({}, { client })}
    >
      {children}
    </FormSignup>
  )
})

export default BlockAuthClassicFormSignup

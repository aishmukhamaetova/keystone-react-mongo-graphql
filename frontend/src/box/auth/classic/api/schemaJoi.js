import Joi                  from 'joi'


export default Joi.object().keys({
  nameFirst: Joi.string().min(2).label(`First name`),
  nameLast: Joi.string().min(4).label(`Last name`),
  username: Joi.string().email({ minDomainAtoms: 2, errorLevel: 10 }).label(`Email`),
  password: Joi.string().regex(/^[a-zA-Z0-9]{3,30}$/).label(`Password`),
}).with(`nameFirst`, [`nameLast`, `username`, `password`])

import camelCase            from 'lodash.camelcase'
import upperFirst           from 'lodash.upperfirst'


import boxConfig            from 'box/auth/manager/api/config'
import typeMap              from 'evoke-me/type/api/typeMap'


const ENTITY = `classic`

const name = `${camelCase(boxConfig.namespace)}${upperFirst(ENTITY)}`
const nameType = upperFirst(name)

export default {
  ...boxConfig,
  entity: ENTITY,

  name,
  nameType,

  fields: {}
}

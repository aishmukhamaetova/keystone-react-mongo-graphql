import uuid                 from 'uuid/v4'
import nodemailer           from 'nodemailer'


import authErrorMap         from 'box/auth/manager/api/authErrorMap'
import config               from 'box/auth/manager/api/config'


const smtpSettings = {
  host: 'smtp.gmail.com',
  port: 465,
  secure: true, // use SSL
  auth: {
    user: 'system@all-itera.ru',
    pass: 'H<j6V6=A',
  }
}

const transporter = nodemailer.createTransport(smtpSettings)

const link = `${process.env.CLIENT_HOST}/box.auth/classic/confirm`

const send = (email, emailCode) => new Promise((resolve, reject) => {
  const mailOptions = {
    from: '"latelierdumiel.com" <system@all-itera.ru>',
    to: email,
    subject: 'latelierdumiel.com - Email confirmation',
    text: `Open next link to confirm your email ${link}/${emailCode}`,
    html: `<a href='${link}/${emailCode}'>Click to confirm your email</a>`
  }

  transporter.sendMail(mailOptions, (error, info) => {
    if (error) {
      return reject(error)
    }

    return resolve(info.response)
  })
})

const who = [config.namespace, `classic`, `api`, `keystone`, `graphql`, `server`, `authClassicSignup`]

const boxAuthClassicSignup = async (obj, args, { keystone, req, res }, info) => {
  try {
    const { payload = {} } = args
    const {
      username,
      password,
      nameFirst: first,
      nameLast: last,
    } = payload

    const User = keystone.list('User')

    const userCurrent = await User.model.findOne({
      email: { $eq: username },
    })

    if (userCurrent) {
      return {
        result: null,
        errors: [
          {
            type: config.namespace,
            code: authErrorMap.SIGNUP_USER_ALREADY_EXISTS,
            message: `User already exists`,
            props: [
              { name: `username`, value: username },
            ]
          }
        ],
      }
    } else {
      const emailCode = uuid()

      const user = await new User.model({
        email: username,
        emailCode,
        password,
        name: {
          first,
          last,
        }
      }).save()

      const mail = await send(username, emailCode)
      console.log(`Mail sent`, mail)

      return {
        result: {
          status: true
        },
        errors: [],
      }
    }
  } catch (err) {
    console.log(`ERROR`, who.join(':'), err)

    const message = process.env.NODE_ENV === `development` ?
      err : `Signup catch error`

    return {
      result: null,
      errors: [
        {
          type: config.namespace,
          code: authErrorMap.UNKNOWN,
          message,
        }
      ],
    }
  }
}

export default boxAuthClassicSignup

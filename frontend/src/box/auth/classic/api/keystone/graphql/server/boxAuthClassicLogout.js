import config               from 'box/auth/manager/api/config'
import authErrorMap         from 'box/auth/manager/api/authErrorMap'


const who = [config.namespace, `classic`, `api`, `keystone`, `graphql`, `server`, `authClassicLogout`]

const keystoneLogout = (keystone, req, res) => new Promise((resolve, reject) => {
  keystone.session.signout(req, res, () => {
    return resolve()
  })
})

const boxAuthClassicLogout = async (obj, args, { keystone, req, res }, info) => {
  try {
    await keystoneLogout(keystone, req, res)

    return {
      result: {
        status: true,
      },
      errors: []
    }
  } catch (err) {
    console.log(`ERROR`, who.join(':'), err)

    const message = process.env.NODE_ENV === `development` ?
      err : `Logout unknown error`

    return {
      result: null,
      errors: [
        {
          type: config.namespace,
          code: authErrorMap.UNKNOWN,
          message,
        }
      ],
    }
  }
}

export default boxAuthClassicLogout

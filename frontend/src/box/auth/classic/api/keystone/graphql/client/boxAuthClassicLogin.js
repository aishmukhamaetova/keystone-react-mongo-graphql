import gql                  from 'graphql-tag'


export default gql`
mutation boxAuthClassicLogin(
  $username: String
  $password: String
) {
  boxAuthClassicLogin(
    payload: {
      username: $username
      password: $password
    }
  ) {
    result {
      userId
    }
    
    errors {
      type
      code
      message
      props {
        name
        value
      }
    }
  }
}
`

import authErrorMap         from 'box/auth/manager/api/authErrorMap'
import config               from 'box/auth/manager/api/config'


const who = [config.namespace, `classic`, `api`, `keystone`, `graphql`, `server`, `authClassicLogin`]

const keystoneLogin = (keystone, req, res, userId) => new Promise((resolve, reject) => {
  keystone.session.signin(
    userId,
    req,
    res,
    (user) => {
      return resolve(user)
    },
    (err) => {
      return reject(err)
    }
  )
})

const passwordCompare = (user, password) => new Promise((resolve, reject) => {
  user._.password.compare(password, (err, isMatch) => {
    if (err) return reject(err)
    return resolve(isMatch)
  })
})

const boxAuthClassicLogin = async (obj, args, { keystone, req, res }, info) => {
  try {
    const { payload = {} } = args
    const {
      username,
      password,
    } = payload

    const User = keystone.list('User')

    const userCurrent = await User.model.findOne({
      email: { $eq: username },
    })

    const errorWrongUsernameOrPassword = {
      type: config.namespace,
      code: authErrorMap.LOGIN_WRONG_USERNAME_OR_PASSWORD,
      message: `Wrong username or password`,
      props: [
        { name: `username`, value: username },
      ]
    }

    if (userCurrent) {
      if (userCurrent.isConfirmed) {
        const isMatch = await passwordCompare(userCurrent, password)

        if (isMatch) {
          const userAuthenticated = await keystoneLogin(keystone, req, res, String(userCurrent._id))

          if (userAuthenticated) {
            return {
              result: {
                userId: String(userAuthenticated._id)
              },
              errors: []
            }
          }
        }

        return {
          result: null,
          errors: [errorWrongUsernameOrPassword],
        }
      } else {
        return {
          result: null,
          errors: [
            {
              type: config.namespace,
              code: authErrorMap.LOGIN_NOT_CONFIRMED,
              message: `Your email not confirmed`,
              props: [
                { name: `username`, value: username },
              ]
            }
          ],
        }
      }
    } else {
      return {
        result: null,
        errors: [errorWrongUsernameOrPassword],
      }
    }
  } catch (err) {
    console.log(`ERROR`, who.join(':'), err)

    const message = process.env.NODE_ENV === `development` ?
      err : `Login catch error`

    return {
      result: null,
      errors: [
        {
          type: config.namespace,
          code: authErrorMap.UNKNOWN,
          message,
        }
      ],
    }
  }
}

export default boxAuthClassicLogin

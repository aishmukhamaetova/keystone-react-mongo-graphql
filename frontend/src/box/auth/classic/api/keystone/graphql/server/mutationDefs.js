const mutationConfirm = `
  boxAuthClassicConfirm(
    payload: BoxAuthClassicConfirmPayload
  ): BoxAuthClassicConfirm
`

const mutationLogin = `
  boxAuthClassicLogin(
    payload: BoxAuthClassicLoginPayload
  ): BoxAuthClassicLogin    
`

const mutationLogout = `
  boxAuthClassicLogout: BoxAuthClassicLogout
`

const mutationSignup = `
  boxAuthClassicSignup(
    payload: BoxAuthClassicSignupPayload
  ): BoxAuthClassicSignup
`

export default [
  mutationConfirm,
  mutationLogin,
  mutationLogout,
  mutationSignup,
].join(`\n`)

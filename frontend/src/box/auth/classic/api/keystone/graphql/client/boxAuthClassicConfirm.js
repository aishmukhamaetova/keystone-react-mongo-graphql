import gql                  from 'graphql-tag'


export default gql`
mutation boxAuthClassicConfirm(
  $emailCode: String
) {
  boxAuthClassicConfirm(
    payload: {
      emailCode: $emailCode
    }
  ) {
    result {
      userId
    }
    
    errors {
      type
      code
      message
      props {
        name
        value
      }
    }
  }
}
`

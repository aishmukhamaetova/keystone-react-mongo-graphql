import authErrorMap         from 'box/auth/manager/api/authErrorMap'
import config               from '../../../config'


const NAME = `Check`

const { name } = config
const { nameType } = config

const queryName = `${name}${NAME}`
const typeQuery = `${nameType}${NAME}`
const typePayload = `${typeQuery}Payload`
const typeResult = `${typeQuery}Result`

export const type = `
  type ${typeQuery} {
    result: ${typeResult}
    errors: [Error]
  }
  
  type ${typeResult} {
    status: Boolean
  }
  
  input ${typePayload} {
    empty: Boolean
  }
`

export const query = `
  ${queryName}(
    payload: ${typePayload}
  ): ${typeQuery}
`

const mutation = ``

const mutationMap = {}

const error = (code, message, props = []) => ({ result: null, errors: [{
  type: config.namespace,
  code,
  message,
  props
}]})

const queryMap = {
  [queryName]: async (obj, args, { keystone, req, res }, info) => {
    try {
      const { user } = res.locals

      if (user) {
        return {
          result: { status: true },
          errors: [],
        }
      }

      return {
        result: { status: false },
        errors: [],
      }
    } catch (err) {
      console.log(`ERROR`, queryName, err)

      const message = process.env.NODE_ENV === `development` ?
        err : `${queryName} unknown error`

      return error(authErrorMap.UNKNOWN, message)
    }
  }
}

export default {
  type,

  mutation,
  mutationMap,

  query,
  queryMap,
}

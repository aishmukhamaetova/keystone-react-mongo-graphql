import boxAuthClassicConfirm   from './boxAuthClassicConfirm'
import boxAuthClassicLogin     from './boxAuthClassicLogin'
import boxAuthClassicLogout    from './boxAuthClassicLogout'
import boxAuthClassicSignup    from './boxAuthClassicSignup'


export default {
  boxAuthClassicConfirm,
  boxAuthClassicLogin,
  boxAuthClassicLogout,
  boxAuthClassicSignup,
}

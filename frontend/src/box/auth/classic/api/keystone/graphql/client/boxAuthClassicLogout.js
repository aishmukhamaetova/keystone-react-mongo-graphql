import gql                  from 'graphql-tag'


export default gql`
mutation boxAuthClassicLogout {
  boxAuthClassicLogout {
    result {
      status
    }
    
    errors {
      type
      code
      message
      props {
        name
        value
      }
    }
  }
}
`

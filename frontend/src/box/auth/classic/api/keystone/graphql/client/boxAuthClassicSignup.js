import gql                  from 'graphql-tag'


export default gql`
mutation boxAuthClassicSignup(
  $nameFirst: String
  $nameLast: String
  $username: String
  $password: String
) {
  boxAuthClassicSignup(
    payload: {
      nameFirst: $nameFirst
      nameLast: $nameLast
      username: $username
      password: $password
    }
  ) {
    result {
      status
    }
    errors {
      type
      message
      props {
        name
        value
      }
    }
  }
}
`

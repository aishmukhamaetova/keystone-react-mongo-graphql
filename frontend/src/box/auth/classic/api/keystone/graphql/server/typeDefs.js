const typeConfirm = `
  type BoxAuthClassicConfirm {
    result: BoxAuthClassicConfirmResult
    errors: [Error]
  }
  
  input BoxAuthClassicConfirmPayload {
    emailCode: String
  }

  type BoxAuthClassicConfirmResult {
    userId: ID
  }
`

const typeLogin = `
  type BoxAuthClassicLogin {
    result: BoxAuthClassicLoginResult
    errors: [Error]
  }
  
  input BoxAuthClassicLoginPayload {
    username: String
    password: String
  }
  
  type BoxAuthClassicLoginResult {
    userId: ID
  }
`

const typeLogout = `
  type BoxAuthClassicLogout {
    result: BoxAuthClassicLogoutResult
    errors: [Error]
  }
  
  type BoxAuthClassicLogoutResult {
    status: Boolean
  }
`

const typeSignup = `
  type BoxAuthClassicSignup {
    result: BoxAuthClassicSignupResult
    errors: [Error]    
  }
  
  input BoxAuthClassicSignupPayload {
    nameFirst: String
    nameLast: String
    username: String
    password: String  
  }
  
  type BoxAuthClassicSignupResult {
    status: Boolean
  }
`

export default [
  typeConfirm,
  typeLogin,
  typeLogout,
  typeSignup,
].join(`\n`)

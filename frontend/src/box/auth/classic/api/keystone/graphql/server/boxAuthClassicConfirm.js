import nodemailer           from 'nodemailer'


import authErrorMap         from 'box/auth/manager/api/authErrorMap'
import config               from 'box/auth/manager/api/config'


const smtpSettings = {
  host: 'smtp.gmail.com',
  port: 465,
  secure: true, // use SSL
  auth: {
    user: 'system@all-itera.ru',
    pass: 'H<j6V6=A',
  }
}

const transporter = nodemailer.createTransport(smtpSettings)

const send = (email) => new Promise((resolve, reject) => {
  const mailOptions = {
    from: '"latelierdumiel.com" <system@all-itera.ru>',
    to: email,
    subject: 'latelierdumiel.com - Email confirmed',
    text: `You email confirmed`,
    html: `You email confirmed`
  }

  transporter.sendMail(mailOptions, (error, info) => {
    if (error) {
      return reject(error)
    }

    return resolve(info.response)
  })
})

const who = [config.namespace, `classic`, `api`, `keystone`, `graphql`, `server`, `authClassicConfirm`]

const keystoneLogin = (keystone, req, res, userId) => new Promise((resolve, reject) => {
  keystone.session.signin(
    userId,
    req,
    res,
    (user) => {
      return resolve(user)
    },
    (err) => {
      return reject(err)
    }
  )
})

const boxAuthClassicConfirm = async (obj, args, { keystone, req, res }, info) => {
  try {
    const { payload = {} } = args
    const {
      emailCode
    } = payload

    const User = keystone.list('User')

    const userCurrent = await User.model.findOne({
      emailCode: { $eq: emailCode },
    })

    if (userCurrent) {
      if (userCurrent.isConfirmed) {
        return {
          result: null,
          errors: [
            {
              type: config.namespace,
              code: authErrorMap.CONFIRM_CODE_ALREADY_USED,
              message: `Email code already used`,
              props: [
                { name: `emailCode`, value: emailCode },
              ]
            }
          ],
        }
      } else {
        await send(userCurrent.email)

        userCurrent.isConfirmed = true
        await userCurrent.save()
        const userAuthenticated = await keystoneLogin(keystone, req, res, String(userCurrent._id))

        if (userAuthenticated) {
          return {
            result: {
              userId: String(userCurrent._id),
            },
            errors: [],
          }
        } else {
          return {
            result: null,
            errors: [
              {
                type: config.namespace,
                code: authErrorMap.CONFIRM_LOGIN_FAILED,
                message: `Email confirmation login failed`,
                props: [
                  { name: `emailCode`, value: emailCode },
                ]
              }
            ],
          }
        }
      }
    }

    return {
      result: null,
      errors: [
        {
          type: config.namespace,
          code: authErrorMap.CONFIRM_CODE_NOT_FOUND,
          message: `Email code not found`,
          props: [
            { name: `emailCode`, value: emailCode },
          ]
        }
      ]
    }
  } catch (err) {
    console.log(`ERROR`, who.join(':'), err)

    const message = process.env.NODE_ENV === `development` ?
      err : `Signup unknown error`

    return {
      result: null,
      errors: [
        {
          type: config.namespace,
          code: authErrorMap.UNKNOWN,
          message,
        }
      ],
    }
  }
}

export default boxAuthClassicConfirm

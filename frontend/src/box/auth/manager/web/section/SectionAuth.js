import Grid                 from '@material-ui/core/Grid'
import React, { Component } from 'react'
import compose              from 'recompose/compose'
import styled               from 'styled-components'


import withSpace            from 'evoke-me/space/all/withSpace'
import spaceAuth            from '../../space/spaceAuth'
import BlockAuth            from '../block/BlockAuth'
import BlockAuthSwitch      from '../block/BlockAuthSwitch'
import theme                from '../theme'


const Section = styled.div``

const Item = styled(Grid)`
  max-width: 320px !important; 
`

const enhance = compose(
  withSpace(spaceAuth),
)

const SectionAuth = enhance(({
  fromAuth
}) => {
  const { isAuth } = fromAuth

  return (
    <Section>
      <Grid
        container
        justify='center'
        spacing={8}
      >
        <Item item xs={8}>
          <BlockAuth theme={theme} />
        </Item>
      </Grid>
      {!isAuth ? (
        <Grid
          container
          justify='center'
          spacing={8}
        >
          <Item item xs={8}>
            <BlockAuthSwitch theme={theme}/>
          </Item>
        </Grid>
      ) : null}
    </Section>
  )
})

export default SectionAuth

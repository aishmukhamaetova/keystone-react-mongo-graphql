import React, { Component } from 'react'
import compose              from 'recompose/compose'
import { Icon }             from 'semantic-ui-react'
import { Message }          from 'semantic-ui-react'
import styled               from 'styled-components'


import withSpace            from 'evoke-me/space/all/withSpace'
import authScreenMap        from '../../api/authScreenMap'
import spaceAuth            from '../../space/spaceAuth'


const enhance = compose(
  withSpace(spaceAuth),
)

const BlockAuthSwitch = enhance(({ fromAuth }) => {
  return fromAuth.screen === authScreenMap.LOGIN ? (
    <Message warning>
      <Icon name='help' />
      Don't have an account?&nbsp;
      <a
        href='#'
        onClick={(e) => {
          e.preventDefault()
          fromAuth.screenSet(authScreenMap.SIGNUP)
        }}
      >
        Sign up
      </a>
    </Message>
  ) : (
    <Message warning>
      <Icon name='help' />
      Already signed up?&nbsp;
      <a
        href='#'
        onClick={(e) => {
          e.preventDefault()
          fromAuth.screenSet(authScreenMap.LOGIN)
        }}
      >
      Login&nbsp;here
      </a>
    </Message>
  )
})

export default BlockAuthSwitch

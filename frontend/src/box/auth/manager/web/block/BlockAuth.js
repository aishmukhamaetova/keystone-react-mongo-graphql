import React, { Component } from 'react'
import compose              from 'recompose/compose'
import styled               from 'styled-components'


import withSpace            from 'evoke-me/space/all/withSpace'
import authScreenMap        from '../../api/authScreenMap'
import authTypeMap          from '../../api/authTypeMap'
import spaceAuth            from '../../space/spaceAuth'


const enhance = compose(
  withSpace(spaceAuth),
)

const BlockAuth = enhance(({ fromAuth, theme }) => {
  const { BlockAuthClassicFormLogin } = theme
  const { BlockAuthClassicFormLogout } = theme
  const { BlockAuthClassicFormSignup } = theme
  const { BlockAuthFacebookButtonLogin } = theme

  if (fromAuth.isAuth) {
    return (
      <BlockAuthClassicFormLogout />
    )
  }

  return fromAuth.screen === authScreenMap.LOGIN ? (
    <BlockAuthClassicFormLogin>
      {fromAuth.supportList.includes(authTypeMap.FACEBOOK) ? (
        <BlockAuthFacebookButtonLogin />
      ) : null}
    </BlockAuthClassicFormLogin>
  ) : (
    <BlockAuthClassicFormSignup />
  )
})

export default BlockAuth

import themeClassic         from '../../classic/web/theme'
import themeFacebook        from '../../facebook/web/theme'


export default {
  ...themeClassic,
  ...themeFacebook,
}

import spaceNotify          from 'box/notify/manager/space/spaceNotify'
import spacePersist         from 'box/persist/manager/space/spacePersist'

import config               from '../api/config'
import authTypeMap          from '../api/authTypeMap'
import authScreenMap        from '../api/authScreenMap'


const ENTITY = `auth`

export default [
  config.namespace,
  ENTITY,
  [
    spaceNotify,
    spacePersist,
  ],
  () => ({
    isAuth: false,
    screen: authScreenMap.LOGIN,
    supportList: [
      authTypeMap.CLASSIC,
      authTypeMap.FACEBOOK,
    ],

    persist: true,
    persistName: `${config.namespace}.${ENTITY}`
  }),
  {
    restore: (getState, setState, methods, { fromPersist }) => function* () {
      const { persist, persistName } = yield getState()
      if (persist) {
        const statePersist = yield fromPersist.get(persistName)
        if (statePersist) {
          const { isAuth } = statePersist
          yield setState({ isAuth })
        }
      }
    },
    isAuthSet: (getState, setState, methods, { fromPersist }) => function* (isAuth) {
      const { persist, persistName } = yield getState()

      yield setState({ isAuth })

      if (persist) {
        const { data, orderMap } = yield getState()
        yield fromPersist.set(persistName, { isAuth })
      }
    },
    screenSet: (getState, setState) => function* (screen) { yield setState({ screen }) },
    supportListAdd: (getState, setState) => function* (authType) {
      const { supportList } = yield getState()
      const index = supportList.indexOf(authType)

      if (index === -1) {
        yield setState({
          supportList: [
            ...supportList,
            authType,
          ]
        })
      }
    },
    supportListRemove: (getState, setState) => function* (authType) {
      const { supportList } = yield getState()
      const index = supportList.indexOf(authType)

      if (index !== -1) {
        yield setState({
          supportList: [
            ...supportList.slice(0, index),
            ...supportList.slice(index + 1),
          ]
        })
      }
    },
  }
]

import React, { Component } from 'react'
import { PoseGroup }        from 'react-pose'
import compose              from 'recompose/compose'
import lifecycle            from 'recompose/lifecycle'
import styled               from 'styled-components'


import withSpace            from 'evoke-me/space/all/withSpace'

import spaceNotify          from '../../space/spaceNotify'
import NotifyList           from '../NotifyList'


export default compose(
  withSpace(spaceNotify),
)(({
  fromNotify,
}) => {
  const { data, orderMap } = fromNotify

  return (
    <NotifyList
      data={data}
      order={orderMap.show}

      onClose={({ id }) => fromNotify.orderMove(id, `show`, `history`)}
    />
  )
})

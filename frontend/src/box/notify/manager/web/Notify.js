import React, { Component } from 'react'
import posed                from 'react-pose'
import compose              from 'recompose/compose'
import defaultProps         from 'recompose/defaultProps'
import lifecycle            from 'recompose/lifecycle'
import withHandlers         from 'recompose/withHandlers'
import withStateHandlers    from 'recompose/withStateHandlers'

import styled               from 'styled-components'


import Block                from 'evoke-me/layout/web/Block'


const delay = (time) => new Promise((resolve) => setTimeout(resolve, time))

const poseMap = {
  BEGIN: `BEGIN`,
  OPEN: `OPEN`,
  CLOSE: `CLOSE`,
}

const enhance = compose(
  defaultProps({
    message: `Message`,
  }),
  withStateHandlers(
    () => ({
      pose: poseMap.BEGIN,
    }),
    {
      poseSet: () => (pose) => ({ pose })
    }
  ),
  withHandlers(() => {
    return {
      mount: ({ poseSet, onClose }) => async () => {
        poseSet(poseMap.OPEN)
        await delay(1000)
        poseSet(poseMap.CLOSE)
        await delay(300)
        onClose && onClose()
      }
    }
  }),
  lifecycle({
    componentDidMount() {
      this.props.mount()
    }
  })
)

const background = ({ error }) => {
  const color = error
    ? `rgba(200, 56, 56, 0.8)`
    : `rgba(56, 56, 200, 0.8)`

  return `background: ${ color };`
}

const Item = styled(posed.div({
  [poseMap.BEGIN]: {
    y: -50,
    opacity: 0,
  },
  [poseMap.OPEN]: {
    y: 0,
    opacity: 1,
  },
  [poseMap.CLOSE]: {
    y: -50,
    opacity: 0,
  },
}))`
  ${ background };

  color: white;

  position: absolute;

  width: 300px;
  height: 50px;
  border-radius: 10px;

  margin-top: 5px;  
`

const Notify = enhance(({
  message,
  error,
  pose,
}) => {
  return (
    <Item error={error} pose={pose}>
      <Block center middle>
        {message}
      </Block>
    </Item>
  )
})

export default Notify

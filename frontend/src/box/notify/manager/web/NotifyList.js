import React, { Component } from 'react'
import { PoseGroup }        from 'react-pose'
import compose              from 'recompose/compose'
import defaultProps         from 'recompose/defaultProps'
import lifecycle            from 'recompose/lifecycle'
import styled               from 'styled-components'


import Notify               from './Notify'


const enhance = compose(
  defaultProps({
    data: {},
    order: [],
  })
)

const NotifyList = enhance(({
  data,
  order,
  onClose,
}) => {
  return order.length > 0 ? (
    <PoseGroup>
      {order.map((id) => (
        <Notify
          data-key={ id }
          key={ id }
          {...data[id]}

          onClose={() => onClose({ id })}
        />
      ))}
    </PoseGroup>
  ) : null
})

export default NotifyList
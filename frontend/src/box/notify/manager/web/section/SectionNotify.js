import React, { Component } from 'react'
import styled               from 'styled-components'


import Block                from 'evoke-me/layout/web/Block'
import BlockNotify          from '../block/BlockNotify'


const Section = styled.div`
  position: fixed;
  top: 0;
  width: 100vw;
  height: 100vh;
  pointer-events: none;
  text-align: right;

  z-index: 10000;
`

const SectionNotify = ({ theme }) => {
  return (
    <Section>
      <Block right>
        <BlockNotify />
      </Block>
    </Section>
  )
}

export default SectionNotify

export default (type = 'box.error', code, message, props = []) => ({
  result: null,
  errors: [{
    type,
    code,
    message,
    props
  }]
})

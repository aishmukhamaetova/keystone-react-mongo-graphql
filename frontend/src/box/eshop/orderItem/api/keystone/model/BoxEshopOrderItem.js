export default (keystone) => {
  const Types = keystone.Field.Types
  const BoxEshopOrderItem = new keystone.List('BoxEshopOrderItem', {
    label: `Order Items`,
    singular: `OrderItem`,
    plural: `Order Items`,
  })

  BoxEshopOrderItem.add({
    price: { type: Types.Money, },
    quantity: { type: Types.Number, },

    product: { type: Types.Relationship, ref: `Product`, },
    order: { type: Types.Relationship, ref: `BoxEshopOrder`, },
  })

  BoxEshopOrderItem.defaultColumns = 'price, quantity'
  BoxEshopOrderItem.register()
}


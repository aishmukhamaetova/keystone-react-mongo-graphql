import React, { Component } from 'react'
import { withApollo }       from 'react-apollo'
import compose              from 'recompose/compose'
import lifecycle            from 'recompose/lifecycle'
import withHandlers         from 'recompose/withHandlers'

import { Segment }          from 'semantic-ui-react'
import { Header }           from 'semantic-ui-react'
import { Icon }             from 'semantic-ui-react'


import spaceBoxEshopOrder   from 'box/eshop/order/space/spaceBoxEshopOrder'
import withSpace            from 'evoke-me/space/all/withSpace'


const enhance = compose(
  withApollo,
  withSpace(spaceBoxEshopOrder),
  withHandlers({
    mount: ({
      client,
      fromBoxEshopOrder,
    }) => () => {
      fromBoxEshopOrder.queryMy({}, { client })
    },
  }),
  lifecycle({
    componentDidMount() {
      this.props.mount()
    }
  })
)

const BlockOrderMyList = enhance(({
  fromBoxEshopOrder,
}) => {
  const { data, orderMap, isLoading } = fromBoxEshopOrder

  if (!orderMap.my || !orderMap.my.length) {
    return (
      <Segment placeholder loading={isLoading}>
        <Header icon>
          <Icon name='hand peace' />
          No orders
        </Header>
      </Segment>
    )
  }

  return orderMap.my.map((id) => {
    const { type } = data[id]

    return (
      <div key={id}>
        ORDER {id} - {type}
      </div>
    )
  })
})

export default BlockOrderMyList

import typeMap              from '../../typeMap'


const options = Object.values(typeMap).join(', ')

export default (keystone) => {
  const Types = keystone.Field.Types
  const BoxEshopOrder = new keystone.List('BoxEshopOrder', {
    label: `Orders`,
    singular: `Order`,
    plural: `Orders`,
  })

  BoxEshopOrder.add({
    amount: { type: Types.Money, },
    description: { type: String, },
    email: { type: String, },
    type: { type: Types.Select, options, },

    author: { type: Types.Relationship, ref: 'User' },
  })

  BoxEshopOrder.relationship({ ref: 'BoxEshopOrderItem', path: 'items', refPath: 'order' })

  BoxEshopOrder.defaultColumns = 'author'
  BoxEshopOrder.register()
}


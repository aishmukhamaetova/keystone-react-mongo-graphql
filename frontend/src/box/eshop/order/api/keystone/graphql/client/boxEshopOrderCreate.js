import gql                  from 'graphql-tag'


export default gql`
mutation boxEshopOrderCreate(
  $amount: Float
  $description: String
  $email: String
  $items: [BoxEshopOrderCreateResultItem]
) {
  boxEshopOrderCreate(
    payload: {
      amount: $amount
      description: $description
      email: $email
      items: $items
    }
  ) {
    result {
      id
      amount
      description
      email
    }
    
    errors {
      type
      code
      message
      props {
        name
        value
      }
    }
  }
}
`

import boxEshopOrderCreate  from './boxEshopOrderCreate'
import boxEshopOrderMyQuery from './boxEshopOrderMyQuery'


export default [
  boxEshopOrderCreate,
  boxEshopOrderMyQuery,
]

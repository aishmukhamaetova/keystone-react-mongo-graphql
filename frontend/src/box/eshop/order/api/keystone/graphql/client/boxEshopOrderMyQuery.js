import gql                  from 'graphql-tag'


export default gql`
query boxEshopOrderMy {
  boxEshopOrderMy {
    result {
      id
      amount
      type
    }
  }
}
`

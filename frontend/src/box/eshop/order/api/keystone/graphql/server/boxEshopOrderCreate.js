import errorMap             from 'box/error/manager/api/errorMap'
import config               from '../../../config'
import typeMap              from '../../../typeMap'


const NAME = `Create`

const { name } = config
const { nameType } = config

const queryName = `${name}${NAME}`
const typeQuery = `${nameType}${NAME}`
const typePayload = `${typeQuery}Payload`
const typeResult = `${typeQuery}Result`

export const type = `
  type ${typeQuery} {
    result: ${typeResult}
    errors: [Error]
  }
  
  type ${typeResult} {
    id: ID
    
    amount: Float
    description: String
    email: String    
  }

  input ${typeResult}Item {
    productId: ID
    quantity: Int
    price: Float
  }
  
  input ${typePayload} {
    amount: Float
    description: String
    email: String
    
    items: [${typeResult}Item]
  }
`

export const query = ``

const mutation = `
  ${queryName}(
    payload: ${typePayload}
  ): ${typeQuery}
`

const error = (type, code, message, props = []) => ({
  result: null,
  errors: [{
    type,
    code,
    message,
    props
  }]
})

const mutationMap = {
  [queryName]: async (obj, args, { keystone, req, res }, info) => {
    try {
      const { payload = {} } = args
      const { amount, description, items, email } = payload
      const { user } = res.locals

      const BoxEshopOrder = keystone.list('BoxEshopOrder')
      const BoxEshopOrderItem = keystone.list('BoxEshopOrderItem')

      const boxEshopOrder = await new BoxEshopOrder.model({
        author: user,
        type: typeMap.DRAFT,
        amount,
        description,
        email,
      }).save()

      for (let { productId, quantity, price } of items) {
        const boxEshopOrderItem = await new BoxEshopOrderItem.model({
          price,
          quantity,
          product: productId,
          order: boxEshopOrder,
        }).save()
      }

      if (boxEshopOrder) {
        return {
          result: boxEshopOrder,
          errors: []
        }
      }

      throw new Error(`Cannot create boxEshopOrder`)
    } catch (err) {
      console.log(`ERROR`, queryName, err)

      const message = process.env.NODE_ENV === `development` ?
        err : `${queryName} unknown error`

      return error(`box.error`, errorMap.UNKNOWN, message)
    }
  }
}

const queryMap = {}

export default {
  type,

  mutation,
  mutationMap,

  query,
  queryMap,
}

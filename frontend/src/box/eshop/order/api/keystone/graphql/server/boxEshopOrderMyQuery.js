import errorMap             from 'box/error/manager/api/errorMap'
import config               from '../../../config'
import typeMap              from '../../../typeMap'


const NAME = `My`

const { name } = config
const { nameType } = config

const queryName = `${name}${NAME}`
const typeQuery = `${nameType}${NAME}`
const typePayload = `${typeQuery}Payload`
const typeResult = `${typeQuery}Result`

export const type = `
  type ${typeQuery} {
    result: [${typeResult}]
    errors: [Error]
  }
  
  type ${typeResult} {
    id: ID
    
    amount: Float
    description: String
    type: String
    email: String    
  }

  input ${typeResult}Item {
    productId: ID
    quantity: Int
    price: Float
  }
  
  input ${typePayload} {
    amount: Float
    description: String
    email: String
    
    items: [${typeResult}Item]
  }
`

export const query = `
  ${queryName}(
    payload: ${typePayload}
  ): ${typeQuery}
`

const mutation = ``

const error = (type, code, message, props = []) => ({
  result: null,
  errors: [{
    type,
    code,
    message,
    props
  }]
})

const mutationMap = {}

const queryMap = {
  [queryName]: async (obj, args, { keystone, req, res }, info) => {
    try {
      const { payload = {} } = args
      const { user } = res.locals

      if (!user) {
        throw new Error(`Need authorization`)
      }

      const BoxEshopOrder = keystone.list('BoxEshopOrder')

      const boxEshopOrderList = await BoxEshopOrder.model.find({
        author: user
      })

      if (boxEshopOrderList) {
        return {
          result: boxEshopOrderList,
          errors: []
        }
      }

      throw new Error(`Cannot create boxEshopOrder`)
    } catch (err) {
      console.log(`ERROR`, queryName, err)

      const message = process.env.NODE_ENV === `development` ?
        err : `${queryName} unknown error`

      return error(`box.error`, errorMap.UNKNOWN, message)
    }
  }
}

export default {
  type,

  mutation,
  mutationMap,

  query,
  queryMap,
}

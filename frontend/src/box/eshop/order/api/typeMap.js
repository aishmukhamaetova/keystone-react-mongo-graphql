export default {
  DRAFT: `Draft`,
  PENDING_FOR_PAY: `Pending-for-Pay`,
  PAID: `Paid`,
  NOT_SHIPPED: `Not-Shipped`,
  SHIPPED: `Shipped`,
}

import spaceBoxEshopCart    from 'box/eshop/cart/space/spaceBoxEshopCart'
import createErrorResult    from 'box/error/manager/api/createErrorResult'
import errorMap             from 'box/error/manager/api/errorMap'

import spaceDefaults        from 'evoke-me/space/api/spaceDefaults'

import config               from '../api/config'
import QUERY_MY             from '../api/keystone/graphql/client/boxEshopOrderMyQuery'
import MUTATION_CREATE      from '../api/keystone/graphql/client/boxEshopOrderCreate'


export default [
  config.namespace,
  config.name,
  [
    spaceBoxEshopCart,
  ],
  () => ({
    ...spaceDefaults.state,

    currentOrderId: null,

    isLoading: false,
  }),
  {
    ...spaceDefaults.updaters,

    currentOrderIdSet: (getState, setState) => function* (currentOrderId) { yield setState({ currentOrderId }) },

    queryMy: (getState, setState, { create, orderClear }) => function* (payload, { client }) {
      try {
        if (client) {
          yield setState({ isLoading: true })

          const { data } = yield client.query({ query: QUERY_MY })
          const { boxEshopOrderMy: json } = data

          if (json && json.result) {
            yield orderClear('my')
            for (let doc of json.result) {
              yield create(doc, { orderName: `my` })
            }
          }

          yield setState({ isLoading: false })
        }
      } catch (err) {
        yield setState({ isLoading: false })

        console.log(`ERROR`, config.name, err)
      }
    },

    checkout: (getState, setState, { create, currentOrderIdSet }, { fromBoxEshopCart }) => function* (payload, { client }) {
      try {
        if (client) {
          yield setState({ isLoading: true })

          const itemList = yield fromBoxEshopCart.getItemList()

          let amount = 0

          for (let { price, quantity } of itemList) {
            amount += quantity * price
          }

          const { data } = yield client.mutate({
            mutation: MUTATION_CREATE,
            variables: {
              amount,
              items: itemList.map(({ id: productId, price, quantity }) => ({ productId, price, quantity }))
            },
          })

          const { boxEshopOrderCreate: json } = data

          if (json) {
            const { errors, result } = json

            if (errors && errors.length) {
              return yield setState({
                isLoading: false,
                errors: errors.map(({ type, code, message, props}) => {
                  return {
                    type, code, message,
                    props: props.reduce((acc, { name, value }) => acc[name] = value, {})
                  }
                })
              })
            }

            if (result) {
              yield create(result)
              yield currentOrderIdSet(result.id)
              yield setState({ isLoading: false })
              return result.id
            }
          }

          throw new Error(`checkout json is wrong`, json)
        }

        throw new Error(`checkout error`)
      } catch (err) {
        yield setState({ isLoading: false })

        console.log(`ERROR`, config.name, err)
      }
    }
  }
]

import spacePersist         from 'box/persist/manager/space/spacePersist'
import spaceDefaults        from 'evoke-me/space/api/spaceDefaults'

import config               from '../api/config'


export default [
  config.namespace,
  config.name,
  [
    spacePersist,
  ],
  () => ({
    ...spaceDefaults.state,
    persist: true,
    persistName: config.name,
  }),
  {
    ...spaceDefaults.updaters,

    getItemList: (getState) => function* () {
      const { orderMap, data } = yield getState()

      return orderMap.default.map((id) => data[id])
    }
  }
]

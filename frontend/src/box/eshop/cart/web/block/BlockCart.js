import React, { Component } from 'react'
import { withApollo }       from 'react-apollo'
import compose              from 'recompose/compose'
import lifecycle            from 'recompose/lifecycle'
import withHandlers         from 'recompose/withHandlers'
import withPropsOnChange    from 'recompose/withPropsOnChange'

import { Input }            from 'semantic-ui-react'
import { Item }             from 'semantic-ui-react'
import { Segment }          from 'semantic-ui-react'


import withSpace            from 'evoke-me/space/all/withSpace'

import spaceBoxEshopCart    from 'box/eshop/cart/space/spaceBoxEshopCart'
import spaceBoxEshopOrder   from 'box/eshop/order/space/spaceBoxEshopOrder'

import CartProduct          from '../CartProduct'


const enhance = compose(
  withApollo,
  withSpace(spaceBoxEshopCart),
  withSpace(spaceBoxEshopOrder),
  withPropsOnChange(
    ({ fromBoxEshopCart }, nextProps) => fromBoxEshopCart.data !== nextProps.fromBoxEshopCart.data,
    ({ fromBoxEshopCart: { data, orderMap }}) => ({
      amount: orderMap.default.reduce((result, id) => result + data[id].quantity * data[id].price, 0)
    })
  ),
  withHandlers({
    onCheckout: ({
      amount,
      client,

      fromBoxEshopCart,
      fromBoxEshopOrder,
    }) => async () => {
      const { data, orderMap } = fromBoxEshopCart

      fromBoxEshopOrder.checkout({}, { client })
    },
  }),
  lifecycle({
    componentDidMount() {
    },
    componentDidUpdate() {
      const {
        amount,
        fromBoxEshopOrder,
        fromBoxEshopCart,
      } = this.props

      if (fromBoxEshopOrder.currentOrderId && !this.checkout) {
        this.checkout = true

        Checkout.configure({
          merchant: 'ADM',
          order: {
            amount: () => amount,
            currency: 'USD',
            description: `Ordered goods`,
            id: fromBoxEshopOrder.currentOrderId,
          },
          interaction: {
            merchant: {
              name: 'AMD',
              address: {
                line1: '200 Sample St',
                line2: '1234 Example Town'
              }
            }
          }
        })

        Checkout.showPaymentPage()

        fromBoxEshopOrder.currentOrderIdSet(null)
        fromBoxEshopCart.removeAll()
      }
    }
  })
)

const BlockCart = enhance(({
  amount,

  fromBoxEshopCart,
  fromBoxEshopOrder,

  onCheckout,
}) => {
  const { data, orderMap } = fromBoxEshopCart

  return (
    <Segment>
      CURRENT ORDER ID: {fromBoxEshopOrder.currentOrderId}
      <Item.Group>
      {orderMap.default.map((id) => {
        const { quantity } = data[id]

        return (
          <CartProduct
            key={id}
            {...data[id]}

            onMinus={() => {
              if (quantity === 1) {
                fromBoxEshopCart.remove({ id })
              } else {
                fromBoxEshopCart.update({ id, quantity: quantity > 0 ? quantity - 1 : quantity})
              }
            }}

            onPlus={() => fromBoxEshopCart.update({ id, quantity: quantity + 1 })}
          />
        )
      })}
      </Item.Group>
      <Input
        action={{
          color: 'teal',
          labelPosition: 'left',
          icon: 'cart',
          content: 'Checkout',
          onClick: onCheckout,
        }}
        placeholder='TOTAL'
        value={`$${amount}`}
      />
    </Segment>
  )
})

export default BlockCart

import React, { Component } from 'react'
import compose              from 'recompose/compose'

import { Button }           from 'semantic-ui-react'


const enhance = compose()

const CartProductQuantity = enhance(({
  quantity,
  onMinus,
  onPlus,
}) => {
  return (
    <Button.Group>
      <Button
        icon='minus'
        onClick={onMinus}
      />
      <Button.Or text={quantity} />
      <Button
        icon='plus'
        positive
        onClick={onPlus}
      />
    </Button.Group>
  )
})

export default CartProductQuantity

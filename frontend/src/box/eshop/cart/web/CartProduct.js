import React, { Component } from 'react'
import compose              from 'recompose/compose'

import { Item }             from 'semantic-ui-react'


import CartProductQuantity  from './CartProductQuantity'


const enhance = compose()

const CartProduct = enhance(({
  name,
  price,
  sku,
  quantity,
  onMinus,
  onPlus,
}) => {
  return (
    <Item>
      <Item.Image size='tiny' src='https://react.semantic-ui.com/images/wireframe/image.png' />

      <Item.Content>
        <Item.Header>{name} - {sku}</Item.Header>
        <Item.Meta>
          <span>${price}</span>
        </Item.Meta>
        <Item.Description>
          <CartProductQuantity
            quantity={quantity}
            onMinus={onMinus}
            onPlus={onPlus}
          />
        </Item.Description>
      </Item.Content>
    </Item>
  )
})

export default CartProduct

import dotenv               from 'dotenv'


import retreiveOrder        from 'box/mastercard/gateway/api/v50/retreiveOrder'


dotenv.config({ path: `../.env` })

const main = async (orderId = `393cf2ba-60a0-4fbd-8e39-57972804206b`) => {
  const result = await retreiveOrder(
    {
      orderId,
    },
    {
      baseURL: process.env.MASTERCARD_BASE_URL,
      merchantId: process.env.MASTERCARD_MERCHANT_ID,
      merchantPassword: process.env.MASTERCARD_MERCHANT_PASSWORD,
    }
  )
}

if (process.argv.length >= 2) {
  main(process.argv[2])
}


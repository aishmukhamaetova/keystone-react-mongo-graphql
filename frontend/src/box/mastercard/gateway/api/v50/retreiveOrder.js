import axios                from 'axios'


import config               from './config'


const who = [`box`, `mastercard`, `api`, `v50`, `retreiveOrder`]

export default async (
  { orderId },
  {
    baseURL,
    merchantId,
    merchantPassword,
  }
) => {
  try {
    const url = `/${config.path}/merchant/${merchantId}/order/${orderId}`
    const response = await axios.request({
      method: `get`,
      baseURL,
      url,
      auth: {
        username: `merchant.${merchantId}`,
        password: merchantPassword,
      }
    })

    if (response.status === 401) {
      return {
        errors: [
          { type: `mastercard`, message: `Unauthorized` }
        ]
      }
    }

    if (response.status === 200) {
      const { data } = response

      console.log(`RESPONSE`, data)

      return data
    }

    throw new Error([...who, `error`].join(':'))
  } catch (err) {
    console.warn(err)
  }
}

import config               from '../../../manager/api/config'
import retreiveOrder        from '../../api/retreiveOrder'


export default [
  config.namespace,
  `gateway`,
  [
  ],
  () => ({
    baseURL: `https://test-bobsal.gateway.mastercard.com`,
    merchant: {
      id: ``,
    },
    operator: {
      id: ``,
      password: ``,
    }
  }),
  {
    retrieveOrder: (getState, setState) => function* ({ orderId }) {
      const {
        linkGate,
        pathVersion,
        merchant,
      } = yield getState()

      const URI = `${linkGate}/${pathVersion}/merchant/${merchant.id}/order/${orderId}`
    },
  }
]

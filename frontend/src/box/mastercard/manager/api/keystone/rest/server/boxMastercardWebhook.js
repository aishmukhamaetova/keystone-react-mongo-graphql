const keystone = require('keystone')
const BoxEshopOrder = keystone.list('BoxEshopOrder')


import orderTypeMap         from 'box/eshop/order/api/typeMap'

const HEADER_SECRET = `x-notification-secret`

const statusMap = {
  CAPTURED: `CAPTURED`,
}

export default async (req, res, next) => {
  const notificationSecret = req.headers[HEADER_SECRET]
  const body = req.body

  console.log(`HEADERS`, req.headers)
  console.log(`RECEIVED BODY`, body)

  const { order = {} } = body
  const { id, amount, status } = order

  console.log(`ID AMOUNT STATUS`, id, amount, status)

  if (notificationSecret !== process.env.MASTERCARD_WEBHOOK_SECRET) {
    console.log(`Wron process.env.MASTERCARD_WEBHOOK_SECRET`)

    return res.status(400).send('Something wrong')
  }

  if (amount && id && status === `CAPTURED`) {
    const boxEshopOrder = await BoxEshopOrder.model.findById(id)

    console.log(`FOUND BOX_ESHOP_ORDER`, boxEshopOrder)

    if (boxEshopOrder) {
      boxEshopOrder.type = orderTypeMap.PAID
      await boxEshopOrder.save()

      return res.send('processed-webhook')
    }
  }

  return res.status(400).send('Something wrong')
}

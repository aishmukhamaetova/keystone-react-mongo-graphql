export default {
  BOOLEAN: `BOOLEAN`,
  INT: `INT`,
  FLOAT: `FLOAT`,
  RELATION: `RELATION`,
  STRING: `STRING`,
}

export default (run, timeout) => {
  let time

  return (...args) => {
    const next = Date.now()
    if (!time || next - time >= timeout) {
      time = next
      return run(...args)
    }
  }
}

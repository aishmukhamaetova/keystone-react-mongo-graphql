import uuid                 from 'uuid/v4'


export default{
  state: {
    data: {},
    orderMap: {
      default: [],
    },
    persist: false,
    persistName: `default`,
  },
  updaters: {
    restore: (getState, setState, methods, { fromPersist }) => function* () {
      const { persist, persistName } = yield getState()

      if (persist) {
        const statePersist = yield fromPersist.get(persistName)
        if (statePersist) {
          const { data, orderMap } = statePersist
          yield setState({ data, orderMap })
        }
      }
    },
    create: (getState, setState, methods, { fromPersist }) => function* (doc, options = {}) {
      const { orderName } = options
      const { data, orderMap, persist, persistName } = yield getState()

      const id = doc.id || uuid()

      const orderMapNext = data[id]
        ? { default: orderMap.default }
        : { default: [...orderMap.default, id] }

      if (orderName) {
        orderMapNext[orderName] = [...(orderMap[orderName] || []), id]
      }

      yield setState({
        data: { ...data, [id]: doc },
        orderMap: { ...orderMap, ...orderMapNext },
      })

      if (persist) {
        const { data, orderMap } = yield getState()
        yield fromPersist.set(persistName, { data, orderMap })
      }
    },
    update: (getState, setState, methods, { fromPersist }) => function* (doc) {
      const { data, persist, persistName } = yield getState()

      if (!data[doc.id]) return null

      yield setState({
        data: {
          ...data,
          [doc.id]: {
            ...data[doc.id],
            ...doc,
          }
        }
      })

      if (persist) {
        const { data, orderMap } = yield getState()
        yield fromPersist.set(persistName, { data, orderMap })
      }
    },
    removeAll: (getState, setState, methods, { fromPersist }) => function* () {
      const { persist, persistName } = yield getState()

      const data = {}
      const orderMap = {
        default: []
      }

      yield setState({ data, orderMap })

      if (persist) {
        yield fromPersist.remove(persistName)
      }
    },
    remove: (getState, setState, methods, { fromPersist }) => function* ({ id }, options = {}) {
      const { orderName = `default` } = options
      const { data: prevData, orderMap, persist, persistName } = yield getState()
      const { [id]: docRemoved, ...data } = prevData

      yield setState({
        data,
        orderMap: {
          ...orderMap,
          [orderName]: orderMap[orderName].filter((_id) => _id !== id)
        }
      })

      if (persist) {
        const { data, orderMap } = yield getState()
        yield fromPersist.set(persistName, { data, orderMap })
      }
    },
    orderClear: (getState, setState) => function* (name) {
      const { orderMap: orderMapPrev } = yield getState()
      const { [name]: orderSelected, ...orderMap } = orderMapPrev

      yield setState({ orderMap })
    },
    orderMove: (getState, setState) => function* (id, from, to) {
      const { orderMap } = yield getState()

      const fromOrder = orderMap[from].filter((_id) => _id != id)
      const toOrder = [...(orderMap[to] || []), id]

      yield setState({
        orderMap: {
          ...orderMap,
          [from]: fromOrder,
          [to]: toOrder,
        }
      })
    },
  }
}

import upperFirst           from 'lodash.upperfirst'
import { compose }          from 'redux'
import { createStore }      from 'redux'
import { combineReducers }  from 'redux'
import { applyMiddleware }  from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension'

import createSagaMiddleware from 'redux-saga'
import { all }              from 'redux-saga/effects'
import { put }              from 'redux-saga/effects'
import { select }           from 'redux-saga/effects'
import { takeEvery }        from 'redux-saga/effects'


const spaceMap = {}

let options = {
  HMR: false,
}

const NAME = `evoke-me/space`
const VIEW_DEFAULT = `default`

const serviceUpdaterMap = {
  BEFORE_CALL: `__before`,
  AFTER_CALL: `__after`,
}

export const types = {
  ASSIGN: `@@redux-space/ASSIGN`,
  CALL: `@@redux-space/CALL`,
}

export const optionSet = (value) => {
  options = {
    ...options,
    ...value,
  }
}

const afterMap = {}
const beforeMap = {}

export const createSpace = ([
  namespace,
  entity,
  dependencies,
  stateInitial,
  updaters,
  mapPropsToView,
], store) => {
  if (!spaceMap[namespace]) spaceMap[namespace] = {}
  
  if (!spaceMap[namespace][entity] || options.HMR) {
    const methodsFrom = dependencies.reduce((result, spaceConfig) => {
      const [namespace, entity] = spaceConfig
      const { methods } = createSpace(spaceConfig, store)
      const name = `from${upperFirst(entity)}`
      result[name] = methods
      return result
    }, {})

    if (updaters[serviceUpdaterMap.AFTER_CALL]) {
      afterMap[namespace] = updaters[serviceUpdaterMap.AFTER_CALL]
    }

    if (updaters[serviceUpdaterMap.BEFORE_CALL]) {
      beforeMap[namespace] = updaters[serviceUpdaterMap.BEFORE_CALL]
    }

    const nameList = Object.keys(updaters).filter((name) => !Object.values(serviceUpdaterMap).includes(name))

    const methods = nameList.reduce((result, updaterName) => {
      result[updaterName] = function* (...updaterArgs) {
        let view = VIEW_DEFAULT
        try {
          view = mapPropsToView
            ? mapPropsToView(updaterArgs)
            : VIEW_DEFAULT
        } catch (err) {
          console.warn(NAME, namespace, entity, `mapPropsToView`, `error`, err)
        }

        function setState(value) {
          return put({
            type: [types.ASSIGN, namespace, entity, view],
            payload: {
              value,
            }
          })
        }

        function* getState() {
          try {
            const state = (yield select()).space[namespace][entity][view]
            if (state) return state
            throw new Error()
          } catch (err) {
            yield setState({})
            const state = yield select()
            return state.space[namespace][entity][view]
          }
        }

        try {
          for (let beforeMethod of Object.values(beforeMap)) {
            yield beforeMethod(namespace, entity, view, updaterName)
          }

          const method = yield updaters[updaterName](getState, setState, methods, methodsFrom)
          const result = yield method(...updaterArgs)

          for (let afterMethod of Object.values(afterMap)) {
            yield afterMethod(namespace, entity, view, updaterName)
          }

          return result
        } catch (err) {
          console.warn(NAME, namespace, entity, view, updaterName, `error`, err)
        }
      }
      return result
    }, {})

    const actions = nameList.reduce((result, updaterName) => {
      result[updaterName] = (...updaterArgs) => {
        return {
          type: types.CALL,
          payload: {
            updaterName,
            updaterArgs,
            meta: { namespace, entity, },
          }
        }
      }
      return result
    }, {})

    const handlers = nameList.reduce((result, updaterName) => {
      result[updaterName] = (...payload) => {
        return store.dispatch(actions[updaterName](...payload))
      }
      return result
    }, {
      initState: (view = `default`) => {
        store.dispatch({
          type: [types.ASSIGN, namespace, entity, view],
          payload: {
            value: {},
          }
        })
      }
    })

    return spaceMap[namespace][entity] = {
      actions,
      entity,
      handlers,
      mapPropsToView,
      methods,
      methodsFrom,
      namespace,
      stateInitial,
      updaters,
      //updatersFrom,
    }
  }

  return spaceMap[namespace][entity]
}

const merge = (state = {}, type, payload) => {
  const { value } = payload
  const [, namespace, entity, view] = type

  const mergeView = (state = {}) => {
    return {
      ...state,
      [view]: {
        ...(state[view] || spaceMap[namespace][entity].stateInitial()),
        ...value,
      },
    }
  }

  const mergeNamespace = (state = {}) => {
    return {
      ...state,
      [entity]: mergeView(state[entity])
    }
  }

  return {
    ...state,
    [namespace]: mergeNamespace(state[namespace]),
  }
}

export const spaceReducer = (state = {}, { type, payload }) => {
  if (Array.isArray(type) && type[0] === types.ASSIGN) {
    return merge(state, type, payload)
  }
  return state
}

function* call({ payload}) {
  try {
    const { updaterName, updaterArgs, meta } = payload
    const { namespace, entity } = meta

    const spaceMapEntity = spaceMap[namespace][entity]
    const { methods } = spaceMap[namespace][entity]

    const method = methods[updaterName]

    return yield method(...updaterArgs)
  } catch (error) {
    console.warn(NAME, `* call`, `error`, error)
  }
}

export function* spaceSaga() {
  yield takeEvery(types.CALL, call)
}

function* rootSaga() {
  yield all([
    spaceSaga(),
  ])
}

export const createSpaceStore = () => {
  const sagaMiddleware = createSagaMiddleware()

  const store = createStore(
    combineReducers({
      space: spaceReducer,
    }),
    composeWithDevTools(
      applyMiddleware(sagaMiddleware)
    ),
  )

  sagaMiddleware.run(rootSaga)

  return store
}

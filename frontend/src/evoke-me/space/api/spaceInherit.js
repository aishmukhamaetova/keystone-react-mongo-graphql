export default (
  spaceConfig,
  {
    dependencies,
    entity,
    mapPropsToView,
    namespace,
    stateInit,
    updaters,
  },
) => [
  namespace || spaceConfig[0],
  entity || spaceConfig[1],
  dependencies || spaceConfig[2],
  stateInit || spaceConfig[3],
  updaters || spaceConfig[4],
  mapPropsToView || spaceConfig[5],
]
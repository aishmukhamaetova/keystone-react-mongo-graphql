import { compose }          from 'redux'
import { createStore }      from 'redux'
import { combineReducers }  from 'redux'
import { applyMiddleware }  from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension'

import createSagaMiddleware from 'redux-saga'
import { all }              from 'redux-saga/effects'


import { spaceSaga }        from 'evoke-me/space/api/manager'
import { spaceReducer }     from 'evoke-me/space/api/manager'


function* rootSaga() {
  yield all([
    spaceSaga(),
  ])
}

export default () => {
  const sagaMiddleware = createSagaMiddleware()

  const store = createStore(
    combineReducers({
      space: spaceReducer,
    }),
    composeWithDevTools(
      applyMiddleware(sagaMiddleware)
    ),
  )

  sagaMiddleware.run(rootSaga)

  return store
}
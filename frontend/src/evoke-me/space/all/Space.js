import upperFirst           from 'lodash.upperfirst'
import React, { Component } from 'react'
import { connect }          from 'react-redux'
import compose              from 'recompose/compose'
import defaultProps         from 'recompose/defaultProps'
import lifecycle            from 'recompose/lifecycle'
import withHandlers         from 'recompose/withHandlers'
import withStateHandlers    from 'recompose/withStateHandlers'


import { createSpace }      from '../api/manager'
import Context              from './Context'


const withSpaceContext = (WrappedComponent) => (props) => (
  <Context.Consumer>
    {(context) => (
      <WrappedComponent
        {...props}
        {...context}
      />
    )}
  </Context.Consumer>
)

const enhanceSpace = compose(
  defaultProps({
    view: `default`,
  }),
  connect((state, { spaceConfig, view }) => {
    const [namespace, entity] = spaceConfig

    try {
      return {
        viewState: state.space[namespace][entity][view],
      }
    } catch (err) {
      return {
        viewState: null,
      }
    }
  }),
  withSpaceContext,
  withStateHandlers(() => ({
    handlers: null,
  }), {
    handlersSet: () => (handlers) => ({ handlers }),
  }),
  withHandlers(() => {
    return {
      mount: ({
        handlersSet,
        store,
        spaceConfig,
        viewState,
        view,
      }) => () => {
        const { handlers } = createSpace(spaceConfig, store)
        handlersSet(handlers)
      }
    }
  }),
  lifecycle({
    componentDidMount() {
      this.props.mount()
    },
    componentDidUpdate() {
      const { handlers, view, viewState } = this.props
      if (handlers && !viewState) {
        handlers.initState(view)
      }
    }
  })
)

const Space = enhanceSpace(({
  children,
  handlers,
  viewState,
  spaceConfig,
}) => {
  const [, entity] = spaceConfig
  const name = `from${upperFirst(entity)}`

  return (handlers && viewState)
    ? children({
      [name]: {
        ...viewState,
        ...handlers,
      }
    })
    : null
})

export default Space
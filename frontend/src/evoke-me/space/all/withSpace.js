import React, { Component } from 'react'


import Space                from './Space'


const withSpace = (spaceConfig) => (WrappedComponent) => (props) => (
  <Space spaceConfig={spaceConfig}>
    {(context) => (
      <WrappedComponent
        {...props}
        {...context}
      />      
    )}
  </Space>
)

export default withSpace
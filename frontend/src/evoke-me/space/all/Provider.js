import React, { Component } from 'react'
import compose              from 'recompose/compose'
import lifecycle            from 'recompose/lifecycle'
import withHandlers         from 'recompose/withHandlers'
import withStateHandlers    from 'recompose/withStateHandlers'
import { Provider }         from 'react-redux'


import createStore          from '../api/createStore'
import Context              from './Context'


const enhance = compose(
  withStateHandlers(
    () => ({
      store: null,
    }),
    {
      storeSet: () => (store) => ({ store }),
    }
  ),
  withHandlers(() => {
    return {
      mount: ({ storeSet }) => () => {
        storeSet(createStore())
      },
    }
  }),
  lifecycle({
    componentDidMount() {
      this.props.mount()
    },
  }),
)

const ProviderSpace = enhance(({
  children,
  store,
}) => {
  return store ? (
    <Context.Provider value={{ store }}>
      <Provider store={store}>
        <React.Fragment>
          {children}
        </React.Fragment>
      </Provider>
    </Context.Provider>
  ) : null
})

export default ProviderSpace
import React, { Component } from 'react'
import styled               from 'styled-components'


const Content = styled.div`
  flex: 1;
`

const Page = ({ children, hasFooter }) => {
  const list = React.Children.toArray(children)

  if (hasFooter) {
    return (
      <React.Fragment>
        <Content>
          { list.slice(0, list.length - 1) }
        </Content>
        <div>
          { list.slice(-1) }
        </div>
      </React.Fragment>
    )
  }

  return (
    <React.Fragment>
      <Content>
        { children }
      </Content>
    </React.Fragment>
  )
}


export default Page

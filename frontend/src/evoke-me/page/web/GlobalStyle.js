import React, { Component } from 'react'
import { createGlobalStyle } from 'styled-components'


export default createGlobalStyle`
  html, body {
    font-family: 'Open Sans', sans-serif;
    margin: 0;
  }

  html {
    height: 100%;
  }
  
  body {
    min-height: 100%;
  }
  
  #root {
    display: flex;
    min-height: 100vh;
    flex-direction: column;    
  }
`

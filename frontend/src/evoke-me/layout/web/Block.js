import PropTypes            from 'prop-types'
import React, { Component } from 'react'

import styled               from 'styled-components'


const alignHContainer = (props) => {
  const { left, center, right } = props

  return `text-align: ${ center ? 'center' : right ? 'right' : 'left' };`
}

const alignVContainer = (props) => {
  const { top, middle, bottom } = props

  return (top || middle || bottom) && `
    display: table;
    height: 100%;
    width: 100%;
  `;
}

const alignVItem = (props) => {
  const { top, middle, bottom } = props

  return (top || middle || bottom) && `
    display: table-cell;
    vertical-align: ${ top ? `top` : middle ? 'middle' : 'bottom' };
  `;
}

const maxWidth = ({ maxWidth }) => {
  if (maxWidth) {
    return `width: 100%`
  }
}

const Vertical = styled.div`
  ${alignVContainer}
`

const Horizontal = styled.div`
  ${ alignHContainer }
  ${ alignVItem }
`

const Content = styled.div`
  ${ maxWidth }

  display: inline-block;

  text-align: left;
`

class Block extends Component {
  static propTypes = {
    left: PropTypes.bool,
    center: PropTypes.bool,
    right: PropTypes.bool,

    top: PropTypes.bool,
    middle: PropTypes.bool,
    bottom: PropTypes.bool,
  }

  static defaultProps = {
    left: false,
    center: false,
    right: false,

    top: false,
    middle: false,
    bottom: false,
  }

  render() {
    const { children } = this.props
    const { style, ...rest } = this.props

    return (
      <Vertical {...this.props}>
        <Horizontal {...rest}>
          <Content {...rest}>
            {children}
          </Content>
        </Horizontal>
      </Vertical>
    )
  }
}

export default Block
require(`dotenv`).config({ path: `../.env` })

const express = require('express')
const next = require('next')


const routes = require('./src/app/honey/page/api/routes')

const dev = process.env.NODE_ENV !== 'production'
const app = next({ dev })

app.prepare()
  .then(() => {
    const server = express()

    const proxyMiddleware = require('http-proxy-middleware')

    server.use(proxyMiddleware('/api', {
      target: 'http://localhost:3010/',
      pathRewrite: {'^/api': '/api'},
      changeOrigin: true,
    }))

    server.use(proxyMiddleware('/keystone', {
      target: 'http://localhost:3010/keystone',
      pathRewrite: {'^/keystone': '/'},
      changeOrigin: true,
    }))

    const handle = routes.getRequestHandler(app)

    server.get('*', (req, res) => {
      return handle(req, res)
    })

    server.listen(3000, (err) => {
      if (err) throw err


      console.log('> Ready on http://localhost:3000')
    })
  })
  .catch((ex) => {
    console.error(ex.stack)
    process.exit(1)
  })


# Setup local env

## Run mongo
```
#new tab
sudo mongod --config /usr/local/etc/mongod.conf
#check it and if needs kill previous process  
lsof -t -i tcp:27017
#sudo kill -9 :pid
#add to etc/host
#code /etc/hosts  add 127.0.0.1 mongo

```
## Prebuild packages
```
#new tab
yarn prebuild
```

## Run backend

```
#new tab
yarn backend:dev
#new tab
yarn watch

```

## Run frontend

```
#new tab
yarn frontend:dev
```

## Deploy to staging

```
#ask for pem  HoneySecond.pem and do  right permissions for file
sudo chmod -R 0400 ./HoneySecond.pem
#connect remotely to ec2 
ssh -i "./HoneySecond.pem" ubuntu@ec2-35-180-58-244.eu-west-3.compute.amazonaws.com
#screen init
screen -x
#stop current process
CTRL + C 
#update from git
git fetch --all
git reset --hard origin/develop
yarn prebuild
yarn backend:build
yarn --cwd frontend start
# Detach screen, open new tab
CTRL + A + N
#stop current process
CTRL + C 
#use right env config
cp .env.staging .env
yarn --cwd backend start
#hult  server
CTRL + A and then D

#if backend is not compiled do:
#yarn --cwd frontend build:backend
#npx babel src --out-dir ../backend/lib --presets=next/babel 
yarn backend:build

```

## Deploy to EC2 with Docker
```
ssh -i "~/Desktop/Honey/key/HoneySecond.pem" ubuntu@ec2-35-180-58-244.eu-west-3.compute.amazonaws.com
cd ~/dev/adm-client/
git fetch --all
git reset --hard origin/develop
mv .env.staging .env
sudo docker-compose build
sudo docker-compose up -d
```

## SSL Integration for dev.lateilerdumiel.com

```
#look to REQUEST-149 
brew install certbot
#no need , sudo certbot --manual --preferred-challenges dns certonly -d dev.latelierdumiel.com
cd /usr/local/nginx
sudo mkdir /etc/nginx/sites-enabled
sudo mkdir /etc/nginx/sites-available
```

```
#sudo vim /usr/local/etc/nginx/nginx.conf

worker_processes  1;

events {
  worker_connections  1024;
}

http {
  include mime.types;
  default_type application/octet-stream;

  sendfile on;
  tcp_nopush on;
  tcp_nodelay on;
  keepalive_timeout 65;
  types_hash_max_size 2048;

  ssl_protocols TLSv1 TLSv1.1 TLSv1.2; # Dropping SSLv3, ref: POODLE
  ssl_prefer_server_ciphers on;

  gzip on;

  include /usr/local/etc/nginx/sites-enabled/*;
}
```

```
#sudo vim /usr/local/etc/nginx/sites-available/default

server {
  listen 80;
  listen [::]:80;
  server_name dev.latelierdumiel.com;
  return 301 https://$server_name$request_uri;
}

server {
  listen 443 ssl;
  listen [::]:443 ssl;
  server_name dev.latelierdumiel.com;


  listen 443 ssl;
  ssl_certificate /etc/letsencrypt/live/dev.latelierdumiel.com/fullchain.pem;
  ssl_certificate_key /etc/letsencrypt/live/dev.latelierdumiel.com/privkey.pem;

  access_log /var/log/nginx/access.log;
  error_log /var/log/nginx/error.log;

  location / {
    proxy_pass http://127.0.0.1:3000/;
    proxy_set_header Host $host;
    proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header X-Forwarded-Proto $scheme;
    proxy_http_version 1.1;
    proxy_set_header Upgrade $http_upgrade;
    proxy_set_header Connection "upgrade";
    proxy_read_timeout 900s;
  }
}

#etc/hosts
#127.0.0.1 dev.latelierdumiel.com

#next
ln -s /usr/local/etc/nginx/sites-available/default /usr/local/etc/nginx/sites-enabled/
mkdir /var/log/nginx
sudo nginx

```

## Access GraphQL

```
https://dev.latelierdumiel.com/api/graphql
```



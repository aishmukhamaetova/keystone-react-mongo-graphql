FROM node:10
WORKDIR /usr/src/app
COPY . .
RUN yarn add pm2 -g
RUN yarn
RUN yarn prebuild
EXPOSE 3000
EXPOSE 3010
CMD [ "yarn", "docker" ]
